import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class TqDqtvService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }
   // DM Chức danh
   getLoaiDn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaidn/get', data, permission);
  }
  insertLoaiDn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaidn/insert', data, permission);
  }
  editLoaiDn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaidn/update', data, permission);
  }
  deleteLoaiDn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaidn/delete', data, permission);
  }
   // DM Chức danh
   getPhanCap(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phancap/get', data, permission);
  }
  insertPhanCap(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phancap/insert', data, permission);
  }
  editPhanCap(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phancap/update', data, permission);
  }
  deletePhanCap(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phancap/delete', data, permission);
  }
   // DM Chức danh
   getPhanLoaiDn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaidn/get', data, permission);
  }
  insertPhanLoaiDn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaidn/insert', data, permission);
  }
  editPhanLoaiDn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaidn/update', data, permission);
  }
  deletePhanLoaiDn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaidn/delete', data, permission);
  }
   // DM Chức danh
   getTrucThuoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tructhuoc/get', data, permission);
  }
  insertTrucThuoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tructhuoc/insert', data, permission);
  }
  editTrucThuoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tructhuoc/update', data, permission);
  }
  deleteTrucThuoc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tructhuoc/delete', data, permission);
  }
  // DM Quân hàm
  getQuanHam(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quanham/get', data, permission);
  }
  insertQuanHam(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quanham/insert', data, permission);
  }
  editQuanHam(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quanham/update', data, permission);
  }
  deleteQuanHam(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quanham/delete', data, permission);
  }
  // DMCapHanhChinh
  getCapHanhChinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-caphanhchinh/get', data, permission);
  }
  insertCapHanhChinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-caphanhchinh/insert', data, permission);
  }
  editCapHanhChinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-caphanhchinh/update', data, permission);
  }
  deleteCapHanhChinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-caphanhchinh/delete', data, permission);
  }
  // DMLoaiXa
  getLoaiXa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaixa/get', data, permission);
  }
  insertLoaiXa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaixa/insert', data, permission);
  }
  editLoaiXa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaixa/update', data, permission);
  }
  deleteLoaiXa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-loaixa/delete', data, permission);
  }
  // DMLucLuong
  getLucLuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-lucluong/get', data, permission);
  }
  insertLucLuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-lucluong/insert', data, permission);
  }
  editLucLuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-lucluong/update', data, permission);
  }
  deleteLucLuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-lucluong/delete', data, permission);
  }
  // DMPhanLoaiDiaLy
  getPhanLoaiDiaLy(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaidialy/get', data, permission);
  }
  insertPhanLoaiDiaLy(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaidialy/insert', data, permission);
  }
  editPhanLoaiDiaLy(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaidialy/update', data, permission);
  }
  deletePhanLoaiDiaLy(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaidialy/delete', data, permission);
  }
  // DMPhanLoaiLucLuong
  getPhanLoaiLucLuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloailucluong/get', data, permission);
  }
  insertPhanLoaiLucLuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloailucluong/insert', data, permission);
  }
  editPhanLoaiLucLuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloailucluong/update', data, permission);
  }
  deletePhanLoaiLucLuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloailucluong/delete', data, permission);
  }
  // DMPhanLoaiTrongDiem
  getPhanLoaiTrongDiem(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaitrongdiem/get', data, permission);
  }
  insertPhanLoaiTrongDiem(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaitrongdiem/insert', data, permission);
  }
  editPhanLoaiTrongDiem(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaitrongdiem/update', data, permission);
  }
  deletePhanLoaiTrongDiem(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-phanloaitrongdiem/delete', data, permission);
  }
  // DMquyMoToChuc
  getQuyMoToChuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quymotochuc/get', data, permission);
  }
  insertQuyMoToChuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quymotochuc/insert', data, permission);
  }
  editQuyMoToChuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quymotochuc/update', data, permission);
  }
  deleteQuyMoToChuc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quymotochuc/delete', data, permission);
  }

  // DM cấp bậc
  getCapBac(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-capbac/get', data, permission);
  }
  insertCapBac(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-capbac/insert', data, permission);
  }
  editCapBac(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-capbac/update', data, permission);
  }
  deleteCapBac(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-capbac/delete', data, permission);
  }
  // DM đơn vị
  getDonViDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvidbdv/get', data, permission);
  }
  insertDonViDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvidbdv/insert', data, permission);
  }
  editDonViDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvidbdv/update', data, permission);
  }
  deleteDonViDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvidbdv/delete', data, permission);
  }
   // DM đơn vị
   getDonViTv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvitv/get', data, permission);
  }
  insertDonViTv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvitv/insert', data, permission);
  }
  editDonViTv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvitv/update', data, permission);
  }
  deleteDonViTv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-donvitv/delete', data, permission);
  }
  // DM Chức danh
  getChucDanh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-chucdanh/get', data, permission);
  }
  insertChucDanh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-chucdanh/insert', data, permission);
  }
  editChucDanh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-chucdanh/update', data, permission);
  }
  deleteChucDanh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-chucdanh/delete', data, permission);
  }
  // DM Đối tượng
  getDoiTuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-doituong/get', data, permission);
  }
  insertDoiTuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-doituong/insert', data, permission);
  }
  editDoiTuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-doituong/update', data, permission);
  }
  deleteDoiTuong(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-doituong/delete', data, permission);
  }
  // Nang khieu
  getNangKhieu(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nangkhieu/get', data, permission);
  }
  insertNangKhieu(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nangkhieu/insert', data, permission);
  }
  editNangKhieu(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nangkhieu/update', data, permission);
  }
  deleteNangKhieu(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nangkhieu/delete', data, permission);
  }
  // Nghề Nghiệp
  getNgheNghiep(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nghenghiep/get', data, permission);
  }
  insertNgheNghiep(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nghenghiep/insert', data, permission);
  }
  editNgheNghiep(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nghenghiep/update', data, permission);
  }
  deleteNgheNghiep(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nghenghiep/delete', data, permission);
  }
  // Sức Khỏe
  getSucKhoe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-suckhoe/get', data, permission);
  }
  insertSucKhoe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-suckhoe/insert', data, permission);
  }
  editSucKhoe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-suckhoe/update', data, permission);
  }
  deleteSucKhoe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-suckhoe/delete', data, permission);
  }
  // tổ chức xã hội
  getToChucXaHoi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tochucxahoi/get', data, permission);
  }
  insertToChucXaHoi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tochucxahoi/insert', data, permission);
  }
  editToChucXaHoi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tochucxahoi/update', data, permission);
  }
  deleteToChucXaHoi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tochucxahoi/delete', data, permission);
  }
  // trình độ học vấn
  getTrinhDoHocVan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-trinhdohocvan/get', data, permission);
  }
  insertTrinhDoHocVan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-trinhdohocvan/insert', data, permission);
  }
  editTrinhDoHocVan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-trinhdohocvan/update', data, permission);
  }
  deleteTrinhDoHocVan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-trinhdohocvan/delete', data, permission);
  }
  // tạm hoãn
  getTamHoan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tamhoan/get', data, permission);
  }
  insertTamHoan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tamhoan/insert', data, permission);
  }
  editTamHoan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tamhoan/update', data, permission);
  }
  deleteTamHoan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-tamhoan/delete', data, permission);
  }
  // trình độ văn hóa
  getTrinhDoVanHoa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-trinhdovanhoa/get', data, permission);
  }
  insertTrinhDoVanHoa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-trinhdovanhoa/insert', data, permission);
  }
  editTrinhDoVanHoa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-trinhdovanhoa/update', data, permission);
  }
  deleteTrinhDoVanHoa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-trinhdovanhoa/delete', data, permission);
  }
  // học vị
  getHocVi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-hocvi/get', data, permission);
  }
  insertHocVi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-hocvi/insert', data, permission);
  }
  editHocVi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-hocvi/update', data, permission);
  }
  deleteHocVi(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-hocvi/delete', data, permission);
  }
  // quan hệ
  getQuanHe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quanhe/get', data, permission);
  }
  insertQuanHe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quanhe/insert', data, permission);
  }
  editQuanHe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quanhe/update', data, permission);
  }
  deleteQuanHe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-quanhe/delete', data, permission);
  }
  // ngành nghề cmkt
  getNganhNgheCmkt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nganhnghecmkt/get', data, permission);
  }
  insertNganhNgheCmkt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nganhnghecmkt/insert', data, permission);
  }
  editNganhNgheCmkt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nganhnghecmkt/update', data, permission);
  }
  deleteNganhNgheCmkt(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-nganhnghecmkt/delete', data, permission);
  }
  // miễn
  getMien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-mien/get', data, permission);
  }
  insertMien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-mien/insert', data, permission);
  }
  editMien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-mien/update', data, permission);
  }
  deleteMien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq-dqtv/dm-mien/delete', data, permission);
  }
  // dân tộc
  getDanToc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-dantoc/get', data, permission);
  }
  insertDanToc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-dantoc/insert', data, permission);
  }
  editDanToc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-dantoc/update', data, permission);
  }
  deleteDanToc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-dantoc/delete', data, permission);
  }
  // tôn giáo
  getTonGiao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-tongiao/get', data, permission);
  }
  insertTonGiao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-tongiao/insert', data, permission);
  }
  editTonGiao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-tongiao/update', data, permission);
  }
  deleteTonGiao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-tongiao/delete', data, permission);
  }
}
