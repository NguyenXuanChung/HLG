import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class DqtvService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  getDqtv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/get', data, permission);
  }
  getListDqtv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/getlist', data, permission);
  }
  getBienChe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/getbc', data, permission);
  }
  insertDqtv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/insert', data, permission);
  }
  editDqtv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/update', data, permission);
  }
  deleteDqtv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dqtv/delete', data, permission);
  }

}
