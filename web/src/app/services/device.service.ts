import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  //#region Device
  // Retrieve data with condition in body
  getDevice(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/devices/get', data, permission);
  }
  // Create a new item
  insertDevice(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/devices/insert', data, permission);
  }
  // Update a list item
  updateListDevice(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/devices/update-groupDiaBanConfigration', data, permission);
  }
  // Update a item with condition {id: id}
  editDevice(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/devices/update', data, permission);
  }
  // Delete a item with id
  deleteDevice(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/devices/delete', data, permission);
  }
  // Update a project item with condition {id: id}
  updateDiaBanDevice(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/devices/update-diaban', data, permission);
  }
  // Update a config item with condition {id: id}
  updateConfigDevice(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/devices/update-configuration', data, permission);
  }
  // Update a group item with condition {id: id}
  updateGroupDevice(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/devices/update-group', data, permission);
  }
  //#endregion

}
