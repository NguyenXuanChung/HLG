import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  //#region Group
  // Retrieve data with condition in body
  getGroup(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/groups/get', data, permission);
  }
  // Create a new item
  insertGroup(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/groups/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editGroup(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/groups/update', data, permission);
  }
  // Delete a item with id
  deleteGroup(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/groups/delete', data, permission);
  }
  //#endregion

}
