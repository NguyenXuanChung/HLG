import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  //#region Config
  // Retrieve data with condition in body
  getConfig(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/configurations/get', data, permission);
  }
  // Create a new item
  insertConfig(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/configurations/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editConfig(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/configurations/update', data, permission);
  }
  // Delete a item with id
  deleteConfig(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/configurations/delete', data, permission);
  }
  //#endregion

}
