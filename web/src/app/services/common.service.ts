import { Injectable } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { ReplaySubject } from 'rxjs';
import { take } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
@Injectable({ providedIn: 'root' })
export class CommonService {
  language$ = new ReplaySubject<LangChangeEvent>(1);
  translate = this.translateService;
  public Login: any = {
    View: { 'code': 'Login', 'action': '_view' },
  };
  public ALL: any = {
    View: { 'code': 'ALL', 'action': '_view' },
    Export: { 'code': 'ALL', 'action': '_export' },
    Insert: { 'code': 'ALL', 'action': '_insert' },
    Update: { 'code': 'ALL', 'action': '_update' },
    Delete: { 'code': 'ALL', 'action': '_delete' },
  };
  public QTHT: any = {
    View: { 'code': 'QTHT', 'action': '_view' },
    Export: { 'code': 'QTHT', 'action': '_export' },
    Insert: { 'code': 'QTHT', 'action': '_insert' },
    Update: { 'code': 'QTHT', 'action': '_update' },
    Delete: { 'code': 'QTHT', 'action': '_delete' },
  };
  public HT_QuanLyNguoiDung: any = {
    View: { 'code': 'HT_QuanLyNguoiDung', 'action': '_view' },
    Export: { 'code': 'HT_QuanLyNguoiDung', 'action': '_export' },
    Insert: { 'code': 'HT_QuanLyNguoiDung', 'action': '_insert' },
    Update: { 'code': 'HT_QuanLyNguoiDung', 'action': '_update' },
    Delete: { 'code': 'HT_QuanLyNguoiDung', 'action': '_delete' },
  };
  public HT_QuanLyQuyen: any = {
    View: { 'code': 'HT_QuanLyQuyen', 'action': '_view' },
    Export: { 'code': 'HT_QuanLyQuyen', 'action': '_export' },
    Insert: { 'code': 'HT_QuanLyQuyen', 'action': '_insert' },
    Update: { 'code': 'HT_QuanLyQuyen', 'action': '_update' },
    Delete: { 'code': 'HT_QuanLyQuyen', 'action': '_delete' },
  };
  public HT_QuanLyNhomQuyen: any = {
    View: { 'code': 'HT_QuanLyNhomQuyen', 'action': '_view' },
    Export: { 'code': 'HT_QuanLyNhomQuyen', 'action': '_export' },
    Insert: { 'code': 'HT_QuanLyNhomQuyen', 'action': '_insert' },
    Update: { 'code': 'HT_QuanLyNhomQuyen', 'action': '_update' },
    Delete: { 'code': 'HT_QuanLyNhomQuyen', 'action': '_delete' },
  };
  public HT_ThamSoHeThong: any = {
    View: { 'code': 'HT_ThamSoHeThong', 'action': '_view' },
    Export: { 'code': 'HT_ThamSoHeThong', 'action': '_export' },
    Insert: { 'code': 'HT_ThamSoHeThong', 'action': '_insert' },
    Update: { 'code': 'HT_ThamSoHeThong', 'action': '_update' },
    Delete: { 'code': 'HT_ThamSoHeThong', 'action': '_delete' },
  };
  public HT_NhatKyHeThong: any = {
    View: { 'code': 'HT_NhatKyHeThong', 'action': '_view' },
    Export: { 'code': 'HT_NhatKyHeThong', 'action': '_export' },
    Insert: { 'code': 'HT_NhatKyHeThong', 'action': '_insert' },
    Update: { 'code': 'HT_NhatKyHeThong', 'action': '_update' },
    Delete: { 'code': 'HT_NhatKyHeThong', 'action': '_delete' },
  };
  public TQ_CongDan: any = {
    View: { 'code': 'TQ_CongDan', 'action': '_view' },
    Export: { 'code': 'TQ_CongDan', 'action': '_export' },
    Insert: { 'code': 'TQ_CongDan', 'action': '_insert' },
    Update: { 'code': 'TQ_CongDan', 'action': '_update' },
    Delete: { 'code': 'TQ_CongDan', 'action': '_delete' },
  };
  public TQ_SSNN: any = {
    View: { 'code': 'TQ_SSNN', 'action': '_view' },
    Export: { 'code': 'TQ_SSNN', 'action': '_export' },
    Insert: { 'code': 'TQ_SSNN', 'action': '_insert' },
    Update: { 'code': 'TQ_SSNN', 'action': '_update' },
    Delete: { 'code': 'TQ_SSNN', 'action': '_delete' },
  };
  //đợt tuyển quân
  public TQ_DotTuyenQuan: any = {
    View: { 'code': 'TQ_DotTuyenQuan', 'action': '_view' },
    Export: { 'code': 'TQ_DotTuyenQuan', 'action': '_export' },
    Insert: { 'code': 'TQ_DotTuyenQuan', 'action': '_insert' },
    Update: { 'code': 'TQ_DotTuyenQuan', 'action': '_update' },
    Delete: { 'code': 'TQ_DotTuyenQuan', 'action': '_delete' },
  };
  public TQ_DonVidbdv: any = {
    View: { 'code': 'TQ_DonVidbdv', 'action': '_view' },
    Export: { 'code': 'TQ_DonVidbdv', 'action': '_export' },
    Insert: { 'code': 'TQ_DonVidbdv', 'action': '_insert' },
    Update: { 'code': 'TQ_DonVidbdv', 'action': '_update' },
    Delete: { 'code': 'TQ_DonVidbdv', 'action': '_delete' },
  };
  public TQ_XetDuyetXa: any = {
    View: { 'code': 'TQ_XetDuyetXa', 'action': '_view' },
    Export: { 'code': 'TQ_XetDuyetXa', 'action': '_export' },
    Insert: { 'code': 'TQ_XetDuyetXa', 'action': '_insert' },
    Update: { 'code': 'TQ_XetDuyetXa', 'action': '_update' },
    Delete: { 'code': 'TQ_XetDuyetXa', 'action': '_delete' },
  };
  public TQ_XetDuyetHuyen: any = {
    View: { 'code': 'TQ_XetDuyetHuyen', 'action': '_view' },
    Export: { 'code': 'TQ_XetDuyetHuyen', 'action': '_export' },
    Insert: { 'code': 'TQ_XetDuyetHuyen', 'action': '_insert' },
    Update: { 'code': 'TQ_XetDuyetHuyen', 'action': '_update' },
    Delete: { 'code': 'TQ_XetDuyetHuyen', 'action': '_delete' },
  };
  public TQ_NhapNgu: any = {
    View: { 'code': 'TQ_NhapNgu', 'action': '_view' },
    Export: { 'code': 'TQ_NhapNgu', 'action': '_export' },
    Insert: { 'code': 'TQ_NhapNgu', 'action': '_insert' },
    Update: { 'code': 'TQ_NhapNgu', 'action': '_update' },
    Delete: { 'code': 'TQ_NhapNgu', 'action': '_delete' },
  };
  public TQ_DQTV_ChucDanh: any = {
    View: { 'code': 'TQ_DQTV_ChucDanh', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_ChucDanh', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_ChucDanh', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_ChucDanh', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_ChucDanh', 'action': '_delete' },
  };
  public DiaBan: any = {
    View: { 'code': 'DiaBan', 'action': '_view' },
    Export: { 'code': 'DiaBan', 'action': '_export' },
    Insert: { 'code': 'DiaBan', 'action': '_insert' },
    Update: { 'code': 'DiaBan', 'action': '_update' },
    Delete: { 'code': 'DiaBan', 'action': '_delete' },
  };
  public Configuration: any = {
    View: { 'code': 'Configuration', 'action': '_view' },
    Export: { 'code': 'Configuration', 'action': '_export' },
    Insert: { 'code': 'Configuration', 'action': '_insert' },
    Update: { 'code': 'Configuration', 'action': '_update' },
    Delete: { 'code': 'Configuration', 'action': '_delete' },
  };
  public Group: any = {
    View: { 'code': 'Group', 'action': '_view' },
    Export: { 'code': 'Group', 'action': '_export' },
    Insert: { 'code': 'Group', 'action': '_insert' },
    Update: { 'code': 'Group', 'action': '_update' },
    Delete: { 'code': 'Group', 'action': '_delete' },
  };
  public Application: any = {
    View: { 'code': 'Application', 'action': '_view' },
    Export: { 'code': 'Application', 'action': '_export' },
    Insert: { 'code': 'Application', 'action': '_insert' },
    Update: { 'code': 'Application', 'action': '_update' },
    Delete: { 'code': 'Application', 'action': '_delete' },
  };
  public Location: any = {
    View: { 'code': 'Location', 'action': '_view' },
    Export: { 'code': 'Location', 'action': '_export' },
    Insert: { 'code': 'Location', 'action': '_insert' },
    Update: { 'code': 'Location', 'action': '_update' },
    Delete: { 'code': 'Location', 'action': '_delete' },
  };
  public DM_Tinh_Huyen_Xa: any = {
    View: { 'code': 'DM_Tinh_Huyen_Xa', 'action': '_view' },
    Export: { 'code': 'DM_Tinh_Huyen_Xa', 'action': '_export' },
    Insert: { 'code': 'DM_Tinh_Huyen_Xa', 'action': '_insert' },
    Update: { 'code': 'DM_Tinh_Huyen_Xa', 'action': '_update' },
    Delete: { 'code': 'DM_Tinh_Huyen_Xa', 'action': '_delete' },
  };
  public QLTB_ThongBao: any = {
    View: { 'code': 'QLTB_ThongBao', 'action': '_view' },
    Export: { 'code': 'QLTB_ThongBao', 'action': '_export' },
    Insert: { 'code': 'QLTB_ThongBao', 'action': '_insert' },
    Update: { 'code': 'QLTB_ThongBao', 'action': '_update' },
    Delete: { 'code': 'QLTB_ThongBao', 'action': '_delete' },
  };
  public DM_TrangThaiPhat: any = {
    View: { 'code': 'DM_TrangThaiPhat', 'action': '_view' },
    Export: { 'code': 'DM_TrangThaiPhat', 'action': '_export' },
    Insert: { 'code': 'DM_TrangThaiPhat', 'action': '_insert' },
    Update: { 'code': 'DM_TrangThaiPhat', 'action': '_update' },
    Delete: { 'code': 'DM_TrangThaiPhat', 'action': '_delete' },
  };
  public DM_CheDoPhat: any = {
    View: { 'code': 'DM_CheDoPhat', 'action': '_view' },
    Export: { 'code': 'DM_CheDoPhat', 'action': '_export' },
    Insert: { 'code': 'DM_CheDoPhat', 'action': '_insert' },
    Update: { 'code': 'DM_CheDoPhat', 'action': '_update' },
    Delete: { 'code': 'DM_CheDoPhat', 'action': '_delete' },
  };
  public DM_Kenh: any = {
    View: { 'code': 'DM_Kenh', 'action': '_view' },
    Export: { 'code': 'DM_Kenh', 'action': '_export' },
    Insert: { 'code': 'DM_Kenh', 'action': '_insert' },
    Update: { 'code': 'DM_Kenh', 'action': '_update' },
    Delete: { 'code': 'DM_Kenh', 'action': '_delete' },
  };
  public DM_KieuPhat: any = {
    View: { 'code': 'DM_KieuPhat', 'action': '_view' },
    Export: { 'code': 'DM_KieuPhat', 'action': '_export' },
    Insert: { 'code': 'DM_KieuPhat', 'action': '_insert' },
    Update: { 'code': 'DM_KieuPhat', 'action': '_update' },
    Delete: { 'code': 'DM_KieuPhat', 'action': '_delete' },
  };
  public DM_KieuLap: any = {
    View: { 'code': 'DM_KieuLap', 'action': '_view' },
    Export: { 'code': 'DM_KieuLap', 'action': '_export' },
    Insert: { 'code': 'DM_KieuLap', 'action': '_insert' },
    Update: { 'code': 'DM_KieuLap', 'action': '_update' },
    Delete: { 'code': 'DM_KieuLap', 'action': '_delete' },
  };
  public DM_LoaiThongBao: any = {
    View: { 'code': 'DM_LoaiThongBao', 'action': '_view' },
    Export: { 'code': 'DM_LoaiThongBao', 'action': '_export' },
    Insert: { 'code': 'DM_LoaiThongBao', 'action': '_insert' },
    Update: { 'code': 'DM_LoaiThongBao', 'action': '_update' },
    Delete: { 'code': 'DM_LoaiThongBao', 'action': '_delete' },
  };
  public DM_LoaiTin: any = {
    View: { 'code': 'DM_LoaiTin', 'action': '_view' },
    Export: { 'code': 'DM_LoaiTin', 'action': '_export' },
    Insert: { 'code': 'DM_LoaiTin', 'action': '_insert' },
    Update: { 'code': 'DM_LoaiTin', 'action': '_update' },
    Delete: { 'code': 'DM_LoaiTin', 'action': '_delete' },
  };
  public DM_TrangThaiDuyet: any = {
    View: { 'code': 'DM_TrangThaiDuyet', 'action': '_view' },
    Export: { 'code': 'DM_TrangThaiDuyet', 'action': '_export' },
    Insert: { 'code': 'DM_TrangThaiDuyet', 'action': '_insert' },
    Update: { 'code': 'DM_TrangThaiDuyet', 'action': '_update' },
    Delete: { 'code': 'DM_TrangThaiDuyet', 'action': '_delete' },
  };
  // đối tượng
  public TQ_DQTV_DoiTuong: any = {
    View: { 'code': 'TQ_DQTV_DoiTuong', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_DoiTuong', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_DoiTuong', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_DoiTuong', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_DoiTuong', 'action': '_delete' },
  };
  // nang khieu
  public TQ_DQTV_NangKhieu: any = {
    View: { 'code': 'TQ_DQTV_NangKhieu', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_NangKhieu', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_NangKhieu', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_NangKhieu', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_NangKhieu', 'action': '_delete' },
  };
  // nghề nghiệp
  public TQ_DQTV_NgheNghiep: any = {
    View: { 'code': 'TQ_DQTV_NgheNghiep', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_NgheNghiep', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_NgheNghiep', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_NgheNghiep', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_NgheNghiep', 'action': '_delete' },
  };
  // sức khỏe
  public TQ_DQTV_SucKhoe: any = {
    View: { 'code': 'TQ_DQTV_SucKhoe', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_SucKhoe', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_SucKhoe', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_SucKhoe', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_SucKhoe', 'action': '_delete' },
  };
  // tổ chức xã hội
  public TQ_DQTV_ToChucXaHoi: any = {
    View: { 'code': 'TQ_DQTV_ToChucXaHoi', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_ToChucXaHoi', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_ToChucXaHoi', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_ToChucXaHoi', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_ToChucXaHoi', 'action': '_delete' },
  };
  // trình độ học vấn
  public TQ_DQTV_TrinhDoHocVan: any = {
    View: { 'code': 'TQ_DQTV_TrinhDoHocVan', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_TrinhDoHocVan', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_TrinhDoHocVan', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_TrinhDoHocVan', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_TrinhDoHocVan', 'action': '_delete' },
  };
  // trình độ văn hóa
  public TQ_DQTV_TrinhDoVanHoa: any = {
    View: { 'code': 'TQ_DQTV_TrinhDoVanHoa', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_TrinhDoVanHoa', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_TrinhDoVanHoa', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_TrinhDoVanHoa', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_TrinhDoVanHoa', 'action': '_delete' },
  };
  // tạm hoãn
  public TQ_DQTV_TamHoan: any = {
    View: { 'code': 'TQ_DQTV_TamHoan', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_TamHoan', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_TamHoan', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_TamHoan', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_TamHoan', 'action': '_delete' },
  };
  // học vị
  public TQ_DQTV_HocVi: any = {
    View: { 'code': 'TQ_DQTV_HocVi', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_HocVi', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_HocVi', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_HocVi', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_HocVi', 'action': '_delete' },
  };
  // quan hệ
  public TQ_DQTV_QuanHe: any = {
    View: { 'code': 'TQ_DQTV_QuanHe', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_QuanHe', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_QuanHe', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_QuanHe', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_QuanHe', 'action': '_delete' },
  };
  // nganhnghecmkt
  public TQ_DQTV_NganhNgheCmkt: any = {
    View: { 'code': 'TQ_DQTV_NganhNgheCmkt', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_NganhNgheCmkt', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_NganhNgheCmkt', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_NganhNgheCmkt', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_NganhNgheCmkt', 'action': '_delete' },
  };
  // miễn
  public TQ_DQTV_Mien: any = {
    View: { 'code': 'TQ_DQTV_Mien', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_Mien', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_Mien', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_Mien', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_Mien', 'action': '_delete' },
  };
  // dân tộc
  public TQ_DQTV_DanToc: any = {
    View: { 'code': 'TQ_DQTV_DanToc', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_DanToc', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_DanToc', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_DanToc', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_DanToc', 'action': '_delete' },
  };
  // tôn giáo
  public TQ_DQTV_TonGiao: any = {
    View: { 'code': 'TQ_DQTV_TonGiao', 'action': '_view' },
    Export: { 'code': 'TQ_DQTV_TonGiao', 'action': '_export' },
    Insert: { 'code': 'TQ_DQTV_TonGiao', 'action': '_insert' },
    Update: { 'code': 'TQ_DQTV_TonGiao', 'action': '_update' },
    Delete: { 'code': 'TQ_DQTV_TonGiao', 'action': '_delete' },
  };
  //GD_QPAN
  // trường học
  public GD_QPAN_TruongHoc: any = {
    View: { 'code': 'GD_QPAN_TruongHoc', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_TruongHoc', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_TruongHoc', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_TruongHoc', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_TruongHoc', 'action': '_delete' },
  };
  // phân loại đối tượng
  public GD_QPAN_PhanLoaiDoiTuong: any = {
    View: { 'code': 'GD_QPAN_PhanLoaiDoiTuong', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_PhanLoaiDoiTuong', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_PhanLoaiDoiTuong', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_PhanLoaiDoiTuong', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_PhanLoaiDoiTuong', 'action': '_delete' },
  };
  // đối tượng
  public GD_QPAN_DoiTuong: any = {
    View: { 'code': 'GD_QPAN_DoiTuong', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_DoiTuong', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_DoiTuong', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_DoiTuong', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_DoiTuong', 'action': '_delete' },
  };
  // phân loại lóp học
  public GD_QPAN_PhanLoaiLopHoc: any = {
    View: { 'code': 'GD_QPAN_PhanLoaiLopHoc', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_PhanLoaiLopHoc', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_PhanLoaiLopHoc', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_PhanLoaiLopHoc', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_PhanLoaiLopHoc', 'action': '_delete' },
  };
  // kết quả
  public GD_QPAN_KetQua: any = {
    View: { 'code': 'GD_QPAN_KetQua', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_KetQua', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_KetQua', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_KetQua', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_KetQua', 'action': '_delete' },
  };
  // hình thức dào tạo
  public GD_QPAN_HinhThucDaoTao: any = {
    View: { 'code': 'GD_QPAN_HinhThucDaoTao', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_HinhThucDaoTao', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_HinhThucDaoTao', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_HinhThucDaoTao', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_HinhThucDaoTao', 'action': '_delete' },
  };
  // lý luận chính trị
  public GD_QPAN_LyLuanChinhTri: any = {
    View: { 'code': 'GD_QPAN_LyLuanChinhTri', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_LyLuanChinhTri', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_LyLuanChinhTri', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_LyLuanChinhTri', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_LyLuanChinhTri', 'action': '_delete' },
  };
  // xếp loại lớp học
  public GD_QPAN_XepLoaiLopHoc: any = {
    View: { 'code': 'GD_QPAN_XepLoaiLopHoc', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_XepLoaiLopHoc', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_XepLoaiLopHoc', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_XepLoaiLopHoc', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_XepLoaiLopHoc', 'action': '_delete' },
  };
  // quản lý giáo viên
  public GD_QPAN_QuanLyGiaoVien: any = {
    View: { 'code': 'GD_QPAN_QuanLyGiaoVien', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_QuanLyGiaoVien', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_QuanLyGiaoVien', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_QuanLyGiaoVien', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_QuanLyGiaoVien', 'action': '_delete' },
  };
  // đào tạo theo lớp học
  public GD_QPAN_DaoTaoTheoLopHoc: any = {
    View: { 'code': 'GD_QPAN_DaoTaoTheoLopHoc', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_DaoTaoTheoLopHoc', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_DaoTaoTheoLopHoc', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_DaoTaoTheoLopHoc', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_DaoTaoTheoLopHoc', 'action': '_delete' },
  };
  //kết quả đào tạo theo lớp học
  public GD_QPAN_QuanLyKQDTTheoLop: any = {
    View: { 'code': 'GD_QPAN_QuanLyKQDTTheoLop', 'action': '_view' },
    Export: { 'code': 'GD_QPAN_QuanLyKQDTTheoLop', 'action': '_export' },
    Insert: { 'code': 'GD_QPAN_QuanLyKQDTTheoLop', 'action': '_insert' },
    Update: { 'code': 'GD_QPAN_QuanLyKQDTTheoLop', 'action': '_update' },
    Delete: { 'code': 'GD_QPAN_QuanLyKQDTTheoLop', 'action': '_delete' },
  };
  public navData: any;
  constructor(
    private translateService: TranslateService,
    private toastrService: ToastrService,
  ) { }

  setInitState() {
    this.translateService.addLangs(['en', 'vi']);
    const browserLang = (this.translate.getBrowserLang().includes('vi')) ? 'en' : 'vi';
    this.setLang(browserLang);
  }
  trans(data: string) {
    return this.translate.instant(data);
  }
  setLang(lang: string) {
    this.translateService.onLangChange.pipe(take(1)).subscribe(result => {
      this.language$.next(result);
    });
    this.translateService.use(lang);
  }
  bitToValue = (value) => {
    if (value == null) {
      return 0;
    } else if (value.data) {
      return value.data[0];
    } else {
      return value ? 1 : 0;
    }
  }
  messageRes(message: any) {
    this.toastrService.success(this.translate.instant(message));
  }
  messageErr(res: any) {
    if (res.error && res.error.message) {
      this.toastrService.error(this.translate.instant(res.error.message));
    } else if (res.error) {
      this.toastrService.error(this.translate.instant(res.error));
    }
  }
}

