import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class DiaBanService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  // Retrieve data with condition in body
  getDiaBan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/diaban/get', data, permission);
  }
  getDiaBanByAccount(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/diaban/get-by-account', data, permission);
  }
  getListDiaBan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/diaban/get-list', data, permission);
  }
  // Create a new item
  insertDiaBan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/diaban/insert', data, permission);
  }
  // Update a item with condition {id: id}
  editDiaBan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/diaban/update', data, permission);
  }
  // Delete a item with id
  deleteDiaBan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('mdm/diaban/delete', data, permission);
  }
  // DM Tỉnh
  getDMTinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-tinh/get', data, permission);
  }
  deleteTinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-tinh/delete', data, permission);
  }
  insertTinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-tinh/insert', data, permission);
  }
  editTinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-tinh/update', data, permission);
  }

  // DM Huyện
  getDMHuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-huyen/get', data, permission);
  }
  deleteHuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-huyen/delete', data, permission);
  }
  insertHuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-huyen/insert', data, permission);
  }
  editHuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-huyen/update', data, permission);
  }

  // DM Xã
  getDMXa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-xa/get', data, permission);
  }
  deleteXa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-xa/delete', data, permission);
  }
  insertXa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-xa/insert', data, permission);
  }
  editXa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-xa/update', data, permission);
  }
}
