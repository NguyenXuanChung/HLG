import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class TuyenQuanService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }
  // Công dân

  // updateCongDanSSDBDV(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('tq/congdan/updatessdbdv', data, permission);
  // }
  updateQuaTrinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/updatequatrinh', data, permission);
  }
  updateCongDanDetail(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/updatedetail', data, permission);
  }
  updateBienChe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/updatebienche', data, permission);
  }
  updateDongVien(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/updatedongvien', data, permission);
  }
  updateGiaiNgach(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/updategiaingach', data, permission);
  }
  // updateCongDanTq(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('tq/congdan/updatetq', data, permission);
  // }
  updateCongDanXd(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/updatexd', data, permission);
  }
  // updateCongDanNn(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('tq/congdan/updatenn', data, permission);
  // }
  getCongDan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/get', data, permission);
  }
  getDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/getdbdv', data, permission);
  }
  getQNDB(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/getqndb', data, permission);
  }
  getCongDanByCmnd(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/getbycmnd', data, permission);
  }
  getCongDanNu(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/getlistnu', data, permission);
  }
  getCongDanSsnn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/getssnn', data, permission);
  }
  getCongDanSsDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/getssdbdv', data, permission);
  }
  getCongDanDetail(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/getdetail', data, permission);
  }
  getCongDanXd(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/getxd', data, permission);
  }
  getCongDanNn(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/getnn', data, permission);
  }
  getLoaiCongDan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdantypestateaction/get', data, permission);
  }
  getCongDanTypeState(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdantypestate/get', data, permission);
  }
  deleteCongDan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/delete', data, permission);
  }
  insertCongDan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/insert', data, permission);
  }
  editCongDan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdan/update', data, permission);
  }

  // DM CongDanDetail
  // getCongDanDetail(data: any, permission: any): Observable<any> {
  //   return this.httpService.postPermission('tq/congdandetail/get', data, permission);
  // }
  deleteCongDanDetail(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdandetail/delete', data, permission);
  }
  insertCongDanDetail(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdandetail/insert', data, permission);
  }
  editCongDanDetail(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdandetail/update', data, permission);
  }
  editCongDanDetailLatest(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/congdandetail/updatelatest', data, permission);
  }

  // DM DotTuyenQuan
  getDotTuyenQuan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/dottuyenquan/get', data, permission);
  }
  getDotTuyenQuanByDate(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/dottuyenquan/getbydate', data, permission);
  }
  deleteDotTuyenQuan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/dottuyenquan/delete', data, permission);
  }
  insertDotTuyenQuan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/dottuyenquan/insert', data, permission);
  }
  editDotTuyenQuan(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/dottuyenquan/update', data, permission);
  }
  // QuanHe
  getQuanHe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/quanhe/get', data, permission);
  }
  deleteQuanHe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/quanhe/delete', data, permission);
  }
  insertQuanHe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/quanhe/insert', data, permission);
  }
  editQuanHe(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/quanhe/update', data, permission);
  }
  // KeHoachDbdv
  getKeHoachDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/kehoachdbdv/get', data, permission);
  }
  deleteKeHoachDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/kehoachdbdv/delete', data, permission);
  }
  insertKeHoachDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/kehoachdbdv/insert', data, permission);
  }
  editKeHoachDbdv(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/kehoachdbdv/update', data, permission);
  }
   // TheoDoiHuanLuyen
   getTheoDoiHuanLuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/theodoihuanluyen/get', data, permission);
  }
  deleteTheoDoiHuanLuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/theodoihuanluyen/delete', data, permission);
  }
  insertTheoDoiHuanLuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/theodoihuanluyen/insert', data, permission);
  }
  editTheoDoiHuanLuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('tq/theodoihuanluyen/update', data, permission);
  }
}
