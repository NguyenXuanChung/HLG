import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class DanhMucService {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  getDanToc(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-dantoc/get', data, permission);
  }
  getTonGiao(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-tongiao/get', data, permission);
  }

  // DM Tỉnh
  getTinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-tinh/get', data, permission);
  }
  deleteTinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-tinh/delete', data, permission);
  }
  insertTinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-tinh/insert', data, permission);
  }
  editTinh(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-tinh/update', data, permission);
  }

  // DM Huyện
  getHuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-huyen/get', data, permission);
  }
  deleteHuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-huyen/delete', data, permission);
  }
  insertHuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-huyen/insert', data, permission);
  }
  editHuyen(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-huyen/update', data, permission);
  }

  // DM Xã
  getXa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-xa/get', data, permission);
  }
  deleteXa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-xa/delete', data, permission);
  }
  insertXa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-xa/insert', data, permission);
  }
  editXa(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-xa/update', data, permission);
  }
   // DM Thôn
   getThon(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-thon/get', data, permission);
  }
  deleteThon(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-thon/delete', data, permission);
  }
  insertThon(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-thon/insert', data, permission);
  }
  editThon(data: any, permission: any): Observable<any> {
    return this.httpService.postPermission('dm/dm-thon/update', data, permission);
  }
}
