import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
    providedIn: 'root'
})
export class DashBoardService {

    constructor(
        private httpService: HttpService,
        private router: Router
    ) { }

    getDashBoard(data: any, permission: any): Observable<any> {
        return this.httpService.postPermission('sys/dashboard/get', data, permission);
    }
}
