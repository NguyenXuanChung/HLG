import { SsDbdv2Component } from './dbdv/ssdbdv2.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CongDanComponent } from './congdan/congdan.component';
import { AppSsnnComponent } from './congdan/ssnn.component';
import { DotTqComponent } from './dottq/dottq.component';
import { XetDuyetXaComponent } from './xetduyet/xetDuyetXa.component';
import { XetDuyetHuyenComponent } from './xetduyet/xetDuyetHuyen.component';
import { NhapNguComponent } from './xetduyet/nhapNgu.component';
import { CongDanNuComponent } from './dbdv/congdannu.component';
import { SsDbdv1Component } from './dbdv/ssdbdv1.component';
import { Dbdv1Component } from './dbdv/dbdv1.component';
import { Dbdv2Component } from './dbdv/dbdv2.component';
import { SqdbComponent } from './dbdv/sqdb.component';
import { QndbComponent } from './dbdv/qndb.component';
import { KeHoachDbdvComponent } from './kehoachdbdv/kehoachdbdv.component';
import { TheoDoiHuanLuyenComponent } from './theodoihuanluyen/theodoihuanluyen.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'thongbao'
      },
      {
        path: 'congdan',
        component: CongDanComponent,
      },
      {
        path: 'ssnn',
        component: AppSsnnComponent,
      },
      {
        path: 'dottuyenquan',
        component: DotTqComponent,
      },
      {
        path: 'xetduyetxa',
        component: XetDuyetXaComponent,
      },
      {
        path: 'xetduyethuyen',
        component: XetDuyetHuyenComponent,
      },
      {
        path: 'nhapngu',
        component: NhapNguComponent,
      },
      {
        path: 'congdannu',
        component: CongDanNuComponent,
      },
      {
        path: 'ssdbdv1',
        component: SsDbdv1Component,
      },
      {
        path: 'ssdbdv2',
        component: SsDbdv2Component,
      },
      {
        path: 'dbdv1',
        component: Dbdv1Component,
      },
      {
        path: 'dbdv2',
        component: Dbdv2Component,
      },
      {
        path: 'sqdb',
        component: SqdbComponent,
      },
      {
        path: 'qndb',
        component: QndbComponent,
      },
      {
        path: 'kehoachdbdv',
        component: KeHoachDbdvComponent,
      },
      {
        path: 'theodoihuanluyen',
        component: TheoDoiHuanLuyenComponent,
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TuyenQuanRoutingModule { }
