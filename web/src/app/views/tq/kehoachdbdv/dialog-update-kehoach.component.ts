import { TqDqtvService } from './../../../services/tq-dqtv.service';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
// Translate
import { CommonService } from '../../../services/common.service';
import { TuyenQuanService } from '../../../services/tq.service';
import * as moment from 'moment';
import * as dateFormat from 'dateformat';
@Component({
  selector: 'app-dialog-update-kehoach',
  templateUrl: './dialog-update-kehoach.component.html',
})
export class DialogUpdateKeHoachComponent implements OnInit {
  dotTqForm: FormGroup;
  submitted = false;
  numbers: number[];
  donViDbdvs: any[];
  // convenience getter for easy access to form fields
  get f() { return this.dotTqForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private tuyenQuanService: TuyenQuanService,
    private dmTqService: TqDqtvService,
    public dialogRef: MatDialogRef<DialogUpdateKeHoachComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
    const date = new Date();
    this.numbers = Array(30).fill(0).map((x, i) => (i + date.getFullYear() - 25));
  }

  ngOnInit() {
    this.createForm();
    this.loadCombo();
  }
  createForm() {
    const date = new Date();
    const firstDay = dateFormat(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy-mm-dd');
    const lastDay = dateFormat(new Date(date.getFullYear(), date.getMonth() + 1, 1), 'yyyy-mm-dd');
    this.dotTqForm = this.fb.group(
      {
        nam: [date.getFullYear(), [Validators.required]],
        id_donViDbdv: ['', [Validators.required]],
        soLuongSq: [null, [Validators.required, Validators.min(0)]],
        soLuongHsqbs: [null, [Validators.required, Validators.min(0)]],
        ghiChu: [''],
      });
    if (this.data) {
      this.dotTqForm.setValue({
        'nam': this.data.nam,
        'id_donViDbdv': this.data.id_donViDbdv,
        'soLuongSq': this.data.soLuongSq,
        'soLuongHsqbs': this.data.soLuongHsqbs,
        'ghiChu': this.data.ghiChu,
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.dotTqForm.invalid) {
      return;
    }
    if (!this.data) {
      this.tuyenQuanService.insertKeHoachDbdv(this.dotTqForm.value, this.common.TQ_DotTuyenQuan.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.tuyenQuanService.editKeHoachDbdv({
        data: this.dotTqForm.value,
        condition: { id: this.data.id }
      }, this.common.TQ_DotTuyenQuan.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  loadCombo() {
    this.dmTqService.getDonViDbdv({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.donViDbdvs = res.data;
      }
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
