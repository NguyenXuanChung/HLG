import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { TuyenQuanService } from '../../../services/tq.service';
import * as dateFormat from 'dateformat';
import * as moment from 'moment';
import { TqDqtvService } from '../../../services/tq-dqtv.service';


@Component({
  selector: 'app-dialog-update-huanluyen',
  templateUrl: './dialog-update-huanluyen.component.html',
})
export class DialogUpdateHuanLuyenComponent implements OnInit {
  submitted = false;
  isInputDate = false;
  // convenience getter for easy access to form fields
  dotTuyenQuans: any[];
  tqForm: FormGroup;
  capBacs: any[];
  donViDbdvs: any[];
  numbers: number[];

  // convenience getter for easy access to form fields
  get f() { return this.tqForm.controls; }

  constructor(
    private common: CommonService,
    private tuyenQuanService: TuyenQuanService,
    private dmTqService: TqDqtvService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogUpdateHuanLuyenComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
    const date = new Date();
    this.numbers = Array(30).fill(0).map((x, i) => (i + date.getFullYear() - 25));
  }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    const date = new Date();
    this.tqForm = this.fb.group(
      {
        id_details: [this.data.id_details],
        nam: [date.getFullYear(), [Validators.required]],
        ngayHuanLuyen: [null, [Validators.required]]
      });
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.tqForm.invalid) {
      return;
    }
    this.tuyenQuanService.insertTheoDoiHuanLuyen(this.tqForm.value, this.common.TQ_CongDan.Update)
      .subscribe(res => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.dialogRef.close(res.message);
        }
      });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
