import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { TuyenQuanService } from '../../../services/tq.service';
import * as dateFormat from 'dateformat';
import * as moment from 'moment';


@Component({
  selector: 'app-dialog-update-xd',
  templateUrl: './dialog-update-xd.component.html',
})
export class DialogUpdateXdComponent implements OnInit {
  congDanForm: FormGroup;
  congDans: any[] = [];
  typeCongDans: any[];
  typeCongDanSelection: any;
  submitted = false;
  isInputDate = false;
  // convenience getter for easy access to form fields
  get f() { return this.congDanForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private tuyenQuanService: TuyenQuanService,
    public dialogRef: MatDialogRef<DialogUpdateXdComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.loadCombo();
  }
  createForm() {
    this.congDanForm = this.fb.group(
      {
        id_congDans: this.data.id_congDans,
        id_congDanUpdates: this.data.id_congDanUpdates,
        id_detailUpdates: this.data.id_detailUpdates,
        id_tq_type_congDan: ['', [Validators.required]],
        id_tq_type_congDan_before: this.data.id_tq_type_congDan,
        nameSelect: [''],
        id_dotTuyenQuan: this.data.id_dotTuyenQuan,
        ngay: moment().format('YYYY-MM-DD')
      });
  }
  onSubmit() {
    this.typeCongDanSelection = this.typeCongDans.find(x => x.next_state === this.f.id_tq_type_congDan.value);
    this.f.nameSelect.setValue(this.typeCongDanSelection.name);
    this.submitted = true;
    // stop here if form is invalid
    if (this.congDanForm.invalid) {
      return;
    }
    this.data.id_congDans.split(',').forEach(element => {
      this.congDans.push(
        {
          id_congDan: element,
          id_type_state: this.f.id_tq_type_congDan.value,
          id_type_beforeState: this.data.id_tq_type_congDan,
          noiDung: this.f.nameSelect.value,
          tuNgay: this.f.ngay.value,
          denNgayThongBao: null,
          id_dotTuyenQuan: this.f.id_dotTuyenQuan.value,
          id_quanHam: null,
          id_capBac: null,
          dkDongVien: null,
          id_donViDbdv: null,
          id_donViTv: null,
          typeDqtv: null,
          id_lucLuong: null,
          latest: 1
        }
      );
    });
    this.tuyenQuanService.updateQuaTrinh({ congDans: this.congDans }
      , this.common.TQ_CongDan.Insert)
      .subscribe(res => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.dialogRef.close(res.message);
        }
      });
  }
  loadCombo() {
    this.tuyenQuanService.getLoaiCongDan({
      id_workflow_state: this.data.id_tq_type_congDan
    }, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.typeCongDans = res.data;
      }
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
