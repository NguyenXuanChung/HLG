import { TuyenQuanService } from './../../../services/tq.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';

import * as moment from 'moment';
import { map, startWith, isEmpty } from 'rxjs/operators';

// Translate
import { CommonService } from './../../../services/common.service';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';
import { Router } from '@angular/router';
import { CheckableSettings } from '@progress/kendo-angular-treeview';
import { Observable, of } from 'rxjs';
import { DialogUpdateCongDanComponent } from '../congdan/dialog-update-congdan.component';
import { DialogUpdateXdComponent } from './dialog-update-xd.component';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}
@Component({
  selector: 'app-xetduyetxa',
  templateUrl: './xetduyetxa.component.html'
})
export class XetDuyetXaComponent implements OnInit {
  displayedColumns = ['index', 'hoTen', 'cmnd', 'ngaySinh', 'id_trinhDoVanHoa', 'id_donViQuanLy', 'noiDung', 'actions'];
  dataSource: MatTableDataSource<any>;
  typeStateCongDans: any[];
  dotTuyenQuans: any[];
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  searchForm: FormGroup;
  permission: any;
  checkArray: any[] = [];
  checkArrayUpdate: any[] = [];
  checkArrayDetailUpdate: any[] = [];
  get fs() { return this.searchForm.controls; }
  form: FormGroup;
  obj: any = [];
  numbers: number[];
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private tuyenQuanService: TuyenQuanService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
    const date = new Date();
    this.numbers = Array(30).fill(0).map((x, i) => (i + date.getFullYear() - 25));
  }

  ngOnInit() {
    this.createSearchForm();
    // this.onSearch();
    if (!this.permission.TQ_CongDan._view) {
      this.router.navigate(['/']);
    }
    this.loadCombo();
  }

  createSearchForm() {
    const date = new Date();
    this.searchForm = this.fb.group(
      {
        id_dotTuyenQuan: [null],
        id_tq_type_congDan: [5],
        nam: [date.getFullYear()],
      });
    this.loadDotTuyenQuan(date.getFullYear());
  }

  onSearch() {
    this.getCongDan();
  }

  getCongDan() {
    this.obj = [];
    this.tuyenQuanService.getCongDanXd(this.searchForm.value, this.common.TQ_CongDan.View).subscribe(res => {
      if (res.error) {
        this.dataSource = new MatTableDataSource();
      } else {
        console.log(res.data);
        res.data.forEach(element => {
          if (!element.noiDung || element.latest === 1) {
            this.obj.push({ checked: false, ...element });
          } else {
            this.obj.push(element);
          }
        });
        this.dataSource = new MatTableDataSource(this.obj);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
  loadDotTuyenQuan(e) {
    this.fs.id_dotTuyenQuan.setValue(0);
    this.tuyenQuanService.getDotTuyenQuan({ nam: e }, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.dotTuyenQuans = res.data;
        this.fs.id_dotTuyenQuan.setValue(res.data[0].id);
      }
      this.onSearch();

    });
  }
  openDetailDialog(data: any): void {
    // const dialogRef = this.dialog.open(DialogDetailGiamSatComponent, {
    //   height: 'auto',
    //   width: '70%',
    //   data: data
    // });
    // dialogRef.afterClosed().subscribe(result => {
    // });
  }
  openUpdateDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogUpdateCongDanComponent, {
      height: '80%',
      width: '70%',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openUpdateXdDialog(): void {
    this.checkArray = [];
    this.obj.forEach(element => {
      if (element.checked === true) {
        if (element.latest === 1) {
          this.checkArrayUpdate.push(element.id);
          this.checkArrayDetailUpdate.push(element.id_detail);
        } else {
          this.checkArray.push(element.id);
        }
      }
    });
    if (this.checkArray.length === 0 && this.checkArrayUpdate.length === 0) {
      this.common.messageErr({ error: { message: 'tq.validate.chonCongDan' } });
      return;
    }
    const tenDotTuyenQuan = this.dotTuyenQuans.find(x => x.id === this.fs.id_dotTuyenQuan.value);

    const dialogRef = this.dialog.open(DialogUpdateXdComponent, {
      width: '350px',
      data: {
        tenDotTuyenQuan: tenDotTuyenQuan.name,
        id_dotTuyenQuan: tenDotTuyenQuan.id,
        id_tq_type_congDan: 5,
        id_congDans: this.checkArray.toString(),
        id_congDanUpdates: this.checkArrayUpdate.toString(),
        id_detailUpdates: this.checkArrayDetailUpdate.toString()
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.hoTen }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.tuyenQuanService.deleteCongDan({ id: data.id }, this.common.TQ_CongDan.Delete)
          .subscribe(res => {
            if (res.error) {
              this.common.messageErr(res);
            } else {
              this.onSearch();
              this.common.messageRes(res.message);
            }
          });
      }
    });
  }
  checkAll(data: any) {
    if (data === true) {
      this.obj.forEach(element => {
        if (!element.noiDung || element.latest === 1) {
          element.checked = true;
        }

      });
    } else {
      this.obj.forEach(element => {
        if (!element.noiDung || element.latest === 1) {
          element.checked = false;
        }
      });
    }
  }

  loadCombo() {
    this.tuyenQuanService.getCongDanTypeState({ id_congDan_type: 1, view: 1 }, this.common.TQ_CongDan.View).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.typeStateCongDans = res.data;
      }
    });
  }
  public get checkableSettings(): CheckableSettings {
    return {
      checkChildren: true,
      checkParents: false,
      enabled: true,
      mode: 'multiple',
      checkOnClick: false
    };
  }
  public children = (dataItem: any): Observable<any[]> => of(dataItem.children);
  public hasChildren(dataItem: any): boolean {
    return dataItem.children && dataItem.children.length > 0;
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
