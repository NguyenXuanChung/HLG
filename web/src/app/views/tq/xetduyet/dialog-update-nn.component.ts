import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { TuyenQuanService } from '../../../services/tq.service';
import * as dateFormat from 'dateformat';
import * as moment from 'moment';
import { TqDqtvService } from '../../../services/tq-dqtv.service';


@Component({
  selector: 'app-dialog-update-nn',
  templateUrl: './dialog-update-nn.component.html',
})
export class DialogUpdateNnComponent implements OnInit {
  congDanForm: FormGroup;
  congDans: any[] = [];
  typeCongDans: any[];
  quanHams: any[];
  donViDbdvs: any[];
  typeCongDanSelection: any;
  submitted = false;
  isInputDate = false;
  // convenience getter for easy access to form fields
  get f() { return this.congDanForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private tuyenQuanService: TuyenQuanService,
    private dmTqService: TqDqtvService,
    public dialogRef: MatDialogRef<DialogUpdateNnComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.loadCombo();
  }
  createForm() {
    this.congDanForm = this.fb.group(
      {
        id_congDans: [this.data.id_congDans],
        noiDung: [null],
        id_type_state: [null, [Validators.required]],
        id_type_beforeState: this.data.id_tq_type_congDan,
        id_dotTuyenQuan: this.data.id_dotTuyenQuan,
        id_quanHam: [null, [Validators.required]],
        id_donViDbdv: [null, [Validators.required]],
        tuNgay: [moment().format('YYYY-MM-DD')],
        denNgay: [null],
      });
  }
  onSubmit() {
    if (this.f.id_type_state.value === '15') {
      this.f.id_quanHam.enable();
      this.f.id_donViDbdv.enable();
    } else if (this.f.id_type_state.value === '8') {
      this.f.id_donViDbdv.enable();
    } else {
      this.f.id_quanHam.disable();
      this.f.id_donViDbdv.disable();
    }
    this.typeCongDanSelection = this.typeCongDans.find(x => x.next_state === this.f.id_type_state.value);
    this.f.noiDung.setValue(this.typeCongDanSelection.name);
    this.submitted = true;
    // stop here if form is invalid
    if (this.congDanForm.invalid) {
      return;
    }
    this.data.id_congDans.split(',').forEach(element => {
      this.congDans.push(
        {
          id_congDan: element,
          id_type_state: this.f.id_type_state.value,
          id_type_beforeState: this.data.id_tq_type_congDan,
          noiDung: this.f.noiDung.value,
          tuNgay: this.f.tuNgay.value,
          denNgayThongBao: this.f.denNgay.value,
          id_dotTuyenQuan: this.f.id_dotTuyenQuan.value,
          id_quanHam: null,
          id_capBac: null,
          dkDongVien: null,
          id_donViDbdv: null,
          id_donViTv: null,
          typeDqtv: null,
          id_lucLuong: null,
          latest: 1
        }
      );
    });
    this.tuyenQuanService.updateQuaTrinh({ congDans: this.congDans }
      , this.common.TQ_CongDan.Insert)
      .subscribe(res => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.dialogRef.close(res.message);
        }
      });
  }
  loadCombo() {
    this.dmTqService.getQuanHam({}, this.common.TQ_CongDan.Insert)
    .subscribe(res => {
      if (!res.error) {
        this.quanHams = res.data;
      }
    });
    this.dmTqService.getDonViDbdv({}, this.common.TQ_CongDan.Insert)
    .subscribe(res => {
      if (!res.error) {
        this.donViDbdvs = res.data;
      }
    });

    this.tuyenQuanService.getLoaiCongDan({
      id_workflow_state: this.data.id_tq_type_congDan
    }, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.typeCongDans = res.data;
      }
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
