import { DialogUpdateDotTqComponent } from './dialog-update-dottq.component';
import { GroupService } from '../../../services/group.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { CheckableSettings } from '@progress/kendo-angular-treeview';

import * as moment from 'moment';
import * as dateFormat from 'dateformat';

// Translate
import { CommonService } from '../../../services/common.service';
import { Observable, of } from 'rxjs';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';
import { Router } from '@angular/router';
import { TuyenQuanService } from '../../../services/tq.service';

@Component({
  selector: 'app-dottq',
  templateUrl: './dottq.component.html'
})
export class DotTqComponent implements OnInit {
  displayedColumns = ['index', 'nam', 'name', 'tuNgay', 'denNgay', 'ngayKhoaDk', 'description', 'actions'];
  dataSource: MatTableDataSource<any>;
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  searchForm: FormGroup;
  permission: any;
  numbers: number[];
  get fs() { return this.searchForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private tuyenQuanService: TuyenQuanService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
    const date = new Date();
    this.numbers = Array(30).fill(0).map((x, i) => (i + date.getFullYear() - 25));

  }

  ngOnInit() {
    this.createSearchForm();
    this.onSearch();
    console.log(this.getDotTuyenQuan);
    if (!this.permission.TQ_DotTuyenQuan._view) {
      this.router.navigate(['/']);
    }
  }

  createSearchForm() {
    const date = new Date();
    this.searchForm = this.fb.group(
      {
        nam: [date.getFullYear()],
        name: [null],
        description: [null]
      });
  }

  onSearch() {
    this.getDotTuyenQuan();
  }
  getDotTuyenQuan() {
    this.tuyenQuanService.getDotTuyenQuan(this.searchForm.value, this.common.TQ_DotTuyenQuan.View).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource = new MatTableDataSource();
      } else {
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
  openUpdateDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogUpdateDotTqComponent, {
      width: '700px',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.tuyenQuanService.deleteDotTuyenQuan({ id: data.id }, this.common.TQ_DotTuyenQuan.Delete).subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.onSearch();
            this.common.messageRes(res.message);
          }
        });
      }
    });
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
