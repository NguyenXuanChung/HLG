import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
// Translate
import { CommonService } from '../../../services/common.service';
import { TuyenQuanService } from '../../../services/tq.service';
import * as moment from 'moment';
import * as dateFormat from 'dateformat';
@Component({
  selector: 'app-dialog-update-dottq',
  templateUrl: './dialog-update-dottq.component.html',
})
export class DialogUpdateDotTqComponent implements OnInit {
  dotTqForm: FormGroup;
  configs: any[];
  diaBans: any[];
  groups: any[];
  submitted = false;
  falseDate = false;
  numbers: number[];

  // convenience getter for easy access to form fields
  get f() { return this.dotTqForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private tuyenQuanService: TuyenQuanService,
    public dialogRef: MatDialogRef<DialogUpdateDotTqComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
    const date = new Date();
    this.numbers = Array(30).fill(0).map((x, i) => (i + date.getFullYear() - 25));
  }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    const date = new Date();
    const firstDay = dateFormat(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy-mm-dd');
    const lastDay = dateFormat(new Date(date.getFullYear(), date.getMonth() + 1, 1), 'yyyy-mm-dd');
    this.dotTqForm = this.fb.group(
      {
        nam: [date.getFullYear(), [Validators.required]],
        name: ['', [Validators.required]],
        ghiChu: [''],
        tuNgay: [firstDay, [Validators.required]],
        denNgay: [lastDay, [Validators.required]],
        ngayKhoaDk: [lastDay, [Validators.required]]
      });
    if (this.data) {
      this.dotTqForm.setValue({
        'nam': this.data.nam,
        'name': this.data.name,
        'ghiChu': this.data.ghiChu,
        'tuNgay': null,
        'denNgay': null,
        'ngayKhoaDk': null,
      });
      if (this.data.tuNgay) {
        this.f.tuNgay.setValue(dateFormat(this.data.tuNgay, 'yyyy-mm-dd'));
      }
      if (this.data.denNgay) {
        this.f.denNgay.setValue(dateFormat(this.data.denNgay, 'yyyy-mm-dd'));
      }
      if (this.data.ngayKhoaDk) {
        this.f.ngayKhoaDk.setValue(dateFormat(this.data.ngayKhoaDk, 'yyyy-mm-dd'));
      }
    }
  }
  onSubmit() {
    if (this.f.tuNgay.value === '') {
      this.f.tuNgay.setValue(null);
    }
    if (this.f.denNgay.value === '') {
      this.f.denNgay.setValue(null);
    }
    const denNgay = new Date(this.f.denNgay.value);
    const tuNgay = new Date(this.f.tuNgay.value);
    if (denNgay.getTime() < tuNgay.getTime()) {
      this.falseDate = true;
    }
    // if(this.f.tuNgay)
    this.submitted = true;
    // stop here if form is invalid
    if (this.dotTqForm.invalid) {
      return;
    }
    if (!this.data) {
      this.tuyenQuanService.insertDotTuyenQuan(this.dotTqForm.value, this.common.TQ_DotTuyenQuan.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.tuyenQuanService.editDotTuyenQuan({
        data: this.dotTqForm.value,
        condition: { id: this.data.id }
      }, this.common.TQ_DotTuyenQuan.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
