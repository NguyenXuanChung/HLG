import { TuyenQuanRoutingModule } from './tq-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { AngularMaterial } from '../../angular-material';
import { MomentModule } from 'ngx-moment';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { EditorModule } from 'primeng/editor';
import { QuillModule } from 'ngx-quill';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { MatTabsModule } from '@angular/material/tabs';
import { TreeModule } from 'primeng/tree';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { CongDanComponent } from './congdan/congdan.component';
import { DialogUpdateCongDanComponent } from './congdan/dialog-update-congdan.component';
import { AppSsnnComponent } from './congdan/ssnn.component';
import { DotTqComponent } from './dottq/dottq.component';
import { DialogUpdateDotTqComponent } from './dottq/dialog-update-dottq.component';
import { DialogUpdateTqComponent } from './congdan/dialog-update-tq.component';
import { XetDuyetXaComponent } from './xetduyet/xetDuyetXa.component';
import { XetDuyetHuyenComponent } from './xetduyet/xetDuyetHuyen.component';
import { DialogUpdateXdComponent } from './xetduyet/dialog-update-xd.component';
import { NhapNguComponent } from './xetduyet/nhapNgu.component';
import { DialogUpdateNnComponent } from './xetduyet/dialog-update-nn.component';
import { DialogUpdateQuanHeComponent } from './congdan/dialog-update-quanhe.component';
import { CongDanNuComponent } from './dbdv/congdannu.component';
import { SsDbdv2Component } from './dbdv/ssdbdv2.component';
import { DialogUpdateDbdvComponent } from './dbdv/dialog-update-dbdv.component';
import { DialogUpdateTrangThaiComponent } from './congdan/dialog-update-trangthai.component';
import { SsDbdv1Component } from './dbdv/ssdbdv1.component';
import { Dbdv1Component } from './dbdv/dbdv1.component';
import { DialogUpdateBcComponent } from './dbdv/dialog-update-bc.component';
import { DialogUpdateDongVienComponent } from './dbdv/dialog-update-dongvien.component';
import { KeHoachDbdvComponent } from './kehoachdbdv/kehoachdbdv.component';
import { DialogUpdateKeHoachComponent } from './kehoachdbdv/dialog-update-kehoach.component';
import { SqdbComponent } from './dbdv/sqdb.component';
import { Dbdv2Component } from './dbdv/dbdv2.component';
import { QndbComponent } from './dbdv/qndb.component';
import { TheoDoiHuanLuyenComponent } from './theodoihuanluyen/theodoihuanluyen.component';
import { DialogUpdateHuanLuyenComponent } from './theodoihuanluyen/dialog-update-huanluyen.component';
import { DialogUpdateGiaiNgachComponent } from './dbdv/dialog-update-giaingach.component';
import { DialogUpdateLichSuComponent } from './congdan/dialog-update-lichsu.component';

export function translateHttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
    CongDanComponent,
    AppSsnnComponent,
    DialogUpdateCongDanComponent,
    DotTqComponent,
    DialogUpdateDotTqComponent,
    DialogUpdateTqComponent,
    XetDuyetXaComponent,
    XetDuyetHuyenComponent,
    DialogUpdateXdComponent,
    NhapNguComponent,
    DialogUpdateNnComponent,
    DialogUpdateQuanHeComponent,
    CongDanNuComponent,
    SsDbdv2Component,
    DialogUpdateDbdvComponent,
    DialogUpdateTrangThaiComponent,
    SsDbdv1Component,
    Dbdv1Component,
    DialogUpdateBcComponent,
    DialogUpdateDongVienComponent,
    KeHoachDbdvComponent,
    DialogUpdateKeHoachComponent,
    SqdbComponent,
    Dbdv2Component,
    QndbComponent,
    TheoDoiHuanLuyenComponent,
    DialogUpdateHuanLuyenComponent,
    DialogUpdateGiaiNgachComponent,
    DialogUpdateLichSuComponent
  ],
  entryComponents: [
    DialogUpdateCongDanComponent,
    DialogUpdateLichSuComponent,
    DialogUpdateDotTqComponent,
    DialogUpdateTqComponent,
    DialogUpdateXdComponent,
    DialogUpdateNnComponent,
    DialogUpdateQuanHeComponent,
    DialogUpdateDbdvComponent,
    DialogUpdateTrangThaiComponent,
    DialogUpdateBcComponent,
    DialogUpdateDongVienComponent,
    DialogUpdateKeHoachComponent,
    DialogUpdateHuanLuyenComponent,
    DialogUpdateGiaiNgachComponent
  ],
  imports: [
    CommonModule,
    TuyenQuanRoutingModule,
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: translateHttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    AngularMaterial,
    MomentModule,
    InputTextareaModule,
    EditorModule,
    QuillModule.forRoot({
      modules: {
        syntax: false,
        toolbar: [
          ['bold', 'italic', 'underline', 'strike'],
          [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
          [{ 'list': 'ordered' }, { 'list': 'bullet' }],
          [{ 'indent': '-1' }, { 'indent': '+1' }],
          ['blockquote', 'code-block']
        ]
      }
    }),
    TreeViewModule,
    BsDropdownModule.forRoot(),
    MatTabsModule,
    TreeModule,
    MatAutocompleteModule
  ]
})
export class TuyenQuanModule { }
