import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { TuyenQuanService } from '../../../services/tq.service';
import * as dateFormat from 'dateformat';
import * as moment from 'moment';
import { TqDqtvService } from '../../../services/tq-dqtv.service';


@Component({
  selector: 'app-dialog-update-dbdv',
  templateUrl: './dialog-update-dbdv.component.html',
})
export class DialogUpdateDbdvComponent implements OnInit {
  submitted = false;
  isInputDate = false;
  // convenience getter for easy access to form fields
  dotTuyenQuans: any[];
  tqForm: FormGroup;
  quanHams: any[];
  congDans: any[] = [];
  // convenience getter for easy access to form fields
  get f() { return this.tqForm.controls; }

  constructor(
    private common: CommonService,
    private tuyenQuanService: TuyenQuanService,
    private dmTqService: TqDqtvService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogUpdateDbdvComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.getQuanHam();
  }
  getQuanHam() {
    this.dmTqService.getQuanHam({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.quanHams = res.data;
      }
    });
  }

  createForm() {
    const date = new Date();
    this.tqForm = this.fb.group(
      {
        id_congDans: [this.data.id_congDans],
        noiDung: [null, [Validators.required]],
        id_type_state: [null],
        id_type_beforeState: this.data.id_type_state,
        id_dotTuyenQuan: [null],
        id_quanHam: [null, [Validators.required]],
        tuNgay: [moment().format('YYYY-MM-DD'), [Validators.required]],
        denNgay: [moment().format('YYYY-MM-DD'), [Validators.required]],
      });
    this.f.tuNgay.setValue(dateFormat(this.data.tuNgay, 'yyyy-mm-dd'));
    if (this.data.hang === 1) {
      this.f.id_type_state.setValue(17);
    } else if (this.data.hang === 2) {
      this.f.id_type_state.setValue(18);
    } else if (this.data.hang === 0) {
      this.f.id_type_state.setValue(25);
    }
  }
  onSubmit() {
    this.f.denNgay.setValue(this.f.tuNgay.value);

    this.submitted = true;
    // stop here if form is invalid
    if (this.tqForm.invalid) {
      return;
    }
    this.data.id_congDans.split(',').forEach(element => {
      this.congDans.push(
        {
          id_congDan: element,
          id_type_state: this.f.id_type_state.value,
          id_type_beforeState: this.f.id_type_beforeState.value,
          noiDung: this.f.noiDung.value,
          tuNgay: this.f.tuNgay.value,
          denNgayThongBao: null,
          id_dotTuyenQuan: null,
          id_quanHam: this.f.id_quanHam.value,
          id_capBac: null,
          dkDongVien: null,
          id_donViDbdv: null,
          id_donViTv: null,
          typeDqtv: null,
          id_lucLuong: null,
          latest: 1
        }
      );
    });
    this.tuyenQuanService.updateQuaTrinh({ congDans: this.congDans }
      , this.common.TQ_CongDan.Update)
      .subscribe(res => {
        console.log(res);
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.dialogRef.close(res.message);
        }
      });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
