import { DonViService } from './../../../services/don-vi.service';
import { TuyenQuanService } from './../../../services/tq.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';

import * as moment from 'moment';
import { map, startWith, isEmpty } from 'rxjs/operators';

// Translate
import { CommonService } from './../../../services/common.service';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';
import { Router } from '@angular/router';
import { CheckableSettings } from '@progress/kendo-angular-treeview';
import { Observable, of } from 'rxjs';
import * as dateFormat from 'dateformat';
import { TqDqtvService } from '../../../services/tq-dqtv.service';
import { DialogUpdateCongDanComponent } from '../congdan/dialog-update-congdan.component';
import { DialogUpdateDbdvComponent } from './dialog-update-dbdv.component';
import { DialogUpdateBcComponent } from './dialog-update-bc.component';
import { DialogUpdateDongVienComponent } from './dialog-update-dongvien.component';
import { DialogUpdateGiaiNgachComponent } from './dialog-update-giaingach.component';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}
@Component({
  selector: 'app-sqdb',
  templateUrl: './sqdb.component.html'
})
export class SqdbComponent implements OnInit {
  displayedColumns = ['index', 'hoTen', 'ngaySinh', 'id_donViQuanLy',
    'duDkDongVien', 'ngayBienChe', 'donViDbdv', 'id_capBac', 'id_quanHam', 'viTriBienChe', 'actions'];
  dataSource: MatTableDataSource<any>;
  typeStateCongDans: any[];
  dotTuyenQuans: any[];
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  searchForm: FormGroup;
  permission: any;
  checkArray: any[] = [];
  public id_donVis: any[] = [];
  donVis: any[];
  public id_types: any[] = [];

  trinhDoVanHoas: any[] = [];
  numbers: any[] = ['1', '2'];
  trinhDoChuyenMons: any[] = [];
  get fs() { return this.searchForm.controls; }
  form: FormGroup;
  obj: any = [];
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private tuyenQuanService: TuyenQuanService,
    public dialog: MatDialog,
    private router: Router,
    private donVi: DonViService,
    private storageService: StorageService,
    private dmTqService: TqDqtvService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  ngOnInit() {
    this.createSearchForm();
    if (!this.permission.TQ_CongDan._view) {
      this.router.navigate(['/']);
    }
    this.loadCombo();
    this.onSearch();
  }

  createSearchForm() {
    const date = new Date();
    const today = moment().format('YYYY-MM-DD');
    const firstDay = dateFormat(new Date(date.getFullYear(), 0, 1), 'yyyy-mm-dd');
    const lastDay = dateFormat(new Date(date.getFullYear(), 11, 31), 'yyyy-mm-dd');
    this.searchForm = this.fb.group(
      {
        id_tq_type_congDan: '24,25',
        id_donViQuanLys: null,
        content: '',
        ngay: [firstDay, [Validators.required]],
      });
  }

  onSearch() {
    this.getCongDan();
  }

  getCongDan() {
    console.log(this.searchForm.value);
    this.obj = [];
    this.tuyenQuanService.getDbdv(this.searchForm.value, this.common.TQ_CongDan.View).subscribe(res => {
      if (res.error) {
        this.dataSource = new MatTableDataSource();
      } else {
        res.data.forEach(element => {
          this.obj.push({ checked: false, ...element });
        });
        this.dataSource = new MatTableDataSource(this.obj);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  openDetailDialog(data: any): void {
    // const dialogRef = this.dialog.open(DialogDetailGiamSatComponent, {
    //   height: 'auto',
    //   width: '70%',
    //   data: data
    // });
    // dialogRef.afterClosed().subscribe(result => {
    // });
  }
  openUpdateDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogUpdateCongDanComponent, {
      height: '80%',
      width: '70%',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openUpdateDbdvDialog(): void {
    this.checkArray = [];
    this.obj.forEach(element => {
      if (element.checked === true) {
        this.checkArray.push(element.id);
      }
    });
    if (this.checkArray.length === 0) {
      this.common.messageErr({ error: { message: 'tq.validate.chonCongDan' } });
      return;
    }
    const dialogRef = this.dialog.open(DialogUpdateDbdvComponent, {
      width: '350px',
      data: {
        tuNgay: this.fs.tuNgay.value,
        denNgay: this.fs.denNgay.value,
        hang: 0,
        id_congDans: this.checkArray.toString(),
        id_type_state: this.fs.id_tq_type_congDan.value,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openUpdateDongVienDialog(): void {
    this.checkArray = [];
    this.obj.forEach(element => {
      if (element.checked === true) {
        this.checkArray.push(element.id);
      }
    });
    if (this.checkArray.length === 0) {
      this.common.messageErr({ error: { message: 'tq.validate.chonCongDan' } });
      return;
    }
    const dialogRef = this.dialog.open(DialogUpdateDongVienComponent, {
      width: '350px',
      data: {
        id_details: this.checkArray.toString(),
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openUpdateBienCheDialog(): void {
    this.checkArray = [];
    this.obj.forEach(element => {
      if (element.checked === true) {
        this.checkArray.push(element.id_congDan);
      }
    });
    if (this.checkArray.length === 0) {
      this.common.messageErr({ error: { message: 'tq.validate.chonCongDan' } });
      return;
    }
    const dialogRef = this.dialog.open(DialogUpdateBcComponent, {
      width: '350px',
      data: {
        tuNgay: this.fs.ngay.value,
        denNgay: this.fs.ngay.value,
        id_type_bienChe: 20,
        id_congDans: this.checkArray.toString(),
        id_type_state: 17,
        id_quanHam: this.fs.id_tq_type_congDan.value,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openGiaiNgachDialog() {
    this.checkArray = [];
    this.obj.forEach(element => {
      if (element.checked === true) {
        this.checkArray.push(element.id_congDan);
      }
    });
    if (this.checkArray.length === 0) {
      this.common.messageErr({ error: { message: 'tq.validate.chonCongDan' } });
      return;
    }
    const dialogRef = this.dialog.open(DialogUpdateGiaiNgachComponent, {
      width: '350px',
      data: {
        id_type_beforeState: 24,
        id_congDans: this.checkArray.toString(),
        id_type_state: 26,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openInsertListDialog(): void {
    // const dialogRef = this.dialog.open(DialogUpdateDeviceListComponent, {
    // height: '80%',
    // width: '70%',
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {
    //     this.onSearch();
    //     this.common.messageRes(result);
    //   }
    // });
  }
  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.hoTen }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.tuyenQuanService.deleteCongDan({ id: data.id }, this.common.TQ_CongDan.Delete)
          .subscribe(res => {
            if (res.error) {
              this.common.messageErr(res);
            } else {
              this.onSearch();
              this.common.messageRes(res.message);
            }
          });
      }
    });
  }
  checkAll(data: any) {
    console.log(data);
    if (data === true) {
      this.obj.forEach(element => {
        element.checked = true;
      });
    } else {
      this.obj.forEach(element => {
        element.checked = false;
      });
    }
  }

  loadCombo() {
    const today = moment().format('YYYY/MM/DD');
    this.donVi.getTreeToValue({ id_trungTam: 1, tuNgay: today, denNgay: today }, this.common.TQ_CongDan.View)
      .subscribe((res: any) => {
        console.log(res);
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.donVis = res.data;
        }
      });
  }
  public get checkableSettings(): CheckableSettings {
    return {
      checkChildren: true,
      checkParents: false,
      enabled: true,
      mode: 'multiple',
      checkOnClick: false
    };
  }
  public children = (dataItem: any): Observable<any[]> => of(dataItem.children);
  public hasChildren(dataItem: any): boolean {
    return dataItem.children && dataItem.children.length > 0;
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
