import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { TuyenQuanService } from '../../../services/tq.service';
import * as dateFormat from 'dateformat';
import * as moment from 'moment';
import { TqDqtvService } from '../../../services/tq-dqtv.service';


@Component({
  selector: 'app-dialog-update-giaingach',
  templateUrl: './dialog-update-giaingach.component.html',
})
export class DialogUpdateGiaiNgachComponent implements OnInit {
  submitted = false;
  isInputDate = false;
  // convenience getter for easy access to form fields
  dotTuyenQuans: any[];
  tqForm: FormGroup;
  capBacs: any[];
  donViDbdvs: any[];
  // convenience getter for easy access to form fields
  get f() { return this.tqForm.controls; }

  constructor(
    private common: CommonService,
    private tuyenQuanService: TuyenQuanService,
    private dmTqService: TqDqtvService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogUpdateGiaiNgachComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    const date = new Date();
    this.tqForm = this.fb.group(
      {
        id_congDans: [this.data.id_congDans],
        id_type_beforeState: [this.data.id_type_beforeState],
        id_type_state: [this.data.id_type_state],
        ngay: [moment().format('YYYY-MM-DD')],
        noiDung: ''
      });
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.tqForm.invalid) {
      return;
    }
    this.tuyenQuanService.updateGiaiNgach(this.tqForm.value, this.common.TQ_CongDan.Update)
      .subscribe(res => {
        console.log(res);
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.dialogRef.close(res.message);
        }
      });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
