import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { TuyenQuanService } from '../../../services/tq.service';
import * as dateFormat from 'dateformat';
import * as moment from 'moment';
import { TqDqtvService } from '../../../services/tq-dqtv.service';


@Component({
  selector: 'app-dialog-update-bc',
  templateUrl: './dialog-update-bc.component.html',
})
export class DialogUpdateBcComponent implements OnInit {
  submitted = false;
  isInputDate = false;
  // convenience getter for easy access to form fields
  dotTuyenQuans: any[];
  tqForm: FormGroup;
  capBacs: any[];
  donViDbdvs: any[];
  // convenience getter for easy access to form fields
  get f() { return this.tqForm.controls; }

  constructor(
    private common: CommonService,
    private tuyenQuanService: TuyenQuanService,
    private dmTqService: TqDqtvService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogUpdateBcComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.loadCombo();
  }
  loadCombo() {
    this.dmTqService.getCapBac({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.capBacs = res.data;
      }
    });
    this.dmTqService.getDonViDbdv({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.donViDbdvs = res.data;
      }
    });
  }

  createForm() {
    const date = new Date();
    this.tqForm = this.fb.group(
      {
        id_congDans: [null],
        id_donViDbdv: [null, [Validators.required]],
        noiDung: [null, [Validators.required]],
        id_type_state: [this.data.id_type_bienChe],
        id_type_beforeState: this.data.id_type_state,
        id_dotTuyenQuan: [null],
        id_capBac: [null, [Validators.required]],
        tuNgay: [moment().format('YYYY-MM-DD'), [Validators.required]],
        denNgay: [null],
      });
    this.f.tuNgay.setValue(dateFormat(this.data.tuNgay, 'yyyy-mm-dd'));
  }
  onSubmit() {
    this.f.denNgay.setValue(this.f.tuNgay.value);

    this.submitted = true;
    // stop here if form is invalid
    if (this.tqForm.invalid) {
      return;
    }
    // {
    //   id_congDan: element,
    //   id_type_state: 5,
    //   id_type_beforeState: 1,
    //   noiDung: this.f.noiDung.value,
    //   tuNgay: this.f.tuNgay.value,
    //   denNgayThongBao: null,
    //   id_dotTuyenQuan: this.f.id_dotTuyenQuan.value,
    //   id_quanHam: null,
    //   id_capBac: null,
    //   dkDongVien: null,
    //   id_donViDbdv: null,
    //   latest: 1
    // }id_donViDbdv
    this.data.congDans.forEach(element => {
      element.id_type_state = this.f.id_type_state.value;
      element.id_type_beforeState = this.f.id_type_beforeState.value;
      element.id_capBac = this.f.id_capBac.value;
      element.tuNgay = this.f.tuNgay.value;
      element.id_dotTuyenQuan = this.f.id_dotTuyenQuan.value;
      element.noiDung = this.f.noiDung.value;
      element.id_donViDbdv = this.f.id_donViDbdv.value;
    });
    this.tuyenQuanService.updateQuaTrinh({congDans: this.data.congDans}, this.common.TQ_CongDan.Update)
      .subscribe(res => {
        console.log(res);
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.dialogRef.close(res.message);
        }
      });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
