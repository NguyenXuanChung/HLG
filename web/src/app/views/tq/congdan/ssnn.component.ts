import { DonViService } from './../../../services/don-vi.service';
import { TuyenQuanService } from './../../../services/tq.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';

import * as moment from 'moment';
import { map, startWith, isEmpty } from 'rxjs/operators';

// Translate
import { CommonService } from './../../../services/common.service';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';
import { Router } from '@angular/router';
import { CheckableSettings } from '@progress/kendo-angular-treeview';
import { Observable, of } from 'rxjs';
import { DialogUpdateCongDanComponent } from './dialog-update-congdan.component';
import { DialogUpdateTqComponent } from './dialog-update-tq.component';
import * as dateFormat from 'dateformat';
import { TqDqtvService } from '../../../services/tq-dqtv.service';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}
@Component({
  selector: 'app-ssnn',
  templateUrl: './ssnn.component.html'
})
export class AppSsnnComponent implements OnInit {
  displayedColumns = ['index', 'hoTen', 'cmnd', 'ngaySinh', 'id_trinhDoVanHoa',
    'id_tq_type_congDan', 'id_donViQuanLy', 'actions'];
  dataSource: MatTableDataSource<any>;
  typeStateCongDans: any[];
  dotTuyenQuans: any[];
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  searchForm: FormGroup;
  permission: any;
  checkArray: any[] = [];
  public id_donVis: any[] = [];
  donVis: any[];
  public id_types: any[] = [];

  trinhDoVanHoas: any[] = [];
  trinhDoChuyenMons: any[] = [];
  get fs() { return this.searchForm.controls; }
  form: FormGroup;
  obj: any = [];
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private tuyenQuanService: TuyenQuanService,
    public dialog: MatDialog,
    private router: Router,
    private donVi: DonViService,
    private storageService: StorageService,
    private dmTqService: TqDqtvService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  ngOnInit() {
    this.createSearchForm();
    if (!this.permission.TQ_CongDan._view) {
      this.router.navigate(['/']);
    }
    this.loadCombo();
    this.onSearch();
  }

  createSearchForm() {
    const date = new Date();
    const today = moment().format('YYYY-MM-DD');
    const firstDay = dateFormat(new Date(date.getFullYear(), 0, 1), 'yyyy-mm-dd');
    const lastDay = dateFormat(new Date(date.getFullYear(), 11, 31), 'yyyy-mm-dd');
    this.searchForm = this.fb.group(
      {
        id_tq_type_congDan: 1,
        doiTuong: null,
        id_donViQuanLys: null,
        id_trinhDoVanHoa: null,
        id_trinhDoChuyenMon: null,
        tuoi: 45,
        content: null,
        tuNgay: [firstDay, [Validators.required]],
        denNgay: [lastDay, [Validators.required]]
      });
  }

  onSearch() {
    this.fs.id_donViQuanLys.setValue(this.id_donVis.toString());
    if (this.fs.doiTuong.value === '1') {
      this.fs.id_trinhDoVanHoa.setValue(1);
      this.fs.id_trinhDoChuyenMon.setValue(2);
      this.fs.tuoi.setValue(24);
    } else if (this.fs.doiTuong.value === '2') {
      this.fs.id_trinhDoVanHoa.setValue(1);
      this.fs.id_trinhDoChuyenMon.setValue(1);
      this.fs.tuoi.setValue(27);
    }
      this.getCongDan();
  }

  getCongDan() {
    console.log(this.searchForm.value);
    this.obj = [];
    this.tuyenQuanService.getCongDanSsnn(this.searchForm.value, this.common.TQ_CongDan.View).subscribe(res => {
      if (res.error) {
        this.dataSource = new MatTableDataSource();
      } else {
        res.data.forEach(element => {
          this.obj.push({ checked: false, ...element });
        });
        this.dataSource = new MatTableDataSource(this.obj);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  openDetailDialog(data: any): void {
    // const dialogRef = this.dialog.open(DialogDetailGiamSatComponent, {
    //   height: 'auto',
    //   width: '70%',
    //   data: data
    // });
    // dialogRef.afterClosed().subscribe(result => {
    // });
  }
  openUpdateDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogUpdateCongDanComponent, {
      height: '80%',
      width: '70%',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openUpdateTqDialog(): void {
    this.checkArray = [];
    this.obj.forEach(element => {
      if (element.checked === true) {
        this.checkArray.push(element.id);
      }
    });
    if (this.checkArray.length === 0) {
      this.common.messageErr({ error: { message: 'tq.validate.chonCongDan' } });
      return;
    }
    const dialogRef = this.dialog.open(DialogUpdateTqComponent, {
      width: '350px',
      data: {
        tuNgay: this.fs.tuNgay.value,
        denNgay: this.fs.denNgay.value,
        id_congDans: this.checkArray.toString(),
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openInsertListDialog(): void {
    // const dialogRef = this.dialog.open(DialogUpdateDeviceListComponent, {
    // height: '80%',
    // width: '70%',
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {
    //     this.onSearch();
    //     this.common.messageRes(result);
    //   }
    // });
  }
  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.hoTen }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.tuyenQuanService.deleteCongDan({ id: data.id }, this.common.TQ_CongDan.Delete)
          .subscribe(res => {
            if (res.error) {
              this.common.messageErr(res);
            } else {
              this.onSearch();
              this.common.messageRes(res.message);
            }
          });
      }
    });
  }
  checkAll(data: any) {
    console.log(data);
    if (data === true) {
      this.obj.forEach(element => {
        element.checked = true;
      });
    } else {
      this.obj.forEach(element => {
        element.checked = false;
      });
    }
  }

  loadCombo() {
    this.tuyenQuanService.getCongDanTypeState({ id_congDan_type: 1, view: 1 }, this.common.TQ_CongDan.View).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.typeStateCongDans = res.data;
      }
    });
    const today = moment().format('YYYY/MM/DD');
    this.dmTqService.getTrinhDoHocVan({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.trinhDoChuyenMons = res.data;
      }
    });
    this.dmTqService.getTrinhDoVanHoa({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.trinhDoVanHoas = res.data;
      }
    });
    this.donVi.getTreeToValue({ id_trungTam: 1, tuNgay: today, denNgay: today }, this.common.TQ_CongDan.View)
      .subscribe((res: any) => {
        console.log(res);
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.donVis = res.data;
        }
      });
  }
  public get checkableSettings(): CheckableSettings {
    return {
      checkChildren: true,
      checkParents: false,
      enabled: true,
      mode: 'multiple',
      checkOnClick: false
    };
  }
  public children = (dataItem: any): Observable<any[]> => of(dataItem.children);
  public hasChildren(dataItem: any): boolean {
    return dataItem.children && dataItem.children.length > 0;
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
