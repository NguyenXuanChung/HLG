import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
// Translate
import { CommonService } from '../../../services/common.service';
import { TuyenQuanService } from '../../../services/tq.service';
import * as moment from 'moment';
import * as dateFormat from 'dateformat';
import { TqDqtvService } from '../../../services/tq-dqtv.service';
@Component({
  selector: 'app-dialog-update-quanhe',
  templateUrl: './dialog-update-quanhe.component.html',
})
export class DialogUpdateQuanHeComponent implements OnInit {
  quanHeForm: FormGroup;
  quanHes: any[];
  ngheNghieps: any[];

  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.quanHeForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private tuyenQuanService: TuyenQuanService,
    private dmTqService: TqDqtvService,
    public dialogRef: MatDialogRef<DialogUpdateQuanHeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.loadCombo();
  }
  createForm() {
    const date = new Date();
    const firstDay = dateFormat(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy-mm-dd');
    const lastDay = dateFormat(new Date(date.getFullYear(), date.getMonth() + 1, 1), 'yyyy-mm-dd');
    this.quanHeForm = this.fb.group(
      {
        id_congDan: [this.data.id],
        id_quanHe: [null, [Validators.required]],
        id_ngheNghiep: [null],
        name: ['', [Validators.required]],
        namSinh: [''],
        ghiChu: [''],
      });
    if (this.data.body) {
      this.quanHeForm.setValue({
        'id_congDan': this.data.id,
        'id_quanHe': this.data.body.id_quanHe,
        'id_ngheNghiep': this.data.body.id_ngheNghiep,
        'name': this.data.body.name,
        'namSinh': this.data.body.namSinh,
        'ghiChu': this.data.body.ghiChu,

      });
    }
  }
  loadCombo() {
    this.dmTqService.getQuanHe({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.quanHes = res.data;
      }
    });
    this.dmTqService.getNgheNghiep({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.ngheNghieps = res.data;
      }
    });
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.quanHeForm.invalid) {
      return;
    }
    if (!this.data.body) {
      this.tuyenQuanService.insertQuanHe(this.quanHeForm.value, this.common.TQ_CongDan.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.tuyenQuanService.editQuanHe({
        data: this.quanHeForm.value,
        condition: { id: this.data.body.id }
      }, this.common.TQ_CongDan.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
