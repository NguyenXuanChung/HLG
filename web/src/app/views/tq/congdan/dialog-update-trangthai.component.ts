import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { TuyenQuanService } from '../../../services/tq.service';
import * as dateFormat from 'dateformat';
import * as moment from 'moment';
import { TqDqtvService } from '../../../services/tq-dqtv.service';


@Component({
  selector: 'app-dialog-update-trangthai',
  templateUrl: './dialog-update-trangthai.component.html',
})
export class DialogUpdateTrangThaiComponent implements OnInit {
  congDanForm: FormGroup;
  configs: any[];
  diaBans: any[];
  groups: any[];
  typeCongDans: any[];
  typeCongDanSelection: any;
  submitted = false;
  isInputDate = false;
  tamHoans: any[];
  miens: any[];
  lyDo: string = null;
  // convenience getter for easy access to form fields
  get f() { return this.congDanForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private tuyenQuanService: TuyenQuanService,
    private dmTqService: TqDqtvService,
    public dialogRef: MatDialogRef<DialogUpdateTrangThaiComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    console.log(this.data);
    this.createForm();
  }
  createForm() {
    this.congDanForm = this.fb.group(
      {
        id_tq_type_congDan_tuNgay: [null],
        id_tq_type_congDan_denNgay: [null],
      });
    if (this.data) {
      this.congDanForm.setValue({
        'id_tq_type_congDan_tuNgay': null,
        'id_tq_type_congDan_denNgay': null,
      });
      if (this.data.tuNgay) {
        this.f.id_tq_type_congDan_tuNgay.setValue(dateFormat(this.data.tuNgay, 'yyyy-mm-dd'));
      }
      if (this.data.denNgay) {
        this.f.id_tq_type_congDan_denNgay.setValue(dateFormat(this.data.denNgay, 'yyyy-mm-dd'));
      }
      this.loadCombo(this.data.id_type_state);
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.congDanForm.invalid) {
      return;
    }
    this.tuyenQuanService.editCongDanDetail({
      data: {
        tuNgay: this.f.id_tq_type_congDan_tuNgay.value,
        denNgay: this.f.id_tq_type_congDan_denNgay.value,
        noiDung: this.lyDo
      },
      condition: { id: this.data.id }
    }, this.common.TQ_CongDan.Insert).subscribe(response => {
      if (response.error) {
        this.common.messageErr(response.error);
      } else {
        this.dialogRef.close(response.message);
      }
    });
  }
  loadCombo(e) {
    this.tuyenQuanService.getCongDanTypeState({
      id: this.data.id_type_state
    }, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.typeCongDanSelection = res.data[0];
        if (this.typeCongDanSelection.code === 'DKNN') {
          this.f.id_tq_type_congDan_denNgay.setValue(null);
          this.isInputDate = false;
        } else if (this.typeCongDanSelection.code === 'MIEN') {
          this.isInputDate = false;
          this.f.id_tq_type_congDan_denNgay.setValue(null);
          this.loadDmMien();
        } else if (this.typeCongDanSelection.code === 'NHAPNGU') {
          this.isInputDate = true;
        } else if (this.typeCongDanSelection.code === 'HOAN') {
          this.loadDmTamHoan();
          this.isInputDate = true;
        } else if (this.typeCongDanSelection.code === 'VANG') {
          this.isInputDate = true;
        }
      }
    });
  }
  loadDmMien() {
    this.dmTqService.getMien({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.miens = res.data;
      }
    });
  }
  loadDmTamHoan() {
    this.dmTqService.getTamHoan({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.tamHoans = res.data;
      }
    });
  }
  choiseLyDo(e) {
    this.lyDo = e;
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
