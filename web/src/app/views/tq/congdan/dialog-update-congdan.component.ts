import { DanhMucService } from './../../../services/danhmuc.service';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { TuyenQuanService } from '../../../services/tq.service';
import * as dateFormat from 'dateformat';
import * as moment from 'moment';

import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { DialogUpdateQuanHeComponent } from './dialog-update-quanhe.component';
import { TqDqtvService } from '../../../services/tq-dqtv.service';
import { DonViService } from '../../../services/don-vi.service';
import { CheckableSettings } from '@progress/kendo-angular-treeview';
import { Observable, of } from 'rxjs';
import { DialogUpdateTrangThaiComponent } from './dialog-update-trangthai.component';
import { DialogUpdateLichSuComponent } from './dialog-update-lichsu.component';

@Component({
  selector: 'app-dialog-update-congdan',
  templateUrl: './dialog-update-congdan.component.html',
})
export class DialogUpdateCongDanComponent implements OnInit {
  congDanForm: FormGroup;
  typeCongDans: any[];
  typeCongDanSelection: any;
  submitted = false;
  isInputDate = false;
  tinhs: any[];
  huyens: any[];
  xas: any[];
  tamHoans: any[];
  miens: any[];
  trinhDoChuyenMons: any[];
  trinhDoVanHoas: any[];
  hocVis: any[];
  danTocs: any[];
  tonGiaos: any[];
  nganhNgheCmkts: any[];
  public id_donVis: any[] = [];
  quanHams: any[];
  donVis: any[];
  quanHes: any[];
  ngheNghieps: any[];
  lyDo: string = null;
  latestDataDetail: any;
  // convenience getter for easy access to form fields
  get f() { return this.congDanForm.controls; }
  //#region  quanHe
  displayedColumns = ['index', 'id_quanHe', 'name', 'namSinh', 'id_ngheNghiep', 'actions'];
  displayedColumnQuaTrinhs = ['tuNgay', 'denNgay', 'tenQuaTrinh', 'noiDung', 'donViQuanLy', 'actions'];
  dataSource: MatTableDataSource<any>;
  dataSourceQuaTrinh: MatTableDataSource<any>;
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private tuyenQuanService: TuyenQuanService,
    public dialog: MatDialog,
    private donVi: DonViService,
    private dmTqService: TqDqtvService,
    public dmService: DanhMucService,
    public dialogRef: MatDialogRef<DialogUpdateCongDanComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.loadCombo();
  }
  createForm() {
    this.congDanForm = this.fb.group(
      {
        hoTen: [null, [Validators.required]],
        gioiTinh: [null],
        ngaySinh: [null, [Validators.required]],
        id_tq_type_congDan: [null, [Validators.required]],
        id_tq_type_congDan_tuNgay: [null, [Validators.required]],
        id_tq_type_congDan_denNgay: [null],
        cmnd: [null, [Validators.required]],
        cmnd_ngayCap: [null],
        cmnd_noiCap: [null],
        cmnd_ngayHetHan: [null],
        id_danToc: [null],
        id_tonGiao: [null],
        id_trinhDoVanHoa: [null, [Validators.required]],
        id_trinhDoChuyenMon: [null],
        id_hocVi: [null],
        id_nganhNgheCmkt: [null],
        ngayVaoDoan: [null],
        ngayVaoDang: [null],
        ngayVaoDangChinhThuc: [null],
        noiCuTru: [null],
        nguyenQuan: [null],
        id_tinh: [null, [Validators.required]],
        id_huyen: [null, [Validators.required]],
        id_xa: [null, [Validators.required]],
        id_donViQuanLy: [null, [Validators.required]],
        noiLamViec: [null],
        vuongChinhTri: [false],
        noiDungVuongChinhTri: [null],
        ghiChu: [null],
        sizeAo: [null],
        sizeGiay: [null],
        sizeMu: [null],
        truongDaoTao: [null],
        id_quanHam: [null],
        id_vuKhi: [null],
        tuNgayDt: [null],
        denNgayDt: [null],
      });
    if (this.data.type === 'congDan') {
      this.f.gioiTinh.setValue('1');
    } else if (this.data.type === 'congDanNu') {
      this.f.gioiTinh.setValue('0');
    } else if (this.data.type === 'syQuan') {
      this.f.gioiTinh.setValue('1');
      this.f.id_tq_type_congDan.setValue(24);
    }
    if (this.data.body) {
      this.setValueEdit();
    }
  }
  setValueEdit() {
    this.congDanForm.setValue({
      'hoTen': this.data.body.hoTen,
      'id_tq_type_congDan': this.data.body.id_tq_type_congDan,
      'cmnd': this.data.body.cmnd,
      'ngaySinh': null,
      'id_tq_type_congDan_tuNgay': null,
      'id_tq_type_congDan_denNgay': null,
      'cmnd_noiCap': this.data.body.cmnd_noiCap,
      'cmnd_ngayCap': null,
      'gioiTinh': this.data.body.gioiTinh,
      'cmnd_ngayHetHan': this.data.body.cmnd_ngayHetHan,
      'id_danToc': this.data.body.id_danToc,
      'id_tonGiao': this.data.body.id_tonGiao,
      'id_trinhDoVanHoa': this.data.body.id_trinhDoVanHoa,
      'id_trinhDoChuyenMon': this.data.body.id_trinhDoChuyenMon,
      'id_hocVi': this.data.body.id_hocVi,
      'id_nganhNgheCmkt': this.data.body.id_nganhNgheCmkt,
      'ngayVaoDoan': null,
      'ngayVaoDang': null,
      'ngayVaoDangChinhThuc': null,
      'noiCuTru': this.data.body.noiCuTru,
      'nguyenQuan': this.data.body.nguyenQuan,
      'id_tinh': this.data.body.id_tinh,
      'id_huyen': this.data.body.id_huyen,
      'id_xa': this.data.body.id_xa,
      'id_donViQuanLy': this.data.body.id_donViQuanLy,
      'noiLamViec': this.data.body.noiLamViec,
      'vuongChinhTri': this.data.body.vuongChinhTri,
      'noiDungVuongChinhTri': this.data.body.noiDungVuongChinhTri,
      'ghiChu': this.data.body.ghiChu,
      'sizeGiay': this.data.body.sizeGiay,
      'sizeAo': this.data.body.sizeAo,
      'sizeMu': this.data.body.sizeMu,
      'id_quanHam': this.data.body.id_quanHam,
      'id_vuKhi': this.data.body.id_vuKhi,
      'truongDaoTao': this.data.body.truongDaoTao,
      'tuNgayDt': null,
      'denNgayDt': null,
    });
    this.f.id_tq_type_congDan.disable();
    if (this.data.body.ngaySinh) {
      this.f.ngaySinh.setValue(dateFormat(this.data.body.ngaySinh, 'yyyy-mm-dd'));
    }
    if (this.data.body.id_tq_type_congDan_tuNgay) {
      this.f.id_tq_type_congDan_tuNgay.setValue(dateFormat(this.data.body.id_tq_type_congDan_tuNgay, 'yyyy-mm-dd'));
    }
    if (this.data.body.id_tq_type_congDan_denNgay) {
      this.f.id_tq_type_congDan_denNgay.setValue(dateFormat(this.data.body.id_tq_type_congDan_denNgay, 'yyyy-mm-dd'));
    }
    if (this.data.body.cmnd_ngayCap) {
      this.f.cmnd_ngayCap.setValue(dateFormat(this.data.body.cmnd_ngayCap, 'yyyy-mm-dd'));
    }
    if (this.data.cmnd_ngayHetHan) {
      this.f.cmnd_ngayHetHan.setValue(dateFormat(this.data.cmnd_ngayHetHan, 'yyyy-mm-dd'));
    }
    if (this.data.body.ngayVaoDangChinhThuc) {
      this.f.ngayVaoDangChinhThuc.setValue(dateFormat(this.data.body.ngayVaoDangChinhThuc, 'yyyy-mm-dd'));
    }
    if (this.data.body.ngayVaoDang) {
      this.f.ngayVaoDang.setValue(dateFormat(this.data.body.ngayVaoDang, 'yyyy-mm-dd'));
    }
    if (this.data.body.ngayVaoDoan) {
      this.f.ngayVaoDoan.setValue(dateFormat(this.data.body.ngayVaoDoan, 'yyyy-mm-dd'));
    }
    if (this.data.body.tuNgayDt) {
      this.f.tuNgayDt.setValue(dateFormat(this.data.body.tuNgayDt, 'yyyy-mm-dd'));
    }
    if (this.data.body.denNgayDt) {
      this.f.denNgayDt.setValue(dateFormat(this.data.body.denNgayDt, 'yyyy-mm-dd'));
    }
    if (this.data.type === 'syQuan') {
      this.f.id_tq_type_congDan.setValue(24);
    }
    this.id_donVis.push(this.data.body.id_donViQuanLy);
  }
  onSubmit() {
    this.f.id_donViQuanLy.setValue(this.id_donVis.toString());
    this.submitted = true;
    // stop here if form is invalid
    if (this.congDanForm.invalid) {
      return;
    }
    if (this.data.type === 'syQuan') {
      this.f.id_tq_type_congDan.setValue(24);
    }
    if (this.f.id_tq_type_congDan_tuNgay.value === '') {
      this.f.id_tq_type_congDan_tuNgay.setValue(null);
    }
    if (this.f.id_tq_type_congDan_denNgay.value === '') {
      this.f.id_tq_type_congDan_denNgay.setValue(null);
    }
    if (this.f.ngayVaoDoan.value === '') {
      this.f.ngayVaoDoan.setValue(null);
    }
    if (this.f.ngayVaoDang.value === '') {
      this.f.ngayVaoDang.setValue(null);
    }
    if (this.f.ngayVaoDangChinhThuc.value === '') {
      this.f.ngayVaoDangChinhThuc.setValue(null);
    }
    if (this.f.cmnd_ngayCap.value === '') {
      this.f.cmnd_ngayCap.setValue(null);
    }
    if (this.f.vuongChinhTri.value === false) {
      this.f.vuongChinhTri.setValue(0);
      this.f.noiDungVuongChinhTri.setValue(null);
    } else {
      this.f.vuongChinhTri.setValue(1);
    }
    if (!this.data.body) {
      this.tuyenQuanService.insertCongDan(this.congDanForm.value, this.common.TQ_CongDan.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.tuyenQuanService.insertCongDanDetail({
              id_congDan: res.data.id,
              tuNgay: this.f.id_tq_type_congDan_tuNgay.value,
              denNgayThongBao: this.f.id_tq_type_congDan_denNgay.value,
              id_type: 1,
              latest: 1,
              id_quanham: this.f.id_quanHam.value,
              id_type_state: this.f.id_tq_type_congDan.value,
              noiDung: this.lyDo
            }, this.common.TQ_CongDan.Insert).subscribe(response => {
              if (response.error) {
                this.common.messageErr(res.error);
              } else {
                this.dialogRef.close(res.message);
              }
            });
          }
        });
    } else {
      this.tuyenQuanService.editCongDan({
        data: this.congDanForm.value,
        condition: { id: this.data.body.id }
      }, this.common.TQ_CongDan.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            if (this.data.type === 'syQuan' && this.data.id_tq_type_congDan !== this.data.body.id_tq_type_congDan) {
              this.tuyenQuanService.updateQuaTrinh({
                congDans: [{
                  id_congDan: this.data.body.id,
                  id_type_state: 24,
                  id_type_beforeState: this.data.body.id_tq_type_congDan,
                  noiDung: null,
                  tuNgay: this.f.tuNgay.value,
                  denNgayThongBao: null,
                  id_dotTuyenQuan: null,
                  id_quanHam: this.f.id_quanHam.value,
                  id_capBac: null,
                  dkDongVien: null,
                  id_donViDbdv: null,
                  id_donViTv: null,
                  typeDqtv: null,
                  id_lucLuong: null,
                  latest: 1
                }]
              }, this.common.TQ_CongDan.Insert).subscribe(response => {
                if (response.error) {
                  this.common.messageErr(response.error);
                } else {
                  this.dialogRef.close(res.message);
                }
              });
            }
          }
        });
    }

  }
  loadCombo() {
    const today = moment().format('YYYY/MM/DD');
    this.donVi.getTreeToValue({ id_trungTam: 1, tuNgay: today, denNgay: today }, this.common.TQ_CongDan.View)
      .subscribe((res: any) => {
        console.log(res);
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.donVis = res.data;
        }
      });
    this.dmService.getTinh({}, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (res.error) {
      } else {
        this.tinhs = res.data;
      }
    });
    this.dmTqService.getTrinhDoHocVan({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.trinhDoChuyenMons = res.data;
      }
    });
    this.dmTqService.getTrinhDoVanHoa({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.trinhDoVanHoas = res.data;
      }
    });
    this.dmTqService.getHocVi({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.hocVis = res.data;
      }
    });
    this.dmService.getDanToc({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.danTocs = res.data;
      }
    });
    this.dmService.getTonGiao({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.tonGiaos = res.data;
      }
    });
    this.dmTqService.getQuanHam({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.quanHams = res.data;
      }
    });
    this.dmTqService.getNganhNgheCmkt({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.nganhNgheCmkts = res.data;
      }
    });
    if (this.data.body) {
      this.loadComboEdit();
    } else {
      this.tuyenQuanService.getLoaiCongDan({ id_workflow_state: '0' }, this.common.TQ_CongDan.Insert).subscribe(res => {
        if (res.error) {
        } else {
          this.typeCongDans = res.data;
        }
      });
    }
  }
  loadComboEdit() {
    this.tuyenQuanService.getCongDanTypeState({}, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (res.error) {
      } else {
        this.typeCongDans = res.data;
      }
    });
    this.getQuanHe();
    this.getQuaTrinh();
  }
  loadHuyen(e) {
    this.f.id_huyen.setValue(null);
    this.f.id_xa.setValue(null);
    this.getHuyen(e);
  }
  loadXa(e) {
    this.f.id_xa.setValue(null);
    this.getXa(e);
  }
  getHuyen(e) {
    this.dmService.getHuyen({ id_tinh: e }, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (res.error) {
      } else {
        this.huyens = res.data;
      }
    });
  }
  getXa(e) {
    this.dmService.getXa({ id_huyen: e }, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (res.error) {
      } else {
        this.xas = res.data;
      }
    });
  }
  getCongDanByCMND() {
    this.tuyenQuanService.getCongDanByCmnd({ cmnd: this.f.cmnd.value }, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (res.error) {
      } else {
        this.data.body = res.data[0];
        this.setValueEdit();
        this.loadComboEdit();
      }
    });
  }
  loadDateAction(e) {
    this.typeCongDanSelection = this.typeCongDans.find(x => x.next_state === e);
    this.lyDo = null;
    if (this.typeCongDanSelection.code === 'dangKy') {
      this.isInputDate = false;
    } else if (this.typeCongDanSelection.code === 'mien') {
      this.isInputDate = false;
      this.loadDmMien();
    } else if (this.typeCongDanSelection.code === 'tamHoan') {
      this.loadDmTamHoan();
      this.isInputDate = true;
      const date = new Date();
      const firstDay = dateFormat(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy-mm-dd');
      const lastDay = dateFormat(new Date(date.getFullYear() + 7, date.getMonth(), 1), 'yyyy-mm-dd');
      this.f.id_tq_type_congDan_tuNgay.setValue(firstDay);
      this.f.id_tq_type_congDan_denNgay.setValue(lastDay);

    } else if (this.typeCongDanSelection.code === 'tamVang') {
      this.isInputDate = true;
      const date = new Date();
      const firstDay = dateFormat(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy-mm-dd');
      const lastDay = dateFormat(new Date(date.getFullYear(), date.getMonth() + 3, 1), 'yyyy-mm-dd');
      this.f.id_tq_type_congDan_tuNgay.setValue(firstDay);
      this.f.id_tq_type_congDan_denNgay.setValue(lastDay);
    }
  }
  loadDmMien() {
    this.dmTqService.getMien({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.miens = res.data;
      }
    });
  }
  loadDmTamHoan() {
    this.dmTqService.getTamHoan({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.tamHoans = res.data;
      }
    });
  }
  choiseLyDo(e) {
    this.lyDo = e;
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  getQuanHe() {
    this.tuyenQuanService.getQuanHe({ id_congDan: this.data.body.id }, this.common.TQ_DotTuyenQuan.View).subscribe(res => {
      if (res.error) {
        this.dataSource = new MatTableDataSource();
      } else {
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
  getQuaTrinh() {
    this.tuyenQuanService.getCongDanDetail({ id_congDan: this.data.body.id }, this.common.TQ_DotTuyenQuan.View).subscribe(res => {
      if (res.error) {
        this.dataSourceQuaTrinh = new MatTableDataSource();
      } else {
        console.log(res.data);
        this.dataSourceQuaTrinh = new MatTableDataSource(res.data);
        // this.dataSourceQuaTrinh.paginator = this.paginator;
        // this.dataSourceQuaTrinh.sort = this.sort;
        res.data.forEach(element => {
          if (element.latest === 1) {
            this.latestDataDetail = element;
            this.data.body.id_tq_type_congDan = element.id_type_state;
          }
        });
      }
    });
  }
  openUpdateDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogUpdateQuanHeComponent, {
      width: '700px',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getQuanHe();
        this.common.messageRes(result);
      }
    });
  }
  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.tuyenQuanService.deleteQuanHe({ id: data.id }, this.common.TQ_DotTuyenQuan.Delete).subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.getQuanHe();
            this.common.messageRes(res.message);
          }
        });
      }
    });
  }
  openDeleteQuaTrinhDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.tenQuaTrinh }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.tuyenQuanService.deleteCongDanDetail({ id: data.id }, this.common.TQ_DotTuyenQuan.Delete).subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.getQuaTrinh();
            // tslint:disable-next-line:triple-equals

            this.common.messageRes(res.message);
          }
        });
      }
    });
  }
  openUpdateStateDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogUpdateLichSuComponent, {
      width: '60%',
      data: { id: this.data.body.id, body: data }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getQuaTrinh();
        this.common.messageRes(result);
      }
    });
  }
  openUpdateTrangThaiDialog(row): void {
    const dialogRef = this.dialog.open(DialogUpdateTrangThaiComponent, {
      width: '60%',
      data: row
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getQuaTrinh();
        this.common.messageRes(result);
      }
    });
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
  public get checkableSettings(): CheckableSettings {
    return {
      checkChildren: true,
      checkParents: false,
      enabled: true,
      mode: 'single',
      checkOnClick: false
    };
  }
  public children = (dataItem: any): Observable<any[]> => of(dataItem.children);
  public hasChildren(dataItem: any): boolean {
    return dataItem.children && dataItem.children.length > 0;
  }
}
