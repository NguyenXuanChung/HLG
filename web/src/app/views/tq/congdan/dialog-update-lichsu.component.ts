import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { TuyenQuanService } from '../../../services/tq.service';
import * as dateFormat from 'dateformat';
import * as moment from 'moment';
import { TqDqtvService } from '../../../services/tq-dqtv.service';


@Component({
  selector: 'app-dialog-update-lichsu',
  templateUrl: './dialog-update-lichsu.component.html',
})
export class DialogUpdateLichSuComponent implements OnInit {
  congDanForm: FormGroup;
  typeCongDans: any[];
  typeCongDanChoises: any[];
  codeTypeState: any;
  submitted = false;
  tamHoans: any[];
  capBacs: any[];
  quanHams: any[];
  donViDbdvs: any[];
  miens: any[];
  // convenience getter for easy access to form fields
  get f() { return this.congDanForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private tuyenQuanService: TuyenQuanService,
    private dmTqService: TqDqtvService,
    public dialogRef: MatDialogRef<DialogUpdateLichSuComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.loadCombo();
    console.log(this.data);
  }
  createForm() {
    this.congDanForm = this.fb.group(
      {
        id_congDan: this.data.id,
        id_type_state: [null],
        id_type_beforeState: [null],
        noiDung: [null],
        tuNgay: [null],
        denNgayThongBao: [null],
        id_dotTuyenQuan: [null],
        id_quanHam: [null],
        id_capBac: [null],
        dkDongVien: [null],
        id_donViDbdv: [null],
        id_donViTv: [null],
        typeDqtv: [null],
        id_lucLuong: [null],
        latest: [0, [Validators.required]]
      });
    if (this.data.body) {
      this.congDanForm.setValue({
        id_congDan: this.data.id,
        id_type_state: null,
        id_type_beforeState: this.data.body.id_type_state,
        noiDung: null,
        tuNgay: null,
        denNgayThongBao: null,
        id_dotTuyenQuan: this.data.body.id_dotTuyenQuan,
        id_quanHam: this.data.body.id_quanHam,
        id_capBac: this.data.body.id_capBac,
        dkDongVien: this.data.body.dkDongVien,
        id_donViDbdv: this.data.body.id_donViDbdv,
        id_donViTv: null,
        typeDqtv: null,
        id_lucLuong: null,
        latest: 1
      });
      this.loadTypeCongDanChoise();
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.congDanForm.invalid) {
      return;
    }
    this.tuyenQuanService.updateQuaTrinh({
      congDans: [this.congDanForm.value]
    }, this.common.TQ_CongDan.Insert).subscribe(response => {
      if (response.error) {
        this.common.messageErr(response.error);
      } else {
        this.dialogRef.close(response.message);
      }
    });
  }
  loadCombo() {
    this.tuyenQuanService.getCongDanTypeState({
      checkQt: 1, id_congDan_type: 1
    }, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.typeCongDans = res.data;
      }
    });
  }
  loadTypeCongDanChoise() {
    this.tuyenQuanService.getLoaiCongDan({
      id_workflow_state: this.data.body.id_type_state
    }, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.typeCongDanChoises = res.data;
      }
    });
  }
  loadStateAction(e) {
    const date = new Date();
    const firstDay = dateFormat(new Date(date.getFullYear(), date.getMonth(), 1), 'yyyy-mm-dd');
    this.f.tuNgay.setValue(firstDay);
    this.codeTypeState = this.typeCongDans.find(x => x.id === e).code;
    this.f.denNgayThongBao.setValue(null);
    if (this.codeTypeState === 'DKNN') {
    } else if (this.codeTypeState === 'MIEN') {
      this.loadDmMien();
    } else if (this.codeTypeState === 'NHAPNGU') {
      this.loadDonVi();
    } else if (this.codeTypeState === 'XUATNGU') {
      this.loadDonVi();
      this.loadQuanHam();
      this.loadCapBac();
    } else if (this.codeTypeState === 'HOAN') {
      this.loadDmTamHoan();
      const lastDay = dateFormat(new Date(date.getFullYear() + 7, date.getMonth(), 1), 'yyyy-mm-dd');
      this.f.denNgayThongBao.setValue(lastDay);
    } else if (this.codeTypeState === 'VANG') {
      const lastDay = dateFormat(new Date(date.getFullYear(), date.getMonth() + 3, 1), 'yyyy-mm-dd');
      this.f.denNgayThongBao.setValue(lastDay);
    } else if (this.codeTypeState === 'DICHUYEN') {
      this.loadDmDiChuyen();
    } else if (this.codeTypeState === 'DBDV1') {
      this.loadQuanHam();
    } else if (this.codeTypeState === 'DBDV2') {
      this.loadQuanHam();
    } else if (this.codeTypeState === 'BCDBDV1') {
      this.loadCapBac();
      this.loadDonVi();
      this.loadQuanHam();
    } else if (this.codeTypeState === 'BCDBDV2') {
      this.loadCapBac();
      this.loadDonVi();
      this.loadQuanHam();
    } else if (this.codeTypeState === 'GNDBDV1') {
      this.loadQuanHam();
      this.loadCapBac();
      this.loadDonVi();
    } else if (this.codeTypeState === 'GNDBDV2') {
      this.loadQuanHam();
      this.loadCapBac();
      this.loadDonVi();
    } else if (this.codeTypeState === 'SQDB') {
      this.loadQuanHam();
    } else if (this.codeTypeState === 'BCSQDB') {
      this.loadCapBac();
      this.loadDonVi();
      this.loadQuanHam();
    } else if (this.codeTypeState === 'GNSQDB') {
      this.loadQuanHam();
      this.loadCapBac();
      this.loadDonVi();
    }
  }
  loadDmMien() {
    this.dmTqService.getMien({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.miens = res.data;
      }
    });
  }
  loadDmDiChuyen() {
    this.dmTqService.getMien({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.miens = res.data;
      }
    });
  }
  loadDmTamHoan() {
    this.dmTqService.getTamHoan({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.tamHoans = res.data;
      }
    });
  }
  loadCapBac() {
    this.dmTqService.getCapBac({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.capBacs = res.data;
      }
    });

  }
  loadQuanHam() {
    this.dmTqService.getQuanHam({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.quanHams = res.data;
      }
    });
  }
  loadDonVi() {
    this.dmTqService.getDonViDbdv({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.donViDbdvs = res.data;
      }
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
