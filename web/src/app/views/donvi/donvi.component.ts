import { Component, OnInit, ViewChild } from '@angular/core';
// Translate
import { CommonService } from '../../services/common.service';
import { DonViService } from './../../services/don-vi.service';
import { StorageService } from '../../services/storage.service';
import { AuthConstants } from '../../config/auth-constants';
import { Router } from '@angular/router';
import { TreeNode } from 'primeng/api/treenode';

@Component({
  selector: 'app-donvi',
  templateUrl: './donvi.component.html',
  styleUrls: ['./donvi.component.css']
})
export class DonviComponent implements OnInit {
  dsDonVis: TreeNode[];
  colsDonVi: any[];
  permission: any;
  loadTable1: boolean = true;

  constructor(
    private common: CommonService,
    private donViService: DonViService,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  ngOnInit(): void {
    this.loadTable1 = false;
    this.colsDonVi = [
      { field: 'tenDonVi', header: 'Name' },
      { field: 'maDonVi', header: 'Code' },
      { field: 'ngayBatDau', header: 'Start date' },
      { field: 'tuNgay', header: 'From date' }
    ];
    this.onSearch();
    if (!this.permission.DonVi_CoCauToChuc._view) {
      this.router.navigate(['/']);
    }
  }

  onSearch() {
    this.getDonVi();
  }

  getDonVi() {
    this.donViService.getTreeNode({}, this.common.DonVi_CoCauToChuc.View).subscribe(res => {
      this.loadTable1 = true;
      if (res.error) {
        this.common.messageErr(res);
        this.dsDonVis = [];
      } else {
        this.dsDonVis = res.data;
      }
    });
  }
}
