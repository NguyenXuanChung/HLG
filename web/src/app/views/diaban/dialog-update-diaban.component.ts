import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../services/common.service';
import { ConfigurationService } from '../../services/configuration.service';
import { DiaBanService } from '../../services/diaban.service';

@Component({
  selector: 'app-dialog-update-diaban',
  templateUrl: './dialog-update-diaban.component.html',
})
export class DialogUpdateDiaBanComponent implements OnInit {
  diaBanForm: FormGroup;
  configs: any[];
  diaBans: any[];
  tinhs: any[];
  huyens: any[];
  xas: any[];
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.diaBanForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private configService: ConfigurationService,
    private diaBanService: DiaBanService,
    public dialogRef: MatDialogRef<DialogUpdateDiaBanComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.loadCombo();
  }
  createForm() {
    this.diaBanForm = this.fb.group(
      {
        name: ['', [Validators.required]],
        code: ['', [Validators.required]],
        id_dm_tinh: ['', [Validators.required]],
        id_dm_huyen: ['', [Validators.required]],
        id_dm_xa: ['', [Validators.required]],
        description: [''],
        sortOrder: [0, [Validators.required, Validators.min(0)]],
      });
    if (this.data) {
      this.diaBanForm.setValue({
        'name': this.data.name,
        'code': this.data.code,
        'id_dm_tinh': this.data.id_dm_tinh,
        'id_dm_huyen': this.data.id_dm_huyen,
        'id_dm_xa': this.data.id_dm_xa,
        'description': this.data.description,
        'sortOrder': this.data.sortOrder,
      });
      this.onSelectTinh(this.data.id_dm_tinh);
      this.onSelectHuyen(this.data.id_dm_huyen);
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.diaBanForm.invalid) {
      return;
    }
    if (!this.data) {
      this.diaBanService.insertDiaBan(this.diaBanForm.value, this.common.DiaBan.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.diaBanService.editDiaBan({
        data: this.diaBanForm.value,
        condition: { id: this.data.id }
      }, this.common.DiaBan.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  loadCombo() {
    this.diaBanService.getDMTinh({}, this.common.DiaBan.Insert).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.tinhs = res.data;
      }
    });
  }
  onSelectTinh(data: any) {
    this.diaBanService.getDMHuyen({id_tinh: data}, this.common.DiaBan.Insert).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.huyens = res.data;
      }
    });
  }
  onSelectHuyen(data: any) {
    this.diaBanService.getDMXa({id_huyen: data}, this.common.DiaBan.Insert).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.xas = res.data;
      }
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
