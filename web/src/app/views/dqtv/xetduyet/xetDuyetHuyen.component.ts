import { TuyenQuanService } from './../../../services/tq.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';

import * as moment from 'moment';
import { map, startWith, isEmpty } from 'rxjs/operators';

// Translate
import { CommonService } from './../../../services/common.service';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';
import { Router } from '@angular/router';
import { CheckableSettings } from '@progress/kendo-angular-treeview';
import { Observable, of } from 'rxjs';
import { DialogUpdateCongDanComponent } from '../congdan/dialog-update-congdan.component';
import { DialogUpdateXdComponent } from './dialog-update-xd.component';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}
@Component({
  selector: 'app-xetduyethuyen',
  templateUrl: './xetduyethuyen.component.html'
})
export class XetDuyetHuyenComponent implements OnInit {
  displayedColumns = ['index', 'hoTen', 'cmnd', 'ngaySinh', 'id_trinhDoVanHoa',
    'noiCuTru', 'id_donViQuanLy', 'noiDung', 'actions'];
  dataSource: MatTableDataSource<any>;
  typeStateCongDans: any[];
  dotTuyenQuans: any[];
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  searchForm: FormGroup;
  permission: any;
  checkArray: any[] = [];
  get fs() { return this.searchForm.controls; }
  form: FormGroup;
  obj: any = [];
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private tuyenQuanService: TuyenQuanService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  ngOnInit() {
    this.createSearchForm();
    // this.onSearch();
    if (!this.permission.TQ_CongDan._view) {
      this.router.navigate(['/']);
    }
    this.loadCombo();
  }

  createSearchForm() {
    const today = moment().format('YYYY-MM-DD');

    this.searchForm = this.fb.group(
      {
        id_dotTuyenQuan: [0],
        id_tq_type_congDan: 6,
        ngay: [today, [Validators.required]],
      });
  }

  onSearch() {
    this.getCongDan();
  }

  getCongDan() {
    this.obj = [];
    this.tuyenQuanService.getCongDanXd(this.searchForm.value, this.common.TQ_CongDan.View).subscribe(res => {
      if (res.error) {
        this.dataSource = new MatTableDataSource();
      } else {
        res.data.forEach(element => {
          if (!element.noiDung) {
            this.obj.push({ checked: false, ...element });
          } else {
            this.obj.push(element);
          }
        });
        this.dataSource = new MatTableDataSource(this.obj);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  openDetailDialog(data: any): void {
    // const dialogRef = this.dialog.open(DialogDetailGiamSatComponent, {
    //   height: 'auto',
    //   width: '70%',
    //   data: data
    // });
    // dialogRef.afterClosed().subscribe(result => {
    // });
  }
  openUpdateDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogUpdateCongDanComponent, {
      height: '80%',
      width: '70%',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openUpdateXdDialog(): void {
    this.checkArray = [];
    this.obj.forEach(element => {
      if (element.checked === true) {
        this.checkArray.push(element.id);
      }
    });
    if (this.checkArray.length === 0) {
      this.common.messageErr({ error: { message: 'choiseDevice' } });
      return;
    }
    const tenDotTuyenQuan = this.dotTuyenQuans.find(x => x.id === this.fs.id_dotTuyenQuan.value);

    const dialogRef = this.dialog.open(DialogUpdateXdComponent, {
      width: '350px',
      data: {
        tenDotTuyenQuan: tenDotTuyenQuan.name,
        id_dotTuyenQuan: tenDotTuyenQuan.id,
        id_tq_type_congDan: 6,
        id_congDans: this.checkArray.toString(),
        ngay: this.fs.ngay.value
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openInsertListDialog(): void {
    // const dialogRef = this.dialog.open(DialogUpdateDeviceListComponent, {
    // height: '80%',
    // width: '70%',
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {
    //     this.onSearch();
    //     this.common.messageRes(result);
    //   }
    // });
  }
  openConfigDialog(): void {
    this.checkArray = [];
    this.obj.forEach(element => {
      if (element.checked === true) {
        this.checkArray.push(element.id);
      }
    });
    if (this.checkArray.length === 0) {
      this.common.messageErr({ error: { message: 'tq.validate.chonCongDan' } });
      return;
    } else {
      // const dialogRef = this.dialog.open(DialogUpdateDeviceConfigComponent, {
      //   height: 'auto',
      //   width: '70%',
      //   data: { id_devices: this.checkArray.toString() }
      // });
      // dialogRef.afterClosed().subscribe(result => {
      //   if (result) {
      //     this.onSearch();
      //     this.common.messageRes(result);
      //   }
      // });
    }
  }
  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.hoTen }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        // this.deviceService.deleteGiamSatDieuDuong({ id: data.id }, this.common.Device.Delete)
        //   .subscribe(res => {
        //     if (res.error) {
        //       this.common.messageErr(res);
        //     } else {
        //       this.onSearch();
        //       this.common.messageRes(res.message);
        //     }
        //   });
      }
    });
  }
  checkAll(data: any) {
    if (data === true) {
      this.obj.forEach(element => {
        if (!element.noiDung) {
          element.checked = true;
        }

      });
    } else {
      this.obj.forEach(element => {
        if (!element.noiDung) {
          element.checked = false;
        }
      });
    }
  }

  loadCombo() {
    this.tuyenQuanService.getCongDanTypeState({ id_congDan_type: 1, view: 1 }, this.common.TQ_CongDan.View).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.typeStateCongDans = res.data;
      }
    });
    const today = moment().format('YYYY/MM/DD');
    this.tuyenQuanService.getDotTuyenQuanByDate({ ngay: today }, this.common.TQ_CongDan.View).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.dotTuyenQuans = res.data;
        this.fs.id_dotTuyenQuan.setValue(res.data[res.data.length - 1].id);
        this.onSearch();
      }
    });
  }
  public get checkableSettings(): CheckableSettings {
    return {
      checkChildren: true,
      checkParents: false,
      enabled: true,
      mode: 'multiple',
      checkOnClick: false
    };
  }
  public children = (dataItem: any): Observable<any[]> => of(dataItem.children);
  public hasChildren(dataItem: any): boolean {
    return dataItem.children && dataItem.children.length > 0;
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
