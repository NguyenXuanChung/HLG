import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { TuyenQuanService } from '../../../services/tq.service';
import * as dateFormat from 'dateformat';
import * as moment from 'moment';


@Component({
  selector: 'app-dialog-update-nn',
  templateUrl: './dialog-update-nn.component.html',
})
export class DialogUpdateNnComponent implements OnInit {
  congDanForm: FormGroup;
  configs: any[];
  diaBans: any[];
  groups: any[];
  typeCongDans: any[];
  typeCongDanSelection: any;
  submitted = false;
  isInputDate = false;
  // convenience getter for easy access to form fields
  get f() { return this.congDanForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private tuyenQuanService: TuyenQuanService,
    public dialogRef: MatDialogRef<DialogUpdateNnComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.loadCombo();
  }
  createForm() {
    this.congDanForm = this.fb.group(
      {
        id_congDans: this.data.id_congDans,
        id_tq_type_congDan: ['', [Validators.required]],
        id_dotTuyenQuan: this.data.id_dotTuyenQuan,
        id_tq_type_congDan_before: this.data.id_tq_type_congDan,
        nameSelect: [''],
        tuNgay: this.data.ngay,
        denNgay: this.data.ngay
      });
  }
  onSubmit() {
    this.typeCongDanSelection = this.typeCongDans.find(x => x.next_state === this.f.id_tq_type_congDan.value);
    this.f.nameSelect.setValue(this.typeCongDanSelection.name);
    this.submitted = true;
    // stop here if form is invalid
    if (this.congDanForm.invalid) {
      return;
    }
    this.tuyenQuanService.updateCongDanNn(this.congDanForm.value, this.common.TQ_CongDan.Insert)
      .subscribe(res => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.dialogRef.close(res.message);
        }
      });
  }
  loadCombo() {
    this.tuyenQuanService.getLoaiCongDan({
      id_workflow_state: this.data.id_tq_type_congDan
    }, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.typeCongDans = res.data;
      }
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
