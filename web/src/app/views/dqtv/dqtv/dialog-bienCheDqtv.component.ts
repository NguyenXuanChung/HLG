import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { TuyenQuanService } from '../../../services/tq.service';
import * as dateFormat from 'dateformat';
import * as moment from 'moment';
import { TqDqtvService } from '../../../services/tq-dqtv.service';


@Component({
  selector: 'app-dialog-bienche-dqtv',
  templateUrl: './dialog-bienCheDqtv.component.html',
})
export class DialogBienCheDqtvComponent implements OnInit {
  submitted = false;
  isInputDate = false;
  // convenience getter for easy access to form fields
  lucLuongs: any[];
  phanLoaiLucLuongs: any[];
  donViTvs: any[];
  capBacs: any[];
  quanHams: any[];
  tqForm: FormGroup;
  congDans: any[] = [];
  typeStateCongDans: any[];
  // convenience getter for easy access to form fields
  get f() { return this.tqForm.controls; }

  constructor(
    private common: CommonService,
    private tuyenQuanService: TuyenQuanService,
    private tqDqtvService: TqDqtvService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogBienCheDqtvComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.loadCombo();
    this.createForm();
    const today = moment().format('YYYY/MM/DD');
     this.tuyenQuanService.getCongDanTypeState({ id_congDan_type: 1 }, this.common.TQ_CongDan.View).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.typeStateCongDans = res.data;
      }
    });
  }

  createForm() {
    this.tqForm = this.fb.group(
      {
        id_congDans: this.data.id_congDans,
        noiDung: [null],
        id_type_state: this.data.id_type_state,
        typeDqtv: [null, [Validators.required]],
        id_quanHam: [null, [Validators.required]],
        id_capBac: [null, [Validators.required]],
        id_donViTv: null,
        id_lucLuong: null,
        id_type_beforeState: this.data.id_type_beforeState,
        tuNgay: [moment().format('YYYY-MM-DD')],
        denNgay: [null],
      });
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.tqForm.invalid) {
      return;
    }
    // const noiDung = this.typeStateCongDans.find(x => x.id === this.f.id_type_beforeState.value);
    // this.f.noiDung.setValue('Từ' + noiDung.name);
    this.data.id_congDans.split(',').forEach(element => {
      this.congDans.push(
        {
          id_congDan: element,
          id_type_state: this.f.id_type_state.value,
          id_type_beforeState: this.f.id_type_beforeState.value,
          noiDung: this.f.noiDung.value,
          tuNgay: this.f.tuNgay.value,
          denNgayThongBao: null,
          id_dotTuyenQuan: null,
          id_quanHam: this.f.id_quanHam.value,
          id_capBac: this.f.id_capBac.value,
          dkDongVien: null,
          id_donViDbdv: null,
          id_donViTv: this.f.id_donViTv.value,
          typeDqtv: this.f.typeDqtv.value,
          id_lucLuong: this.f.id_lucLuong.value,
          latest: 1
        }
      );
    });
    this.tuyenQuanService.updateQuaTrinh({ congDans: this.congDans }
      , this.common.TQ_CongDan.Update)
      .subscribe(res => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.dialogRef.close(res.message);
        }
      });
  }
  loadCombo() {
    this.tqDqtvService.getQuanHam({}, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (!res.error) {
        this.quanHams = res.data;
      }
    });
    this.tqDqtvService.getCapBac({}, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (!res.error) {        console.log(res);

        this.capBacs = res.data;
      }
    });
    this.tqDqtvService.getDonViTv({}, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (!res.error) {
        this.donViTvs = res.data;
      }
    });
    this.tqDqtvService.getPhanLoaiLucLuong({}, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (!res.error) {
        this.phanLoaiLucLuongs = res.data;
      }
    });
    this.tqDqtvService.getLucLuong({}, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (!res.error) {
        this.lucLuongs = res.data;
      }
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
