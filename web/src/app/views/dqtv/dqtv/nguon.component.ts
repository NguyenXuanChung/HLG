import { DqtvService } from './../../../services/dqtv.service';
import { TuyenQuanService } from './../../../services/tq.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import * as dateFormat from 'dateformat';

import * as moment from 'moment';
import { map, startWith, isEmpty } from 'rxjs/operators';

// Translate
import { CommonService } from './../../../services/common.service';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';
import { Router } from '@angular/router';
import { CheckableSettings } from '@progress/kendo-angular-treeview';
import { Observable, of } from 'rxjs';
import { DonViService } from '../../../services/don-vi.service';
import { DialogUpdateCongDanComponent } from '../../tq/congdan/dialog-update-congdan.component';
import { DialogDkDqtvComponent } from './dialog-dkDqtv.component';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}
@Component({
  selector: 'app-nguon',
  templateUrl: './nguon.component.html'
})
export class NguonComponent implements OnInit {
  displayedColumns = ['index', 'hoTen', 'cmnd', 'ngaySinh', 'id_trinhDoVanHoa',
    'id_tq_type_congDan', 'id_donViQuanLy', 'actions'];
  dataSource: MatTableDataSource<any>;
  typeStateCongDans: any[];
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  searchForm: FormGroup;
  permission: any;
  public id_types: any[] = [];
  public id_donVis: any[] = [];
  donVis: any[];
  checkArray: any[] = [];
  get fs() { return this.searchForm.controls; }
  form: FormGroup;
  obj: any = [];
  numbers: any[] = ['1', '2'];
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private dqtvService: DqtvService,
    public dialog: MatDialog,
    private donVi: DonViService,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  ngOnInit() {
    this.createSearchForm();
    this.onSearch();
    if (!this.permission.TQ_CongDan._view) {
      this.router.navigate(['/']);
    }
    this.loadCombo();
  }

  createSearchForm() {
    const date = new Date();
    const today = moment().format('YYYY-MM-DD');
    const firstDay = dateFormat(new Date(date.getFullYear(), 0, 1), 'yyyy-mm-dd');
    const lastDay = dateFormat(new Date(date.getFullYear(), 11, 31), 'yyyy-mm-dd');
    this.searchForm = this.fb.group(
      {
        id_tq_type_congDan: null,
        doiTuong: '1',
        id_donViQuanLys: null,
        gioiTinh: null,
        tuTuoi: 18,
        denTuoi: 90,
        content: null,
        tuNgay: [firstDay, [Validators.required]],
        denNgay: [lastDay, [Validators.required]]
      });
  }

  onSearch() {
    this.fs.id_donViQuanLys.setValue(this.id_donVis.toString());
    if (this.fs.doiTuong.value === '1') {
      this.fs.gioiTinh.setValue(null);
      this.fs.id_tq_type_congDan.setValue(18);
      this.fs.denTuoi.setValue(90);
    } else if (this.fs.doiTuong.value === '2') {
      this.fs.gioiTinh.setValue(0);
      this.fs.id_tq_type_congDan.setValue(1);
      this.fs.denTuoi.setValue(40);
    }
    this.getCongDan();
  }

  getCongDan() {
    this.obj = [];
    this.dqtvService.getListDqtv(this.searchForm.value, this.common.TQ_CongDan.View).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource = new MatTableDataSource();
      } else {
        res.data.forEach(element => {
          this.obj.push({ checked: false, ...element });
        });
        this.dataSource = new MatTableDataSource(this.obj);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
  openDetailDialog(data: any): void {
    // const dialogRef = this.dialog.open(DialogDetailGiamSatComponent, {
    //   height: 'auto',
    //   width: '70%',
    //   data: data
    // });
    // dialogRef.afterClosed().subscribe(result => {
    // });
  }
  checkAll(data: any) {
    if (data === true) {
      this.obj.forEach(element => {
        element.checked = true;
      });
    } else {
      this.obj.forEach(element => {
        element.checked = false;
      });
    }
  }

  openUpdateDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogUpdateCongDanComponent, {
      height: '80%',
      width: '70%',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openInsertListDialog(): void {
    // const dialogRef = this.dialog.open(DialogUpdateDeviceListComponent, {
    // height: '80%',
    // width: '70%',
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {
    //     this.onSearch();
    //     this.common.messageRes(result);
    //   }
    // });
  }
  openDkDqtvDialog(data: number): void {
    this.checkArray = [];
    this.obj.forEach(element => {
      if (element.checked === true) {
        this.checkArray.push(element.id);
      }
    });
    if (this.checkArray.length === 0) {
      this.common.messageErr({ error: { message: 'tq.validate.chonCongDan' } });
      return;
    }
    const dialogRef = this.dialog.open(DialogDkDqtvComponent, {
      width: '350px',
      data: {
        id_type_beforeState: this.fs.id_tq_type_congDan.value,
        id_congDans: this.checkArray.toString(),
        id_type_state: data
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.hoTen }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.dqtvService.deleteDqtv({ id: data.id }, this.common.TQ_CongDan.Delete)
          .subscribe(res => {
            if (res.error) {
              this.common.messageErr(res);
            } else {
              this.onSearch();
              this.common.messageRes(res.message);
            }
          });
      }
    });
  }
  loadCombo() {
    const today = moment().format('YYYY/MM/DD');
    this.donVi.getTreeToValue({ id_trungTam: 1, tuNgay: today, denNgay: today }, this.common.TQ_CongDan.View)
      .subscribe((res: any) => {
        console.log(res);
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.donVis = res.data;
        }
      });
  }
  public get checkableSettings(): CheckableSettings {
    return {
      checkChildren: true,
      checkParents: false,
      enabled: true,
      mode: 'multiple',
      checkOnClick: false
    };
  }
  public children = (dataItem: any): Observable<any[]> => of(dataItem.children);
  public hasChildren(dataItem: any): boolean {
    return dataItem.children && dataItem.children.length > 0;
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
