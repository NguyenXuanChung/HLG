import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { TuyenQuanService } from '../../../services/tq.service';
import * as dateFormat from 'dateformat';
import * as moment from 'moment';


@Component({
  selector: 'app-dialog-dk-dqtv',
  templateUrl: './dialog-dkDqtv.component.html',
})
export class DialogDkDqtvComponent implements OnInit {
  submitted = false;
  isInputDate = false;
  // convenience getter for easy access to form fields

  tqForm: FormGroup;
  congDans: any[] = [];
  typeStateCongDans: any[];
  // convenience getter for easy access to form fields
  get f() { return this.tqForm.controls; }

  constructor(
    private common: CommonService,
    private tuyenQuanService: TuyenQuanService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogDkDqtvComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    const today = moment().format('YYYY/MM/DD');
     this.tuyenQuanService.getCongDanTypeState({ id_congDan_type: 1 }, this.common.TQ_CongDan.View).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.typeStateCongDans = res.data;
      }
    });
  }

  createForm() {
    this.tqForm = this.fb.group(
      {
        id_congDans: [this.data.id_congDans],
        noiDung: [null],
        id_type_state: this.data.id_type_state,
        id_type_beforeState: this.data.id_type_beforeState,
        tuNgay: [moment().format('YYYY-MM-DD')],
        denNgay: [null],
      });
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.tqForm.invalid) {
      return;
    }
    const noiDung = this.typeStateCongDans.find(x => x.id === this.f.id_type_beforeState.value);
    this.f.noiDung.setValue('Từ' + noiDung.name);
    this.data.id_congDans.split(',').forEach(element => {
      this.congDans.push(
        {
          id_congDan: element,
          id_type_state: this.f.id_type_state.value,
          id_type_beforeState: this.f.id_type_beforeState.value,
          noiDung: this.f.noiDung.value,
          tuNgay: this.f.tuNgay.value,
          denNgayThongBao: null,
          id_dotTuyenQuan: null,
          id_quanHam: null,
          id_capBac: null,
          dkDongVien: null,
          id_donViDbdv: null,
          id_donViTv: null,
          typeDqtv: null,
          id_lucLuong: null,
          latest: 1
        }
      );
    });


    this.tuyenQuanService.updateQuaTrinh({ congDans: this.congDans }
      , this.common.TQ_CongDan.Update)
      .subscribe(res => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.dialogRef.close(res.message);
        }
      });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
