import { DonViService } from './../../../services/don-vi.service';
import { TuyenQuanService } from './../../../services/tq.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';

import * as moment from 'moment';
import { map, startWith, isEmpty } from 'rxjs/operators';

// Translate
import { CommonService } from './../../../services/common.service';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';
import { Router } from '@angular/router';
import { CheckableSettings } from '@progress/kendo-angular-treeview';
import { Observable, of } from 'rxjs';
import * as dateFormat from 'dateformat';
import { TqDqtvService } from '../../../services/tq-dqtv.service';
import { DqtvService } from '../../../services/dqtv.service';
import { DialogUpdateCongDanComponent } from '../../tq/congdan/dialog-update-congdan.component';
import { DialogBienCheDqtvComponent } from './dialog-bienCheDqtv.component';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}
@Component({
  selector: 'app-qltv',
  templateUrl: './qltv.component.html'
})
export class QltvComponent implements OnInit {
  displayedColumns = ['index', 'hoTen', 'cmnd', 'ngaySinh',
     'ngayBienChe', 'id_capBac', 'id_quanHam', 'id_lucLuong', 'id_donViQuanLy', 'actions'];
  dataSource: MatTableDataSource<any>;
  typeStateCongDans: any[];
  phanLoaiLucLuongs: any[];
  lucLuongs: any[] = [];
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  searchForm: FormGroup;
  permission: any;
  checkArray: any[] = [];
  donVis: any[] = [];
  donViTvs: any[] = [];
  public id_donViTvs: any[] = [];
  public id_donVis: any[] = [];
  public id_lucLuongs: any[] = [];

  get fs() { return this.searchForm.controls; }
  form: FormGroup;
  obj: any = [];
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private tuyenQuanService: TuyenQuanService,
    public dialog: MatDialog,
    private router: Router,
    private donVi: DonViService,
    private storageService: StorageService,
    private dmTqService: TqDqtvService,
    private dqtvService: DqtvService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  ngOnInit() {
    this.createSearchForm();
    if (!this.permission.TQ_CongDan._view) {
      this.router.navigate(['/']);
    }
    this.loadCombo();
    this.onSearch();
  }

  createSearchForm() {
    const date = new Date();
    const today = moment().format('YYYY-MM-DD');
    const firstDay = dateFormat(new Date(date.getFullYear(), 0, 1), 'yyyy-mm-dd');
    const lastDay = dateFormat(new Date(date.getFullYear(), 11, 31), 'yyyy-mm-dd');
    this.searchForm = this.fb.group(
      {
        id_tq_type_congDan: 103,
        id_donViQuanLys: null,
        id_donViTvs: null,
        id_lucLuongs: null,
        id_phanLoaiLucLuong: null,
        content: '',
        ngay: [firstDay, [Validators.required]],
      });
  }

  onSearch() {
    this.fs.id_donViQuanLys.setValue(this.id_donVis.toString());
    this.fs.id_donViQuanLys.setValue(this.id_donVis.toString());
    this.fs.id_donViQuanLys.setValue(this.id_donVis.toString());

    this.getCongDan();
  }

  getCongDan() {
    this.obj = [];
    this.dqtvService.getBienChe(this.searchForm.value, this.common.TQ_CongDan.View).subscribe(res => {
      if (res.error) {
        this.dataSource = new MatTableDataSource();
      } else {
        res.data.forEach(element => {
          this.obj.push({ checked: false, ...element });
        });
        this.dataSource = new MatTableDataSource(this.obj);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  openDetailDialog(data: any): void {
    // const dialogRef = this.dialog.open(DialogDetailGiamSatComponent, {
    //   height: 'auto',
    //   width: '70%',
    //   data: data
    // });
    // dialogRef.afterClosed().subscribe(result => {
    // });
  }
  openUpdateDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogUpdateCongDanComponent, {
      height: '80%',
      width: '70%',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  // openUpdateBienCheDialog(): void {
  //   this.checkArray = [];
  //   this.obj.forEach(element => {
  //     if (element.checked === true) {
  //       this.checkArray.push(element);
  //     }
  //   });
  //   if (this.checkArray.length === 0) {
  //     this.common.messageErr({ error: { message: 'tq.validate.chonCongDan' } });
  //     return;
  //   }
  //   const dialogRef = this.dialog.open(DialogBienCheDqtvComponent, {
  //     width: '350px',
  //     data: {
  //       tuNgay: this.fs.ngay.value,
  //       denNgay: this.fs.ngay.value,
  //       id_type_bienChe: 20,
  //       congDans: this.checkArray,
  //       id_type_state: 17,
  //       id_quanHam: this.fs.id_tq_type_congDan.value,
  //     }
  //   });
  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result) {
  //       this.onSearch();
  //       this.common.messageRes(result);
  //     }
  //   });
  // }
  openInsertListDialog(): void {
    // const dialogRef = this.dialog.open(DialogUpdateDeviceListComponent, {
    // height: '80%',
    // width: '70%',
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {
    //     this.onSearch();
    //     this.common.messageRes(result);
    //   }
    // });
  }
  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.hoTen }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.tuyenQuanService.deleteCongDan({ id: data.id }, this.common.TQ_CongDan.Delete)
          .subscribe(res => {
            if (res.error) {
              this.common.messageErr(res);
            } else {
              this.onSearch();
              this.common.messageRes(res.message);
            }
          });
      }
    });
  }
  checkAll(data: any) {
    console.log(data);
    if (data === true) {
      this.obj.forEach(element => {
        element.checked = true;
      });
    } else {
      this.obj.forEach(element => {
        element.checked = false;
      });
    }
  }

  loadCombo() {
    this.tuyenQuanService.getCongDanTypeState({ id_congDan_type: 2, start: 1 }, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.typeStateCongDans = res.data;
      }
    });
    this.dmTqService.getPhanLoaiLucLuong({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.phanLoaiLucLuongs = res.data;
      }
    });
    this.dmTqService.getLucLuong({}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.lucLuongs = res.data;
      }
    });
    const today = moment().format('YYYY/MM/DD');
    this.donVi.getTreeToValue({ id_trungTam: 1, tuNgay: today, denNgay: today }, this.common.TQ_CongDan.View)
      .subscribe((res: any) => {
        console.log(res);
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.donVis = res.data;
        }
      });
  }
  loadLucLuong(e: number) {
    this.id_lucLuongs = [];
    this.dmTqService.getLucLuong({id_phanLoaiLucLuong: e}, this.common.TQ_CongDan.View).subscribe(res => {
      if (!res.error) {
        this.lucLuongs = res.data;
      }
    });
  }
  public get checkableSettings(): CheckableSettings {
    return {
      checkChildren: true,
      checkParents: false,
      enabled: true,
      mode: 'multiple',
      checkOnClick: false
    };
  }
  public children = (dataItem: any): Observable<any[]> => of(dataItem.children);
  public hasChildren(dataItem: any): boolean {
    return dataItem.children && dataItem.children.length > 0;
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
