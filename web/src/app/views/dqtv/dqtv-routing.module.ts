import { NguonComponent } from './dqtv/nguon.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { XdhnComponent } from './dqtv/xdhn.component';
import { SsmrComponent } from './dqtv/ssmr.component';
import { BcDqtvComponent } from './dqtv/bcdqtv.component';
import { QltvComponent } from './dqtv/qltv.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'xdhn'
      },
      {
        path: 'nguon',
        component: NguonComponent,
      },
      {
        path: 'xdhn',
        component: XdhnComponent,
      },
      {
        path: 'ssmr',
        component: SsmrComponent,
      },
      {
        path: 'bcdqtv',
        component: BcDqtvComponent,
      },
      {
        path: 'qltv',
        component: QltvComponent,
      },
      {
        path: 'tctg',
        component: QltvComponent,
      },
      {
        path: 'htdqtv',
        component: QltvComponent,
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DqtvRoutingModule { }
