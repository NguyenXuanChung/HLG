import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { CheckableSettings } from '@progress/kendo-angular-treeview';

import * as moment from 'moment';

// Translate
import { CommonService } from './../../../services/common.service';
import { Observable, of } from 'rxjs';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';
import { Router } from '@angular/router';
import { QltbService } from '../../../services/qltb.service';
import { DialogUpdateThongBaoComponent } from './dialog-update-thongbao.component';
import { DialogUpdateDuyetComponent } from './dialog-update-duyet.component';

@Component({
  selector: 'app-thongbao',
  templateUrl: './thongbao.component.html'
})
export class ThongBaoComponent implements OnInit {
  displayedColumns = ['index', 'noiDung', 'id_dm_cheDoPhat', 'actions'];
  dataSource: MatTableDataSource<any>;
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  searchForm: FormGroup;
  permission: any;
  get fs() { return this.searchForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private qltbService: QltbService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  ngOnInit() {
    this.createSearchForm();
    this.onSearch();
    if (!this.permission.QLTB_ThongBao._view) {
      this.router.navigate(['/']);
    }
  }

  createSearchForm() {
    this.searchForm = this.fb.group(
      {
        code: [null],
        name: [null],
        description: [null]
      });
  }

  onSearch() {
    this.getThongBao();
  }
  getThongBao() {
    this.qltbService.getThongBao(this.searchForm.value, this.common.QLTB_ThongBao.View).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource = new MatTableDataSource();
      } else {
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
  openUpdateDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogUpdateThongBaoComponent, {
      width: '800px',
      height: '80%',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      this.onSearch();
      if (result) {
        this.common.messageRes(result);
      }
    });
  }
  openUpdateDuyetDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogUpdateDuyetComponent, {
      width: '700px',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.qltbService.deleteThongBao({ id: data.id }, this.common.QLTB_ThongBao.Delete).subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.onSearch();
            this.common.messageRes(res.message);
          }
        });
      }
    });
  }
  // openAttachFiles(data: any): void {
  //   const dialogRef = this.dialog.open(AttachFilesDialogComponent, {
  //     height: '80%',
  //     width: '700px',
  //     data: data
  //   });
  // }
  // openComments(data: any): void {
  //   const dialogRef = this.dialog.open(CommentsComponent, {
  //     height: '80%',
  //     width: '800px',
  //     data: data
  //   });
  // }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
