import { DiaBanService } from './../../../services/diaban.service';
import { QltbService } from './../../../services/qltb.service';
import { Component, OnInit, Inject, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { AttachFilesService } from './../../../services/attach-files.service';
import { saveAs } from 'file-saver';
import { MatTabGroup } from '@angular/material/tabs';
import * as dateFormat from 'dateformat';
const RecordRTC = require('recordrtc/RecordRTC.min');
import { DomSanitizer } from '@angular/platform-browser';
import * as moment from 'moment';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}
@Component({
  selector: 'app-dialog-update-thongbao',
  templateUrl: './dialog-update-thongbao.component.html',
  styleUrls: ['./attach-files-dialog.component.css']
})
export class DialogUpdateThongBaoComponent implements OnInit {

  title = 'record-app';
  isRecording = false;
  private record;
  private error;
  t2s_giong = new FormControl('banmai');
  t2s_content = new FormControl('');

  @ViewChild(MatTabGroup) tabGroup: MatTabGroup;

  thongBaoForm: FormGroup;
  loaiThongBaos: select[] = [];
  loaiTins: select[] = [];
  kieuPhats: select[] = [];
  kieuLaps: select[] = [];
  cheDoPhats: select[] = [];
  diaBans: select[] = [];
  dataTTS: any;
  submitted = false;
  //#region  file
  public files: Array<File> = [];
  displayedColumns = ['index', 'filename', 'actions'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  loadListFiles: boolean = false;
  fileInsert: Array<String> = [];
  fileType = new FormControl('File');
  validateFile = true;
  gioPhat: string = '';
  //#endregion

  // convenience getter for easy access to form fields
  get f() { return this.thongBaoForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private qltbService: QltbService,
    private attachFiles: AttachFilesService,
    private diaBanService: DiaBanService,
    private domSanitizer: DomSanitizer,
    public dialogRef: MatDialogRef<DialogUpdateThongBaoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.loadCombo();
  }
  createForm() {
    this.thongBaoForm = this.fb.group(
      {
        noiDung: ['', [Validators.required]],
        id_dm_loaiThongBao: ['', [Validators.required]],
        id_diaBan: ['', [Validators.required]],
        id_dm_loaiTin: ['', [Validators.required]],
        id_dm_cheDoPhat: ['', [Validators.required]],
        id_dm_kieuPhat: ['', [Validators.required]],
        id_dm_kieuLap: ['', [Validators.required]],
        id_dm_trangThaiDuyet: [1],
        gioPhat: ['13:00', [Validators.required]],
        tuNgay: ['', [Validators.required]],
        denNgay: ['', [Validators.required]],
        ghiChu: [''],
      });
    if (this.data) {
      this.thongBaoForm.setValue({
        'noiDung': this.data.noiDung,
        'id_dm_loaiThongBao': this.data.id_dm_loaiThongBao,
        'id_dm_loaiTin': this.data.id_dm_loaiTin,
        'id_diaBan': this.data.id_diaBan,
        'id_dm_cheDoPhat': this.data.id_dm_cheDoPhat,
        'id_dm_kieuPhat': this.data.id_dm_kieuPhat,
        'id_dm_kieuLap': this.data.id_dm_kieuLap,
        'id_dm_trangThaiDuyet': this.data.id_dm_trangThaiDuyet,
        'gioPhat': '13:00',
        'tuNgay': this.data.tuNgay,
        'denNgay': this.data.denNgay,
        'ghiChu': this.data.ghiChu,
      });
      this.gioPhat = this.data.gioPhat;
      this.thongBaoForm.controls['tuNgay'].setValue(dateFormat(this.data.tuNgay, 'yyyy-mm-dd'));
      this.thongBaoForm.controls['denNgay'].setValue(dateFormat(this.data.denNgay, 'yyyy-mm-dd'));
      this.getAttachFile('mdm_thongBao', this.data.id);
    }
  }
  loadCombo() {
    this.getCheDoPhat();
    this.getKieuLap();
    this.getKieuPhat();
    this.getLoaiThongBao();
    this.getLoaiTin();
    this.getDiaBan();
  }
  onSubmit() {
    this.submitted = true;
    this.f.gioPhat.setValue(this.gioPhat);
    // stop here if form is invalid
    if (this.thongBaoForm.invalid || (this.files.length === 0 && this.loadListFiles === false)) {
      if (this.files.length === 0 && this.loadListFiles === false) {
        this.validateFile = false;
        this.tabGroup.selectedIndex = 1;
      } else {
        this.tabGroup.selectedIndex = 0;
      }
      return;
    }
    if (!this.data) {
      this.qltbService.insertThongBao(this.thongBaoForm.value, this.common.QLTB_ThongBao.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            if (this.files.length !== 0) {
              this.common.messageRes(res.message);
              this.insertAttachFile('mdm_thongBao', res.data.id);
            } else {
              this.dialogRef.close(res.message);
            }
          }
        });
    } else {
      this.qltbService.editThongBao({
        data: this.thongBaoForm.value,
        condition: { id: this.data.id }
      }, this.common.QLTB_ThongBao.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            if (this.files.length !== 0) {
              this.common.messageRes(res.message);
              this.insertAttachFile('mdm_thongBao', this.data.id);
            } else {
              this.dialogRef.close(res.message);
            }
          }
        });
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  //#region  DM
  getLoaiThongBao() {
    this.qltbService.getDmLoaiThongBao({}, this.common.QLTB_ThongBao.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.loaiThongBaos.push({ value: element.id, display: element.name });
          });
        }
      });
  }
  getLoaiTin() {
    this.qltbService.getDmLoaiTin({}, this.common.QLTB_ThongBao.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.loaiTins.push({ value: element.id, display: element.name });
          });
        }
      });
  }
  getKieuPhat() {
    this.qltbService.getDmKieuPhat({}, this.common.QLTB_ThongBao.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.kieuPhats.push({ value: element.id, display: element.name });
          });
        }
      });
  }
  getKieuLap() {
    this.qltbService.getDmKieuLap({}, this.common.QLTB_ThongBao.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.kieuLaps.push({ value: element.id, display: element.name });
          });
        }
      });
  }
  getCheDoPhat() {
    this.qltbService.getDmCheDoPhat({}, this.common.QLTB_ThongBao.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.cheDoPhats.push({ value: element.id, display: element.name });
          });
        }
      });
  }
  getDiaBan() {
    this.diaBanService.getDiaBan({}, this.common.QLTB_ThongBao.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.diaBans.push({ value: element.id, display: element.name });
          });
        }
      });
  }
  //#endregion
  //#region File
  onClick() {
    const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
    fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        this.files.push(file);
      }
    };
    fileUpload.click();
  }
  cancelFile(file: File) {
    const index = this.files.indexOf(file);
    if (index > -1) {
      this.files.splice(index, 1);
    }
  }
  clickMethodDelete(data: any) {
    if (confirm('You sure delete file' + data.filename)) {
      this.deleteAttachFile(data);
    }
  }
  getAttachFile(tableName: string, id_data: string) {
    this.attachFiles.getAttachFilesDetail({
      tableName: tableName,
      id_data: id_data
    }, this.common.QLTB_ThongBao.Insert).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.loadListFiles = true;
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    });
  }
  removeGioPhat() {
    this.gioPhat = '';
  }
  addGioPhat(data: string) {
    const n = this.gioPhat.includes(data);
    if (!n) {
      if (this.gioPhat === '') {
        this.gioPhat = data;
      } else {
        this.gioPhat = this.gioPhat + ',' + data;
      }

    }
  }
  insertAttachFile(tableName: string, id_data: string) {
    this.fileInsert = [];
    const fd = new FormData();
    if (!this.files[0]) {
      this.common.messageErr({ error: { message: 'No file uploaded' } });
      return;
    }
    fd.append('attach', this.files[0], id_data + ',' + tableName);
    for (let index = 0; index < this.files.length; index++) {
      this.fileInsert.push(this.files[index].name);
      fd.append('attach', this.files[index], this.files[index].name);
    }
    // tslint:disable-next-line: max-line-length
    this.attachFiles.insertAttachFiles({ tableName: tableName, id_data: id_data, files: this.fileInsert.toString() }, this.common.QLTB_ThongBao.Insert)
      .subscribe(res1 => {
        if (res1.error) {
          this.common.messageErr(res1);
        } else {
          this.attachFiles.uploadAttachFiles(fd, this.common.QLTB_ThongBao.Insert)
            .subscribe(response => {
              if (response.error) {
                this.common.messageErr(response);
              } else {
                // this.getAttachFile('mdm_thongBao', id_data);
                this.files = [];
                this.tabGroup.selectedIndex = 1;
                this.dialogRef.close();
              }
            });
        }
      });
  }
  deleteAttachFile(data: any) {
    this.attachFiles.deleteAttachFilesDetail({ id: data.id }, this.common.QLTB_ThongBao.Insert).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.getAttachFile('mdm_thongBao', data.id_data);
      }
    });
  }
  downloadAttachFiles(data: any) {
    this.attachFiles.downloadAttachFiles({ path: data.path }, this.common.QLTB_ThongBao.Insert).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        saveAs(new File([res], data.filename));
      }
    });
  }
  //#endregion


  //#region  record
  startRecording() {
    this.isRecording = true;
    const mediaConstraints = {
      video: false,
      audio: true
    };
    navigator.mediaDevices
      .getUserMedia(mediaConstraints)
      .then(this.successCallback.bind(this), this.errorCallback.bind(this));
  }

  successCallback(stream) {
    const options = {
      mimeType: 'audio/mp3',
      numberOfAudioChannels: 1
    };

    // Start Actuall Recording
    const StereoAudioRecorder = RecordRTC.StereoAudioRecorder;
    this.record = new StereoAudioRecorder(stream, options);
    this.record.record();
  }

  stopRecording() {
    this.isRecording = false;
    this.record.stop(this.processRecording.bind(this));
  }

  processRecording(blob) {
    const now = moment().format('x');
    const newFile: File = new File([blob], now + '.mp3');
    this.files.push(newFile);
  }

  sanitize(file: any) {
    return this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(file));
  }

  errorCallback(error) {
    this.error = 'Can not play audio in your browser';
  }
  //#endregion
  textToSpeech() {
    // this.qltbService.textToSpeech(this.t2s_content.value, this.t2s_giong.value)
    //   .subscribe(res => {
    //     console.log(res);
    //     this.dataTTS = res;
    //     // saveAs(res.async, now + '.mp3');
    //   });
    // this.qltbService.getT2S({ inputtext: this.t2s_content.value, giong: this.t2s_giong.value }, this.common.QLTB_ThongBao.Insert)
    //   .subscribe(res => {
    //     console.log(res);
    //     const now = moment().format('x');
    //     // saveAs(res.async, now + '.mp3');
    //   });
  }
}
