import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { GroupService } from '../../../services/group.service';
import { QltbService } from '../../../services/qltb.service';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}

@Component({
  selector: 'app-dialog-update-duyet',
  templateUrl: './dialog-update-duyet.component.html',
})
export class DialogUpdateDuyetComponent implements OnInit {
  duyetForm: FormGroup;
  configs: any[];
  diaBans: any[];
  groups: any[];
  trangThaiDuyets: select[] = [];
  submitted = false;
  isEdit = false;
  // convenience getter for easy access to form fields
  get f() { return this.duyetForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private qltbService: QltbService,
    public dialogRef: MatDialogRef<DialogUpdateDuyetComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.getTrangThaiDuyet();
    this.getDuyet();
  }
  createForm() {
    this.duyetForm = this.fb.group(
      {
        noiDung: ['', [Validators.required]],
        id_thongBao: ['', [Validators.required]],
        id_dm_trangThaiDuyet: [null, [Validators.required]],
        ghiChu: [''],
      });
    if (this.data) {
      this.duyetForm.setValue({
        'noiDung': this.data.noiDung,
        'id_thongBao': this.data.id,
        'id_dm_trangThaiDuyet': this.data.id_dm_trangThaiDuyet,
        'ghiChu': this.data.ghiChu,
      });
    }
    this.f.noiDung.disable();
  }
  getDuyet() {
    this.qltbService.getDuyet({id_thongBao: this.data.id}, this.common.QLTB_ThongBao.Insert)
      .subscribe(res => {
        if (!res.error) {
          this.isEdit = true;
        }
      });
  }
  getTrangThaiDuyet() {
    this.qltbService.getDmTrangThaiDuyet({}, this.common.QLTB_ThongBao.Insert)
    .subscribe(res => {
      if (!res.error) {
        res.data.forEach(element => {
          this.trangThaiDuyets.push({ value: element.id, display: element.name });
        });
      }
    });
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.duyetForm.invalid) {
      return;
    }
    if (this.isEdit === false) {
      this.qltbService.insertDuyet(this.duyetForm.value, this.common.DiaBan.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.qltbService.editThongBao({
              data: {id_dm_trangThaiDuyet: this.f.id_dm_trangThaiDuyet.value},
              condition: { id: this.data.id }
            }, this.common.DiaBan.Insert)
              .subscribe(response => {
                if (response.error) {
                  this.common.messageErr(res);
                } else {
                  this.dialogRef.close(res.message);
                }
              });
          }
        });
    } else {
      this.qltbService.editDuyet({
        data: this.duyetForm.value,
        condition: { id: this.data.id }
      }, this.common.DiaBan.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.qltbService.editThongBao({
              data: {id_dm_trangThaiDuyet: this.f.id_dm_trangThaiDuyet.value},
              condition: { id: this.data.id }
            }, this.common.DiaBan.Insert)
              .subscribe(response => {
                if (response.error) {
                  this.common.messageErr(res);
                } else {
                  this.dialogRef.close(res.message);
                }
              });
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
