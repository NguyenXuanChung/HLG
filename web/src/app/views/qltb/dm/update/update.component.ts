import { QltbService } from './../../../../services/qltb.service';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../../services/common.service';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}
@Component({
  selector: 'app-update',
  templateUrl: './update.component.html'
})
export class UpdateComponent implements OnInit {
  public dmForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.dmForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private qltbService: QltbService,
    public dialogRef: MatDialogRef<UpdateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.currentLanguage();
  }
  currentLanguage() {
    return this.common.translate.currentLang;
  }
  createForm() {
    this.dmForm = this.fb.group(
      {
        code: ['', [Validators.required]],
        name: ['', [Validators.required]],
        description: [''],
        sortOrder: [0, [Validators.required, Validators.min(0)]],
      });
    if (this.data.body) {
      this.dmForm.setValue({
        'code': this.data.body.code,
        'name': this.data.body.name,
        'description': this.data.body.description,
        'sortOrder': this.data.body.sortOrder,
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.dmForm.invalid) {
      return;
    }
    // kiểm tra xem thuộc loại danh mục gì để update theo danh mục tương ứng
    let updateRespone;
    if (!this.data.body) {
      if (this.data.name === 'cheDoPhat') {
        updateRespone = this.qltbService.insertDmCheDoPhat(this.dmForm.value, this.common.DM_CheDoPhat.Insert);
      } else if (this.data.name === 'kieuLap') {
        updateRespone = this.qltbService.insertDmKieuLap(this.dmForm.value, this.common.DM_KieuLap.Insert);
      } else if (this.data.name === 'kieuPhat') {
        updateRespone = this.qltbService.insertDmKieuPhat(this.dmForm.value, this.common.DM_KieuLap.Insert);
      } else if (this.data.name === 'loaiThongBao') {
        updateRespone = this.qltbService.insertDmLoaiThongBao(this.dmForm.value, this.common.DM_KieuLap.Insert);
      } else if (this.data.name === 'loaiTin') {
        updateRespone = this.qltbService.insertDmLoaiTin(this.dmForm.value, this.common.DM_KieuLap.Insert);
      } else if (this.data.name === 'trangThaiDuyet') {
        updateRespone = this.qltbService.insertDmTrangThaiDuyet(this.dmForm.value, this.common.DM_KieuLap.Insert);
      } else if (this.data.name === 'trangThaiPhat') {
        updateRespone = this.qltbService.insertDmTrangThaiPhat(this.dmForm.value, this.common.DM_KieuLap.Insert);
      }
    } else {
      if (this.data.name === 'cheDoPhat') {
        updateRespone = this.qltbService.editDmCheDoPhat({
          data: this.dmForm.value,
          condition: { id: this.data.body.id }
        }, this.common.DM_CheDoPhat.Update);
      } else if (this.data.name === 'kieuLap') {
        updateRespone = this.qltbService.editDmKieuLap({
          data: this.dmForm.value,
          condition: { id: this.data.body.id }
        }, this.common.DM_KieuLap.Update);
      } else if (this.data.name === 'kieuPhat') {
        updateRespone = this.qltbService.editDmKieuPhat({
          data: this.dmForm.value,
          condition: { id: this.data.body.id }
        }, this.common.DM_KieuLap.Update);
      } else if (this.data.name === 'loaiThongBao') {
        updateRespone = this.qltbService.editDmLoaiThongBao({
          data: this.dmForm.value,
          condition: { id: this.data.body.id }
        }, this.common.DM_KieuLap.Update);
      } else if (this.data.name === 'loaiTin') {
        updateRespone = this.qltbService.editDmLoaiTin({
          data: this.dmForm.value,
          condition: { id: this.data.body.id }
        }, this.common.DM_KieuLap.Update);
      } else if (this.data.name === 'trangThaiDuyet') {
        updateRespone = this.qltbService.editDmTrangThaiDuyet({
          data: this.dmForm.value,
          condition: { id: this.data.body.id }
        }, this.common.DM_KieuLap.Update);
      } else if (this.data.name === 'trangThaiPhat') {
        updateRespone = this.qltbService.editDmTrangThaiPhat({
          data: this.dmForm.value,
          condition: { id: this.data.body.id }
        }, this.common.DM_KieuLap.Update);
      }
    }
    if (updateRespone) {
      updateRespone.subscribe(res => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.dialogRef.close(res.message);
        }
      });
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
