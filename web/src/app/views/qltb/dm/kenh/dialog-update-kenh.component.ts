import { QltbService } from './../../../../services/qltb.service';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../../services/common.service';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}
@Component({
  selector: 'app-dialog-update-kenh',
  templateUrl: './dialog-update-kenh.component.html'
})
export class DialogUpdateKenhComponent implements OnInit {
  public dmForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.dmForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private qltbService: QltbService,
    public dialogRef: MatDialogRef<DialogUpdateKenhComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.currentLanguage();
  }
  currentLanguage() {
    return this.common.translate.currentLang;
  }
  createForm() {
    this.dmForm = this.fb.group(
      {
        code: ['', [Validators.required]],
        name: ['', [Validators.required]],
        url: ['', [Validators.required]],
        tanSo: ['', [Validators.min(0)]],
        description: [''],
        sortOrder: [0, [Validators.required, Validators.min(0)]],
      });
    if (this.data) {
      this.dmForm.setValue({
        'code': this.data.code,
        'name': this.data.name,
        'url': this.data.url,
        'tanSo': this.data.tanSo,
        'description': this.data.description,
        'sortOrder': this.data.body.sortOrder,
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.dmForm.invalid) {
      return;
    }
    let updateRespone;
    if (!this.data) {
     updateRespone = this.qltbService.insertDmKenh(this.dmForm.value, this.common.DM_Kenh.Insert);
    } else {
      updateRespone = this.qltbService.editDmKenh({
        data: this.dmForm.value,
        condition: { id: this.data.id }
      }, this.common.DM_Kenh.Update);
    }
    if (updateRespone) {
      updateRespone.subscribe(res => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.dialogRef.close(res.message);
        }
      });
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
