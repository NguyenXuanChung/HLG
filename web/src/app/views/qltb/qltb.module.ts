import { QltbRoutingModule } from './qltb-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { AngularMaterial } from '../../angular-material';
import { MomentModule } from 'ngx-moment';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { EditorModule } from 'primeng/editor';
import { QuillModule } from 'ngx-quill';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { MatTabsModule } from '@angular/material/tabs';
import { TreeModule } from 'primeng/tree';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { KieuLapComponent } from './dm/kieulap/kieulap.component';
import { KieuPhatComponent } from './dm/kieuphat/kieuphat.component';
import { LoaiThongBaoComponent } from './dm/loaithongbao/loaithongbao.component';
import { LoaiTinComponent } from './dm/loaitin/loaitin.component';
import { TrangThaiDuyetComponent } from './dm/trangthaiduyet/trangthaiduyet.component';
import { UpdateComponent } from './dm/update/update.component';
import { TrangThaiPhatComponent } from './dm/trangthaiphat/trangthaiphat.component';
import { KenhComponent } from './dm/kenh/kenh.component';
import { CheDoPhatComponent } from './dm/chedophat/chedophat.component';
import { DialogUpdateKenhComponent } from './dm/kenh/dialog-update-kenh.component';
import { ThongBaoComponent } from './thongbao/thongbao.component';
import { DialogUpdateThongBaoComponent } from './thongbao/dialog-update-thongbao.component';
import { DialogUpdateDuyetComponent } from './thongbao/dialog-update-duyet.component';
import {MatExpansionModule} from '@angular/material/expansion';

export function translateHttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
    KieuLapComponent,
    KieuPhatComponent,
    LoaiThongBaoComponent,
    LoaiTinComponent,
    TrangThaiDuyetComponent,
    UpdateComponent,
    TrangThaiPhatComponent,
    KenhComponent,
    CheDoPhatComponent,
    DialogUpdateKenhComponent,
    ThongBaoComponent,
    DialogUpdateThongBaoComponent,
    DialogUpdateDuyetComponent
  ],
  entryComponents: [
    DialogUpdateKenhComponent,
    UpdateComponent,
    DialogUpdateThongBaoComponent,
    DialogUpdateDuyetComponent
  ],
  imports: [
    CommonModule,
    QltbRoutingModule,
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: translateHttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    AngularMaterial,
    MomentModule,
    InputTextareaModule,
    EditorModule,
    QuillModule.forRoot({
      modules: {
        syntax: false,
        toolbar: [
          ['bold', 'italic', 'underline', 'strike'],
          [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
          [{ 'list': 'ordered' }, { 'list': 'bullet' }],
          [{ 'indent': '-1' }, { 'indent': '+1' }],
          ['blockquote', 'code-block']
        ]
      }
    }),
    TreeViewModule,
    BsDropdownModule.forRoot(),
    MatTabsModule,
    TreeModule,
    MatAutocompleteModule,
    MatExpansionModule
  ]
})
export class QltbModule { }
