import { ThongBaoComponent } from './thongbao/thongbao.component';
import { TrangThaiPhatComponent } from './dm/trangthaiphat/trangthaiphat.component';
import { TrangThaiDuyetComponent } from './dm/trangthaiduyet/trangthaiduyet.component';
import { LoaiTinComponent } from './dm/loaitin/loaitin.component';
import { LoaiThongBaoComponent } from './dm/loaithongbao/loaithongbao.component';
import { KieuPhatComponent } from './dm/kieuphat/kieuphat.component';
import { KieuLapComponent } from './dm/kieulap/kieulap.component';
import { KenhComponent } from './dm/kenh/kenh.component';
import { CheDoPhatComponent } from './dm/chedophat/chedophat.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'thongbao'
      },
      {
        path: 'thongbao',
        component: ThongBaoComponent,
      },
      {
        path: 'dm/chedophat',
        component: CheDoPhatComponent,
      },
      {
        path: 'dm/kenh',
        component: KenhComponent,
      },
      {
        path: 'dm/kieulap',
        component: KieuLapComponent,
      },
      {
        path: 'dm/kieuphat',
        component: KieuPhatComponent,
      },
      {
        path: 'dm/loaithongbao',
        component: LoaiThongBaoComponent,
      },
      {
        path: 'dm/loaitin',
        component: LoaiTinComponent,
      },
      {
        path: 'dm/trangthaiduyet',
        component: TrangThaiDuyetComponent,
      },
      {
        path: 'dm/trangthaiphat',
        component: TrangThaiPhatComponent,
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QltbRoutingModule { }
