import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../services/common.service';
import { ConfigurationService } from '../../services/configuration.service';
import { DiaBanService } from '../../services/diaban.service';
import { GroupService } from '../../services/group.service';
import { StorageService } from '../../services/storage.service';
import { AuthConstants } from '../../config/auth-constants';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-config-update',
  templateUrl: './config-update.component.html',
})
export class ConfigUpdateComponent implements OnInit {
  configForm: FormGroup;
  configs: any[];
  diaBans: any[];
  groups: any[];
  data: any;
  submitted = false;
  permission: any;
  displayedColumnApps = ['pkg', 'name', 'version', 'activity', 'icon', 'actions'];
  dataSourceApp: MatTableDataSource<any>;
  displayedColumnFiles = ['url', 'link', 'description', 'createDate', 'actions'];
  dataSourceFile: MatTableDataSource<any>;
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  // convenience getter for easy access to form fields
  get f() { return this.configForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private configService: ConfigurationService,
    private groupService: GroupService,
    private diaBanService: DiaBanService,
    private storageService: StorageService
  ) {
    console.log('123');
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }
  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.configForm = this.fb.group(
      {
        name: ['', [Validators.required]],
        description: [''],
        type: [''],
        password: [''],
        backgroundColor: [''],
        textColor: [''],
        backgroundImageUrl: [''],
        iconSize: [''],
        desktopHeader: [''],
        useDefaultDesignSettings: [''],
        customerId: [''],
        gps: [''],
        bluetooth: [''],
        wifi: [''],
        mobileData: [''],
        mainAppId: [''],
        eventReceivingComponent: [''],
        kioskMode: [''],
        qrCodeKey: [''],
        contentAppId: [''],
        autoUpdate: [''],
        blockStatusBar: [''],
        systemUpdateType: [''],
        systemUpdateFrom: [''],
        systemUpdateTo: [''],
        usbStorage: [''],
        requestUpdates: [''],
        pushOptions: [''],
        autoBrightness: [''],
        brightness: [''],
        manageTimeout: [''],
        timeout: [''],
        lockVolume: [''],
        wifiSSID: [''],
        wifiPassword: [''],
        wifiSercurityType: [''],
        passwordMode: ['']
      });
    // if (this.data) {
    //   this.configForm.setValue({
    //     'code': this.data.code,
    //     'name': this.data.name,
    //     'licenseKey': this.data.licenseKey,
    //     'description': this.data.description,
    //   });
    // }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.configForm.invalid) {
      return;
    }
    if (!this.data) {
      this.configService.insertConfig(this.configForm.value, this.common.Configuration.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.common.messageRes(res.message);
          }
        });
    } else {
      this.configService.editConfig({
        data: this.configForm.value,
        condition: { id: this.data.id }
      }, this.common.Configuration.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.common.messageRes(res.message);
          }
        });
    }
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
