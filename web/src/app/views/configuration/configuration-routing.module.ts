import { ConfigUpdateComponent } from './config-update.component';
import { ConfigurationComponent } from './configuration.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Config'
    },
    children: [
      {
        path: '',
        data: {
          title: 'Config'
        },
        component: ConfigurationComponent,
      },
      {
        path: 'update',
        component: ConfigUpdateComponent,
        data: {
          title: 'update'
        }
      },
      {
        path: 'update/:id_project',
        component: ConfigUpdateComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationRoutingModule { }
