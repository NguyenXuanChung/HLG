import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../shared/confirmation-dialog/confirmation-dialog.component';
import { icon, latLng, Map, marker, point, polyline, tileLayer, featureGroup, FeatureGroup, DrawEvents, map } from 'leaflet';
import * as L from 'leaflet';

import * as moment from 'moment';

// Translate
import { CommonService } from './../../services/common.service';
import { StorageService } from '../../services/storage.service';
import { AuthConstants } from '../../config/auth-constants';
import { Router } from '@angular/router';
import { CheckableSettings } from '@progress/kendo-angular-treeview';
import { Observable, of } from 'rxjs';
import { DeviceService } from '../../services/device.service';
import { ConfigurationService } from '../../services/configuration.service';
import { GroupService } from '../../services/group.service';
import { DiaBanService } from '../../services/diaban.service';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}
@Component({
  selector: 'app-location',
  templateUrl: './location.component.html'
})
export class LocationComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  searchForm: FormGroup;
  permission: any;
  listLocation: any[];
  configs: any[];
  diaBans: any[];
  groups: any[];
  checkArray: any[] = [];
  get fs() { return this.searchForm.controls; }
  form: FormGroup;
  ////#region  Map

  // world = new L.LatLngBounds([[20.5, 104], [22, 107]]);

  // options = {
  //   layers: [this.gtMaps],
  //   zoom: 12,
  //   center: latLng([21.028511, 105.804817]),
  //   minZoom: 8,
  //   maxBounds: this.world,
  //   maxBoundsViscosity: 1,
  // };

  streetMaps = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
    maxZoom: 20,
    subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
  });
  map: any;
  options = {
    layers: [this.streetMaps],
    zoom: 12,
    center: latLng([21.028511, 105.804817]),
  };
  markerData: any[] = [];
  //#endregion
  obj: any = [
    {
      id: 1,
      name: '123',
      id_configuration: 1,
      id_group: 1,
      id_diaBan: 1,
      checked: false
    },
    {
      id: 2,
      name: '123',
      id_configuration: 1,
      id_group: 1,
      id_diaBan: 1,
      checked: false
    },
    {
      id: 3,
      name: '123',
      id_configuration: 1,
      id_group: 1,
      id_diaBan: 1,
      checked: false

    },
    {
      id: 4,
      name: '123',
      id_configuration: 1,
      id_group: 1,
      id_diaBan: 1,
      checked: false

    },
    {
      id: 5,
      name: '123',
      id_configuration: 1,
      id_group: 1,
      id_diaBan: 1,
      checked: false

    },
  ];
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private deviceService: DeviceService,
    private configService: ConfigurationService,
    private groupService: GroupService,
    private diaBanService: DiaBanService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  ngOnInit() {
    this.createSearchForm();
    this.onSearch();
    if (!this.permission.Location._view) {
      this.router.navigate(['/']);
    }
    this.loadCombo();
  }

  createSearchForm() {
    this.searchForm = this.fb.group(
      {
        id_group: [null],
        id_configuration: [null],
        id_diaBan: [null],
        tuNgay: [null],
        denNgay: [null],
        contents: [null]
      });
  }

  onSearch() {
    this.getLocation();
  }

  getLocation() {
    this.markerData = [];
    this.deviceService.getDevice(this.searchForm.value, this.common.Location.View)
      .subscribe(res => {
        if (res.error) {
          this.common.messageErr(res);
        } else {
          res.data.forEach(element => {
            this.markerData.push(marker(element.location, {
              icon: icon({
                iconSize: [25, 45],
                iconUrl: 'marker-icon.png',
              })
            }
            ).bindPopup(element.name)
            );
          });
        }
      });
  }
  // tslint:disable-next-line: no-shadowed-variable
  onMapReady(map: L.Map) {
    this.map = map;
    map.attributionControl.setPrefix(''); // Don't show the 'Powered by Leaflet' text.
    L.control.scale().addTo(this.map);
    // setTimeout(() => {
    //   L.control.fullscreen({
    //     position: 'topleft',
    //     title: this.common.trans('showFullScreen'),
    //     titleCancel: this.common.trans('exitFullScreen'),
    //     content: null,
    //     forceSeparateButton: true,
    //     forcePseudoFullscreen: true,
    //     fullscreenElement: false
    //   }).addTo(this.map);
    // }, 100);
    // this.map.on('enterFullscreen', () => this.map.invalidateSize());
    // this.map.on('exitFullscreen', () => this.map.invalidateSize());
  }
  loadCombo() {
    this.configService.getConfig({}, this.common.Location.Insert).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.configs = res.data;
      }
    });
    this.diaBanService.getDiaBan({}, this.common.Location.Insert).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.diaBans = res.data;
      }
    });
    this.groupService.getGroup({}, this.common.Location.Insert).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.groups = res.data;
      }
    });
  }
}
