import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChucVuComponent } from './dm/chucvu/chucvu.component';
import { ChucDanhComponent } from './dm/chucdanh/chucdanh.component';
import { QuocGiaComponent } from './dm/quocgia/quocgia.component';
import { TonGiaoComponent } from './dm/tongiao/ton-giao.component';
import { DanTocComponent } from './dm/dantoc/dan-toc.component';
import { TinhHuyenXaComponent } from './dm/tinh-huyen-xa/tinh-huyen-xa.component';
import { ViTriViecLamComponent } from './dm/vi-tri-viec-lam/vi-tri-viec-lam.component';
import { TrinhDoTinHocComponent } from './dm/trinh-do-tin-hoc/trinh-do-tin-hoc.component';
import { TrinhDoNgoaiNguComponent } from './dm/trinh-do-ngoai-ngu/trinh-do-ngoai-ngu.component';
import { BenhComponent } from './dm/benh/benh.component';
import { ChuyenNganhComponent } from './dm/chuyen-nganh/chuyen-nganh.component';
import { TinhTrangHonNhanComponent } from './dm/tinh-trang-hon-nhan/tinh-trang-hon-nhan.component';
import { VanBangComponent } from './dm/van-bang/van-bang.component';
import { LyLuanChinhTriComponent } from './dm/ly-luan-chinh-tri/ly-luan-chinh-tri.component';
import { TrinhDoQuanLyComponent } from './dm/trinh-do-quan-ly/trinh-do-quan-ly.component';
import { QuanLyBenhVienComponent } from './dm/quan-ly-benh-vien/quan-ly-benh-vien.component';
import { XepLoaiComponent } from './dm/xep-loai/xep-loai.component';
import { XepLoaiSucKhoeComponent } from './dm/xep-loai-suc-khoe/xep-loai-suc-khoe.component';
import { NgoaiNguComponent } from './dm/ngoai-ngu/ngoai-ngu.component';
import { HocHamComponent } from './dm/hoc-ham/hoc-ham.component';
import { HocViComponent } from './dm/hoc-vi/hoc-vi.component';
import { CapDaoTaoComponent } from './dm/cap-dao-tao/cap-dao-tao.component';
import { QuanHeComponent } from './dm/quan-he/quan-he.component';
import { CanBoBangCapComponent } from './cb/canbo-bangcap/canbo-bangcap.component';
import { LuongCoBanComponent } from './dm/luongcoban/luong-co-ban.component';
import { CanBoComponent } from './cb/canbo/canbo.component';
import { CanBoQTCTComponent } from './cb/canbo-qtct/canbo-qtct.component';
import { CanBoThanNhanComponent } from './cb/canbo-thannhan/canbo-thannhan.component';
import { CanBoBenhAnComponent } from './cb/canbo-benhan/canbo-benhan.component';
import{ CanBoSucKhoeHangNamComponent } from './cb/canbo-suckhoehangnam/canbo-suckhoehangnam.component'
import { CanBoNgoaiNguComponent } from './cb/canbo-ngoaingu/canbo-ngoaingu.component';
import { CanBoHopDongComponent } from './cb/canbo-hopdong/canbo-hopdong.component';
import { LoaiHopDongComponent } from './dm/loai-hop-dong/loai-hop-dong.component';
import { MucLuongCoSoComponent } from './dm/muc-luong-co-so/muc-luong-co-so.component';
import { LyDoNghiComponent } from './dm/ly-do-nghi/ly-do-nghi.component';
import { NguoiKyBaoCaoComponent } from './dm/nguoi-ky-bao-cao/nguoi-ky-bao-cao.component';
import { HinhThucKyLuatComponent } from './dm/hinh-thuc-ky-luat/hinh-thuc-ky-luat.component';
import { NoiBanHanhComponent } from './dm/noi-ban-hanh/noi-ban-hanh.component';
import { HinhThucKhenThuongComponent } from './dm/hinh-thuc-khen-thuong/hinh-thuc-khen-thuong.component';
import { CanBoKyLuatComponent } from './cb/canbo-kyluat/canbo-kyluat.component';
import { CanBoKhenThuongComponent } from './cb/canbo-khenthuong/canbo-khenthuong.component';
import { CanBoKiemNhiemComponent } from './cb/canbo-kiemnhiem/canbo-kiemnhiem.component';
import { CanBoQuaTrinhLuongComponent } from './cb/canbo-qua-trinh-luong/canbo-qua-trinh-luong.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Hscb'
    },
    children: [
      {
        path: '',
        redirectTo: 'canbo'
      },
      {
        path: 'chucvu',
        component: ChucVuComponent,
        data: {
          title: 'Chuc Vu'
        }
      },
      {
        path:'chucdanh',
        component: ChucDanhComponent,
        data: {
          title: 'Chuc Danh'
        }
      },
      {
        path:'quocgia',
        component: QuocGiaComponent,
        data: {
          title: 'Quốc gia'
        }
      },
      {
        path:'ton-giao',
        component: TonGiaoComponent,
        data: {
          title: 'Tôn giáo'
        }
      },
      {
        path:'dan-toc',
        component: DanTocComponent,
        data: {
          title: 'DanToc'
        }
      },
      {
        path:'tinh-huyen-xa',
        component: TinhHuyenXaComponent,
        data: {
          title: 'titleTinhHuyenXa'
        }
      },
      {
        path:'vi-tri-viec-lam',
        component: ViTriViecLamComponent,
        data: {
          title: 'titleViTriViecLam'
        }
      },
      {
        path:'trinh-do-tin-hoc',
        component: TrinhDoTinHocComponent,
        data: {
          title: 'titleTrinhDoTinHoc'
        }
      },
      {
        path:'trinh-do-ngoai-ngu',
        component: TrinhDoNgoaiNguComponent,
        data: {
          title: 'titleTrinhDoNgoaiNgu'
        }
      },
      {
        path:'benh',
        component: BenhComponent,
        data: {
          title: 'titleBenh'
        }
      },
      {
        path:'chuyen-nganh',
        component: ChuyenNganhComponent,
        data: {
          title: 'titleChuyenNganh'
        }
      },
      {
        path:'tinh-trang-hon-nhan',
        component: TinhTrangHonNhanComponent,
        data: {
          title: 'titleTinhTrangHonNhan'
        }
      },
      {
        path:'van-bang',
        component: VanBangComponent,
        data: {
          title: 'titleVanBang'
        }
      },
      {
        path:'ly-luan-chinh-tri',
        component: LyLuanChinhTriComponent,
        data: {
          title: 'titleLyLuanChinhTri'
        }
      },
      {
        path:'trinh-do-quan-ly',
        component: TrinhDoQuanLyComponent,
        data: {
          title: 'titleTrinhDoQuanLy'
        }
      },
      {
        path:'quan-ly-benh-vien',
        component: QuanLyBenhVienComponent,
        data: {
          title: 'titleQuanLyBenhVien'
        }
      },
      {
        path:'xep-loai',
        component: XepLoaiComponent,
        data: {
          title: 'titleXepLoai'
        }
      },
      {
        path:'xep-loai-suc-khoe',
        component: XepLoaiSucKhoeComponent,
        data: {
          title: 'titleXepLoaiSucKhoe'
        }
      },
      {
        path:'ngoai-ngu',
        component: NgoaiNguComponent,
        data: {
          title: 'titleNgoaiNgu'
        }
      },
      {
        path:'hoc-ham',
        component: HocHamComponent,
        data: {
          title: 'titleHocHam'
        }
      },
      {
        path:'hoc-vi',
        component: HocViComponent,
        data: {
          title: 'titleHocVi'
        }
      },
      {
        path:'cap-dao-tao',
        component: CapDaoTaoComponent,
        data: {
          title: 'titleCapDaoTao'
        }
      },
      {
        path:'quan-he',
        component: QuanHeComponent,
        data: {
          title: 'titleQuanHe'
        }
      },
      {
        path:'loai-hop-dong',
        component: LoaiHopDongComponent,
        data: {
          title: 'titleLoaiHopDong'
        }
      },
      {
        path:'canbo',
        component: CanBoComponent,
        data: {
          title: 'titleCanBo'
        }
      },
      {
        path:'canbo/canbo-bangcap/:id_canbo',
        component: CanBoBangCapComponent,
        data: {
          title: 'titleCanBoBangCap'
        }
      },
      {
        path:'canbo/canbo-qtct/:id_canbo',
        component: CanBoQTCTComponent,
        data: {
          title: 'titleCanBoQTCT'
        }
      },
      {
        path:'canbo/canbo-thannhan/:id_canbo',
        component: CanBoThanNhanComponent,
        data: {
          title: 'titleCanBoQTCT'
        }
      },
      {
        path:'canbo/canbo-benhan/:id_canbo',
        component: CanBoBenhAnComponent,
        data: {
          title: 'titleCanBoQTCT'
        }
      },
      {
        path:'canbo/canbo-suckhoehangnam/:id_canbo',
        component: CanBoSucKhoeHangNamComponent,
        data: {
          title: 'titleCanBoQTCT'
        }
      },
      {
        path:'canbo/canbo-ngoaingu/:id_canbo',
        component: CanBoNgoaiNguComponent,
        data: {
          title: 'titleCanBoNgoaiNgu'
        }
      },
      {
        path:'canbo/canbo-hopdong/:id_canbo',
        component: CanBoHopDongComponent,
        data: {
          title: 'titleCanBoHopDong'
        }
      },
      {
        path:'luong-co-ban',
        component: LuongCoBanComponent,
        data: {
          title: 'LuongCoBan'
        }
      },
      {
        path:'muc-luong-co-so',
        component: MucLuongCoSoComponent,
        data: {
          title: 'titleMucLuongCoBan'
        }
      },
      {
        path:'ly-do-nghi',
        component: LyDoNghiComponent,
        data: {
          title: 'titleLyDoNghi'
        }
      },
      {
        path:'nguoi-ky-bao-cao',
        component: NguoiKyBaoCaoComponent,
        data: {
          title: 'titleNguoiKyBaoCao'
        }
      },
      {
        path:'hinh-thuc-ky-luat',
        component: HinhThucKyLuatComponent,
        data: {
          title: 'titleHinhThucKyLuat'
        }
      },
      {
        path:'noi-ban-hanh',
        component: NoiBanHanhComponent,
        data: {
          title: 'titleNoiBanHanh'
        }
      },
      {
        path:'hinh-thuc-khen-thuong',
        component: HinhThucKhenThuongComponent,
        data: {
          title: 'titleHinhThucKhenThuong'
        }
      },
      {
        path:'canbo/canbo-kyluat/:id_canbo',
        component: CanBoKyLuatComponent,
        data: {
          title: 'titleCanBoKyLuat'
        }
      },
      {
        path:'canbo/canbo-khenthuong/:id_canbo',
        component: CanBoKhenThuongComponent,
        data: {
          title: 'titleCanBoKhenThuong'
        }
      },
      {
        path:'canbo/canbo-kiemnhiem/:id_canbo',
        component: CanBoKiemNhiemComponent,
        data: {
          title: 'titleCanBoKiemNhiem'
        }
      } ,
      {
        path:'canbo/canbo-quatrinhluong/:id_canbo',
        component: CanBoQuaTrinhLuongComponent,
        data: {
          title: 'titleCanBoQuaTrinhLuong'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HscbRoutingModule { }
