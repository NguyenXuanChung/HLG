import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import * as dateFormat from 'dateformat';
// Translate
import { CommonService } from '../../../../services/common.service';
import { HscbService } from '../../../../services/hscb.service';

interface select {
  value: number;
  display: string;
}
@Component({
  selector: 'app-dialog-muc-luong-co-so-update',
  templateUrl: './dialog-muc-luong-co-so-update.component.html',
})
export class DialogUpdateMucLuongCoSoComponent implements OnInit {
  updateForm: FormGroup;
  submitted = false;
  lstPhanLoaiHopDong: select[] = [];
  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    public dialogRef: MatDialogRef<DialogUpdateMucLuongCoSoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.loadcombo();
    this.createForm();
  }
  createForm() {
    this.updateForm = this.fb.group(
      {
        id_phanLoaiHopDong: [null],
        mucTien: [null],
        ngayApDung:[null],
        tenVanBan: [null],
        ghiChu: [null]
      });
    if (this.data) {
      this.updateForm.setValue({
        id_phanLoaiHopDong: this.data.id_phanLoaiHopDong,
        mucTien: this.data.mucTien,
        ngayApDung: this.data.ngayApDung,
        tenVanBan: this.data.tenVanBan,
        ghiChu: this.data.ghiChu
      });
      if (this.data.ngayApDung) {
        this.f.ngayApDung.setValue(dateFormat(this.data.ngayApDung, 'yyyy-mm-dd', true));
      }
    }
  }
  loadcombo(){
    this.getPhanLoaiHopDong();
  }
  getPhanLoaiHopDong(){
    this.lstPhanLoaiHopDong = [];
    this.hscbService.getPhanLoaiHopDong({}, this.common.HSCB_MucLuongCoSo.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstPhanLoaiHopDong.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    if (!this.data) {
      this.hscbService.insertMucLuongCoSo(this.updateForm.value, this.common.HSCB_MucLuongCoSo.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.hscbService.editMucLuongCoSo({
        data: this.updateForm.value,
        condition: { id: this.data.id }
      }, this.common.HSCB_MucLuongCoSo.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
