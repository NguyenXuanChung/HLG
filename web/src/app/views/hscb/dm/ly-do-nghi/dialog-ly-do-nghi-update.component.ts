import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import * as dateFormat from 'dateformat';
// Translate
import { CommonService } from '../../../../services/common.service';
import { HscbService } from '../../../../services/hscb.service';

interface select {
  value: number;
  display: string;
}
@Component({
  selector: 'app-dialog-ly-do-nghi-update',
  templateUrl: './dialog-ly-do-nghi-update.component.html',
})
export class DialogUpdateLyDoNghiComponent implements OnInit {
  updateForm: FormGroup;
  submitted = false;
  lstloailydonghi: select[] = [];
  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    public dialogRef: MatDialogRef<DialogUpdateLyDoNghiComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.loadcombo();
    this.createForm();
  }
  createForm() {
    this.updateForm = this.fb.group(
      {
        id_loaiLyDoNghi: [null],
        code: [null],
        name:[null],
        sortOrder: [null]
      });
    if (this.data) {
      this.updateForm.setValue({
        id_loaiLyDoNghi: this.data.id_loaiLyDoNghi,
        code: this.data.code,
        name: this.data.name,
        sortOrder: this.data.sortOrder
      });
    }
  }
  loadcombo(){
    this.getLoaiLyDoNghi();
  }
  getLoaiLyDoNghi(){
    this.lstloailydonghi = [];
    this.hscbService.getLoaiLyDoNghi({}, this.common.HSCB_LyDoNghi.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstloailydonghi.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    if (!this.data) {
      this.hscbService.insertLyDoNghi(this.updateForm.value, this.common.HSCB_LyDoNghi.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.hscbService.editLyDoNghi({
        data: this.updateForm.value,
        condition: { id: this.data.id }
      }, this.common.HSCB_LyDoNghi.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
