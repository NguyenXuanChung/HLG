import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators, RequiredValidator } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import * as dateFormat from 'dateformat';
// Translate
import { CommonService } from '../../../../services/common.service';
import { HscbService } from '../../../../services/hscb.service';

interface select {
  value: number;
  display: string;
}
@Component({
  selector: 'app-dialog-hinh-thuc-khen-thuong-update',
  templateUrl: './dialog-hinh-thuc-khen-thuong-update.component.html',
})
export class DialogUpdateHinhThucKhenThuongComponent implements OnInit {
  updateForm: FormGroup;
  submitted = false;
  lstloaihinhthuckhenthuong: select[] = [];
  lstcapquyetdinh: select[] = [];
  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    public dialogRef: MatDialogRef<DialogUpdateHinhThucKhenThuongComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.loadcombo();
    this.createForm();
  }
  createForm() {
    this.updateForm = this.fb.group(
      {
        code : [null],
        name: ['', [Validators.required]],
        soThangLenLuongSom: [null],
        id_loaiHinhThucKhenThuong: [null],
        id_capQuyetDinh : [null],
        sortOrder: [null]
      });
    if (this.data) {
      this.updateForm.setValue({
        code : this.data.code,
        name : this.data.name,
        soThangLenLuongSom : this.data.soThangLenLuongSom,
        id_loaiHinhThucKhenThuong: this.data.id_loaiHinhThucKhenThuong,
        id_capQuyetDinh : this.data.id_capQuyetDinh,
        sortOrder : this.data.sortOrder
      });
      this.f.soThangLenLuongSom.setValue(parseInt(this.f.soThangLenLuongSom.value, 10));
      this.f.id_loaiHinhThucKhenThuong.setValue(parseInt(this.f.id_loaiHinhThucKhenThuong.value, 10));
      this.f.id_capQuyetDinh.setValue(parseInt(this.f.id_capQuyetDinh.value, 10));
    }
  }
  loadcombo(){
    this.loadLoaiHinhThucKhenThuong();
    this.loadCapQuyetDinh();
  }
  loadLoaiHinhThucKhenThuong(){
    this.lstloaihinhthuckhenthuong = [];
    this.hscbService.getLoaiHinhThucKhenThuong({}, this.common.HSCB_HinhThucKhenThuong.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstloaihinhthuckhenthuong.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  loadCapQuyetDinh(){
    this.lstcapquyetdinh = [];
    this.hscbService.getCapQuyetDinh({}, this.common.HSCB_HinhThucKhenThuong.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstcapquyetdinh.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    if (!this.data) {
      this.hscbService.insertHinhThucKhenThuong(this.updateForm.value, this.common.HSCB_HinhThucKhenThuong.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.hscbService.updateHinhThucKhenThuong({
        data: this.updateForm.value,
        condition: { id: this.data.id }
      }, this.common.HSCB_HinhThucKhenThuong.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
