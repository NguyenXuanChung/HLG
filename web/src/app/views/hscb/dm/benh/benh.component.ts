import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { HscbService } from '../../../../services/hscb.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CbAddComponent } from './../cb-add.component';
import { CbEditComponent } from './../cb-edit.component';

import { ConfirmationDialogComponent } from '../../../shared/confirmation-dialog/confirmation-dialog.component';
// Translate
import { CommonService } from '../../../../services/common.service';
import { StorageService } from '../../../../services/storage.service';
import { AuthConstants } from '../../../../config/auth-constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-benh',
  templateUrl: './benh.component.html'
})
export class BenhComponent implements OnInit {
  displayedColumns1 = ['code', 'name', 'actions'];
  displayedColumns2 = ['code', 'name', 'actions'];
  dataSource1: MatTableDataSource<any>;
  dataSource2: MatTableDataSource<any>;
  rowPhanLoai: any;
  selectedRow1: boolean;
  selectedRow2: boolean;
  loadTable1: boolean = true;
  loadTable2: boolean = true;
  @ViewChild(MatSort) sort: MatSort;
  permission: any;
  constructor(
    private common: CommonService,
    private hscb: HscbService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  ngOnInit() {
    this.loadTable1 = false;
    this.onSearch('hscb.dmPhanLoaiBenh');
    if (!this.permission.HSCB_Benh._view) {
      this.router.navigate(['/']);
    }
  }

  onSearch(type: string) {
    if (type === 'hscb.dmPhanLoaiBenh') {
      this.getPhanLoaiBenh();
    } else if (type === 'hscb.dmBenh') {
      this.getBenh(this.rowPhanLoai);
    }
  }

  getPhanLoaiBenh() {
    this.hscb.getPhanLoaiBenh({ status: 1 }, this.common.HSCB_Benh.View).subscribe(res => {
      this.loadTable1 = true;
      this.rowPhanLoai = null;
      this.dataSource2 = new MatTableDataSource();
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource1 = new MatTableDataSource();
      } else {
        this.dataSource1 = new MatTableDataSource(res.data);
        this.dataSource1.sort = this.sort;
      }
    });
  }

  getBenh(row: any) {
    this.loadTable2 = false;

    if (!this.selectedRow1) {
      this.selectedRow1 = row;
    } else {
      this.selectedRow1 = row;
    }
    if (row) {
      this.rowPhanLoai = row;
    }
    this.hscb.getBenh({ id_phanLoaiBenh: this.rowPhanLoai.id, status: 1 },
      this.common.HSCB_Benh.View).subscribe(res => {
        this.loadTable2 = true;
        if (res.error) {
          // this.common.messageErr(res);
          this.dataSource2 = new MatTableDataSource();
        } else {
          this.dataSource2 = new MatTableDataSource(res.data);
          this.dataSource2.sort = this.sort;
        }
      });
  }

  // data sẽ có dạng [row.id, (tên danh mục)]
  openAddDialog(data: any): void {
    const dialogRef = this.dialog.open(CbAddComponent, {
      width: '700px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch(data[1]);
        this.common.messageRes(result);
      }
    });
  }

  openEditDialog(data: any): void {
    const dialogRef = this.dialog.open(CbEditComponent, {
      width: '700px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch(data[1]);
        this.common.messageRes(result);
      }
    });
  }

  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data[0].name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        let resDelete;
        if (data[1] === 'hscb.dmPhanLoaiBenh') {
          resDelete = this.hscb.deletePhanLoaiBenh({ id: data[0].id }, this.common.HSCB_Benh.Delete);
        } else if (data[1] === 'hscb.dmBenh') {
          resDelete = this.hscb.deleteBenh({ id: data[0].id }, this.common.HSCB_Benh.Delete);
        }
        resDelete.subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.onSearch(data[1]);
            this.common.messageRes(res.message);
          }
        });
      }
    });
  }

  onSelectedRow(row: any) {
    if (!this.selectedRow2) {
      this.selectedRow2 = row;
    } else {
      this.selectedRow2 = row;
    }
  }
}
