import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HscbService } from './../../../services/hscb.service';
import { ToastrService } from 'ngx-toastr';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
@Component({
    selector: 'app-cb-2truong-edit',
    templateUrl: './cb-2truong-edit.component.html'
})
export class Cb2TruongEditComponent implements OnInit {
    hscbForm: FormGroup;
    lastUpdateForm: FormGroup;
    submitted = false;
    // convenience getter for easy access to form fields
    get f() { return this.hscbForm.controls; }

    constructor(
        private common: CommonService,
        private fb: FormBuilder,
        private hscb: HscbService,
        private toastrService: ToastrService,
        public dialogRef: MatDialogRef<Cb2TruongEditComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        dialogRef.disableClose = true;
    }

    ngOnInit() {
        this.createForm();
    }
    createForm() {
        this.hscbForm = this.fb.group(
            {
                code: ['', [Validators.required]],
                name: ['', [Validators.required]],
            });
        this.hscbForm.setValue({
            'code': this.data[0].code,
            'name': this.data[0].name,
        });
    }
    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.hscbForm.invalid) {
            return;
        }
        this.lastUpdateForm = this.fb.group({
            data: this.fb.group({
                code: [this.hscbForm.get('code').value],
                name: [this.hscbForm.get('name').value],
            }),
            condition: this.fb.group({
                id: [this.data[0].id]
            })
        });
        // Edit scyk kiểm tra xem thuộc loại danh mục gì để update theo danh mục tương ứng
        let editRespone;
        if (this.data[1] === 'hscb.dmNhomViTriVieclam') {
            editRespone = this.hscb.editNhomViTriViecLam(this.lastUpdateForm.value, this.common.HSCB_ViTriViecLam.Update);
        } else if (this.data[1] === 'hscb.dmViTriVieclam') {
            editRespone = this.hscb.editViTriViecLam(this.lastUpdateForm.value, this.common.HSCB_ViTriViecLam.Update);
        }

        if (editRespone) {
            editRespone.subscribe(res => {
                if (res.error) {
                    this.common.messageErr(res);
                } else {
                    this.dialogRef.close(res.message);
                }
            });
        }
    }
    onNoClick(): void {
        this.dialogRef.close();
    }
}
