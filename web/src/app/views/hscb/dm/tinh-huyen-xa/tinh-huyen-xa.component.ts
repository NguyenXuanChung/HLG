import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { HscbService } from '../../../../services/hscb.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CbAddComponent } from './../cb-add.component';
import { CbEditComponent } from './../cb-edit.component';

import { ConfirmationDialogComponent } from '../../../shared/confirmation-dialog/confirmation-dialog.component';
// Translate
import { CommonService } from '../../../../services/common.service';
import { StorageService } from '../../../../services/storage.service';
import { AuthConstants } from '../../../../config/auth-constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tinh-huyen-xa',
  templateUrl: './tinh-huyen-xa.component.html'
})
export class TinhHuyenXaComponent implements OnInit {
  displayedColumns1 = ['code', 'name', 'actions'];
  displayedColumns2 = ['code', 'name', 'actions'];
  displayedColumns3 = ['code', 'name', 'actions'];
  dataSource1: MatTableDataSource<any>;
  dataSource2: MatTableDataSource<any>;
  dataSource3: MatTableDataSource<any>;
  rowTinh: any;
  rowHuyen: any;
  selectedRow1: boolean;
  selectedRow2: boolean;
  selectedRow3: boolean;
  loadTable1: boolean = true;
  loadTable2: boolean = true;
  loadTable3: boolean = true;
  @ViewChild(MatSort) sort: MatSort;
  permission: any;
  constructor(
    private common: CommonService,
    private hscb: HscbService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  ngOnInit() {
    this.loadTable1 = false;
    this.onSearch('hscb.dmTinh');
    if (!this.permission.HSCB_Tinh_Huyen_Xa._view) {
      this.router.navigate(['/']);
    }
  }

  onSearch(type: string) {
    if (type === 'hscb.dmTinh') {
      this.getTinh();
    } else if (type === 'hscb.dmHuyen') {
      this.getHuyen(this.rowTinh);
    } else if (type === 'hscb.dmXa') {
      this.getXa(this.rowHuyen);
    }
  }

  getTinh() {
    this.hscb.getDMTinh({ status: 1 }, this.common.HSCB_Tinh_Huyen_Xa.View).subscribe(res => {
      this.loadTable1 = true;
      this.rowHuyen = null;
      this.rowTinh = null;
      this.dataSource2 = new MatTableDataSource();
      this.dataSource3 = new MatTableDataSource();
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource1 = new MatTableDataSource();
      } else {
        this.dataSource1 = new MatTableDataSource(res.data);
        this.dataSource1.sort = this.sort;
        this.dataSource3 = new MatTableDataSource();
      }
    });
  }

  getHuyen(row: any) {
    this.loadTable2 = false;

    if (!this.selectedRow1) {
      this.selectedRow1 = row;
    } else {
      this.selectedRow1 = row;
    }
    if (row) {
      this.rowTinh = row;
    }
    this.rowHuyen = null;
    this.dataSource3 = new MatTableDataSource();
    this.hscb.getDMHuyen({ id_tinh: this.rowTinh.id, status: 1 },
      this.common.HSCB_Tinh_Huyen_Xa.View).subscribe(res => {
        this.loadTable2 = true;
        if (res.error) {
          // this.common.messageErr(res);
          this.dataSource2 = new MatTableDataSource();
        } else {
          this.dataSource2 = new MatTableDataSource(res.data);
          this.dataSource2.sort = this.sort;
        }
      });
  }

  getXa(row: any) {
    this.loadTable3 = false;

    if (!this.selectedRow2) {
      this.selectedRow2 = row;
    } else {
      this.selectedRow2 = row;
    }
    this.rowHuyen = row;
    this.hscb.getDMXa({ id_huyen: this.rowHuyen.id, status: 1 },
      this.common.HSCB_Tinh_Huyen_Xa.View).subscribe(res => {
        this.loadTable3 = true;
        if (res.error) {
          // this.common.messageErr(res);
          this.dataSource3 = new MatTableDataSource();
        } else {
          this.dataSource3 = new MatTableDataSource(res.data);
          this.dataSource3.sort = this.sort;
        }
      });
  }
  //#endregion

  // data sẽ có dạng [row.id, (tên danh mục)]
  openAddDialog(data: any): void {
    const dialogRef = this.dialog.open(CbAddComponent, {
      width: '700px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch(data[1]);
        this.common.messageRes(result);
      }
    });
  }

  openEditDialog(data: any): void {
    const dialogRef = this.dialog.open(CbEditComponent, {
      width: '700px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch(data[1]);
        this.common.messageRes(result);
      }
    });
  }

  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data[0].name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        let resDelete;
        if (data[1] === 'hscb.dmTinh') {
          resDelete = this.hscb.deleteTinh({ id: data[0].id }, this.common.HSCB_Tinh_Huyen_Xa.Delete);
        } else if (data[1] === 'hscb.dmHuyen') {
          resDelete = this.hscb.deleteHuyen({ id: data[0].id }, this.common.HSCB_Tinh_Huyen_Xa.Delete);
        } else if (data[1] === 'hscb.dmXa') {
          resDelete = this.hscb.deleteXa({ id: data[0].id }, this.common.HSCB_Tinh_Huyen_Xa.Delete);
        }
        resDelete.subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.onSearch(data[1]);
            this.common.messageRes(res.message);
          }
        });
      }
    });
  }

  onSelectedRow(row: any) {
    if (!this.selectedRow3) {
      this.selectedRow3 = row;
    } else {
      this.selectedRow3 = row;
    }
  }
}
