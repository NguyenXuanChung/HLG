import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HscbService } from './../../../services/hscb.service';
// Translate
import { CommonService } from './../../../services/common.service';
@Component({
    selector: 'app-cb-2truong-add',
    templateUrl: './cb-2truong-add.component.html'
})
export class Cb2TruongAddComponent implements OnInit {
    hscbForm: FormGroup;
    submitted = false;
    // convenience getter for easy access to form fields
    get f() { return this.hscbForm.controls; }

    constructor(
        private common: CommonService,
        private fb: FormBuilder,
        private hscb: HscbService,
        public dialogRef: MatDialogRef<Cb2TruongAddComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        dialogRef.disableClose = true;
    }

    ngOnInit() {
        this.createForm();
    }
    createForm() {
        this.hscbForm = this.fb.group(
            {
                code: ['', [Validators.required]],
                name: ['', [Validators.required]],
            });
    }

    onSubmit() {
        this.submitted = true;
        this.hscbForm.enable();
        // stop here if form is invalid
        if (this.hscbForm.invalid) {
            return;
        }
        // ADD cb kiểm tra xem thuộc loại danh mục gì để insert theo danh mục tương ứng
        let insertRespone;
        if (this.data[1] === 'hscb.dmNhomViTriVieclam') {
            insertRespone = this.hscb.insertNhomViTriViecLam(this.hscbForm.value, this.common.HSCB_ViTriViecLam.Insert);
        } else if (this.data[1] === 'hscb.dmViTriVieclam') {
            insertRespone = this.hscb.insertViTriViecLam({ id_nhomViTri: this.data[0], ...this.hscbForm.value }, 
            this.common.HSCB_ViTriViecLam.Insert);
        }

        if (insertRespone) {
            insertRespone.subscribe(res => {
                if (res.error) {
                    this.common.messageErr(res);
                } else {
                    this.dialogRef.close(res.message);
                }
            });
        }
    }
    onNoClick(): void {
        this.dialogRef.close();
    }
}
