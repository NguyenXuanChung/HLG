import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HscbService } from './../../../services/hscb.service';
// Translate
import { CommonService } from './../../../services/common.service';
@Component({
    selector: 'app-cb-add',
    templateUrl: './cb-add.component.html'
})
export class CbAddComponent implements OnInit {
    hscbForm: FormGroup;
    submitted = false;
    // convenience getter for easy access to form fields
    get f() { return this.hscbForm.controls; }

    constructor(
        private common: CommonService,
        private fb: FormBuilder,
        private hscb: HscbService,
        public dialogRef: MatDialogRef<CbAddComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        dialogRef.disableClose = true;
    }

    ngOnInit() {
        this.createForm();
    }
    createForm() {
        this.hscbForm = this.fb.group(
            {
                code: ['', [Validators.required]],
                name: ['', [Validators.required]],
                sortOrder: [0, [Validators.required, Validators.min(0)]],
            });
    }

    onSubmit() {
        this.submitted = true;
        this.hscbForm.enable();
        // stop here if form is invalid
        if (this.hscbForm.invalid) {
            return;
        }
        // ADD cb kiểm tra xem thuộc loại danh mục gì để insert theo danh mục tương ứng
        let insertRespone;
        if (this.data[1] === 'hscb.dmTinh') {
            insertRespone = this.hscb.insertTinh(this.hscbForm.value, this.common.HSCB_Tinh_Huyen_Xa.Insert);
        } else if (this.data[1] === 'hscb.dmHuyen') {
            insertRespone = this.hscb.insertHuyen({ id_tinh: this.data[0], ...this.hscbForm.value }, 
            this.common.HSCB_Tinh_Huyen_Xa.Insert);
        } else if (this.data[1] === 'hscb.dmXa') {
            insertRespone = this.hscb.insertXa({ id_huyen: this.data[0], ...this.hscbForm.value }, 
            this.common.HSCB_Tinh_Huyen_Xa.Insert);
        } else if (this.data[1] === 'dm.QuocGia') {
            insertRespone = this.hscb.insertQuocGia(this.hscbForm.value);
        } else if (this.data[1] === 'dm.TonGiao') {
            insertRespone = this.hscb.insertTonGiao(this.hscbForm.value);
        } else if (this.data[1] === 'dm.DanToc') {
            insertRespone = this.hscb.insertDanToc(this.hscbForm.value);
        } else if (this.data[1] === 'hscb.dmPhanLoaiTrinhDoTinHoc') {
            insertRespone = this.hscb.insertPhanLoaiTrinhDoTinHoc(this.hscbForm.value, this.common.HSCB_TrinhDoTinHoc.Insert);
        } else if (this.data[1] === 'hscb.dmTrinhDoTinHoc') {
            insertRespone = this.hscb.insertTrinhDoTinHoc({ id_phanloaitrinhdo_tinhoc: this.data[0], ...this.hscbForm.value }, 
            this.common.HSCB_TrinhDoTinHoc.Insert);
        } else if (this.data[1] === 'hscb.dmPhanLoaiTrinhDoNgoaiNgu') {
            insertRespone = this.hscb.insertPhanLoaiTrinhDoNgoaiNgu(this.hscbForm.value, this.common.HSCB_TrinhDoNgoaiNgu.Insert);
        } else if (this.data[1] === 'hscb.dmTrinhDoNgoaiNgu') {
            insertRespone = this.hscb.insertTrinhDoNgoaiNgu({ id_phanLoaiTrinhDo_ngoaiNgu: this.data[0], ...this.hscbForm.value }, 
            this.common.HSCB_TrinhDoNgoaiNgu.Insert);
        } else if (this.data[1] === 'hscb.dmPhanLoaiBenh') {
            insertRespone = this.hscb.insertPhanLoaiBenh(this.hscbForm.value, this.common.HSCB_Benh.Insert);
        } else if (this.data[1] === 'hscb.dmBenh') {
            insertRespone = this.hscb.insertBenh({ id_phanLoaiBenh: this.data[0], ...this.hscbForm.value }, 
            this.common.HSCB_Benh.Insert);
        } else if (this.data[1] === 'hscb.dmNganh') {
            insertRespone = this.hscb.insertNganh(this.hscbForm.value, this.common.HSCB_ChuyenNganh.Insert);
        } else if (this.data[1] === 'hscb.dmChuyenNganh') {
            insertRespone = this.hscb.insertChuyenNganh({ id_nganh: this.data[0], ...this.hscbForm.value }, 
            this.common.HSCB_ChuyenNganh.Insert);
        } else if (this.data[1] === 'hscb.dmXepLoaiSucKhoe') {
            insertRespone = this.hscb.insertXepLoaiSucKhoe(this.hscbForm.value, this.common.HSCB_XepLoaiSucKhoe.Insert);
        } else if (this.data[1] === 'hscb.dmXepLoai') {
            insertRespone = this.hscb.insertXepLoai(this.hscbForm.value, this.common.HSCB_XepLoai.Insert);
        } else if (this.data[1] === 'hscb.dmTinhTrangHonNhan') {
            insertRespone = this.hscb.insertTinhTrangHonNhan(this.hscbForm.value, this.common.HSCB_TinhTrangHonNhan.Insert);
        } else if (this.data[1] === 'hscb.dmVanBang') {
            insertRespone = this.hscb.insertVanBang(this.hscbForm.value, this.common.HSCB_VanBang.Insert);
        } else if (this.data[1] === 'hscb.dmLyLuanChinhTri') {
            insertRespone = this.hscb.insertLyLuanChinhTri(this.hscbForm.value, this.common.HSCB_LyLuanChinhTri.Insert);
        } else if (this.data[1] === 'hscb.dmTrinhDoQuanLy') {
            insertRespone = this.hscb.insertTrinhDoQuanLy(this.hscbForm.value, this.common.HSCB_TrinhDoQuanLy.Insert);
        } else if (this.data[1] === 'hscb.dmQuanLyBenhVien') {
            insertRespone = this.hscb.insertQuanLyBenhVien(this.hscbForm.value, this.common.HSCB_QuanLyBenhVien.Insert);
        } else if (this.data[1] === 'hscb.dmNgoaiNgu') {
            insertRespone = this.hscb.insertNgoaiNgu(this.hscbForm.value, this.common.HSCB_NgoaiNgu.Insert);
        } else if(this.data[1] === 'hscb.dmNoiBanHanh'){
            insertRespone = this.hscb.insertNoiBanHanh(this.hscbForm.value, this.common.HSCB_NoiBanHanh.insert);
        }

        if (insertRespone) {
            insertRespone.subscribe(res => {
                if (res.error) {
                    this.common.messageErr(res);
                } else {
                    this.dialogRef.close(res.message);
                }
            });
        }
    }
    onNoClick(): void {
        this.dialogRef.close();
    }
}
