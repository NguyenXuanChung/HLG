import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import * as dateFormat from 'dateformat';
// Translate
import { CommonService } from '../../../../services/common.service';
import { HscbService } from '../../../../services/hscb.service';

interface select {
  value: number;
  display: string;
}
@Component({
  selector: 'app-dialog-nguoi-ky-bao-cao-update',
  templateUrl: './dialog-nguoi-ky-bao-cao-update.component.html',
})
export class DialogUpdateNguoiKyBaoCaoComponent implements OnInit {
  updateForm: FormGroup;
  submitted = false;
  lstloaicanbo: select[] = [];
  lstgioitinh: select[] = [];
  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    public dialogRef: MatDialogRef<DialogUpdateNguoiKyBaoCaoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.loadcombo();
    this.createForm();
  }
  createForm() {
    this.updateForm = this.fb.group(
      {
        name: [null],
        id_loaiCanBo: [null],
        gioiTinh:[null],
        sortOrder: [null]
      });
    if (this.data) {
      this.updateForm.setValue({
        id_loaiCanBo : this.data.id_loaiCanBo,
        name : this.data.name,
        gioiTinh: (this.data.gioiTinh == null)? '0' : '' + this.data.gioiTinh.data[0],
        sortOrder : this.data.sortOrder
      });
      this.f.gioiTinh.setValue(parseInt(this.f.gioiTinh.value, 10));
    }
  }
  loadcombo(){
    this.getLoaiCanBo();
    this.getGioiTinh();
  }
  getLoaiCanBo(){
    this.lstloaicanbo = [];
    this.hscbService.getLoaiCanBo({}, this.common.HSCB_NguoiKyBaoCao.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstloaicanbo.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getGioiTinh(){
    this.lstgioitinh = [];
    this.lstgioitinh.push({ value: 1, display: "Nam"})
    this.lstgioitinh.push({ value: 0, display: "Nữ"})
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    if (!this.data) {
      this.hscbService.insertNguoiKyBaoCao(this.updateForm.value, this.common.HSCB_NguoiKyBaoCao.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.hscbService.editNguoiKyBaoCao({
        data: this.updateForm.value,
        condition: { id: this.data.id }
      }, this.common.HSCB_NguoiKyBaoCao.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
