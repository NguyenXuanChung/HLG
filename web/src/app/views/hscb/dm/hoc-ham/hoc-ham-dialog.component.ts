import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import * as dateFormat from 'dateformat';

// Translate
import { CommonService } from './../../../../services/common.service';
import { LanguageService } from './../../../../services/language.service';
import { HscbService } from './../../../../services/hscb.service';

@Component({
  selector: 'app-hoc-ham-dialog',
  templateUrl: './hoc-ham-dialog.component.html',
})
export class HocHamDialogComponent implements OnInit {
  hocHamForm: FormGroup;
  lastUpdateForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.hocHamForm.controls; }

  constructor(
    private languageService: LanguageService,
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    private toastrService: ToastrService,
    public dialogRef: MatDialogRef<HocHamDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.currentLanguage();
  }
  currentLanguage() {
    return this.languageService.translate.currentLang;
  }
  createForm() {
    this.hocHamForm = this.fb.group(
      {
        code: ['', [Validators.required]],
        name: ['', [Validators.required]],
        tenVietTat: [''],
        sortOrder: [0, [Validators.required, Validators.min(0)]],
      });
    if (this.data) {
      this.hocHamForm.setValue({
        'code': this.data.code,
        'name': this.data.name,
        'tenVietTat': this.data.tenVietTat,
        'sortOrder': this.data.sortOrder,
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.hocHamForm.invalid) {
      return;
    }
    if (!this.data) {
      this.hscbService.insertHocHam(this.hocHamForm.value, this.common.HSCB_HocHam.Insert)
      .subscribe(res => {
        if (res.error) {
          this.toastrService.error(res.error.message);
        } else {
          this.dialogRef.close(res.message);
        }
      }, err => {
        this.toastrService.error(err.error.message);
      });
    } else {
      console.log({ data: this.hocHamForm.value, condition: { id: this.data.id } });

      this.hscbService.editHocHam({data: this.hocHamForm.value, condition: { id: this.data.id }}, this.common.HSCB_HocHam.Update)
      .subscribe(res => {
        if (res.error) {
          this.toastrService.error(res.error.message);
        } else {
          this.dialogRef.close(res.message);
        }
      }, err => {
        this.toastrService.error(err.error.message);
      });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
