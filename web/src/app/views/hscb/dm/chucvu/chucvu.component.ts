import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../../shared/confirmation-dialog/confirmation-dialog.component';

// Translate
import { CommonService } from '../../../../services/common.service';
import { StorageService } from '../../../../services/storage.service';
import { AuthConstants } from '../../../../config/auth-constants';
import { Router } from '@angular/router';
import { ChamCongService } from '../../../../services/chamcong.service';
import  { HscbService } from'../../../../services/hscb.service';
import { DialogUpdateChucVuComponent } from './dialog-chucvu-update.component';

@Component({
  selector: 'app-chucvu',
  templateUrl: './chucvu.component.html'
})
export class ChucVuComponent implements OnInit {
  displayedColumns = ['index', 'code', 'name', 'tenVietTat','laLanhDao','moTa', 'actions'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  searchForm: FormGroup;
  selectedRow: any;
  permission: any;
  get fs() { return this.searchForm.controls; }

  constructor(
    private fb: FormBuilder,
    private ccService: ChamCongService,
    private hscbService: HscbService,
    private common: CommonService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  ngOnInit() {
    this.onSearch();
    if (!this.permission.HSCB_ChucVu._view) {
      this.router.navigate(['/']);
    }
  }
  onSearch() {
    this.getChucVu();
  }

  getChucVu() {
    this.hscbService.getDanhMucChucVu({}, this.common.HSCB_ChucVu.View).subscribe(res => {
      if (res.error) {
        this.dataSource = new MatTableDataSource();
        this.common.messageErr(res);
      } else {
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  openChucVuDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogUpdateChucVuComponent, {
      width: '700px',
      height: '80%',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }

  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.hscbService.deleteChucVu({ id: data.id }, this.common.HSCB_ChucVu.Delete).subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.onSearch();
            this.common.messageRes(res.message);
          }
        });
      }
    });
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
