import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from '../../../../services/common.service';
import { HscbService } from '../../../../services/hscb.service';

@Component({
  selector: 'app-dialog-chucvu-update',
  templateUrl: './dialog-chucvu-update.component.html',
})
export class DialogUpdateChucVuComponent implements OnInit {
  updateForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    public dialogRef: MatDialogRef<DialogUpdateChucVuComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.updateForm = this.fb.group(
      {
        code: [null],
        name: ['', [Validators.required]],
        tenVietTat:[null],
        laLanhDao: [0, [Validators.required]],
        moTa: [null]
      });
    if (this.data) {
      this.updateForm.setValue({
        'code': this.data.code,
        'name': this.data.name,
        'tenVietTat': this.data.tenVietTat,
        'laLanhDao': '' + this.data.laLanhDao.data[0],
        moTa: this.data.moTa
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
     this.f.laLanhDao.setValue(parseInt(this.f.laLanhDao.value, 10));
    if (!this.data) {
      this.hscbService.insertChucVu(this.updateForm.value, this.common.HSCB_ChucVu.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.hscbService.editChucVu({
        data: this.updateForm.value,
        condition: { id: this.data.id }
      }, this.common.HSCB_ChucVu.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
