import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { HscbService } from './../../../services/hscb.service';
import { ToastrService } from 'ngx-toastr';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
@Component({
    selector: 'app-cb-edit',
    templateUrl: './cb-edit.component.html'
})
export class CbEditComponent implements OnInit {
    hscbForm: FormGroup;
    lastUpdateForm: FormGroup;
    submitted = false;
    // convenience getter for easy access to form fields
    get f() { return this.hscbForm.controls; }

    constructor(
        private common: CommonService,
        private fb: FormBuilder,
        private hscb: HscbService,
        private toastrService: ToastrService,
        public dialogRef: MatDialogRef<CbEditComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        dialogRef.disableClose = true;
    }

    ngOnInit() {
        this.createForm();
    }
    createForm() {
        this.hscbForm = this.fb.group(
            {
                code: ['', [Validators.required]],
                name: ['', [Validators.required]],
                sortOrder: [0, [Validators.required, Validators.min(0)]],
            });
        this.hscbForm.setValue({
            'code': this.data[0].code,
            'name': this.data[0].name,
            'sortOrder': this.data[0].sortOrder,
        });
    }
    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.hscbForm.invalid) {
            return;
        }
        this.lastUpdateForm = this.fb.group({
            data: this.fb.group({
                code: [this.hscbForm.get('code').value],
                name: [this.hscbForm.get('name').value],
                sortOrder: [this.hscbForm.get('sortOrder').value],
            }),
            condition: this.fb.group({
                id: [this.data[0].id]
            })
        });
        // Edit scyk kiểm tra xem thuộc loại danh mục gì để update theo danh mục tương ứng
        let editRespone;
        if (this.data[1] === 'hscb.dmTinh') {
            editRespone = this.hscb.editTinh(this.lastUpdateForm.value, this.common.HSCB_Tinh_Huyen_Xa.Update);
        } else if (this.data[1] === 'hscb.dmHuyen') {
            editRespone = this.hscb.editHuyen(this.lastUpdateForm.value, this.common.HSCB_Tinh_Huyen_Xa.Update);
        } else if (this.data[1] === 'hscb.dmXa') {
            editRespone = this.hscb.editXa(this.lastUpdateForm.value, this.common.HSCB_Tinh_Huyen_Xa.Update);
        } else if (this.data[1] === 'dm.QuocGia') {
            editRespone = this.hscb.editQuocGia(this.lastUpdateForm.value);
        } else if (this.data[1] === 'dm.TonGiao') {
            editRespone = this.hscb.editTonGiao(this.lastUpdateForm.value);
        } else if (this.data[1] === 'dm.DanToc') {
            editRespone = this.hscb.editDanToc(this.lastUpdateForm.value);
        } else if (this.data[1] === 'hscb.dmPhanLoaiTrinhDoTinHoc') {
            editRespone = this.hscb.editPhanLoaiTrinhDoTinHoc(this.lastUpdateForm.value, this.common.HSCB_TrinhDoTinHoc.Update);
        } else if (this.data[1] === 'hscb.dmTrinhDoTinHoc') {
            editRespone = this.hscb.editTrinhDoTinHoc(this.lastUpdateForm.value, this.common.HSCB_TrinhDoTinHoc.Update);
        } else if (this.data[1] === 'hscb.dmPhanLoaiTrinhDoNgoaiNgu') {
            editRespone = this.hscb.editPhanLoaiTrinhDoNgoaiNgu(this.lastUpdateForm.value, this.common.HSCB_TrinhDoNgoaiNgu.Update);
        } else if (this.data[1] === 'hscb.dmTrinhDoNgoaiNgu') {
            editRespone = this.hscb.editTrinhDoNgoaiNgu(this.lastUpdateForm.value, this.common.HSCB_TrinhDoNgoaiNgu.Update);
        } else if (this.data[1] === 'hscb.dmPhanLoaiBenh') {
            editRespone = this.hscb.editPhanLoaiBenh(this.lastUpdateForm.value, this.common.HSCB_Benh.Update);
        } else if (this.data[1] === 'hscb.dmBenh') {
            editRespone = this.hscb.editBenh(this.lastUpdateForm.value, this.common.HSCB_Benh.Update);
        } else if (this.data[1] === 'hscb.dmNganh') {
            editRespone = this.hscb.editNganh(this.lastUpdateForm.value, this.common.HSCB_ChuyenNganh.Update);
        } else if (this.data[1] === 'hscb.dmChuyenNganh') {
            editRespone = this.hscb.editChuyenNganh(this.lastUpdateForm.value, this.common.HSCB_ChuyenNganh.Update);
        } else if (this.data[1] === 'hscb.dmXepLoaiSucKhoe') {
            editRespone = this.hscb.editXepLoaiSucKhoe(this.lastUpdateForm.value, this.common.HSCB_XepLoaiSucKhoe.Update);
        } else if (this.data[1] === 'hscb.dmXepLoai') {
            editRespone = this.hscb.editXepLoai(this.lastUpdateForm.value, this.common.HSCB_XepLoai.Update);
        } else if (this.data[1] === 'hscb.dmTinhTrangHonNhan') {
            editRespone = this.hscb.editTinhTrangHonNhan(this.lastUpdateForm.value, this.common.HSCB_TinhTrangHonNhan.Update);
        } else if (this.data[1] === 'hscb.dmVanBang') {
            editRespone = this.hscb.editVanBang(this.lastUpdateForm.value, this.common.HSCB_VanBang.Update);
        } else if (this.data[1] === 'hscb.dmLyLuanChinhTri') {
            editRespone = this.hscb.editLyLuanChinhTri(this.lastUpdateForm.value, this.common.HSCB_LyLuanChinhTri.Update);
        } else if (this.data[1] === 'hscb.dmTrinhDoQuanLy') {
            editRespone = this.hscb.editTrinhDoQuanLy(this.lastUpdateForm.value, this.common.HSCB_TrinhDoQuanLy.Update);
        } else if (this.data[1] === 'hscb.dmQuanLyBenhVien') {
            editRespone = this.hscb.editQuanLyBenhVien(this.lastUpdateForm.value, this.common.HSCB_QuanLyBenhVien.Update);
        } else if (this.data[1] === 'hscb.dmNgoaiNgu') {
            editRespone = this.hscb.editNgoaiNgu(this.lastUpdateForm.value, this.common.HSCB_NgoaiNgu.Update);
        } else if(this.data[1] === 'hscb.dmNoiBanHanh'){
            editRespone = this.hscb.updateNoiBanHanh(this.lastUpdateForm.value, this.common.HSCB_NoiBanHanh.update);
        }

        if (editRespone) {
            editRespone.subscribe(res => {
                if (res.error) {
                    this.common.messageErr(res);
                } else {
                    this.dialogRef.close(res.message);
                }
            });
        }
    }
    onNoClick(): void {
        this.dialogRef.close();
    }
}
