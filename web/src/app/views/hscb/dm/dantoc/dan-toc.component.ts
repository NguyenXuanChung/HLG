import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import  { HscbService } from'../../../../services/hscb.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { ConfirmationDialogComponent } from '../../../shared/confirmation-dialog/confirmation-dialog.component';
import { CbAddComponent } from '../cb-add.component';
import { CbEditComponent } from '../cb-edit.component';
// Translate
import { CommonService } from '../../../../services/common.service';
import { StorageService } from '../../../../services/storage.service';
import { AuthConstants } from '../../../../config/auth-constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dan-toc',
  templateUrl: './dan-toc.component.html'
})
export class DanTocComponent implements OnInit {
  displayedColumns1 = ['code', 'name', 'actions'];
  dataSource1: MatTableDataSource<any>;
  selectedRow1: boolean;
  loadTable1: boolean = true;
  @ViewChild(MatSort) sort: MatSort;
  permission: any;
  constructor(
    private common: CommonService,
    private hscbService: HscbService,
    private toastrService: ToastrService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  ngOnInit() {
    this.loadTable1 = false;
    this.onSearch('dm.DanToc');
    if (!this.permission.DM_DanToc._view) {
      this.router.navigate(['/']);
    }
  }

  onSearch(type: string) {
    if (type === 'dm.DanToc') {
      this.getDanToc();
    }
  }
  
  getDanToc() {
    this.hscbService.getDanhMucDanToc({ status: 1 }, this.common.DM_DanToc.View).subscribe(res => {
      this.loadTable1 = true;
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource1 = new MatTableDataSource();
      } else {
        this.dataSource1 = new MatTableDataSource(res.data);
        this.dataSource1.sort = this.sort;
      }
    });
  }

  openAddDialog(data: any): void {
    const dialogRef = this.dialog.open(CbAddComponent, {
      width: '700px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch(data[1]);
        this.common.messageRes(result);
      }
    });
  }

  openEditDialog(data: any): void {
    const dialogRef = this.dialog.open(CbEditComponent, {
      width: '700px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch(data[1]);
        this.common.messageRes(result);
      }
    });
  }

  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.hscbService.deleteDanToc({ id: data.id }).subscribe(res => {
          if (res.error) {
            this.toastrService.error(res.error.message);
          } else {
            this.onSearch(data[1]);
            this.toastrService.success(res.message);
          }
        }, err => {
          this.toastrService.error(err.error);
        });
      }
    });
  }

  onSelectedRow(row: any) {
    if (!this.selectedRow1) {
      this.selectedRow1 = row;
    } else {
      this.selectedRow1 = row;
    }
  }
}
