import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import * as dateFormat from 'dateformat';
import { CommonService } from '../../../../services/common.service';

// Translate
import { LanguageService } from '../../../../services/language.service';
import  { HscbService } from'../../../../services/hscb.service';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}

@Component({
  selector: 'app-thang-bang-luong-dialog',
  templateUrl: './thang-bang-luong-dialog.component.html',
})
export class ThangBangLuongDialogComponent implements OnInit {
  ThangBangLuongForm: FormGroup;
  lastUpdateForm: FormGroup;
  submitted = false;
  loaiNgachLuongs: select[] = [];
  // convenience getter for easy access to form fields
  get f() { return this.ThangBangLuongForm.controls; }

  constructor(
    private common: CommonService,
    private languageService: LanguageService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    private toastrService: ToastrService,
    public dialogRef: MatDialogRef<ThangBangLuongDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.loadCombo();
    this.createForm();
    this.currentLanguage();
  }
  currentLanguage() {
    return this.languageService.translate.currentLang;
  }
  createForm() {
    this.ThangBangLuongForm = this.fb.group(
      {
        code: [''],
        name: ['', [Validators.required]],
        id_loaiNgachLuong: ['', [Validators.required]]
      });
    if (this.data) {
      this.ThangBangLuongForm.setValue({
        'code': this.data.code,
        'name': this.data.name,
        'id_loaiNgachLuong': this.data.id_loaiNgachLuong
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.ThangBangLuongForm.invalid) {
      return;
    }
    if (!this.data) {
      this.hscbService.insertThangBangLuong(this.ThangBangLuongForm.value, this.common.HSCB_Luong_Co_Ban.Insert)
      .subscribe(res => {
        if (res.error) {
          this.toastrService.error(res.error.message);
        } else {
          this.dialogRef.close(res.message);
        }
      }, err => {
        this.toastrService.error(err.error.message);
      });
    } else {
      console.log({ data: this.ThangBangLuongForm.value, condition: { id: this.data.id } });

      this.hscbService.editThangBangLuong({data: this.ThangBangLuongForm.value, condition: { id: this.data.id }}, this.common.HSCB_Luong_Co_Ban.Update)
      .subscribe(res => {
        if (res.error) {
          this.toastrService.error(res.error.message);
        } else {
          this.dialogRef.close(res.message);
        }
      }, err => {
        this.toastrService.error(err.error.message);
      });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  loadCombo() {
    this.getLoaiNgachLuong();
  }

  getLoaiNgachLuong() {
    this.hscbService.getLoaiNgachLuong({}, this.common.HSCB_Luong_Co_Ban.view).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        res.data.forEach(element => {
          this.loaiNgachLuongs.push({ value: parseInt(element.id, 10), display: element.name });
        });
      }
    });
  }
}
