import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import * as dateFormat from 'dateformat';
import { CommonService } from '../../../../services/common.service';

// Translate
import { LanguageService } from '../../../../services/language.service';
import  { HscbService } from'../../../../services/hscb.service';

@Component({
  selector: 'app-he-so-luong-update-dialog',
  templateUrl: './he-so-luong-update-dialog.component.html',
})
export class HeSoLuongUpdateDialogComponent implements OnInit {
  HeSoLuongForm: FormGroup;
  lastUpdateForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.HeSoLuongForm.controls; }

  constructor(
    private common: CommonService,
    private languageService: LanguageService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    private toastrService: ToastrService,
    public dialogRef: MatDialogRef<HeSoLuongUpdateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.currentLanguage();
  }
  currentLanguage() {
    return this.languageService.translate.currentLang;
  }
  createForm() {
    this.HeSoLuongForm = this.fb.group(
      {
        ngayApDung: [null, [Validators.required]],
        heSo: ['', [Validators.required]],
      });
    if (this.data) {
      this.HeSoLuongForm.setValue({
        'ngayApDung': this.data.ngayApDung,
        'heSo': this.data.heSo,
      });
      if (this.data.ngayApDung) {
        this.f.ngayApDung.setValue(dateFormat(this.data.ngayApDung, 'yyyy-mm-dd', true));
      }
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.HeSoLuongForm.invalid) {
      return;
    }
    if (this.f.ngayApDung.value === '') {
      return;
    }
    if (this.f.heSo.value === '') {
      return;
    }
    this.lastUpdateForm = this.fb.group({
        data: this.fb.group({
          ngayApDung: [this.HeSoLuongForm.get('ngayApDung').value],
          heSo: [this.HeSoLuongForm.get('heSo').value],
        }),
        condition: this.fb.group({
            id: [this.data.id]
        })
    });
    let editRespone;
    editRespone = this.hscbService.editHeSoLuong({data: this.HeSoLuongForm.value, condition: { id: this.data.id }}, this.common.HSCB_Luong_Co_Ban.update);
    if (editRespone) {
      editRespone.subscribe(res => {
        if (res.error) {
          this.toastrService.error(res.error.message);
        } else {
          this.dialogRef.close(res.message);
        }
      }, err => {
        this.toastrService.error(err.error.message);
      });
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
