import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import * as dateFormat from 'dateformat';
import { CommonService } from '../../../../services/common.service';

// Translate
import { LanguageService } from '../../../../services/language.service';
import  { HscbService } from'../../../../services/hscb.service';

@Component({
  selector: 'app-ngach-luong-add-dialog',
  templateUrl: './ngach-luong-add-dialog.component.html',
})
export class NgachLuongAddDialogComponent implements OnInit {
  NgachLuongForm: FormGroup;
  lastUpdateForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.NgachLuongForm.controls; }

  constructor(
    private common: CommonService,
    private languageService: LanguageService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    private toastrService: ToastrService,
    public dialogRef: MatDialogRef<NgachLuongAddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.currentLanguage();
  }
  currentLanguage() {
    return this.languageService.translate.currentLang;
  }
  createForm() {
    this.NgachLuongForm = this.fb.group(
      {
        code: ['', [Validators.required]],
        name: ['', [Validators.required]],
        moTa: [''],
        apDungTuNgay: [null],
        apDungDenNgay: [null],
      });
    
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.NgachLuongForm.invalid) {
      return;
    }
    if (this.f.apDungTuNgay.value === '') {
      this.f.apDungTuNgay.setValue(null);
    }
    if (this.f.apDungDenNgay.value === '') {
      this.f.apDungDenNgay.setValue(null);
    }
    let insertRespone;
    insertRespone = this.hscbService.insertNgachLuong({id_thangBangLuong: this.data, ...this.NgachLuongForm.value}, this.common.HSCB_Luong_Co_Ban.Insert);
    
    if (insertRespone) {
      insertRespone.subscribe(res => {
        if (res.error) {
          this.toastrService.error(res.error.message);
        } else {
          this.dialogRef.close(res.message);
        } 
      }, err => {
        this.toastrService.error(err.error.message);
      });
    } 

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
