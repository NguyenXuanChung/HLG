import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import * as dateFormat from 'dateformat';
import { CommonService } from '../../../../services/common.service';

// Translate
import { LanguageService } from '../../../../services/language.service';
import  { HscbService } from'../../../../services/hscb.service';

@Component({
  selector: 'app-bac-luong-update-dialog',
  templateUrl: './bac-luong-update-dialog.component.html',
})
export class BacLuongUpdateDialogComponent implements OnInit {
  BacLuongForm: FormGroup;
  lastUpdateForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.BacLuongForm.controls; }

  constructor(
    private common: CommonService,
    private languageService: LanguageService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    private toastrService: ToastrService,
    public dialogRef: MatDialogRef<BacLuongUpdateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.currentLanguage();
  }
  currentLanguage() {
    return this.languageService.translate.currentLang;
  }
  createForm() {
    this.BacLuongForm = this.fb.group(
      {
        code: ['', [Validators.required]],
        name: ['', [Validators.required]],
        soThangGiuBac: ['', [Validators.required]],
        thuTuBacLuong: ['', [Validators.required]],
        laBacLuongCaoNhat: [0, [Validators.required]],
      });
    if (this.data) {
      this.BacLuongForm.setValue({
        'code': this.data.code,
        'name': this.data.name,
        'soThangGiuBac': this.data.soThangGiuBac,
        'thuTuBacLuong': this.data.thuTuBacLuong,
        'laBacLuongCaoNhat': '' + this.data.laBacLuongCaoNhat.data[0]
      });
      
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.BacLuongForm.invalid) {
      return;
    }
    this.f.laBacLuongCaoNhat.setValue(parseInt(this.f.laBacLuongCaoNhat.value, 10));
    this.lastUpdateForm = this.fb.group({
        data: this.fb.group({
          code: [this.BacLuongForm.get('code').value],
          name: [this.BacLuongForm.get('name').value],
          soThangGiuBac: [this.BacLuongForm.get('soThangGiuBac').value],
          thuTuBacLuong: [this.BacLuongForm.get('thuTuBacLuong').value],
          laBacLuongCaoNhat: [this.BacLuongForm.get('laBacLuongCaoNhat').value],
        }),
        condition: this.fb.group({
            id: [this.data.id]
        })
    });
    let editRespone;
    editRespone = this.hscbService.editBacLuong({data: this.BacLuongForm.value, condition: { id: this.data.id }}, this.common.HSCB_Luong_Co_Ban.update);
    if (editRespone) {
      editRespone.subscribe(res => {
        if (res.error) {
          this.toastrService.error(res.error.message);
        } else {
          this.dialogRef.close(res.message);
        }
      }, err => {
        this.toastrService.error(err.error.message);
      });
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
