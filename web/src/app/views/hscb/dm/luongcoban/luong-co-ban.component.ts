import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { HscbService } from '../../../../services/hscb.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ThangBangLuongDialogComponent } from './thang-bang-luong-dialog.component';
import { NgachLuongDialogComponent } from './ngach-luong-dialog.component';
import { NgachLuongAddDialogComponent } from './ngach-luong-add-dialog.component';
import { BacLuongAddDialogComponent } from './bac-luong-add-dialog.component';
import { BacLuongUpdateDialogComponent } from './bac-luong-update-dialog.component';
import { HeSoLuongAddDialogComponent } from './he-so-luong-add-dialog.component';
import { HeSoLuongUpdateDialogComponent } from './he-so-luong-update-dialog.component';
import { ConfirmationDialogComponent } from '../../../shared/confirmation-dialog/confirmation-dialog.component';
// Translate
import { CommonService } from './../../../../services/common.service';
import { StorageService } from '../../../../services/storage.service';
import { AuthConstants } from '../../../../config/auth-constants';
import { Router } from '@angular/router';
@Component({
  selector: 'app-luong-co-ban',
  templateUrl: './luong-co-ban.component.html'
})
export class LuongCoBanComponent implements OnInit {
  displayedColumns1 = ['code', 'name', 'actions'];
  displayedColumns2 = ['code', 'name', 'actions'];
  displayedColumns3 = ['code', 'name', 'actions'];
  displayedColumns4 = ['ngayApDung', 'heSo', 'actions'];
  dataSource1: MatTableDataSource<any>;
  dataSource2: MatTableDataSource<any>;
  dataSource3: MatTableDataSource<any>;
  dataSource4: MatTableDataSource<any>;
  rowThangBangLuong: any;
  rowNgachLuong: any;
  rowBacLuong: any;
  selectedRow1: boolean;
  selectedRow2: boolean;
  selectedRow3: boolean;
  selectedRow4: boolean;
  loadTable1: boolean = true;
  loadTable2: boolean = true;
  loadTable3: boolean = true;
  loadTable4: boolean = true;
  @ViewChild(MatSort) sort: MatSort;
  permission: any;

  constructor(
    private common: CommonService,
    private hscb: HscbService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  ngOnInit() {
    this.loadTable1 = false;
    this.onSearch('ThangBangLuong');
    if (!this.permission.SC_NghiemTrong._view) {
      this.router.navigate(['/']);
    }
  }
  onSearch(type: string) {
    if (type === 'ThangBangLuong') {
      this.getThangBangLuong();
    } else if (type === 'NgachLuong') {
      this.getNgachLuong(this.rowThangBangLuong);
    } else if (type === 'BacLuong') {
      this.getBacLuong(this.rowNgachLuong);
    } else if (type === 'HeSoLuong') {
      this.getHeSoLuong(this.rowBacLuong);
    }

  }
  //#region 
  getThangBangLuong() {
    this.hscb.getThangBangLuong({ status: 1 }, this.common.SC_NghiemTrong.View).subscribe(res => {
      console.log(res);
      this.loadTable1 = true;
      this.rowThangBangLuong = null;
      this.dataSource2 = new MatTableDataSource();
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource1 = new MatTableDataSource();
      } else {
        this.dataSource1 = new MatTableDataSource(res.data);
        this.dataSource1.sort = this.sort;
      }
    });
  }
  //#endregion

  //#region getSuCoNghiemTrong ((2))
  getNgachLuong(row: any) {
    this.loadTable2 = false;
    if (!this.selectedRow1) {
      this.selectedRow1 = row;
    } else {
      this.selectedRow1 = row;
    }
    if (row) {
      this.rowThangBangLuong = row;
    }
    this.hscb.getNgachLuong({ id_ThangBangLuong: this.rowThangBangLuong.id, status: 1 }, this.common.SC_NghiemTrong.View)
      .subscribe(res => {
        this.loadTable2 = true;
        if (res.error) {
          this.common.messageErr(res);
          this.dataSource2 = new MatTableDataSource();
        } else {
          this.dataSource2 = new MatTableDataSource(res.data);
          this.dataSource2.sort = this.sort;
        }
      });
  }

  getBacLuong(row: any) {
    this.loadTable3 = false;
    if (!this.selectedRow2) {
      this.selectedRow2 = row;
    } else {
      this.selectedRow2 = row;
    }
    if (row) {
      this.rowNgachLuong = row;
    }
    this.hscb.getBacLuong({ id_NgachLuong: this.rowNgachLuong.id, status: 1 }, this.common.SC_NghiemTrong.View)
      .subscribe(res => {
        this.loadTable3 = true;
        if (res.error) {
          this.common.messageErr(res);
          this.dataSource3 = new MatTableDataSource();
        } else {
          this.dataSource3 = new MatTableDataSource(res.data);
          this.dataSource3.sort = this.sort;
        }
      });
  }

  getHeSoLuong(row: any) {
    this.loadTable4 = false;
    if (!this.selectedRow3) {
      this.selectedRow3 = row;
    } else {
      this.selectedRow3 = row;
    }
    if (row) {
      this.rowBacLuong = row;
    }
    this.hscb.getHeSoLuong({ id_bacLuong: this.rowBacLuong.id, status: 1 }, this.common.HSCB_Luong_Co_Ban.View)
      .subscribe(res => {
        this.loadTable4 = true;
        if (res.error) {
          this.common.messageErr(res);
          this.dataSource4 = new MatTableDataSource();
        } else {
          this.dataSource4 = new MatTableDataSource(res.data);
          this.dataSource4.sort = this.sort;
        }
      });
  }
  //#endregion


  openThangBangLuongDialog(data: any): void {
    const dialogRef = this.dialog.open(ThangBangLuongDialogComponent, {
      width: '700px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch('ThangBangLuong');
        this.common.messageRes(result);
      }
    });
  }

  openNgachLuongAddDialog(data: any): void {
    const dialogRef = this.dialog.open(NgachLuongAddDialogComponent, {
      width: '700px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch('NgachLuong');
        this.common.messageRes(result);
      }
    });
  }

  openNgachLuongDialog(data: any): void {
    const dialogRef = this.dialog.open(NgachLuongDialogComponent, {
      width: '700px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch('NgachLuong');
        this.common.messageRes(result);
      }
    });
  }

  openBacLuongAddDialog(data: any): void {
    const dialogRef = this.dialog.open(BacLuongAddDialogComponent, {
      width: '700px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch('BacLuong');
        this.common.messageRes(result);
      }
    });
  }

  openBacLuongUpdateDialog(data: any): void {
    const dialogRef = this.dialog.open(BacLuongUpdateDialogComponent, {
      width: '700px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch('BacLuong');
        this.common.messageRes(result);
      }
    });
  }

  openHeSoLuongAddDialog(data: any): void {
    const dialogRef = this.dialog.open(HeSoLuongAddDialogComponent, {
      width: '700px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch('HeSoLuong');
        this.common.messageRes(result);
      }
    });
  }

  openHeSoLuongUpdateDialog(data: any): void {
    const dialogRef = this.dialog.open(HeSoLuongUpdateDialogComponent, {
      width: '700px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch('HeSoLuong');
        this.common.messageRes(result);
      }
    });
  }

  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data[0].name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        let resDelete;
        if (data[1] === 'ThangBangLuong') {
          resDelete = this.hscb.deleteThangBangLuong({ id: data[0].id }, this.common.HSCB_Luong_Co_Ban.Delete);
        } else if (data[1] === 'NgachLuong') {
          resDelete = this.hscb.deleteNgachLuong({ id: data[0].id }, this.common.HSCB_Luong_Co_Ban.Delete);
        } else if (data[1] === 'BacLuong') {
          resDelete = this.hscb.deleteBacLuong({ id: data[0].id }, this.common.HSCB_Luong_Co_Ban.Delete);
        } else if (data[1] === 'HeSoLuong') {
          resDelete = this.hscb.deleteHeSoLuong({ id: data[0].id }, this.common.HSCB_Luong_Co_Ban.Delete);
        }
        resDelete.subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.common.messageRes(res.message);
            this.onSearch(data[1]);
          }
        });
      }
    });
  }


  onSelectedRow(row: any) {
    if (!this.selectedRow4) {
      this.selectedRow4 = row;
    } else {
      this.selectedRow4 = row;
    }
  }
}
