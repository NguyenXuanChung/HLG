import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import * as dateFormat from 'dateformat';
import { CommonService } from '../../../../services/common.service';

// Translate
import { LanguageService } from '../../../../services/language.service';
import  { HscbService } from'../../../../services/hscb.service';

@Component({
  selector: 'app-he-so-luong-add-dialog',
  templateUrl: './he-so-luong-add-dialog.component.html',
})
export class HeSoLuongAddDialogComponent implements OnInit {
  HeSoLuongForm: FormGroup;
  lastUpdateForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.HeSoLuongForm.controls; }

  constructor(
    private common: CommonService,
    private languageService: LanguageService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    private toastrService: ToastrService,
    public dialogRef: MatDialogRef<HeSoLuongAddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.currentLanguage();
  }
  currentLanguage() {
    return this.languageService.translate.currentLang;
  }
  createForm() {
    this.HeSoLuongForm = this.fb.group(
      {
        ngayApDung: [null, [Validators.required]],
        heSo: ['', [Validators.required]],
      });
    
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.HeSoLuongForm.invalid) {
      return;
    }
    let insertRespone;
    insertRespone = this.hscbService.insertHeSoLuong({id_bacLuong: this.data, ...this.HeSoLuongForm.value}, this.common.HSCB_Luong_Co_Ban.insert);
    
    if (insertRespone) {
      insertRespone.subscribe(res => {
        if (res.error) {
          this.toastrService.error(res.error.message);
        } else {
          this.dialogRef.close(res.message);
        } 
      }, err => {
        this.toastrService.error(err.error.message);
      });
    } 

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
