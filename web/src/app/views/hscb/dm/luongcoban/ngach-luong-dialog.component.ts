import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import * as dateFormat from 'dateformat';
import { CommonService } from '../../../../services/common.service';

// Translate
import { LanguageService } from '../../../../services/language.service';
import  { HscbService } from'../../../../services/hscb.service';

@Component({
  selector: 'app-ngach-luong-dialog',
  templateUrl: './ngach-luong-dialog.component.html',
})
export class NgachLuongDialogComponent implements OnInit {
  NgachLuongForm: FormGroup;
  lastUpdateForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.NgachLuongForm.controls; }

  constructor(
    private common: CommonService,
    private languageService: LanguageService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    private toastrService: ToastrService,
    public dialogRef: MatDialogRef<NgachLuongDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.currentLanguage();
  }
  currentLanguage() {
    return this.languageService.translate.currentLang;
  }
  createForm() {
    this.NgachLuongForm = this.fb.group(
      {
        code: ['', [Validators.required]],
        name: ['', [Validators.required]],
        moTa: [''],
        apDungTuNgay: [null],
        apDungDenNgay: [null]
      });
    if (this.data) {
      console.log(this.data);
      this.NgachLuongForm.setValue({
        'code': this.data.code,
        'name': this.data.name,
        'moTa': this.data.moTa,
        'apDungTuNgay': null,
        'apDungDenNgay': null
      });
      if (this.data.apDungTuNgay) {
        this.f.apDungTuNgay.setValue(dateFormat(this.data.apDungTuNgay, 'yyyy-mm-dd', true));
      }
      if (this.data.apDungDenNgay) {
        this.f.apDungDenNgay.setValue(dateFormat(this.data.apDungDenNgay, 'yyyy-mm-dd', true));
      }
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.NgachLuongForm.invalid) {
      return;
    }
    if (this.f.apDungTuNgay.value === '') {
      this.f.apDungTuNgay.setValue(null);
    }
    if (this.f.apDungDenNgay.value === '') {
      this.f.apDungDenNgay.setValue(null);
    }
    this.lastUpdateForm = this.fb.group({
        data: this.fb.group({
          code: [this.NgachLuongForm.get('code').value],
          name: [this.NgachLuongForm.get('name').value],
          moTa: [this.NgachLuongForm.get('moTa').value],
          apDungTuNgay: [this.NgachLuongForm.get('apDungTuNgay').value],
          apDungDenNgay: [this.NgachLuongForm.get('apDungDenNgay').value],
        }),
        condition: this.fb.group({
            id: [this.data.id]
        })
    });
    let editRespone;
    editRespone = this.hscbService.editNgachLuong({data: this.NgachLuongForm.value, condition: { id: this.data.id }}, this.common.HSCB_Luong_Co_Ban.Update);
    if (editRespone) {
      editRespone.subscribe(res => {
        if (res.error) {
          this.toastrService.error(res.error.message);
        } else {
          this.dialogRef.close(res.message);
        }
      }, err => {
        this.toastrService.error(err.error.message);
      });
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
