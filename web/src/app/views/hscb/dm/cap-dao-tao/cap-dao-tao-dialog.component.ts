import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import * as dateFormat from 'dateformat';

// Translate
import { CommonService } from './../../../../services/common.service';
import { LanguageService } from './../../../../services/language.service';
import { HscbService } from './../../../../services/hscb.service';

@Component({
  selector: 'app-cap-dao-tao-dialog',
  templateUrl: './cap-dao-tao-dialog.component.html',
})
export class CapDaoTaoDialogComponent implements OnInit {
  capDaoTaoForm: FormGroup;
  lastUpdateForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.capDaoTaoForm.controls; }

  constructor(
    private languageService: LanguageService,
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    private toastrService: ToastrService,
    public dialogRef: MatDialogRef<CapDaoTaoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.currentLanguage();
  }
  currentLanguage() {
    return this.languageService.translate.currentLang;
  }
  createForm() {
    this.capDaoTaoForm = this.fb.group(
      {
        code: ['', [Validators.required]],
        name: ['', [Validators.required]],
        chuanPhanTram: [0, [Validators.required, Validators.min(0)]],
        sortOrder: [0, [Validators.required, Validators.min(0)]],
      });
    if (this.data) {
      this.capDaoTaoForm.setValue({
        'code': this.data.code,
        'name': this.data.name,
        'chuanPhanTram': this.data.chuanPhanTram,
        'sortOrder': this.data.sortOrder,
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.capDaoTaoForm.invalid) {
      return;
    }
    if (!this.data) {
      this.hscbService.insertCapDaoTao(this.capDaoTaoForm.value, this.common.HSCB_CapDaoTao.Insert)
      .subscribe(res => {
        if (res.error) {
          this.toastrService.error(res.error.message);
        } else {
          this.dialogRef.close(res.message);
        }
      }, err => {
        this.toastrService.error(err.error.message);
      });
    } else {
      console.log({ data: this.capDaoTaoForm.value, condition: { id: this.data.id } });

      this.hscbService.editCapDaoTao({data: this.capDaoTaoForm.value, condition: { id: this.data.id }}, this.common.HSCB_CapDaoTao.Update)
      .subscribe(res => {
        if (res.error) {
          this.toastrService.error(res.error.message);
        } else {
          this.dialogRef.close(res.message);
        }
      }, err => {
        this.toastrService.error(err.error.message);
      });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
