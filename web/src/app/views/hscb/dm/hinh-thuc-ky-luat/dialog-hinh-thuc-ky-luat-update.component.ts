import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import * as dateFormat from 'dateformat';
// Translate
import { CommonService } from '../../../../services/common.service';
import { HscbService } from '../../../../services/hscb.service';

interface select {
  value: number;
  display: string;
}
@Component({
  selector: 'app-dialog-hinh-thuc-ky-luat-update',
  templateUrl: './dialog-hinh-thuc-ky-luat-update.component.html',
})
export class DialogUpdateHinhThucKyLuatComponent implements OnInit {
  updateForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    public dialogRef: MatDialogRef<DialogUpdateHinhThucKyLuatComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.updateForm = this.fb.group(
      {
        code : [null],
        name: [null],
        soThangLenLuongMuon: [null],
        sortOrder: [null]
      });
    if (this.data) {
      this.updateForm.setValue({
        code : this.data.code,
        name : this.data.name,
        soThangLenLuongMuon : this.data.soThangLenLuongMuon,
        sortOrder : this.data.sortOrder
      });
      this.f.soThangLenLuongMuon.setValue(parseInt(this.f.soThangLenLuongMuon.value, 10));
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    if (!this.data) {
      this.hscbService.insertHinhThucKyLuat(this.updateForm.value, this.common.HSCB_HinhThucKyLuat.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.hscbService.updateHinhThucKyLuat({
        data: this.updateForm.value,
        condition: { id: this.data.id }
      }, this.common.HSCB_HinhThucKyLuat.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
