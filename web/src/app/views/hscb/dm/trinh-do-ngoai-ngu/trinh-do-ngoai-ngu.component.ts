import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { HscbService } from '../../../../services/hscb.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CbAddComponent } from './../cb-add.component';
import { CbEditComponent } from './../cb-edit.component';

import { ConfirmationDialogComponent } from '../../../shared/confirmation-dialog/confirmation-dialog.component';
// Translate
import { CommonService } from '../../../../services/common.service';
import { StorageService } from '../../../../services/storage.service';
import { AuthConstants } from '../../../../config/auth-constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-trinh-do-ngoai-ngu',
  templateUrl: './trinh-do-ngoai-ngu.component.html'
})
export class TrinhDoNgoaiNguComponent implements OnInit {
  displayedColumns1 = ['code', 'name', 'actions'];
  displayedColumns2 = ['code', 'name', 'actions'];
  dataSource1: MatTableDataSource<any>;
  dataSource2: MatTableDataSource<any>;
  rowPhanLoai: any;
  selectedRow1: boolean;
  selectedRow2: boolean;
  loadTable1: boolean = true;
  loadTable2: boolean = true;
  @ViewChild(MatSort) sort: MatSort;
  permission: any;
  constructor(
    private common: CommonService,
    private hscb: HscbService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  ngOnInit() {
    this.loadTable1 = false;
    this.onSearch('hscb.dmPhanLoaiTrinhDoNgoaiNgu');
    if (!this.permission.HSCB_TrinhDoNgoaiNgu._view) {
      this.router.navigate(['/']);
    }
  }

  onSearch(type: string) {
    if (type === 'hscb.dmPhanLoaiTrinhDoNgoaiNgu') {
      this.getPhanLoaiTrinhDoNgoaiNgu();
    } else if (type === 'hscb.dmTrinhDoNgoaiNgu') {
      this.getTrinhDoNgoaiNgu(this.rowPhanLoai);
    }
  }

  getPhanLoaiTrinhDoNgoaiNgu() {
    this.hscb.getPhanLoaiTrinhDoNgoaiNgu({ status: 1 }, this.common.HSCB_TrinhDoNgoaiNgu.View).subscribe(res => {
      this.loadTable1 = true;
      this.rowPhanLoai = null;
      this.dataSource2 = new MatTableDataSource();
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource1 = new MatTableDataSource();
      } else {
        this.dataSource1 = new MatTableDataSource(res.data);
        this.dataSource1.sort = this.sort;
      }
    });
  }

  getTrinhDoNgoaiNgu(row: any) {
    this.loadTable2 = false;

    if (!this.selectedRow1) {
      this.selectedRow1 = row;
    } else {
      this.selectedRow1 = row;
    }
    if (row) {
      this.rowPhanLoai = row;
    }
    this.hscb.getTrinhDoNgoaiNgu({ id_phanLoaiTrinhDo_ngoaiNgu: this.rowPhanLoai.id, status: 1 },
      this.common.HSCB_TrinhDoNgoaiNgu.View).subscribe(res => {
        this.loadTable2 = true;
        if (res.error) {
          // this.common.messageErr(res);
          this.dataSource2 = new MatTableDataSource();
        } else {
          this.dataSource2 = new MatTableDataSource(res.data);
          this.dataSource2.sort = this.sort;
        }
      });
  }

  // data sẽ có dạng [row.id, (tên danh mục)]
  openAddDialog(data: any): void {
    const dialogRef = this.dialog.open(CbAddComponent, {
      width: '700px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch(data[1]);
        this.common.messageRes(result);
      }
    });
  }

  openEditDialog(data: any): void {
    const dialogRef = this.dialog.open(CbEditComponent, {
      width: '700px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch(data[1]);
        this.common.messageRes(result);
      }
    });
  }

  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data[0].name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        let resDelete;
        if (data[1] === 'hscb.dmPhanLoaiTrinhDoNgoaiNgu') {
          resDelete = this.hscb.deletePhanLoaiTrinhDoNgoaiNgu({ id: data[0].id }, this.common.HSCB_TrinhDoNgoaiNgu.Delete);
        } else if (data[1] === 'hscb.dmTrinhDoNgoaiNgu') {
          resDelete = this.hscb.deleteTrinhDoNgoaiNgu({ id: data[0].id }, this.common.HSCB_TrinhDoNgoaiNgu.Delete);
        }
        resDelete.subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.onSearch(data[1]);
            this.common.messageRes(res.message);
          }
        });
      }
    });
  }

  onSelectedRow(row: any) {
    if (!this.selectedRow2) {
      this.selectedRow2 = row;
    } else {
      this.selectedRow2 = row;
    }
  }
}
