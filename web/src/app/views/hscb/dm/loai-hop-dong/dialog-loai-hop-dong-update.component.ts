import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from '../../../../services/common.service';
import { HscbService } from '../../../../services/hscb.service';

interface select {
  value: number;
  display: string;
}
@Component({
  selector: 'app-dialog-loai-hop-dong-update',
  templateUrl: './dialog-loai-hop-dong-update.component.html',
})
export class DialogUpdateLoaiHopDongComponent implements OnInit {
  updateForm: FormGroup;
  submitted = false;
  lstPhanLoaiHopDong: select[] = [];
  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    public dialogRef: MatDialogRef<DialogUpdateLoaiHopDongComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.loadcombo();
    this.createForm();
  }
  createForm() {
    this.updateForm = this.fb.group(
      {
        code: [null],
        name: ['', [Validators.required]],
        id_phanLoaiHopDong:[null],
        soThang: [null],
        sortOrder: [null]
      });
    if (this.data) {
      this.updateForm.setValue({
        code : this.data.code,
        name: this.data.name,
        id_phanLoaiHopDong: this.data.id_phanLoaiHopDong,
        soThang: '' + this.data.soThang,
        sortOrder: '' + this.data.sortOrder
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
     //this.f.laLanhDao.setValue(parseInt(this.f.laLanhDao.value, 10));
    if (!this.data) {
      this.hscbService.insertLoaiHopDong(this.updateForm.value, this.common.HSCB_LoaiHopDong.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.hscbService.editLoaiHopDong({
        data: this.updateForm.value,
        condition: { id: this.data.id }
      }, this.common.HSCB_LoaiHopDong.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }
  }
  loadcombo(){
    this.loadPhanLoaiHopDong();
  }
  loadPhanLoaiHopDong(){
    this.lstPhanLoaiHopDong = [];
    this.hscbService.getPhanLoaiHopDong({}, this.common.HSCB_LoaiHopDong.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstPhanLoaiHopDong.push({ value: element.id, display: element.tenPhanLoaiHopDong});
          });
        }
      });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
