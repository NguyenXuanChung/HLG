import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ToastrService } from 'ngx-toastr';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../../shared/confirmation-dialog/confirmation-dialog.component';
import { HscbService } from './../../../../services/hscb.service';
import { QuanHeDialogComponent } from './quan-he-dialog.component';

// Translate
import { CommonService } from './../../../../services/common.service';
import { TranslateService } from '@ngx-translate/core';
import { LanguageService } from './../../../../services/language.service';

@Component({
  selector: 'app-quan-he',
  templateUrl: './quan-he.component.html'
})
export class QuanHeComponent implements OnInit {
  displayedColumns = ['index', 'code', 'name', 'laRuotThit', 'actions'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  searchForm: FormGroup;
  selectedRow: any;
  get fs() { return this.searchForm.controls; }

  constructor(
    private fb: FormBuilder,
    private common: CommonService,
    private hscbService: HscbService,
    private translateService: TranslateService,
    private languageService: LanguageService,
    private toastrService: ToastrService,
    public dialog: MatDialog
  ) {
  }

  ngOnInit() {
    this.onSearch();
    this.currentLanguage();
  }
  currentLanguage() {
    return this.languageService.translate.currentLang;
  }
  onSearch() {
    this.getQuanHe();
  }

  getQuanHe() {
    this.hscbService.getQuanHe({}, this.common.HSCB_QuanHe.View).subscribe(res => {
      if (res.error) {
        this.dataSource = new MatTableDataSource();
        this.toastrService.error(res.error.message);
      } else {
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    }, err => {
      console.log(err);
      this.toastrService.error(err.error);
    });
  }

  openQuanHeDialog(data: any): void {
    const dialogRef = this.dialog.open(QuanHeDialogComponent, {
      width: '700px',
      height: '80%',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.toastrService.success(result);
      }
    });
  }

  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.hscbService.deleteQuanHe({ id: data.id }, this.common.HSCB_QuanHe.Delete).subscribe(res => {
          if (res.error) {
            this.toastrService.error(res.error.message);
          } else {
            this.onSearch();
            this.toastrService.success(res.message);
          }
        }, err => {
          this.toastrService.error(err.error);
        });
      }
    });
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
