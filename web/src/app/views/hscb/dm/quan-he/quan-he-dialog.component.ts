import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import * as dateFormat from 'dateformat';

// Translate
import { CommonService } from './../../../../services/common.service';
import { LanguageService } from './../../../../services/language.service';
import { HscbService } from './../../../../services/hscb.service';

@Component({
  selector: 'app-quan-he-dialog',
  templateUrl: './quan-he-dialog.component.html',
})
export class QuanHeDialogComponent implements OnInit {
  quanHeForm: FormGroup;
  lastUpdateForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.quanHeForm.controls; }

  constructor(
    private languageService: LanguageService,
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    private toastrService: ToastrService,
    public dialogRef: MatDialogRef<QuanHeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.currentLanguage();
  }
  currentLanguage() {
    return this.languageService.translate.currentLang;
  }
  createForm() {
    this.quanHeForm = this.fb.group(
      {
        code: ['', [Validators.required]],
        name: ['', [Validators.required]],
        laRuotThit: [0, [Validators.required]],
        sortOrder: [0, [Validators.required, Validators.min(0)]],
      });
    if (this.data) {
      this.quanHeForm.setValue({
        'code': this.data.code,
        'name': this.data.name,
        'laRuotThit': '' + this.data.laRuotThit.data[0],
        'sortOrder': this.data.sortOrder,
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.quanHeForm.invalid) {
      return;
    }
    this.f.laRuotThit.setValue(parseInt(this.f.laRuotThit.value, 10));
    if (!this.data) {
      this.hscbService.insertQuanHe(this.quanHeForm.value, this.common.HSCB_QuanHe.Insert)
      .subscribe(res => {
        if (res.error) {
          this.toastrService.error(res.error.message);
        } else {
          this.dialogRef.close(res.message);
        }
      }, err => {
        this.toastrService.error(err.error.message);
      });
    } else {
      console.log({ data: this.quanHeForm.value, condition: { id: this.data.id } });

      this.hscbService.editQuanHe({data: this.quanHeForm.value, condition: { id: this.data.id }}, this.common.HSCB_QuanHe.Update)
      .subscribe(res => {
        if (res.error) {
          this.toastrService.error(res.error.message);
        } else {
          this.dialogRef.close(res.message);
        }
      }, err => {
        this.toastrService.error(err.error.message);
      });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
