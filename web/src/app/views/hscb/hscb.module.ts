import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { AngularMaterial } from '../../angular-material';
import { MomentModule } from 'ngx-moment';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { EditorModule } from 'primeng/editor';
import { MatTabsModule } from '@angular/material/tabs';
// Translate
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TreeModule } from 'primeng/tree';
import { QuillModule } from 'ngx-quill';
import { MatExpansionModule } from '@angular/material/expansion';
import { HscbRoutingModule } from './hscb-routing.module';
import { ChucVuComponent } from './dm/chucvu/chucvu.component';
import { ChucDanhComponent } from './dm/chucdanh/chucdanh.component';
import { DialogUpdateChucDanhComponent } from './dm/chucdanh/dialog-chucdanh-update.component';
import { QuocGiaComponent } from './dm/quocgia/quocgia.component';
import { DanTocComponent } from './dm/dantoc/dan-toc.component';
import { TonGiaoComponent } from './dm/tongiao/ton-giao.component';
import { TinhHuyenXaComponent } from './dm/tinh-huyen-xa/tinh-huyen-xa.component';
import { ViTriViecLamComponent } from './dm/vi-tri-viec-lam/vi-tri-viec-lam.component';
import { CbAddComponent } from './dm/cb-add.component';
import { CbEditComponent } from './dm/cb-edit.component';
import { Cb2TruongAddComponent } from './dm/cb-2truong-add.component';
import { Cb2TruongEditComponent } from './dm/cb-2truong-edit.component';
import { TrinhDoTinHocComponent } from './dm/trinh-do-tin-hoc/trinh-do-tin-hoc.component';
import { TrinhDoNgoaiNguComponent } from './dm/trinh-do-ngoai-ngu/trinh-do-ngoai-ngu.component';
import { BenhComponent } from './dm/benh/benh.component';
import { ChuyenNganhComponent } from './dm/chuyen-nganh/chuyen-nganh.component';
import { TinhTrangHonNhanComponent } from './dm/tinh-trang-hon-nhan/tinh-trang-hon-nhan.component';
import { VanBangComponent } from './dm/van-bang/van-bang.component';
import { LyLuanChinhTriComponent } from './dm/ly-luan-chinh-tri/ly-luan-chinh-tri.component';
import { TrinhDoQuanLyComponent } from './dm/trinh-do-quan-ly/trinh-do-quan-ly.component';
import { QuanLyBenhVienComponent } from './dm/quan-ly-benh-vien/quan-ly-benh-vien.component';
import { XepLoaiComponent } from './dm/xep-loai/xep-loai.component';
import { XepLoaiSucKhoeComponent } from './dm/xep-loai-suc-khoe/xep-loai-suc-khoe.component';
import { NgoaiNguComponent } from './dm/ngoai-ngu/ngoai-ngu.component';
import { HocHamComponent } from './dm/hoc-ham/hoc-ham.component';
import { HocHamDialogComponent } from './dm/hoc-ham/hoc-ham-dialog.component';
import { HocViComponent } from './dm/hoc-vi/hoc-vi.component';
import { HocViDialogComponent } from './dm/hoc-vi/hoc-vi-dialog.component';
import { CapDaoTaoComponent } from './dm/cap-dao-tao/cap-dao-tao.component';
import { CapDaoTaoDialogComponent } from './dm/cap-dao-tao/cap-dao-tao-dialog.component';
import { QuanHeComponent } from './dm/quan-he/quan-he.component';
import { QuanHeDialogComponent } from './dm/quan-he/quan-he-dialog.component';
import { CanBoBangCapComponent } from './cb/canbo-bangcap/canbo-bangcap.component';
import { DialogUpdateCanBoBangCapComponent } from './cb/canbo-bangcap/dialog-canbo-bangcap-update.component';
import { CanBoComponent } from './cb/canbo/canbo.component';
import { DialogUpdateCanBoComponent } from './cb/canbo/dialog-canbo-update.component';
import { LuongCoBanComponent } from './dm/luongcoban/luong-co-ban.component';
import { ThangBangLuongDialogComponent } from './dm/luongcoban/thang-bang-luong-dialog.component';
import { NgachLuongDialogComponent } from './dm/luongcoban/ngach-luong-dialog.component';
import { CanBoQTCTComponent } from './cb/canbo-qtct/canbo-qtct.component';
import { DialogUpdateCanBoQTCTComponent } from './cb/canbo-qtct/dialog-canbo-qtct-update.component';
import { CanBoNgoaiNguComponent } from './cb/canbo-ngoaingu/canbo-ngoaingu.component';
import { DialogUpdateCanBoNgoaiNguComponent } from './cb/canbo-ngoaingu/dialog-canbo-ngoaingu.component';
import { DialogUpdateChucVuComponent } from './dm/chucvu/dialog-chucvu-update.component';
import { NgachLuongAddDialogComponent } from './dm/luongcoban/ngach-luong-add-dialog.component';
import { BacLuongAddDialogComponent } from './dm/luongcoban/bac-luong-add-dialog.component';
import { BacLuongUpdateDialogComponent } from './dm/luongcoban/bac-luong-update-dialog.component';
import { HeSoLuongAddDialogComponent } from './dm/luongcoban/he-so-luong-add-dialog.component';
import { HeSoLuongUpdateDialogComponent } from './dm/luongcoban/he-so-luong-update-dialog.component';
import { CanBoThanNhanComponent } from './cb/canbo-thannhan/canbo-thannhan.component';
import { DialogUpdateCanBoThanNhanComponent } from './cb/canbo-thannhan/dialog-canbo-thannhan-update.component';
import { CanBoBenhAnComponent } from './cb/canbo-benhan/canbo-benhan.component';
import { DialogUpdateCanBoBenhAnComponent } from './cb/canbo-benhan/dialog-canbo-benhan-update.component';
import { CanBoSucKhoeHangNamComponent } from './cb/canbo-suckhoehangnam/canbo-suckhoehangnam.component';
import { DialogUpdateCanBoSucKhoeHangnamComponent } from './cb/canbo-suckhoehangnam/dialog-canbo-suckhoehangnam-update.component';
import { CanBoHopDongComponent } from './cb/canbo-hopdong/canbo-hopdong.component';
import { CanBoHopDongDialogComponent } from './cb/canbo-hopdong/canbo-hopdong-dialog.component';
import { LoaiHopDongComponent } from './dm/loai-hop-dong/loai-hop-dong.component';
import { DialogUpdateLoaiHopDongComponent } from './dm/loai-hop-dong/dialog-loai-hop-dong-update.component';
import { MucLuongCoSoComponent } from './dm/muc-luong-co-so/muc-luong-co-so.component';
import { DialogUpdateMucLuongCoSoComponent } from './dm/muc-luong-co-so/dialog-muc-luong-co-so-update.component';
import { LyDoNghiComponent} from './dm/ly-do-nghi/ly-do-nghi.component';
import { DialogUpdateLyDoNghiComponent } from './dm/ly-do-nghi/dialog-ly-do-nghi-update.component';
import { NguoiKyBaoCaoComponent } from './dm/nguoi-ky-bao-cao/nguoi-ky-bao-cao.component';
import { DialogUpdateNguoiKyBaoCaoComponent} from './dm/nguoi-ky-bao-cao/dialog-nguoi-ky-bao-cao-update.component';
import { HinhThucKyLuatComponent } from './dm/hinh-thuc-ky-luat/hinh-thuc-ky-luat.component';
import { DialogUpdateHinhThucKyLuatComponent } from './dm/hinh-thuc-ky-luat/dialog-hinh-thuc-ky-luat-update.component';
import { NoiBanHanhComponent } from './dm/noi-ban-hanh/noi-ban-hanh.component';
import { HinhThucKhenThuongComponent } from './dm/hinh-thuc-khen-thuong/hinh-thuc-khen-thuong.component';
import { DialogUpdateHinhThucKhenThuongComponent } from './dm/hinh-thuc-khen-thuong/dialog-hinh-thuc-khen-thuong-update.component';
import { CanBoKyLuatComponent } from './cb/canbo-kyluat/canbo-kyluat.component';
import { DialogUpdateCanBoKyLuatComponent } from './cb/canbo-kyluat/dialog-canbo-kyluat-update.component';
import { CanBoKhenThuongComponent } from './cb/canbo-khenthuong/canbo-khenthuong.component';
import { DialogUpdateCanBoKhenThuongComponent } from './cb/canbo-khenthuong/dialog-canbo-khenthuong-update.component';
import { CanBoKiemNhiemComponent } from './cb/canbo-kiemnhiem/canbo-kiemnhiem.component';
import { DialogUpdateCanBoKiemNhiemComponent } from './cb/canbo-kiemnhiem/dialog-canbo-kiemnhiem-update.component';
import { CanBoQuaTrinhLuongComponent } from './cb/canbo-qua-trinh-luong/canbo-qua-trinh-luong.component';
import { DialogUpdateCanBoQuaTrinhLuongComponent } from './cb/canbo-qua-trinh-luong/dialog-canbo-qua-trinh-luong-update.component';

export function translateHttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  imports: [
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: translateHttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    AngularMaterial,
    MomentModule,
    MatProgressSpinnerModule,
    HscbRoutingModule,
    InputTextareaModule,
    EditorModule,
    TreeViewModule,
    BsDropdownModule.forRoot(),
    MatTabsModule,
    TreeModule,
    QuillModule.forRoot({
      modules: {
        syntax: false,
        toolbar: [
          ['bold', 'italic', 'underline', 'strike'],
          [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
          [{ 'list': 'ordered' }, { 'list': 'bullet' }],
          [{ 'indent': '-1' }, { 'indent': '+1' }],
          ['blockquote', 'code-block']
        ]
      }
    }),
    MatExpansionModule
  ],
  declarations: [
    ChucVuComponent,
    DialogUpdateChucVuComponent,
    ChucDanhComponent,
    DialogUpdateChucDanhComponent,
    QuocGiaComponent,
    DanTocComponent,
    TonGiaoComponent,
    TinhHuyenXaComponent,
    CbAddComponent,
    CbEditComponent,
    Cb2TruongAddComponent,
    Cb2TruongEditComponent,
    ViTriViecLamComponent,
    TrinhDoTinHocComponent,
    TrinhDoNgoaiNguComponent,
    BenhComponent,
    ChuyenNganhComponent,
    TinhTrangHonNhanComponent,
    VanBangComponent,
    LyLuanChinhTriComponent,
    TrinhDoQuanLyComponent,
    QuanLyBenhVienComponent,
    XepLoaiComponent,
    XepLoaiSucKhoeComponent,
    NgoaiNguComponent,
    HocHamComponent,
    HocHamDialogComponent,
    HocViComponent,
    HocViDialogComponent,
    CapDaoTaoComponent,
    CapDaoTaoDialogComponent,
    QuanHeComponent,
    QuanHeDialogComponent,
    CanBoBangCapComponent,
    DialogUpdateCanBoBangCapComponent,
    CanBoComponent,
    DialogUpdateCanBoComponent,
    LuongCoBanComponent,
    ThangBangLuongDialogComponent,
    NgachLuongDialogComponent,
    CanBoQTCTComponent,
    DialogUpdateCanBoQTCTComponent,
    NgachLuongAddDialogComponent,
    BacLuongAddDialogComponent,
    BacLuongUpdateDialogComponent,
    HeSoLuongAddDialogComponent,
    HeSoLuongUpdateDialogComponent,
    CanBoThanNhanComponent,
    DialogUpdateCanBoThanNhanComponent,
    CanBoBenhAnComponent,
    DialogUpdateCanBoBenhAnComponent,
    CanBoSucKhoeHangNamComponent,
    DialogUpdateCanBoSucKhoeHangnamComponent,
    CanBoNgoaiNguComponent,
    DialogUpdateCanBoNgoaiNguComponent,
    CanBoHopDongComponent,
    CanBoHopDongDialogComponent,
    LoaiHopDongComponent,
    DialogUpdateLoaiHopDongComponent,
    MucLuongCoSoComponent,
    DialogUpdateMucLuongCoSoComponent,
    LyDoNghiComponent,
    DialogUpdateLyDoNghiComponent,
    NguoiKyBaoCaoComponent,
    DialogUpdateNguoiKyBaoCaoComponent,
    HinhThucKyLuatComponent,
    DialogUpdateHinhThucKyLuatComponent,
    NoiBanHanhComponent,
    DialogUpdateHinhThucKhenThuongComponent,
    HinhThucKhenThuongComponent,
    CanBoKyLuatComponent,
    DialogUpdateCanBoKyLuatComponent,
    CanBoKhenThuongComponent,
    DialogUpdateCanBoKhenThuongComponent,
    CanBoKiemNhiemComponent,
    DialogUpdateCanBoKiemNhiemComponent,
    CanBoQuaTrinhLuongComponent,
    DialogUpdateCanBoQuaTrinhLuongComponent
  ],
  entryComponents: [
    CbAddComponent,
    CbEditComponent,
    Cb2TruongAddComponent,
    Cb2TruongEditComponent,
    DialogUpdateChucDanhComponent,
    DialogUpdateChucVuComponent,
    HocHamDialogComponent,
    HocViDialogComponent,
    CapDaoTaoDialogComponent,
    QuanHeDialogComponent,
    CanBoBangCapComponent,
    DialogUpdateCanBoBangCapComponent,
    CanBoComponent,
    DialogUpdateCanBoComponent,
    ThangBangLuongDialogComponent,
    NgachLuongDialogComponent,
    CanBoQTCTComponent,
    DialogUpdateCanBoQTCTComponent,
    NgachLuongAddDialogComponent,
    BacLuongAddDialogComponent,
    BacLuongUpdateDialogComponent,
    HeSoLuongAddDialogComponent,
    HeSoLuongUpdateDialogComponent,
    CanBoThanNhanComponent,
    DialogUpdateCanBoThanNhanComponent,
    CanBoBenhAnComponent,
    DialogUpdateCanBoBenhAnComponent,
    CanBoSucKhoeHangNamComponent,
    DialogUpdateCanBoSucKhoeHangnamComponent,
    CanBoNgoaiNguComponent,
    DialogUpdateCanBoNgoaiNguComponent,
    CanBoHopDongDialogComponent,
    DialogUpdateLoaiHopDongComponent,
    DialogUpdateMucLuongCoSoComponent,
    DialogUpdateLyDoNghiComponent,
    DialogUpdateNguoiKyBaoCaoComponent,
    DialogUpdateHinhThucKyLuatComponent,
    DialogUpdateHinhThucKhenThuongComponent,
    DialogUpdateCanBoKyLuatComponent,
    DialogUpdateCanBoKhenThuongComponent,
    DialogUpdateCanBoKiemNhiemComponent,
    DialogUpdateCanBoQuaTrinhLuongComponent
  ],
})
export class HscbModule { }
