import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import { TreeNode } from 'primeng/api';
import * as moment from 'moment';
// Translate
import { CommonService } from './../../../../services/common.service';
import { HscbService } from './../../../../services/hscb.service';
import * as dateFormat from 'dateformat';
import { DonViService } from '../../../../services/don-vi.service';
import { ActivatedRoute, Router } from '@angular/router';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}

@Component({
  selector: 'app-dialog-canbo-qtct-update',
  templateUrl: './dialog-canbo-qtct-update.component.html',
})
export class DialogUpdateCanBoQTCTComponent implements OnInit {
  lstchucdanh: select[] = [];
  lstchucvu: select[] = [];
  lstloaiquatrinhcongtac: select[] = [];
  lstlydoquatrinhcongtac: select[] = [];
  updateForm: FormGroup;
  submitted = false;
  donVis: TreeNode[];
  selectedBaoCaos: TreeNode;
  today: String;
  id_canBo: string;
  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    public dialogRef: MatDialogRef<DialogUpdateCanBoQTCTComponent>,
    private donVi: DonViService,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.today = moment().format('YYYY/MM/DD');
    this.loadcombo();
    this.createForm();
  }
  createForm() {
    this.id_canBo = this.data.id_canBo;
    this.updateForm = this.fb.group(
      {
        id_canBo: [this.id_canBo],
        tuNgay: ['', Validators.required],
        denNgay: [null],
        ngayQuyetDinh: [null],
        ngayKy: [null],
        soQuyetDinh: [null],
        id_donVi: ['', Validators.required],
        //id_donViTinhLuong: [null],
        id_chucDanh: [null],
        id_chucVu: [null],
        id_loaiQuaTrinhCongTac: ['', Validators.required],
        id_lyDoQuaTrinhCongTac: [null],
        thongTinCongTac: [null],
        noiLamViec: [null]
      });
    if (this.data.body) {
      this.updateForm.setValue({
        id_canBo: this.id_canBo,
        tuNgay: this.data.body.tuNgay,
        denNgay: this.data.body.denNgay,
        ngayQuyetDinh: this.data.body.ngayQuyetDinh,
        ngayKy: this.data.body.ngayKy,
        soQuyetDinh: this.data.body.soQuyetDinh,
        id_donVi: this.data.body.id_donVi,
        id_chucDanh: this.data.body.id_chucDanh,
        id_chucVu: this.data.body.id_chucVu,
        id_loaiQuaTrinhCongTac: this.data.body.id_loaiquatrinhcongtac,
        id_lyDoQuaTrinhCongTac: this.data.body.id_lyDoQuaTrinhCongTac,
        thongTinCongTac: this.data.body.thongTinCongTac,
        noiLamViec: this.data.body.noiLamViec
      });
      if (this.data.body.tuNgay) {
        this.f.tuNgay.setValue(dateFormat(this.data.body.tuNgay, 'yyyy-mm-dd', true));
      }
      if (this.data.body.denNgay) {
        this.f.denNgay.setValue(dateFormat(this.data.body.denNgay, 'yyyy-mm-dd', true));
      }
      if (this.data.body.ngayQuyetDinh) {
        this.f.ngayQuyetDinh.setValue(dateFormat(this.data.body.ngayQuyetDinh, 'yyyy-mm-dd', true));
      }
      if (this.data.body.ngayKy) {
        this.f.ngayKy.setValue(dateFormat(this.data.body.ngayKy, 'yyyy-mm-dd', true));
      }
    }
  }
  loadcombo(){
    this.getChucDanh();
    this.getChucVu();
    this.getDonVis();
    this.getLoaiQuaTrinhCongTac();
    this.getLyDoQuaTrinhCongTac();
  }
  getDonVis(){
    this.donVi.getTreeToValue({ id_trungTam: 1, tuNgay: this.today, denNgay: this.today }, this.common.HSCB_CanBoQuaTrinhCongTac.Insert)
    .subscribe((res: any) => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.donVis = res.data;
      }
    });
  }
  getChucDanh(){
    this.lstchucdanh = [];
    this.hscbService.getDanhMucChucDanh({}, this.common.HSCB_CanBoQuaTrinhCongTac.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstchucdanh.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getChucVu(){
    this.lstchucvu = [];
    this.hscbService.getDanhMucChucVu({}, this.common.HSCB_CanBoQuaTrinhCongTac.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstchucvu.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getLoaiQuaTrinhCongTac(){
    this.lstchucvu = [];
    this.hscbService.getLoaiQuaTrinhCongTac({}, this.common.HSCB_CanBoQuaTrinhCongTac.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstloaiquatrinhcongtac.push({ value: element.id, display: element.tenLoaiQuaTrinhCongTac});
          });
        }
      });
  }
  getLyDoQuaTrinhCongTac(){
    this.lstchucvu = [];
    this.hscbService.getLyDoQuaTrinhCongTac({}, this.common.HSCB_CanBoQuaTrinhCongTac.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstlydoquatrinhcongtac.push({ value: element.id, display: element.tenLoaiLyDoNghi});
          });
        }
      });
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    if (!this.data.body) {
      this.hscbService.insertQuaTrinhCongTac(this.updateForm.value, this.common.HSCB_CanBoQuaTrinhCongTac.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.hscbService.editQuaTrinhCongTac({
        data: this.updateForm.value,
        condition: { id: this.data.body.id }
      }, this.common.HSCB_CanBoQuaTrinhCongTac.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  nodeSelectBaoCao(event) {
    if (this.updateForm.controls.id_donVi.value === event.node.data) {
      this.nodeUnselectBaoCao(event);
    } else {
      this.updateForm.controls.id_donVi.setValue(event.node.data);
    }
  }
  nodeUnselectBaoCao(event) {
    this.updateForm.controls.id_donVi.setValue(null);
    this.selectedBaoCaos = null;
  }
  // chi check cho select 1 node (khong multi)
  checkNode(nodes: TreeNode[], str: number, selected) {
    nodes.forEach(node => {
      if (str === node.data) {
        if (selected === 'BaoCao') {
          this.selectedBaoCaos = (node);
        }
      }
      if (node.children !== undefined) {
        node.children.forEach(child => {
          if (str === child.data && !str === node.data) {
            node.partialSelected = true;
            child.parent = node;
          }
          if (str === node.data) {
            child.parent = node;
          }
        });
      } else {
        return;
      }
      this.checkNode(node.children, str, selected);
      node.children.forEach(child => {
        if (child.partialSelected) {
          node.partialSelected = true;
        }
      });
    });
  }
}
