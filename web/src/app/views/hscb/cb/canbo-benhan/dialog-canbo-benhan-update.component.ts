import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../../services/common.service';
import { HscbService } from './../../../../services/hscb.service';
import * as dateFormat from 'dateformat';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}

@Component({
  selector: 'app-dialog-canbo-benhan-update',
  templateUrl: './dialog-canbo-benhan-update.component.html',
})
export class DialogUpdateCanBoBenhAnComponent implements OnInit {
  lstbenh: select[] = [];
  id_canBo: string;
  updateForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    public dialogRef: MatDialogRef<DialogUpdateCanBoBenhAnComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.loadcombo();
    this.createForm();
  }
  createForm() {
    this.id_canBo = this.data.id_canBo;
    this.updateForm = this.fb.group(
      {
        id_canbo: [''],
        id_benh: ['', [Validators.required]]
      });
    if (this.data.body) {
      this.updateForm.setValue({
       id_canbo: this.data.id_canBo,
       id_benh: this.data.body.id_benh,
      });
    }
  }
  loadcombo(){
    this.getBenh();
  }
  getBenh(){
    this.lstbenh = [];
    this.hscbService.getBenh({}, this.common.HSCB_CanBoBenhAn.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstbenh.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  onSubmit() {
    this.submitted = true;
    this.updateForm.value.id_canbo = this.data.id_canBo;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    if (!this.data.body) {
      this.hscbService.insertBenhAn(this.updateForm.value, this.common.HSCB_CanBoBenhAn.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.hscbService.updateBenhAn({
        data: this.updateForm.value,
        condition: { id: this.data.body.canbo_tiensubenhan_id }
      }, this.common.HSCB_CanBoBenhAn.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
