import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import { TreeNode } from 'primeng/api';
import * as moment from 'moment';
// Translate
import { CommonService } from './../../../../services/common.service';
import { HscbService } from './../../../../services/hscb.service';
import * as dateFormat from 'dateformat';
import { ActivatedRoute, Router } from '@angular/router';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}

@Component({
  selector: 'app-canbo-hopdong-dialog-update',
  templateUrl: './canbo-hopdong-dialog.component.html',
})
export class CanBoHopDongDialogComponent implements OnInit {
  lstchucdanh: select[] = [];
  lstchucvu: select[] = [];
  lstloaihopdong: select[] = [];
  lstnguoiky: select[] = [];
  updateForm: FormGroup;
  submitted = false;
  today: String;
  id_canBo: string;
  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    public dialogRef: MatDialogRef<CanBoHopDongDialogComponent>,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.today = moment().format('YYYY/MM/DD');
    this.loadcombo();
    this.createForm();
  }
  createForm() {
    this.id_canBo = this.data.id_canBo;
    this.updateForm = this.fb.group(
      {
        id_canBo: [this.id_canBo],
        soHopDong: [null],
        id_loaiHopDong: [null],
        tuNgay: ['', Validators.required],
        denNgay: [null],
        ngayHieuLuc: [null],
        ngayQuyetDinh: [null],
        ngayKy: [null],
        soQuyetDinh: [null],
        id_chucDanh: [null],
        id_chucVu: [null],
        id_nguoiKy: [null],
        diaDiemLamViec: [null],
        chucDanhChuyenMon: [null]
      });
    if (this.data.body) {
      this.updateForm.setValue({
        id_canBo: this.id_canBo,
        soHopDong: this.data.body.soHopDong,
        id_loaiHopDong: this.data.body.id_loaiHopDong,
        tuNgay: this.data.body.tuNgay,
        denNgay: this.data.body.denNgay,
        ngayHieuLuc: this.data.body.ngayHieuLuc,
        ngayQuyetDinh: this.data.body.ngayQuyetDinh,
        ngayKy: this.data.body.ngayKy,
        soQuyetDinh: this.data.body.soQuyetDinh,
        id_chucDanh: this.data.body.id_chucDanh,
        id_chucVu: this.data.body.id_chucVu,
        id_nguoiKy: this.data.body.id_nguoiKy,
        diaDiemLamViec: this.data.body.diaDiemLamViec,
        chucDanhChuyenMon: this.data.body.chucDanhChuyenMon
      });
      if (this.data.body.tuNgay) {
        this.f.tuNgay.setValue(dateFormat(this.data.body.tuNgay, 'yyyy-mm-dd', true));
      }
      if (this.data.body.denNgay) {
        this.f.denNgay.setValue(dateFormat(this.data.body.denNgay, 'yyyy-mm-dd', true));
      }
      if (this.data.body.ngayHieuLuc) {
        this.f.ngayHieuLuc.setValue(dateFormat(this.data.body.ngayHieuLuc, 'yyyy-mm-dd', true));
      }
      if (this.data.body.ngayQuyetDinh) {
        this.f.ngayQuyetDinh.setValue(dateFormat(this.data.body.ngayQuyetDinh, 'yyyy-mm-dd', true));
      }
      if (this.data.body.ngayKy) {
        this.f.ngayKy.setValue(dateFormat(this.data.body.ngayKy, 'yyyy-mm-dd', true));
      }
    }
  }
  loadcombo(){
    this.getChucDanh();
    this.getChucVu();
    this.getLoaiHopDong();
    this.getNguoiKyBaoCao();
  }
  getChucDanh(){
    this.lstchucdanh = [];
    this.hscbService.getDanhMucChucDanh({}, this.common.HSCB_CanBo.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstchucdanh.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getChucVu(){
    this.lstchucvu = [];
    this.hscbService.getDanhMucChucVu({}, this.common.HSCB_CanBo.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstchucvu.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getLoaiHopDong(){
    this.lstloaihopdong = [];
    this.hscbService.getLoaiHopDong({}, this.common.HSCB_CanBo.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstloaihopdong.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getNguoiKyBaoCao(){
    this.lstnguoiky = [];
    this.hscbService.getNguoiKyBaoCao({}, this.common.HSCB_CanBo.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstnguoiky.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    if (!this.data.body) {
      this.hscbService.insertHopDongLaoDong(this.updateForm.value, this.common.HSCB_CanBo.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.hscbService.updateHopDongLaoDong({
        data: this.updateForm.value,
        condition: { id: this.data.body.id }
      }, this.common.HSCB_CanBo.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
