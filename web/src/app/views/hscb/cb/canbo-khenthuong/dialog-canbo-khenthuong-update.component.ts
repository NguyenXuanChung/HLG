import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import * as moment from 'moment';
// Translate
import { CommonService } from './../../../../services/common.service';
import { HscbService } from './../../../../services/hscb.service';
import * as dateFormat from 'dateformat';
import { ActivatedRoute, Router } from '@angular/router';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}

@Component({
  selector: 'app-dialog-canbo-khenthuong-update',
  templateUrl: './dialog-canbo-khenthuong-update.component.html',
})
export class DialogUpdateCanBoKhenThuongComponent implements OnInit {
  lstnoibanhanh: select[] = [];
  lstcapquyetdinh: select[] = [];
  lsthinhthuckhenthuong: select[] = [];
  lstloaihinhthuckhenthuong: select[] = [];
  updateForm: FormGroup;
  submitted = false;
  today: String;
  id_canBo: string;
  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    public dialogRef: MatDialogRef<DialogUpdateCanBoKhenThuongComponent>,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.today = moment().format('YYYY/MM/DD');
    this.loadcombo();
    this.createForm();
  }
  createForm() {
    this.id_canBo = this.data.id_canBo;
    this.updateForm = this.fb.group(
      {
        id_canBo: [this.id_canBo],
        nam: ['', [Validators.required, Validators.min(1900), Validators.max(2100)]],
        id_loaiHinhThucKhenThuong: ['',Validators.required],
        soQuyetDinh: [null],
        ngayQuyetDinh: ['',Validators.required],
        id_noiBanHanh: [null],
        id_capQuyetDinh: [null],
        id_hinhThucKhenThuong: ['', Validators.required],
        tuNgay: [null],
        denNgay: [null],
        noiDung: [null],
        ghiChu: [null]
      });
    if (this.data.body) {
      this.updateForm.setValue({
        id_canBo: this.id_canBo,
        nam: this.data.body.nam,
        soQuyetDinh: this.data.body.soQuyetDinh,
        ngayQuyetDinh: this.data.body.ngayQuyetDinh,
        id_noiBanHanh: this.data.body.id_noiBanHanh,
        id_capQuyetDinh: this.data.body.id_capQuyetDinh,
        id_hinhThucKhenThuong: this.data.body.id_hinhThucKhenThuong,
        id_loaiHinhThucKhenThuong: this.data.body.id_loaiHinhThucKhenThuong,
        tuNgay: this.data.body.tuNgay,
        denNgay: this.data.body.denNgay,
        noiDung: this.data.body.noiDung,
        ghiChu: this.data.body.ghiChu
      });
      if (this.data.body.tuNgay) {
        this.f.tuNgay.setValue(dateFormat(this.data.body.tuNgay, 'yyyy-mm-dd', true));
      }
      if (this.data.body.denNgay) {
        this.f.denNgay.setValue(dateFormat(this.data.body.denNgay, 'yyyy-mm-dd', true));
      }
      if (this.data.body.ngayQuyetDinh) {
        this.f.ngayQuyetDinh.setValue(dateFormat(this.data.body.ngayQuyetDinh, 'yyyy-mm-dd', true));
      }
    }
  }
  loadcombo(){
    this.getCapQuyetDinh();
    this.getNoiBanHanh();
    this.getHinhThucKhenThuong();
    this.getLoaiHinhThucKhenThuong();
  }
  getCapQuyetDinh(){
    this.lstcapquyetdinh = [];
    this.hscbService.getCapQuyetDinh({}, this.common.HSCB_CanBoKhenThuong.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstcapquyetdinh.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getNoiBanHanh(){
    this.lstnoibanhanh = [];
    this.hscbService.getNoiBanHanh({}, this.common.HSCB_CanBoKhenThuong.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstnoibanhanh.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getHinhThucKhenThuong(){
    this.lsthinhthuckhenthuong = [];
    this.hscbService.getHinhThucKhenThuong({}, this.common.HSCB_CanBoKhenThuong.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lsthinhthuckhenthuong.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getLoaiHinhThucKhenThuong(){
    this.lstloaihinhthuckhenthuong = [];
    this.hscbService.getLoaiHinhThucKhenThuong({}, this.common.HSCB_CanBoKhenThuong.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstloaihinhthuckhenthuong.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    if (!this.data.body) {
      this.hscbService.insertCanBoKhenThuong(this.updateForm.value, this.common.HSCB_CanBoKhenThuong.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.hscbService.updateCanBoKhenThuong({
        data: this.updateForm.value,
        condition: { id: this.data.body.id }
      }, this.common.HSCB_CanBoKhenThuong.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
