import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import { TreeNode } from 'primeng/api';
import * as moment from 'moment';
// Translate
import { CommonService } from './../../../../services/common.service';
import { HscbService } from './../../../../services/hscb.service';
import * as dateFormat from 'dateformat';
import { DonViService } from '../../../../services/don-vi.service';
import { ActivatedRoute, Router } from '@angular/router';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}

@Component({
  selector: 'app-dialog-canbo-qua-trinh-luong-update',
  templateUrl: './dialog-canbo-qua-trinh-luong-update.component.html',
})
export class DialogUpdateCanBoQuaTrinhLuongComponent implements OnInit {
  lstngachluong: select[] = [];
  lstbacluong: select[] = [];
  lstluongtheo: select[] = [];
  lstphantramhuong: select[] = [];
  lsthinhthuc : select[] = [];
  lstloailuong : select[] = [];
  updateForm: FormGroup;
  updateFormQTL: FormGroup;
  submitted = false;
  donVis: TreeNode[];
  selectedBaoCaos: TreeNode;
  today: String;
  heSo: string;
  tong_HeSo: string;
  thanhTien: string;
  id_laBacCaoNhat: string;
  muctien: string;
  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    public dialogRef: MatDialogRef<DialogUpdateCanBoQuaTrinhLuongComponent>,
    private donVi: DonViService,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.today = moment().format('YYYY/MM/DD');
    this.loadcombo();
    this.createForm();
  }
  createForm() {
    this.heSo = '0';
    this.updateForm = this.fb.group(
      {
        id_ngachLuong : ['', Validators.required],
        id_bacLuong : ['', Validators.required],
        id_heSoLuong : [null],
        phanTramHuong: [null],
        pCTNVuotKhung : [null],
        heSoBaoLuu : [null],

        id_phanLoaiHopDong: ['', Validators.required],
        tienLuong : ['', Validators.required],

        heSoLuong: ['', Validators.required],

        id_canBo: this.data.id_canBo,
        id_loai:[null],
        id_loaiLenLuong: [null],
        soQuyetDinh: [null],
        ngayQuyetDinh: [null],
        tuNgay: ['', Validators.required],
        ngayBienDong : [null],
        ngayGiuBac : [null],
        ghiChu : [null]
       });
      this.f.id_loai.setValue(0);
    if (this.data.body) {
      this.updateForm.setValue({
        id_canBo: this.data.id_canBo,
        id_ngachLuong: this.data.body.id_ngachLuong,
        id_bacLuong: this.data.body.id_bacLuong,
        id_heSoLuong: this.data.body.id_heSoLuong,
        phanTramHuong: this.data.body.phanTramHuong,
        pCTNVuotKhung: this.data.body.pCTNVuotKhung,
        heSoBaoLuu: this.data.body.heSoBaoLuu,

        id_phanLoaiHopDong: this.data.body.id_phanLoaiHopDong,
        tienLuong: this.data.body.tienLuong,

        heSoLuong: this.data.body.heSoLuong,

        id_loai : 0,
        id_loaiLenLuong: this.data.body.id_loaiLenLuong,
        ngayQuyetDinh: this.data.body.ngayQuyetDinh,
        soQuyetDinh: this.data.body.soQuyetDinh,
        tuNgay: this.data.body.tuNgay,
        ngayBienDong: this.data.body.ngayBienDong,
        ngayGiuBac: this.data.body.ngayGiuBac,
        ghiChu: this.data.body.ghiChu
      });
      if(this.data.body.id_heSoLuong != null && this.data.body.id_heSoLuong != ''){ // bậc lương
        this.f.id_loai.setValue(0);
        this.getBacLuongByNgachLuong();
        this.getBacLuongByBacLuong();
      }
      else if(this.data.body.heSoLuong != null && this.data.body.heSoLuong != ''){ // hệ số lương
        this.f.id_loai.setValue(2);
      } 
      else{ // tiền lương
        this.f.id_loai.setValue(1);
      }
      if (this.data.body.tuNgay) {
        this.f.tuNgay.setValue(dateFormat(this.data.body.tuNgay, 'yyyy-mm-dd', true));
      }
      if (this.data.body.ngayQuyetDinh) {
        this.f.ngayQuyetDinh.setValue(dateFormat(this.data.body.ngayQuyetDinh, 'yyyy-mm-dd', true));
      }
      if (this.data.body.ngayBienDong) {
        this.f.ngayBienDong.setValue(dateFormat(this.data.body.ngayBienDong, 'yyyy-mm-dd', true));
      }
      if (this.data.body.ngayGiuBac) {
        this.f.ngayGiuBac.setValue(dateFormat(this.data.body.ngayGiuBac, 'yyyy-mm-dd', true));
      }
    }
  }
  
  loadcombo(){
    this.getLuongTheo();
    this.getNgachLuong();
    this.getPhanTramHuong();
    this.getHinhThuc();
    this.getLoaiLuong();
  }
  getLuongTheo(){
    this.lstluongtheo.push({ value: 0, display: "Theo ngạch bậc"})
    this.lstluongtheo.push({ value: 1, display: "Theo tiền lương"})
    this.lstluongtheo.push({ value: 2, display: "Theo hệ số lương"})
  }
  getNgachLuong(){
    this.lstngachluong = [];
    this.hscbService.getNgachLuong({id_thangBangLuong: 0}, this.common.HSCB_QuaTrinhLuong.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstngachluong.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getPhanTramHuong(){
    this.lstphantramhuong = [];
    this.hscbService.getPhanTramHuong({}, this.common.HSCB_QuaTrinhLuong.Insert)
    .subscribe(res => {
      if (!res.error && res.data.length != 0) {
        res.data.forEach(element => {
          this.lstphantramhuong.push({ value: element.id, display: element.mucPhanTram});
        });
        this.f.phanTramHuong.setValue(res.data[0].id);
      }
    });
  }
  getHinhThuc(){
    this.lsthinhthuc = [];
    this.hscbService.getLoaiLenLuong({}, this.common.HSCB_QuaTrinhLuong.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lsthinhthuc.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getLoaiLuong(){
    this.lstloailuong = [];
    this.hscbService.getPhanLoaiHopDong({}, this.common.HSCB_QuaTrinhLuong.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            if(element.id != 1){
            this.lstloailuong.push({ value: element.id, display: element.name});
            }
          });
        }
      });
  }
  changengachluong(){
      this.getBacLuongByNgachLuong()
      this.heSo = '0';
      this.f.id_heSoLuong.setValue(null);
  }
  changebacluong(){
    this.getBacLuongByBacLuong();
  }
  getBacLuongByNgachLuong(){
    this.lstbacluong = [];
    this.hscbService.getBacLuong({id_ngachLuong: this.f.id_ngachLuong.value}, this.common.HSCB_QuaTrinhLuong.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstbacluong.push({ value: element.id, display: element.name});
          });
        }
        this.tinhHeSo();
      });
  }
 
  getBacLuongByBacLuong(){
    this.hscbService.getBacLuong({id: this.f.id_bacLuong.value}, this.common.HSCB_QuaTrinhLuong.Insert)
    .subscribe(res => {
      if(!res.error) {
        if(res.data[0].laBacLuongCaoNhat.data[0] === 1){
          this.f.pCTNVuotKhung.enable();
        }
        else{
          this.f.pCTNVuotKhung.disable();
          this.f.pCTNVuotKhung.setValue(null);
        }
      }
    });
    if(this.f.tuNgay.value + '' != '')
    {
      this.hscbService.getHeSoLuongbyBacLuong({id_bacLuong: this.f.id_bacLuong.value, tu_ngay: dateFormat(this.f.tuNgay.value, 'yyyy-mm-dd', true)}, this.common.HSCB_CanBoQuaTrinhCongTac.Insert)
        .subscribe(res => {
          if (!res.error && res.data.length != 0) {
            this.heSo = '' + res.data[0].heSo;
            this.f.id_heSoLuong.setValue(res.data[0].id);
          }
          else{
            this.heSo = '0';
            this.f.id_heSoLuong.setValue(null);
          }
          this.tinhHeSo();
        });
    }
  }
  changephantramluong(){
    this.tinhHeSo();
  }
  changevuotkhung(){
    this.tinhHeSo();
  }
  onKey(){
    this.tinhHeSo();
  }
  onKeytuNgay(){
    this.f.ngayBienDong.setValue(dateFormat(this.f.tuNgay.value, 'yyyy-mm-dd', true));
    if(this.f.tuNgay.value + '' != '')
    {
      this.hscbService.getHeSoLuongbyBacLuong({id_bacLuong: this.f.id_bacLuong.value, tu_ngay: dateFormat(this.f.tuNgay.value, 'yyyy-mm-dd', true)}, this.common.HSCB_QuaTrinhLuong.Insert)
        .subscribe(res => {
          if (!res.error && res.data.length != 0) {
            this.heSo = '' + res.data[0].heSo;
          }
          else{
            this.heSo = '0';
          }
          this.tinhHeSo();
        });
    }
    
  }
  clicktungay(date){
    this.f.ngayBienDong.setValue(dateFormat(this.f.tuNgay.value, 'yyyy-mm-dd', true));
    if(this.f.tuNgay.value + '' != '')
    {
      this.hscbService.getHeSoLuongbyBacLuong({id_bacLuong: this.f.id_bacLuong.value, tu_ngay: dateFormat(this.f.tuNgay.value, 'yyyy-mm-dd', true)}, this.common.HSCB_CanBoQuaTrinhCongTac.Insert)
        .subscribe(res => {
          if (!res.error && res.data.length != 0) {
            this.heSo = '' + res.data[0].heSo;
            this.f.id_heSoLuong.setValue(res.data[0].id);
          }
          else{
            this.heSo = '0';
            this.f.id_heSoLuong.setValue(null);
          }
          this.tinhHeSo();
        });
    }
  }
  
  changeloailuong(){
    if(this.f.tuNgay.value != null && this.f.tuNgay.value != ''){
      this.hscbService.getMucLuongCoSoByPhanLoaiHopDong({id_phanLoaiHopDong : this.f.id_phanLoaiHopDong.value,ngay: dateFormat(this.f.tuNgay.value, 'yyyy-mm-dd', true)}, this.common.HSCB_QuaTrinhLuong.Insert)
      .subscribe(res => {
        if (!res.error && res.data.length != 0) {
            this.f.tienLuong.setValue(res.data[0].mucTien);
        }
      });
    }
  }
  // tính toán hệ số.
  tinhHeSo(){
    if(this.f.tuNgay.value +'' != '')
    {
      var phanTramHuong = 0;
      var tongHeSo = 0;
      var vuotKhung = 0;
      if(this.heSo + '' == ''){
        this.heSo = '0';
      }
      if(this.f.heSoBaoLuu.value == null || this.f.heSoBaoLuu.value == '0'){
        this.f.heSoBaoLuu.setValue('0');
      }
      tongHeSo = parseFloat(this.heSo) + parseFloat(this.f.heSoBaoLuu.value);
      if(this.f.pCTNVuotKhung.value != null){
        vuotKhung = parseFloat(this.f.pCTNVuotKhung.value);
      }
      if(this.f.phanTramHuong.value != null){
          this.hscbService.getPhanTramHuong({id : this.f.phanTramHuong.value}, this.common.HSCB_QuaTrinhLuong.Insert)
            .subscribe(res => {
              if (!res.error && res.data.length != 0) {
                phanTramHuong = res.data[0].mucPhanTram;
                this.tong_HeSo = '' + Math.round((((phanTramHuong/100) * (tongHeSo +(vuotKhung /100 * tongHeSo))) + Number.EPSILON) * 100) / 100;
                if(this.tong_HeSo === 'NaN'){
                  this.tong_HeSo = '0';
                }
                this.hscbService.getMucLuongCoSoByPhanLoaiHopDong({id_phanLoaiHopDong: 1,ngay: dateFormat(this.f.tuNgay.value, 'yyyy-mm-dd', true)}, this.common.HSCB_QuaTrinhLuong.Insert)
                .subscribe(res => {
                  if (!res.error && res.data.length != 0) {
                    this.muctien = res.data[0].mucTien;
                    var tien = Math.round((((phanTramHuong/100) * (tongHeSo +(vuotKhung /100 * tongHeSo))) + Number.EPSILON) * 100) / 100 *  parseFloat(this.muctien);
                    this.thanhTien = Math.round(((tien) + Number.EPSILON) * 100) / 100 +'';
                    if(this.thanhTien === 'NaN'){
                      this.thanhTien = '0';
                      }
                    }
                });
              }
            });
      }
      else{

      }
    }
  }

  onSubmit() {
    this.submitted = true;
    if(this.f.id_loai.value == 0)
    {
      this.f.id_phanLoaiHopDong.clearValidators();
      this.f.id_phanLoaiHopDong.updateValueAndValidity();
      this.f.tienLuong.clearValidators();
      this.f.tienLuong.updateValueAndValidity();
      this.f.heSoLuong.clearValidators();
      this.f.heSoLuong.updateValueAndValidity();

      this.f.id_ngachLuong.setValidators([Validators.required]);
      this.f.id_ngachLuong.updateValueAndValidity();
      this.f.id_bacLuong.setValidators([Validators.required]);
      this.f.id_bacLuong.updateValueAndValidity();
    }
    if(this.f.id_loai.value == 2){
      this.f.id_ngachLuong.clearValidators();
      this.f.id_ngachLuong.updateValueAndValidity();
      this.f.id_bacLuong.clearValidators();
      this.f.id_bacLuong.updateValueAndValidity();
      this.f.id_phanLoaiHopDong.clearValidators();
      this.f.id_phanLoaiHopDong.updateValueAndValidity();
      this.f.tienLuong.clearValidators();
      this.f.tienLuong.updateValueAndValidity();
      
      this.f.heSoLuong.setValidators([Validators.required]);
      this.f.heSoLuong.updateValueAndValidity();
    }
   if(this.f.id_loai.value == 1){
      this.f.id_ngachLuong.clearValidators();
      this.f.id_ngachLuong.updateValueAndValidity();
      this.f.id_bacLuong.clearValidators();
      this.f.id_bacLuong.updateValueAndValidity();
      this.f.heSoLuong.clearValidators();
      this.f.heSoLuong.updateValueAndValidity();

      this.f.id_phanLoaiHopDong.setValidators([Validators.required]);
      this.f.id_phanLoaiHopDong.updateValueAndValidity();
      this.f.tienLuong.setValidators([Validators.required]);
      this.f.tienLuong.updateValueAndValidity();
    }
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    this.updateFormQTL = this.fb.group(
      {
        id_canBo : [null],
        tuNgay : [null],
        id_heSoLuong: [null],
        soQuyetDinh: [null],
        ngayQuyetDinh: [null],
        tienLuong: [null],
        pCTNVuotKhung: [null],
        heSoLuong: [null],
        ngayBienDong: [null],
        ngayGiuBac: [null],
        phanTramHuong: [null],
        heSoBaoLuu: [null],
        ghiChu: [null],
        id_loaiLenLuong: [null],
        id_phanLoaiHopDong: [null]
      });
      if(this.f.id_loai.value == 0){
        this.f.id_phanLoaiHopDong.setValue(0);
        this.f.tienLuong.setValue(null);
        this.f.heSoLuong.setValue(null);
        //this.f.id_heSoLuong.setValue(parseFloat(this.heSo));
      }
      else if(this.f.id_loai.value == 1){
        this.f.id_ngachLuong.setValue(null);
        this.f.id_bacLuong.setValue(null);
        this.f.id_heSoLuong.setValue(null);
        this.f.phanTramHuong.setValue(null);
        this.f.pCTNVuotKhung.setValue(null);
        this.f.heSoBaoLuu.setValue(null);
        this.tong_HeSo = null;
        this.thanhTien = null;
        this.f.heSoLuong.setValue(null);
      }
      else{
        this.f.id_ngachLuong.setValue(null);
        this.f.id_bacLuong.setValue(null);
        this.f.id_heSoLuong.setValue(null);
        this.f.phanTramHuong.setValue(null);
        this.f.pCTNVuotKhung.setValue(null);
        this.f.heSoBaoLuu.setValue(null);
        this.tong_HeSo = null;
        this.thanhTien = null;
        this.f.id_heSoLuong.setValue(null);
        this.f.id_phanLoaiHopDong.setValue(null);
      }
      this.updateFormQTL.setValue({
        id_canBo: this.data.id_canBo,
        tuNgay: this.f.tuNgay.value,
        id_heSoLuong: this.f.id_heSoLuong.value,
        soQuyetDinh: this.f.soQuyetDinh.value,
        ngayQuyetDinh: this.f.ngayQuyetDinh.value,
        tienLuong: this.f.tienLuong.value,
        pCTNVuotKhung: this.f.pCTNVuotKhung.value,
        heSoLuong: this.f.heSoLuong.value,
        ngayBienDong: this.f.ngayBienDong.value,
        ngayGiuBac: this.f.tuNgay.value,
        phanTramHuong: this.f.phanTramHuong.value,
        heSoBaoLuu: this.f.heSoBaoLuu.value,
        ghiChu: this.f.ghiChu.value,
        id_loaiLenLuong: this.f.id_loaiLenLuong.value,
        id_phanLoaiHopDong: this.f.id_phanLoaiHopDong.value
      });

    if (!this.data.body) {
      this.hscbService.insertCanBoQuaTrinhLuong(this.updateFormQTL.value, this.common.HSCB_QuaTrinhLuong.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            //this.dialogRef.close(res.message);
            this.hscbService.updateCanBoQuaTrinhLuongDenNgay({id_canbo: this.data.id_canBo}, this.common.HSCB_QuaTrinhLuong.Insert)
            .subscribe(res => {
              if(res.error){
                this.common.messageErr(res);
              }else{
                this.dialogRef.close(res.message);
              }
            });
          }
        });
    } else {
      this.hscbService.updateCanBoQuaTrinhLuong({
        data: this.updateFormQTL.value,
        condition: { id: this.data.body.id }
      }, this.common.HSCB_QuaTrinhLuong.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            //this.dialogRef.close(res.message);
            this.hscbService.updateCanBoQuaTrinhLuongDenNgay({id_canBo: 2}, this.common.HSCB_QuaTrinhLuong.Update)
            .subscribe(res => {
              if(res.error){
                this.common.messageErr(res);
              }else{
                this.dialogRef.close(res.message);
              }
            });
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
