import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import * as moment from 'moment';
// Translate
import { CommonService } from './../../../../services/common.service';
import { HscbService } from './../../../../services/hscb.service';
import * as dateFormat from 'dateformat';
import { ActivatedRoute, Router } from '@angular/router';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}

@Component({
  selector: 'app-dialog-canbo-kyluat-update',
  templateUrl: './dialog-canbo-kyluat-update.component.html',
})
export class DialogUpdateCanBoKyLuatComponent implements OnInit {
  lstcapquyetdinh: select[] = [];
  lsthinhthuckyluat: select[] = [];
  lstloaihinhthuckyluat: select[] = [];
  updateForm: FormGroup;
  submitted = false;
  today: String;
  id_canBo: string;
  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    public dialogRef: MatDialogRef<DialogUpdateCanBoKyLuatComponent>,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.today = moment().format('YYYY/MM/DD');
    this.loadcombo();
    this.createForm();
  }
  createForm() {
    this.id_canBo = this.data.id_canBo;
    this.updateForm = this.fb.group(
      {
        id_canBo: [this.id_canBo],
        soQuyetDinh: [null],
        ngayQuyetDinh: ['', Validators.required],
        id_capQuyetDinh: [null],
        id_hinhThucKyLuat: ['', Validators.required],
        id_loaiHinhThucKyLuat: ['', Validators.required],
        tuNgay: [null],
        denNgay: [null],
        noiDung: [null],
        hinhThucKyLuatBoSung: [null],
      });
    if (this.data.body) {
      this.updateForm.setValue({
        id_canBo: this.id_canBo,
        soQuyetDinh: this.data.body.soQuyetDinh,
        ngayQuyetDinh: this.data.body.ngayQuyetDinh,
        id_capQuyetDinh: this.data.body.id_capQuyetDinh,
        id_hinhThucKyLuat: this.data.body.id_hinhThucKyLuat,
        id_loaiHinhThucKyLuat: this.data.body.id_loaiHinhThucKyLuat,
        tuNgay: this.data.body.tuNgay,
        denNgay: this.data.body.denNgay,
        noiDung: this.data.body.noiDung,
        hinhThucKyLuatBoSung: this.data.body.hinhThucKyLuatBoSung
      });
      if (this.data.body.tuNgay) {
        this.f.tuNgay.setValue(dateFormat(this.data.body.tuNgay, 'yyyy-mm-dd', true));
      }
      if (this.data.body.denNgay) {
        this.f.denNgay.setValue(dateFormat(this.data.body.denNgay, 'yyyy-mm-dd', true));
      }
      if (this.data.body.ngayQuyetDinh) {
        this.f.ngayQuyetDinh.setValue(dateFormat(this.data.body.ngayQuyetDinh, 'yyyy-mm-dd', true));
      }
    }
  }
  loadcombo(){
    this.getCapQuyetDinh();
    this.getHinhThucKyLuat();
    this.getLoaiHinhThucKyLuat();
  }
  getCapQuyetDinh(){
    this.lstcapquyetdinh = [];
    this.hscbService.getCapQuyetDinh({}, this.common.HSCB_CanBoKyLuat.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstcapquyetdinh.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getHinhThucKyLuat(){
    this.lsthinhthuckyluat = [];
    this.hscbService.getHinhThucKyLuat({}, this.common.HSCB_CanBoKyLuat.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lsthinhthuckyluat.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getLoaiHinhThucKyLuat(){
    this.lstloaihinhthuckyluat = [];
    this.hscbService.getLoaiHinhThucKyLuat({}, this.common.HSCB_CanBoKyLuat.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstloaihinhthuckyluat.push({ value: element.id, display: element.name});
          });
        }
      });
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    if (!this.data.body) {
      this.hscbService.insertCanBoKyLuat(this.updateForm.value, this.common.HSCB_CanBoKyLuat.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.hscbService.updateCanBoKyLuat({
        data: this.updateForm.value,
        condition: { id: this.data.body.id }
      }, this.common.HSCB_CanBoKyLuat.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
