import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../../services/common.service';
import { HscbService } from './../../../../services/hscb.service';
import * as dateFormat from 'dateformat';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}

@Component({
  selector: 'app-dialog-canbo-suckhoehangnam-update',
  templateUrl: './dialog-canbo-suckhoehangnam-update.component.html',
})
export class DialogUpdateCanBoSucKhoeHangnamComponent implements OnInit {
  lstxeploaisuckhoe: select[] = [];
  id_canBo: string;
  updateForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    public dialogRef: MatDialogRef<DialogUpdateCanBoSucKhoeHangnamComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.loadcombo();
    this.createForm();
  }
  createForm() {
    this.id_canBo = this.data.id_canBo;
    this.updateForm = this.fb.group(
      {
        id_canbo: [''],
        id_xepLoaiSucKhoe: ['', [Validators.required]],
        nam: [Validators.min(1900), Validators.max(2100)],
        huyetAp : [null],
        canNang: [Validators.min(0), Validators.max(200)],
        chieuCao: [Validators.min(0), Validators.max(200)]
      });
    if (this.data.body) {
      this.updateForm.setValue({
       id_canbo: this.data.id_canBo,
       id_xepLoaiSucKhoe: this.data.body.id_xepLoaiSucKhoe,
       nam: this.data.body.nam,
       huyetAp : this.data.body.huyetAp,
       canNang : this.data.body.canNang,
       chieuCao : this.data.body.chieuCao
      });
    }
  }
  loadcombo(){
    this.getXepLoaiSucKhoe();
  }
  getXepLoaiSucKhoe(){
    this.lstxeploaisuckhoe = [];
    this.hscbService.getXepLoaiSucKhoe({}, this.common.HSCB_CanBoSucKhoeHangNam.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstxeploaisuckhoe.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  onSubmit() {
    this.submitted = true;
    this.updateForm.value.id_canbo = this.data.id_canBo;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    if (!this.data.body) {
      this.hscbService.insertSucKhoeHangNam(this.updateForm.value, this.common.HSCB_CanBoSucKhoeHangNam.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.hscbService.updateSucKhoeHangNam({
        data: this.updateForm.value,
        condition: { id: this.data.body.canbo_suckhoehangnam_id }
      }, this.common.HSCB_CanBoSucKhoeHangNam.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
