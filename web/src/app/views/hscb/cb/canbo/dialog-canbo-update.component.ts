import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import * as dateFormat from 'dateformat';
import { TreeNode } from 'primeng/api';
import * as moment from 'moment';
// Translate
import { CommonService } from './../../../../services/common.service';
import { HscbService } from './../../../../services/hscb.service';
import { DonViService } from '../../../../services/don-vi.service';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}

@Component({
  selector: 'app-dialog-canbo-update',
  templateUrl: './dialog-canbo-update.component.html',
})

export class DialogUpdateCanBoComponent implements OnInit {
  today: String;
  updateForm: FormGroup;
  updateCanBo: FormGroup;
  updateQTCT: FormGroup;
  updateCanBoDiaChi : FormGroup;
  submitted = false;
  lstnhommau: select[] = [];
  lstchucdanh: select[] = [];
  lstchucvu: select[] =[];
  lsttinh: select[] = [];
  lsthuyenns: select[] = [];
  lstxans: select[] = [];
  lsthuyentt: select[] = [];
  lstxatt: select[] = [];
  lsthuyentht: select[] = [];
  lstxatht: select[] = [];
  lsthuyenqq: select[] = [];
  lstxaqq: select[] = [];
  lstgioitinh: select[] = [];
  lstloaiqtct: select[] = [];
  donVis: TreeNode[];
  selectedBaoCaos: TreeNode;
  iddonvi: string;
  idtinh_ns: string;
  idhuyen_ns: string;
  idtinh_tt: string;
  idhuyen_tt: string;
  idtinh_tht: string;
  idhuyen_tht: string;
  idtinh_qq: string;
  idhuyen_qq: string;
  
  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private fbQTCT: FormBuilder,
    private hscbService: HscbService,
    public dialogRef: MatDialogRef<DialogUpdateCanBoComponent>,
    private donVi: DonViService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }
  ngOnInit() {
    this.today = moment().format('YYYY/MM/DD');
    
    if(this.data){
      this.getDiaChis();
    }
    this.createForm();
    this.loadcombo();
  }
  createForm() {
    this.iddonvi = '0';
    this.updateForm = this.fb.group(
      {
        maCanBo: ['', [Validators.required]],
        hoDem: ['', [Validators.required]],
        ten: ['', [Validators.required]],
        ngaySinh: ['',[Validators.required]],
        email: [null],
        dienthoaiDD: [null],
        //anh: [null],
        id_nhomMau: [null],
        gioiTinh: ['',[Validators.required]],
        
        id_canBo: [null],
        tuNgay: ['', Validators.required],
        denNgay: [null],
        ngayQuyetDinh: [null],
        ngayKy: [null],
        soQuyetDinh: [null],
        id_donVi: ['', Validators.required],
        id_chucDanh: [null],
        id_chucVu: [null],
        id_loaiQuaTrinhCongTac: ['',[Validators.required]],
        
        id_tinh_ns: [null],
        id_huyen_ns: [null],
        id_xa_ns: [null],
        thonXom_ns: [null],
        id_tinh_tht: [null],
        id_huyen_tht: [null],
        id_xa_tht: [null],
        thonXom_tht: [null],
        id_tinh_tt: [null],
        id_huyen_tt: [null],
        id_xa_tt: [null],
        thonXom_tt: [null],
        id_tinh_qq: [null],
        id_huyen_qq: [null],
        id_xa_qq: [null],
        thonXom_qq: [null]
      });
      this.f.id_donVi.enable();
      this.f.tuNgay.enable();
      this.f.denNgay.enable();
      this.f.soQuyetDinh.enable();
      this.f.ngayQuyetDinh.enable();
      this.f.ngayKy.enable();
      this.f.id_chucDanh.enable();
      this.f.id_chucVu.enable();
      this.f.id_loaiQuaTrinhCongTac.enable();
    if (this.data) {
      this.updateForm.setValue({
        maCanBo: this.data.maCanBo, 
        hoDem: this.data.hoDem,
        ten: this.data.ten,
        ngaySinh: this.data.ngaySinh,
        email: this.data.email,
        dienthoaiDD: this.data.dienthoaiDD,
        //anh: this.data.anh,
        id_nhomMau: this.data.id_nhomMau,
        gioiTinh: (this.data.gioiTinh == null)? '0' : '' + this.data.gioiTinh.data[0],

        id_canBo: this.data.canbo_id,
        tuNgay: this.data.tuNgay,
        denNgay: this.data.denNgay,
        ngayQuyetDinh: this.data.ngayQuyetDinh,
        ngayKy: this.data.ngayKy,
        soQuyetDinh: this.data.soQuyetDinh,
        id_donVi: this.data.id_donVi,
        id_chucDanh: this.data.id_chucDanh,
        id_chucVu: this.data.id_chucVu,
        id_loaiQuaTrinhCongTac: this.data.id_loaiquatrinhcongtac,
        id_tinh_ns: null,
        id_huyen_ns: null,
        id_xa_ns: null,
        thonXom_ns: null,
        id_tinh_tht: null,
        id_huyen_tht: null,
        id_xa_tht: null,
        thonXom_tht: null,
        id_tinh_tt: null,
        id_huyen_tt: null,
        id_xa_tt: null,
        thonXom_tt: null,
        id_tinh_qq: null,
        id_huyen_qq: null,
        id_xa_qq: null,
        thonXom_qq: null
      });
      if (this.data.id_donVi) {
        this.iddonvi = '' + this.data.id_donVi;
      }
      if (this.data.ngaySinh) {
        this.f.ngaySinh.setValue(dateFormat(this.data.ngaySinh, 'yyyy-mm-dd', true));
      }
      if (this.data.tuNgay) {
        this.f.tuNgay.setValue(dateFormat(this.data.tuNgay, 'yyyy-mm-dd', true));
      }
      if (this.data.denNgay) {
        this.f.denNgay.setValue(dateFormat(this.data.denNgay, 'yyyy-mm-dd', true));
      }
      if (this.data.ngayQuyetDinh) {
        this.f.ngayQuyetDinh.setValue(dateFormat(this.data.ngayQuyetDinh, 'yyyy-mm-dd', true));
      }
      if (this.data.ngayKy) {
        this.f.ngayKy.setValue(dateFormat(this.data.ngayKy, 'yyyy-mm-dd', true));
      }
      this.f.gioiTinh.setValue(parseInt(this.f.gioiTinh.value, 10));

      this.f.id_donVi.disable();
      this.f.tuNgay.disable();
      this.f.denNgay.disable();
      this.f.soQuyetDinh.disable();
      this.f.ngayQuyetDinh.disable();
      this.f.ngayKy.disable();
      this.f.id_chucDanh.disable();
      this.f.id_chucVu.disable();
      this.f.id_loaiQuaTrinhCongTac.disable();
    }
    
  }
  changetinh(loaidc: string){
    if(loaidc ==='ns'){
      this.getHuyen(this.f.id_tinh_ns.value,"ns");
    }else if(loaidc ==='tt'){
      this.getHuyen(this.f.id_tinh_tt.value,"tt");
    }else if(loaidc ==='tht'){
      this.getHuyen(this.f.id_tinh_tht.value,"tht");
    }else if(loaidc ==='qq'){
      this.getHuyen(this.f.id_tinh_qq.value,"qq");
    }
  }
  changehuyen(loaidc: string){
    if(loaidc ==='ns'){
      this.getXa(this.f.id_huyen_ns.value,"ns");
    }else if(loaidc ==='tt'){
      this.getXa(this.f.id_huyen_tt.value,"tt");
    }else if(loaidc ==='tht'){
      this.getXa(this.f.id_huyen_tht.value,"tht");
    }else if(loaidc ==='qq'){
      this.getXa(this.f.id_huyen_qq.value,"qq");
    }
  }
  // lấy dữ liệu của địa chỉ
  getDiaChis(){
    this.hscbService.getDiaChi({id_canbo: this.data.canbo_id, tableName : 'canbo'}, this.common.HSCB_CanBo.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            if(element.fieldName ==='NoiSinh'){
              this.f.id_tinh_ns.setValue(element.id_tinh);
              this.f.id_huyen_ns.setValue(element.id_huyen);
              this.f.id_xa_ns.setValue(element.id_xa);
              this.f.thonXom_ns.setValue(element.thonXom);
              this.idtinh_ns = element.id_tinh;
              this.idhuyen_ns = element.id_huyen;
              this.getHuyen(element.id_tinh, "ns");
              this.getXa(element.id_huyen,"ns");
            }
            else if(element.fieldName ==='TamTru'){
              this.f.id_tinh_tt.setValue(element.id_tinh);
              this.f.id_huyen_tt.setValue(element.id_huyen);
              this.f.id_xa_tt.setValue(element.id_xa);
              this.f.thonXom_tt.setValue(element.thonXom);
              this.idtinh_tt = element.id_tinh;
              this.idhuyen_tt = element.id_huyen;
              this.getHuyen(element.id_tinh, "tt");
              this.getXa(element.id_huyen,"tt");
            }
            else if(element.fieldName ==='ThuongTru'){
              this.f.id_tinh_tht.setValue(element.id_tinh);
              this.f.id_huyen_tht.setValue(element.id_huyen);
              this.f.id_xa_tht.setValue(element.id_xa);
              this.f.thonXom_tht.setValue(element.thonXom);
              this.idtinh_tht = element.id_tinh;
              this.idhuyen_tht = element.id_huyen;
              this.getHuyen(element.id_tinh, "tht");
              this.getXa(element.id_huyen,"tht");
            }
            else if(element.fieldName ==='QueQuan'){
              this.f.id_tinh_qq.setValue(element.id_tinh);
              this.f.id_huyen_qq.setValue(element.id_huyen);
              this.f.id_xa_qq.setValue(element.id_xa);
              this.f.thonXom_qq.setValue(element.thonXom);
              this.idtinh_qq = element.id_tinh;
              this.idhuyen_qq = element.id_huyen;
              this.getHuyen(element.id_tinh, "qq");
              this.getXa(element.id_huyen, "qq");
            }
          });
        }
      });
  }
  loadcombo(){
    this.getNhomMau();
    this.getChucDanh();
    this.getChucVu();
    this.getDonVis();
    this.getTinh();
    this.getGioiTinh();
    this.getLoaiQuaTrinhCongTac();
  }
  getNhomMau(){
    this.lstnhommau = [];
    this.hscbService.getNhomMau({}, this.common.HSCB_CanBo.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstnhommau.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getDonVis(){
    this.donVi.getTreeToValue({ id_trungTam: 1, tuNgay: this.today, denNgay: this.today }, this.common.HSCB_CanBo.Insert)
    .subscribe((res: any) => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.donVis = res.data;
        if (this.donVis) {
          this.checkNode(this.donVis,parseInt(this.iddonvi, 10), 'BaoCao');
        }
      }
    });
  }
  getChucDanh(){
    this.lstchucdanh =[];
    this.hscbService.getDanhMucChucDanh({}, this.common.HSCB_CanBo.Insert)
    .subscribe(res =>{
      if(!res.error){
        res.data.forEach(element => {
          this.lstchucdanh.push({ value: element.id, display: element.name})
        })
      }
    })
  }
  getChucVu(){
    this.lstchucvu =[];
    this.hscbService.getDanhMucChucVu({}, this.common.HSCB_CanBo.Insert)
    .subscribe(res =>{
      if(!res.error){
        res.data.forEach(element => {
          this.lstchucvu.push({ value: element.id, display: element.name})
        })
      }
    })
  }
  getTinh(){
    this.lsttinh = [];
    this.hscbService.getDMTinh({}, this.common.HSCB_CanBo.Insert)
    .subscribe(res =>{
      if(!res.error){
        res.data.forEach(element => {
          this.lsttinh.push({ value: element.id, display: element.name})
        })
      }
    })
  }
  getHuyen(id : any, loaidiachi : string){
    if(loaidiachi ==="tt"){
      this.lsthuyentt =[];
    }else if(loaidiachi ==="ns"){
      this.lsthuyenns =[];
    }else if(loaidiachi ==="tht"){
      this.lsthuyentht =[];
    }else if(loaidiachi ==="qq"){
      this.lsthuyenqq =[];
    }
    this.hscbService.getDMHuyen({id_tinh: id}, this.common.HSCB_CanBo.Insert)
    .subscribe(res =>{
      if(!res.error){
        res.data.forEach(element => {
          if(loaidiachi ==="tt")
          {
            this.lsthuyentt.push({ value: element.id, display: element.name})
          }else if(loaidiachi ==="ns"){
            this.lsthuyenns.push({ value: element.id, display: element.name})
          }else if(loaidiachi ==="tht"){
            this.lsthuyentht.push({ value: element.id, display: element.name})
          }else if(loaidiachi ==="qq"){
            this.lsthuyenqq.push({ value: element.id, display: element.name})
          }
        })
      }
    })
  }
  getXa(id : any, loaidiachi: string){
    if(loaidiachi === "tt"){
      this.lstxatt =[];
    }else if(loaidiachi === "ns"){
      this.lstxans =[];
    }else if(loaidiachi === "tht"){
      this.lstxatht =[];
    }else if(loaidiachi ==="qq"){
      this.lstxaqq =[];
    }
    
    this.hscbService.getDMXa({id_huyen: id}, this.common.HSCB_CanBo.Insert)
    .subscribe(res =>{
      if(!res.error){
        res.data.forEach(element => {
          if(loaidiachi ==="ns"){
            this.lstxans.push({ value: element.id, display: element.name})
          }else if(loaidiachi === "tt"){
            this.lstxatt.push({ value: element.id, display: element.name})
          }else if(loaidiachi === "tht"){
            this.lstxatht.push({ value: element.id, display: element.name})
          }else if(loaidiachi === "qq"){
            this.lstxaqq.push({ value: element.id, display: element.name})
          }
        })
      }
    })
  }
  getGioiTinh(){
    this.lstgioitinh = [];
    this.lstgioitinh.push({ value: 1, display: "Nam"})
    this.lstgioitinh.push({ value: 0, display: "Nữ"})
  }
  getLoaiQuaTrinhCongTac(){
    this.lstloaiqtct = [];
    this.hscbService.getLoaiQuaTrinhCongTac({}, this.common.HSCB_CanBo.Insert)
    .subscribe(res =>{
      if(!res.error){
        res.data.forEach(element => {
          this.lstloaiqtct.push({ value: element.id, display: element.name})
        })
      }
    })
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    this.updateCanBoDiaChi = this.fb.group({
      id_canBo: null,
      id_tinh: null,
      id_huyen: null,
      id_xa: null,
      thonXom: null,
      tableName: null,
      fieldName: null,
    })
    if (!this.data) {
      this.updateCanBo = this.fb.group({
        maCanBo: null,
        hoDem:  null,
        ten: null,
        ngaySinh: null,
        dienthoaiDD: null,
        email: null,
        id_nhomMau: null,
        gioiTinh: null
      })
      this.updateCanBo.setValue({
        maCanBo: this.updateForm.value.maCanBo,
        hoDem: this.updateForm.value.hoDem,
        ten: this.updateForm.value.ten,
        ngaySinh: this.updateForm.value.ngaySinh,
        dienthoaiDD : this.updateForm.value.dienthoaiDD,
        email: this.updateForm.value.email,
        id_nhomMau: this.updateForm.value.id_nhomMau,
        gioiTinh: this.updateForm.value.gioiTinh
      })

      this.updateQTCT = this.fb.group({
        id_canBo: null,
        tuNgay: null,
        denNgay: null,
        ngayQuyetDinh: null,
        ngayKy: null,
        soQuyetDinh: null,
        id_donVi: null,
        id_chucDanh: null,
        id_chucVu: null,
        id_loaiQuaTrinhCongTac: null
      })
      this.updateQTCT.setValue({
      id_canBo: null,
      tuNgay: this.updateForm.value.tuNgay,
      denNgay: this.updateForm.value.denNgay,
      ngayQuyetDinh: this.updateForm.value.ngayQuyetDinh,
      ngayKy: this.updateForm.value.ngayKy,
      soQuyetDinh: this.updateForm.value.soQuyetDinh,
      id_donVi: this.updateForm.value.id_donVi,
      id_chucDanh: this.updateForm.value.id_chucDanh,
      id_chucVu: this.updateForm.value.id_chucVu,
      id_loaiQuaTrinhCongTac: this.updateForm.value.id_loaiQuaTrinhCongTac
      })

      
      //update bảng cán bộ
      this.hscbService.insertCanBo(this.updateCanBo.value, this.common.HSCB_CanBo.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            //this.dialogRef.close(res.message);
            this.updateQTCT.value.id_canBo = res.data.id;
             //update bảng quá trình công tác
              this.hscbService.insertQuaTrinhCongTac(this.updateQTCT.value, this.common.HSCB_CanBo.Insert)
              .subscribe(res => {
                if(res.error){
                  this.common.messageErr(res);
                } else {
                  //update cán bộ địa chỉ nơi sinh
                  if(this.updateForm.value.id_tinh_ns && this.updateForm.value.id_huyen_ns && this.updateForm.value.id_xa_ns && this.updateForm.value.thonXom_ns)
                  {
                    this.updateCanBoDiaChi.setValue({
                      id_canBo: this.updateQTCT.value.id_canBo,
                      id_tinh: this.updateForm.value.id_tinh_ns,
                      id_huyen:this.updateForm.value.id_huyen_ns,
                      id_xa: this.updateForm.value.id_xa_ns,
                      thonXom: this.updateForm.value.thonXom_ns,
                      tableName: "canbo",
                      fieldName: "NoiSinh",
                    })
                    this.hscbService.updateDiaChi(this.updateCanBoDiaChi.value, this.common.HSCB_CanBo.Insert)
                    .subscribe(res => {
                      if(res.error){
                        this.common.messageErr(res);
                      } else {
                        this.dialogRef.close(res.message);
                      }
                    })
                  }
                  if(this.updateForm.value.id_tinh_tht && this.updateForm.value.id_huyen_tht && this.updateForm.value.id_xa_tht && this.updateForm.value.thonXom_tht)
                  {
                     //update cán bộ địa chỉ thường trú
                     this.updateCanBoDiaChi.setValue({
                      id_canBo: this.updateQTCT.value.id_canBo,
                      id_tinh: this.updateForm.value.id_tinh_tht,
                      id_huyen:this.updateForm.value.id_huyen_tht,
                      id_xa: this.updateForm.value.id_xa_tht,
                      thonXom: this.updateForm.value.thonXom_tht,
                      tableName: "canbo",
                      fieldName: "ThuongTru",
                    })
                    this.hscbService.updateDiaChi(this.updateCanBoDiaChi.value, this.common.HSCB_CanBo.Insert)
                    .subscribe(res => {
                      if(res.error){
                        this.common.messageErr(res);
                      } else {
                        this.dialogRef.close(res.message);
                      }
                    })
                  }
                       //update cán bộ địa chỉ tạm trú
                  if(this.updateForm.value.id_tinh_tht && this.updateForm.value.id_huyen_tt && this.updateForm.value.id_xa_tt && this.updateForm.value.thonXom_tt)
                    {
                      this.updateCanBoDiaChi.setValue({
                      id_canBo: this.updateQTCT.value.id_canBo,
                      id_tinh: this.updateForm.value.id_tinh_tt,
                      id_huyen:this.updateForm.value.id_huyen_tt,
                      id_xa: this.updateForm.value.id_xa_tt,
                      thonXom: this.updateForm.value.thonXom_tt,
                      tableName: "canbo",
                      fieldName: "TamTru",
                        })
                      this.hscbService.updateDiaChi(this.updateCanBoDiaChi.value, this.common.HSCB_CanBo.Insert)
                      .subscribe(res => {
                        if(res.error){
                          this.common.messageErr(res);
                        } else {
                          this.dialogRef.close(res.message);
                        }
                      })
                    }
                     //update cán bộ địa chỉ quê quán
                     if(this.updateForm.value.id_tinh_qq && this.updateForm.value.id_huyen_qq && this.updateForm.value.id_xa_qq && this.updateForm.value.thonXom_qq)
                     {
                        this.updateCanBoDiaChi.setValue({
                          id_canBo: this.updateQTCT.value.id_canBo,
                          id_tinh: this.updateForm.value.id_tinh_qq,
                          id_huyen:this.updateForm.value.id_huyen_qq,
                          id_xa: this.updateForm.value.id_xa_qq,
                          thonXom: this.updateForm.value.thonXom_qq,
                          tableName: "canbo",
                          fieldName: "QueQuan",
                        })
                        this.hscbService.updateDiaChi(this.updateCanBoDiaChi.value, this.common.HSCB_CanBo.Insert)
                        .subscribe(res => {
                          if(res.error){
                            this.common.messageErr(res);
                          } else {
                            this.dialogRef.close(res.message);
                          }
                        })
                      }
                }
              })
          }
        });
       
    } else {
        //edit cán bộ
        this.updateCanBo = this.fb.group({
          maCanBo: null,
          hoDem:  null,
          ten: null,
          ngaySinh: null,
          dienthoaiDD: null,
          email: null,
          id_nhomMau: null,
          gioiTinh: null
        })
        this.updateCanBo.setValue({
          maCanBo: this.updateForm.value.maCanBo,
          hoDem: this.updateForm.value.hoDem,
          ten: this.updateForm.value.ten,
          ngaySinh: this.updateForm.value.ngaySinh,
          dienthoaiDD : this.updateForm.value.dienthoaiDD,
          email: this.updateForm.value.email,
          id_nhomMau: this.updateForm.value.id_nhomMau,
          gioiTinh: this.updateForm.value.gioiTinh
        })
  
      this.hscbService.editCanBo({
        data: this.updateCanBo.value,
        condition: { canbo_id: this.data.canbo_id }
      }, this.common.HSCB_CanBo.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            //this.dialogRef.close(res.message);
            //update cán bộ địa chỉ nơi sinh
            this.updateCanBoDiaChi.setValue({
              id_canBo: this.data.canbo_id,
              id_tinh: this.updateForm.value.id_tinh_ns,
              id_huyen:this.updateForm.value.id_huyen_ns,
              id_xa: this.updateForm.value.id_xa_ns,
              thonXom: this.updateForm.value.thonXom_ns,
              tableName: "canbo",
              fieldName: "NoiSinh",
            })
            this.hscbService.updateDiaChi(this.updateCanBoDiaChi.value, this.common.HSCB_CanBo.Insert)
            .subscribe(res => {
              if(res.error){
                this.common.messageErr(res);
              } else {
                this.dialogRef.close(res.message);
              }
            })
               //update cán bộ địa chỉ thường trú
               this.updateCanBoDiaChi.setValue({
                id_canBo: this.data.canbo_id,
                id_tinh: this.updateForm.value.id_tinh_tht,
                id_huyen:this.updateForm.value.id_huyen_tht,
                id_xa: this.updateForm.value.id_xa_tht,
                thonXom: this.updateForm.value.thonXom_tht,
                tableName: "canbo",
                fieldName: "ThuongTru",
              })
              this.hscbService.updateDiaChi(this.updateCanBoDiaChi.value, this.common.HSCB_CanBo.Insert)
              .subscribe(res => {
                if(res.error){
                  this.common.messageErr(res);
                } else {
                  this.dialogRef.close(res.message);
                }
              })
                 //update cán bộ địa chỉ tạm trú
            this.updateCanBoDiaChi.setValue({
              id_canBo: this.data.canbo_id,
              id_tinh: this.updateForm.value.id_tinh_tt,
              id_huyen:this.updateForm.value.id_huyen_tt,
              id_xa: this.updateForm.value.id_xa_tt,
              thonXom: this.updateForm.value.thonXom_tt,
              tableName: "canbo",
              fieldName: "TamTru",
            })
            this.hscbService.updateDiaChi(this.updateCanBoDiaChi.value, this.common.HSCB_CanBo.Insert)
            .subscribe(res => {
              if(res.error){
                this.common.messageErr(res);
              } else {
                this.dialogRef.close(res.message);
              }
            })
               //update cán bộ địa chỉ quê quán
               this.updateCanBoDiaChi.setValue({
                id_canBo: this.data.canbo_id,
                id_tinh: this.updateForm.value.id_tinh_qq,
                id_huyen:this.updateForm.value.id_huyen_qq,
                id_xa: this.updateForm.value.id_xa_qq,
                thonXom: this.updateForm.value.thonXom_qq,
                tableName: "canbo",
                fieldName: "QueQuan",
              })
              this.hscbService.updateDiaChi(this.updateCanBoDiaChi.value, this.common.HSCB_CanBo.Insert)
              .subscribe(res => {
                if(res.error){
                  this.common.messageErr(res);
                } else {
                  this.dialogRef.close(res.message);
                }
              })
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  nodeSelectBaoCao(event) {
    if (this.updateForm.controls.id_donVi.value === event.node.data) {
      this.nodeUnselectBaoCao(event);
    } else {
      this.updateForm.controls.id_donVi.setValue(event.node.data);
    }
  }
  nodeUnselectBaoCao(event) {
    this.updateForm.controls.id_donVi.setValue(null);
    this.selectedBaoCaos = null;
  }
  // chi check cho select 1 node (khong multi)
  checkNode(nodes: TreeNode[], str: number, selected) {
    nodes.forEach(node => {
      if (str === node.data) {
        if (selected === 'BaoCao') {
          this.selectedBaoCaos = (node);
        }
      }
      if (node.children !== undefined) {
        node.children.forEach(child => {
          if (str === child.data && !str === node.data) {
            node.partialSelected = true;
            child.parent = node;
          }
          if (str === node.data) {
            child.parent = node;
          }
        });
      } else {
        return;
      }
      this.checkNode(node.children, str, selected);
      node.children.forEach(child => {
        if (child.partialSelected) {
          node.partialSelected = true;
        }
      });
    });
  }
}
