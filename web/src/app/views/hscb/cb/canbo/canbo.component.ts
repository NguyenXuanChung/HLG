import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../../shared/confirmation-dialog/confirmation-dialog.component';
import { CheckableSettings } from '@progress/kendo-angular-treeview';
import * as dateFormat from 'dateformat';
import * as moment from 'moment';

// Translate
import { CommonService } from './../../../../services/common.service';
import { StorageService } from '../../../../services/storage.service';
import { AuthConstants } from '../../../../config/auth-constants';
import { Router } from '@angular/router';
import  { HscbService } from'../../../../services/hscb.service';
import { DonViService } from '../../../../services/don-vi.service';
import { DialogUpdateCanBoComponent } from './dialog-canbo-update.component';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-canbo',
  templateUrl: './canbo.component.html'
})
export class CanBoComponent implements OnInit {
  displayedColumns = ['index', 'maCanBo', 'hoTen', 'email','tenDonVi', 'actions'];
  dataSource: MatTableDataSource<any>;
  today: String;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public checkedKeys: any[] = [];
  public key = 'data';
  searchForm: FormGroup;
  selectedRow: any;
  permission: any;
  dataTree: any;
  get fs() { return this.searchForm.controls; }

  constructor(
    private fb: FormBuilder,
    private hscbService: HscbService,
    private donviService : DonViService,
    private common: CommonService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  ngOnInit() {
    this.today = moment().format('YYYY/MM/DD');
    this.createSearchForm();
    this.onSearch();
    if (!this.permission.HSCB_CanBo._view) {
      this.router.navigate(['/']);
    }
    this.getLoadDonVi();
  }
  createSearchForm(){
    this.searchForm = this.fb.group({
      id_donVis: [null],
      ngay: [null]
    });
    this.searchForm.controls.ngay.setValue(sessionStorage.getItem('_search_ngay'))
    this.searchForm.controls.id_donVis.setValue(sessionStorage.getItem('_search_id_donVis'))
    var strDonVi = ''
    if(this.searchForm.controls.id_donVis.value != null){
      strDonVi = this.searchForm.controls.id_donVis.value;
    }

    var arrstrDonVi = strDonVi.split(',');
    if (arrstrDonVi.length != 0) {
      arrstrDonVi.forEach(element => {
        if(element != ''){
        this.checkedKeys.push(parseInt(element));
        }
      });
    }
    if(this.searchForm.controls.ngay.value == null){
      this.searchForm.controls.ngay.setValue(dateFormat(this.today , 'yyyy-mm-dd'));
    }
  }
  onSearch() {
    this.getCanBoByDonVis();
    //console.log(this.checkedKeys);
    var _search_id_donVis ='';
    var _search_ngay ='';
     if(this.searchForm.controls.id_donVis.value +'' != ''){
       _search_id_donVis = this.searchForm.controls.id_donVis.value;
       sessionStorage.setItem('_search_id_donVis', _search_id_donVis);
      }
      if(this.searchForm.controls.ngay.value + '' != ''){
        _search_ngay = this.searchForm.controls.ngay.value;
        sessionStorage.setItem('_search_ngay', _search_ngay);
      }
       
       
  }

  getCanBoByDonVis() {
    if (this.checkedKeys) {
      this.searchForm.controls.id_donVis.setValue(this.checkedKeys.toString());
    }
    this.hscbService.getCanBoByDonVis(this.searchForm.value, this.common.HSCB_CanBo.View).subscribe(res => {
      if (res.error) {
        this.dataSource = new MatTableDataSource();
        this.common.messageErr(res);
      } else {
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
  getLoadDonVi(){
    this.donviService.getTreeToValue({id_trungtam: 1, tuNgay: this.today, denNgay: this.today}, this.common.HSCB_CanBo.View)
    .subscribe((res: any) => {
      if(res.error){
        this.common.messageErr(res);
      }else{
        this.dataTree = res.data;
        //console.log(res.data);
      }
    });
    this.onSearch();
  }
  public get checkableSettings(): CheckableSettings {
    return {
      checkChildren: true,
      checkParents: false,
      enabled: true,
      mode: 'multiple',
      checkOnClick: false
    };
  }
  public children = (dataItem: any): Observable<any[]> => of(dataItem.children);
  public hasChildren(dataItem: any): boolean {
    return dataItem.children && dataItem.children.length > 0;
  }

  openCanBoDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogUpdateCanBoComponent, {
      width: '700px',
      height: '80%',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }

  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.hoTen }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.hscbService.deleteCanBo({ canbo_id: data.canbo_id }, this.common.HSCB_CanBo.Delete).subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.onSearch();
            this.common.messageRes(res.message);
          }
        });
      }
    });
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
