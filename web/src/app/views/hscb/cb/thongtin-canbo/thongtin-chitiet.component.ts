import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import * as dateFormat from 'dateformat';
import * as moment from 'moment';
import {Location} from '@angular/common';

// Translate
import { CommonService } from './../../../../services/common.service';
import { StorageService } from '../../../../services/storage.service';
import { AuthConstants } from '../../../../config/auth-constants';
import { ActivatedRoute, Router } from '@angular/router';
import { HscbService } from'../../../../services/hscb.service';
import { Observable, of } from 'rxjs';

interface select {
  value: number;
  display: string;
}

@Component({
  selector: 'app-thongtin-chitiet',
  templateUrl: './thongtin-chitiet.component.html',
  styleUrls: ['./thongtin-chitiet.component.css']
})
export class ThongTinChiTietComponent implements OnInit {
  displayedColumns1 = ['index', 'tuNgay', 'denNgay', 'tenDonVi','soQuyetDinh','noiLamViec','tenChucDanh','tenChucVu'];
  dataSource1: MatTableDataSource<any>;
  displayedColumns2 = ['index', 'tuNgay', 'denNgay', 'soHopDong','diaDiemLamViec','chucDanhChuyenMon','tenChucDanh','tenChucVu'];
  dataSource2: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  searchForm: FormGroup;
  id_canBo: string;
  permission: any;

  get fs() { return this.searchForm.controls; }

  constructor(
    private route: ActivatedRoute,
    private hscbService: HscbService,
    private common: CommonService,
    private router: Router,
    private storageService: StorageService,
    private _location: Location
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }

  backClicked() {
    this._location.back();
  }

  ngOnInit() {
    this.onSearch();
    if (!this.permission.HSCB_CanBoQuaTrinhCongTac._view) {
      this.router.navigate(['/']);
    }
  }

  onSearch() {
    this.id_canBo = this.route.snapshot.paramMap.get('id_canbo');
    this.getQuaTrinhCongTac();
    this.getHopDongLaoDong();
  }

  getQuaTrinhCongTac() {
    this.hscbService.getQuaTrinhCongTacgetByCanBo({id_canBo: this.id_canBo}, this.common.HSCB_CanBoQuaTrinhCongTac.View).subscribe(res => {
      //console.log(res);
      if (res.error) {
        this.dataSource1 = new MatTableDataSource();
        this.common.messageErr(res);
      } else {
        this.dataSource1 = new MatTableDataSource(res.data);
        this.dataSource1.paginator = this.paginator;
        this.dataSource1.sort = this.sort;
      }
    });
  }

  getHopDongLaoDong() {
    this.hscbService.getHopDongLaoDongByCanBo({id_canBo: this.id_canBo}, this.common.HSCB_CanBo.View).subscribe(res => {
      if (res.error) {
        this.dataSource2 = new MatTableDataSource();
        this.common.messageErr(res);
      } else {
        this.dataSource2 = new MatTableDataSource(res.data);
        this.dataSource2.paginator = this.paginator;
        this.dataSource2.sort = this.sort;
      }
    });
  }
}
