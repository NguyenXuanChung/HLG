import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../../services/common.service';
import { HscbService } from './../../../../services/hscb.service';
import * as dateFormat from 'dateformat';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}

@Component({
  selector: 'app-dialog-canbo-thannhan-update',
  templateUrl: './dialog-canbo-thannhan-update.component.html',
})
export class DialogUpdateCanBoThanNhanComponent implements OnInit {
  lstdantoc: select[] = [];
  lsttongiao: select[] = [];
  lstquanhe: select[] = [];
  id_canBo: string;
  updateForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    public dialogRef: MatDialogRef<DialogUpdateCanBoThanNhanComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.loadcombo();
    this.createForm();
  }
  createForm() {
    this.id_canBo = this.data.id_canBo;
    this.updateForm = this.fb.group(
      {
        id_canbo: [''],
        id_quanHe: ['', [Validators.required]],
        hoTen: ['', [Validators.required]],
        namSinh: [Validators.min(1900), Validators.max(2100)],
        queQuan: [null],
        noiOHienNay: [null],
        thongTinCongTac: [null],
        noiCongTac: [null],
        id_danToc: [null],
        id_tonGiao: [null],
        ghiChu: [null],
        laDangVien: [null],
        ngayVaoDang: [null]
      });
    if (this.data.body) {
      this.updateForm.setValue({

       // id: this.data.id,
       id_canbo: this.data.id_canBo,
        id_quanHe: this.data.body.id_quanHe,
        hoTen: this.data.body.hoTen,
        namSinh: this.data.body.namSinh,
        queQuan: this.data.body.queQuan,
        noiOHienNay: this.data.body.noiOHienNay,
        thongTinCongTac: this.data.body.thongTinCongTac,
        noiCongTac: this.data.body.noiCongTac,
        id_danToc: this.data.body.id_danToc,
        id_tonGiao: this.data.body.id_tonGiao,
        ghiChu: this.data.body.ghiChu,
        laDangVien: '0',
        ngayVaoDang: this.data.body.ngayVaoDang
      });
      if(this.data.body.laDangVien != null){
      this.f.laDangVien.setValue('' + this.data.body.laDangVien.data[0]);
      }
      if (this.data.body.ngayVaoDang) {
        this.f.ngayVaoDang.setValue(dateFormat(this.data.body.ngayVaoDang, 'yyyy-mm-dd', true));
      }
    }
  }
  loadcombo(){
    this.getQuanHe();
    this.getDanToc();
    this.getTonGiao();
  }
  getQuanHe(){
    this.lstquanhe = [];
    this.hscbService.getQuanHe({}, this.common.HSCB_CanBoThanNhan.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstquanhe.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getDanToc(){
    this.lstdantoc = [];
    this.hscbService.getDanhMucDanToc({}, this.common.HSCB_CanBoThanNhan.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstdantoc.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getTonGiao(){
    this.lsttongiao = [];
    this.hscbService.getDanhMucTonGiao({}, this.common.HSCB_CanBoThanNhan.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lsttongiao.push({ value: element.id, display: element.name});
          });
        }
      });
  }

  onSubmit() {
    this.submitted = true;
    this.updateForm.value.id_canbo = this.data.id_canBo;
    if(this.updateForm.value.laDangVien != null){
      this.updateForm.value.laDangVien = parseInt(this.f.laDangVien.value, 10);
      }
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    if (!this.data.body) {
      this.hscbService.insertThanNhan(this.updateForm.value, this.common.HSCB_CanBoThanNhan.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.hscbService.updateThanNhan({
        data: this.updateForm.value,
        condition: { id: this.data.body.canbo_thannhan_id }
      }, this.common.HSCB_CanBoThanNhan.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
