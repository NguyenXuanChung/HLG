import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import * as moment from 'moment';
// Translate
import { CommonService } from '../../../../services/common.service';
import { HscbService } from '../../../../services/hscb.service';
import * as dateFormat from 'dateformat';
import { ActivatedRoute, Router } from '@angular/router';
import { TreeNode } from 'primeng/api/treenode';
import { DonViService } from '../../../../services/don-vi.service';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}

@Component({
  selector: 'app-dialog-canbo-kiemnhiem-update',
  templateUrl: './dialog-canbo-kiemnhiem-update.component.html',
})
export class DialogUpdateCanBoKiemNhiemComponent implements OnInit {
  lstvitrivieclam: select[] = [];
  lstchucvu: select[] = [];
  lstloaicanbo: select[] = [];
  updateForm: FormGroup;
  submitted = false;
  today: String;
  id_canBo: string;
  donVis: TreeNode[];
  id_donvi: string;
  selectedBaoCaos: TreeNode;
  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    public dialogRef: MatDialogRef<DialogUpdateCanBoKiemNhiemComponent>,
    private route: ActivatedRoute,
    private donVi: DonViService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.today = moment().format('YYYY/MM/DD');
    this.loadcombo();
    this.createForm();
  }
  createForm() {
    this.id_canBo = this.data.id_canBo;
    this.id_donvi = '0';
    this.updateForm = this.fb.group(
      {
        id_canBo: [this.id_canBo],
        id_chucVu: ['',Validators.required],
        id_viTriViecLam: [null],
        id_loaiCanBo: [null],
        tuNgay: ['', Validators.required],
        denNgay: [null],
        id_donVi: ['', Validators.required],
        thongTinKiemNhiem: [null]
      });
    if (this.data.body) {
      this.updateForm.setValue({
        id_canBo: this.id_canBo,
        tuNgay: this.data.body.tuNgay,
        denNgay: this.data.body.denNgay,
        id_chucVu: this.data.body.id_chucVu,
        id_viTriViecLam: this.data.body.id_viTriViecLam,
        id_loaiCanBo: this.data.body.id_loaiCanBo,
        id_donVi: this.data.body.id_donVi,
        thongTinKiemNhiem: this.data.body.thongTinKiemNhiem,
      });
      if (this.data.body.tuNgay) {
        this.f.tuNgay.setValue(dateFormat(this.data.body.tuNgay, 'yyyy-mm-dd', true));
      }
      if (this.data.body.denNgay) {
        this.f.denNgay.setValue(dateFormat(this.data.body.denNgay, 'yyyy-mm-dd', true));
      }
      if (this.data.body.id_donVi) {
        this.id_donvi = '' + this.data.body.id_donVi;
      }
    }
  }
  loadcombo(){
    this.getChucVu();
    this.getViTriViecLam();
    this.getLoaiCanBo();
    this.getDonVis();
  }
  getChucVu(){
    this.lstchucvu = [];
    this.hscbService.getDanhMucChucVu({}, this.common.HSCB_CanBoKiemNhiem.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstchucvu.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getViTriViecLam(){
    this.lstvitrivieclam = [];
    this.hscbService.getViTriViecLam({}, this.common.HSCB_CanBoKiemNhiem.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstvitrivieclam.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getLoaiCanBo(){
    this.lstloaicanbo = [];
    this.hscbService.getLoaiCanBo({}, this.common.HSCB_CanBoKiemNhiem.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstloaicanbo.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getDonVis(){
    this.donVi.getTreeToValue({ id_trungTam: 1, tuNgay: this.today, denNgay: this.today }, this.common.HSCB_CanBoKiemNhiem.Insert)
    .subscribe((res: any) => {
      if (res.error) {
        this.common.messageErr(res);
      } else {
        this.donVis = res.data;
        if (this.donVis) {
          this.checkNode(this.donVis,parseInt(this.id_donvi, 10), 'BaoCao');
        }
      }
    });
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    if (!this.data.body) {
      this.hscbService.insertCanBoQuaTrinhKiemNhiem(this.updateForm.value, this.common.HSCB_CanBoKiemNhiem.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.hscbService.updateCanBoQuaTrinhKiemNhiem({
        data: this.updateForm.value,
        condition: { id: this.data.body.id }
      }, this.common.HSCB_CanBoKiemNhiem.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  nodeSelectBaoCao(event) {
    if (this.updateForm.controls.id_donVi.value === event.node.data) {
      this.nodeUnselectBaoCao(event);
    } else {
      this.updateForm.controls.id_donVi.setValue(event.node.data);
    }
  }
  nodeUnselectBaoCao(event) {
    this.updateForm.controls.id_donVi.setValue(null);
    this.selectedBaoCaos = null;
  }
  // chi check cho select 1 node (khong multi)
  checkNode(nodes: TreeNode[], str: number, selected) {
    nodes.forEach(node => {
      if (str === node.data) {
        if (selected === 'BaoCao') {
          this.selectedBaoCaos = (node);
        }
      }
      if (node.children !== undefined) {
        node.children.forEach(child => {
          if (str === child.data && !str === node.data) {
            node.partialSelected = true;
            child.parent = node;
          }
          if (str === node.data) {
            child.parent = node;
          }
        });
      } else {
        return;
      }
      this.checkNode(node.children, str, selected);
      node.children.forEach(child => {
        if (child.partialSelected) {
          node.partialSelected = true;
        }
      });
    });
  }
}
