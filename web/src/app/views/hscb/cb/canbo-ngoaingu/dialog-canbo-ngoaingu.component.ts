import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
import { TreeNode } from 'primeng/api';
import * as moment from 'moment';
// Translate
import { CommonService } from '../../../../services/common.service';
import { HscbService } from '../../../../services/hscb.service';
import * as dateFormat from 'dateformat';
import { DonViService } from '../../../../services/don-vi.service';
import { ActivatedRoute, Router } from '@angular/router';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}

@Component({
  selector: 'app-dialog-canbo-ngoaingu',
  templateUrl: './dialog-canbo-ngoaingu.component.html',
})
export class DialogUpdateCanBoNgoaiNguComponent implements OnInit {
  lstNgoaiNgu: select[] = [];
  lstTrinhDoNgoaiNgu: select[] = [];
  lstPhanLoaiTrinhDoNgoaiNgu: select[] = [];
  updateForm: FormGroup;
  submitted = false;
  donVis: TreeNode[];
  selectedBaoCaos: TreeNode;
  today: String;
  id_canBo: string;
  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    public dialogRef: MatDialogRef<DialogUpdateCanBoNgoaiNguComponent>,
    private donVi: DonViService,
    private route: ActivatedRoute,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.today = moment().format('YYYY/MM/DD');
    this.loadcombo();
    this.createForm();
  }
  createForm() {
    this.id_canBo = this.data.id_canBo;
    console.log(this.updateForm);
    this.updateForm = this.fb.group(
      {
        id_canBo: [this.id_canBo],
        id_ngoaiNgu: ['', Validators.required],
        id_trinhDoNgoaiNgu: ['', Validators.required],
        id_phanLoaiTrinhDo_NgoaiNgu: [null],
        diem: [null],
        xepLoai: [null]
      });
    if (this.data.body) {
      this.updateForm.setValue({
        id_canBo: this.id_canBo,
        id_ngoaiNgu: this.data.body.id_ngoaiNgu,
        id_trinhDoNgoaiNgu: this.data.body.id_trinhDoNgoaiNgu,
        id_phanLoaiTrinhDo_NgoaiNgu: this.data.body.id_phanLoaiTrinhDo_NgoaiNgu,
        diem: this.data.body.diem,
        xepLoai: this.data.body.xepLoai
      });
      
    }
  }
  loadcombo(){
    this.getNgoaiNgu();
    this.getTrinhDoNgoaiNgu();
    this.getPhanLoaiTrinhDoNgoaiNgu();
  }

  getNgoaiNgu(){
    this.lstNgoaiNgu = [];
    this.hscbService.getNgoaiNgu({}, this.common.HSCB_CanBoNgoaiNgu.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstNgoaiNgu.push({ value: element.id, display: element.name});
          });
        }
      });
  }

  getTrinhDoNgoaiNgu(){
    this.lstTrinhDoNgoaiNgu = [];
    this.hscbService.getTrinhDoNgoaiNgu({}, this.common.HSCB_CanBoNgoaiNgu.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstTrinhDoNgoaiNgu.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getPhanLoaiTrinhDoNgoaiNgu(){
    this.lstPhanLoaiTrinhDoNgoaiNgu = [];
    this.hscbService.getPhanLoaiTrinhDoNgoaiNgu({}, this.common.HSCB_CanBoNgoaiNgu.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstPhanLoaiTrinhDoNgoaiNgu.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    if (!this.data.body) {
      this.hscbService.insertCanBoNgoaiNgu(this.updateForm.value, this.common.HSCB_CanBoNgoaiNgu.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.hscbService.editCanBoNgoaiNgu({
        data: this.updateForm.value,
        condition: { id: this.data.body.canbo_ngoaingu_id }
      }, this.common.HSCB_CanBoNgoaiNgu.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}