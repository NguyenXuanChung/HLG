import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../../shared/confirmation-dialog/confirmation-dialog.component';
import {Location} from '@angular/common';
// Translate
import { CommonService } from '../../../../services/common.service';
import { StorageService } from '../../../../services/storage.service';
import { AuthConstants } from '../../../../config/auth-constants';
import { ActivatedRoute, Router } from '@angular/router';
import  { HscbService } from'../../../../services/hscb.service';
import { DialogUpdateCanBoNgoaiNguComponent } from './dialog-canbo-ngoaingu.component';

@Component({
  selector: 'app-canbo-ngoaingu',
  templateUrl: './canbo-ngoaingu.component.html'
})
export class CanBoNgoaiNguComponent implements OnInit {
  displayedColumns = ['index', 'tenNgoaiNgu', 'tenTrinhDoNgoaiNgu','tenPhanLoaiTrinhDoNgoaiNgu', 'diem','xepLoai', 'actions'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  searchForm: FormGroup;
  selectedRow: any;
  id_canBo: string;
  permission: any;
  get fs() { return this.searchForm.controls; }

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private hscbService: HscbService,
    private common: CommonService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService,
    private _location: Location
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }
  backClicked() {
    this._location.back();
  }
  ngOnInit() {
    this.onSearch();
    if (!this.permission.HSCB_CanBoNgoaiNgu._view) {
      this.router.navigate(['/']);
    }
  }
  onSearch() {
    this.id_canBo = this.route.snapshot.paramMap.get('id_canbo');
    this.getCanBoNgoaiNgu();
  }

  getCanBoNgoaiNgu() {
    this.hscbService.getNgoaiNguByCanBo({id_canBo: this.id_canBo}, this.common.HSCB_CanBoNgoaiNgu.View).subscribe(res => {
      if (res.error) {
        this.dataSource = new MatTableDataSource();
        this.common.messageErr(res);
      } else {
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }

  openCanBoNgoaiNguDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogUpdateCanBoNgoaiNguComponent, {
      width: '700px',
      height: '80%',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }

  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.tenNgoaiNgu }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.hscbService.deleteCanBoNgoaiNgu({ id: data.canbo_ngoaingu_id }, this.common.HSCB_CanBoNgoaiNgu.Delete).subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.onSearch();
            this.common.messageRes(res.message);
          }
        });
      }
    });
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
