import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../../services/common.service';
import { HscbService } from './../../../../services/hscb.service';
import * as dateFormat from 'dateformat';

// tslint:disable-next-line: class-name
interface select {
  value: number;
  display: string;
}

@Component({
  selector: 'app-dialog-canbo-bangcap-update',
  templateUrl: './dialog-canbo-bangcap-update.component.html',
})
export class DialogUpdateCanBoBangCapComponent implements OnInit {
  lsthocham: select[] = [];
  lstcapdaotao: select[] = [];
  lstvanbang: select[] = [];
  lstchuyennganh: select[] =[];
  //lsthedaotao: select[] = [];
  lstxeploai: select[] = [];
  lsthocvi : select[] =[];
  updateForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private hscbService: HscbService,
    public dialogRef: MatDialogRef<DialogUpdateCanBoBangCapComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.loadcombo();
    this.createForm();
  }
  createForm() {
    this.updateForm = this.fb.group(
      {
        //id: ['', [Validators.required]],
        id_canBo: [''],//, [Validators.required]],
        id_capDaoTao: ['', [Validators.required]],
        id_vanBang: ['', [Validators.required]],
        id_chuyenNganh: [null],
        id_heDaoTao: [null],
        noiCapBang: [null],
        ngayCap: [null],
        id_xepLoai: [null],
        //laBangChinh: [0, [Validators.required]],
        namTotNghiep: [null],
        truongTotNghiep: [null],
        tuNgay: [null],
        denNgay: [null],
        id_khoaDaoTao: [null],
        id_hocHam: [null],
        id_hocVi: [null],
        // createBy: [null],
        // createDate: [null],
        // updateBy: [null],
        // updateDate: [null]
      });
    if (this.data.body) {
      this.updateForm.setValue({

       // id: this.data.id,
        id_canBo: this.data.id_canBo,
        id_capDaoTao: this.data.body.id_capDaoTao,
        id_vanBang: this.data.body.id_vanBang,
        id_chuyenNganh: this.data.body.id_chuyenNganh,
        id_heDaoTao: this.data.body.id_heDaoTao,
        noiCapBang: this.data.body.noiCapBang,
        ngayCap: this.data.body.ngayCap,
        id_xepLoai: this.data.body.id_xepLoai,
        //laBangChinh: [0, [Validators.required]],
        namTotNghiep: this.data.body.namTotNghiep,
        truongTotNghiep: this.data.body.truongTotNghiep,
        tuNgay: this.data.body.tuNgay,
        denNgay: this.data.body.denNgay,
        id_khoaDaoTao: this.data.body.id_khoaDaoTao,
        id_hocHam: this.data.body.id_hocHam,
        id_hocVi: this.data.body.id_hocVi,
        // createBy: this.data.createBy,
        // createDate: this.data.createDate,
        // updateBy: this.data.updateBy,
        // updateDate: this.data.updateDate
      });
      if (this.data.body.tuNgay) {
        this.f.tuNgay.setValue(dateFormat(this.data.body.tuNgay, 'yyyy-mm-dd', true));
      }
      if (this.data.body.denNgay) {
        this.f.denNgay.setValue(dateFormat(this.data.body.denNgay, 'yyyy-mm-dd', true));
      }
      if (this.data.body.ngayCap) {
        this.f.ngayCap.setValue(dateFormat(this.data.body.ngayCap, 'yyyy-mm-dd', true));
      }
    }
  }
  loadcombo(){
    this.getHocHam();
    this.getCapDaoTao();
    this.getVanBang();
    this.getChuyenNganh();
    this.getXepLoai();
    this.getHocVi();
  }
  getHocHam(){
    this.lsthocham = [];
    this.hscbService.getHocHam({}, this.common.HSCB_CanBoBangCap.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lsthocham.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getCapDaoTao(){
    this.lstcapdaotao = [];
    this.hscbService.getCapDaoTao({}, this.common.HSCB_CanBoBangCap.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstcapdaotao.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getVanBang(){
    this.lstvanbang = [];
    this.hscbService.getVanBang({}, this.common.HSCB_CanBoBangCap.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstvanbang.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getChuyenNganh(){
    this.lstchuyennganh = [];
    this.hscbService.getChuyenNganh({}, this.common.HSCB_CanBoBangCap.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstchuyennganh.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getXepLoai(){
    this.lstxeploai = [];
    this.hscbService.getXepLoai({}, this.common.HSCB_CanBoBangCap.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lstxeploai.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  getHocVi(){
    this.lsthocvi = [];
    this.hscbService.getHocVi({}, this.common.HSCB_CanBoBangCap.Insert)
      .subscribe(res => {
        if (!res.error) {
          res.data.forEach(element => {
            this.lsthocvi.push({ value: element.id, display: element.name});
          });
        }
      });
  }
  onSubmit() {
    this.submitted = true;
    this.updateForm.value.id_canBo = this.data.id_canBo;
    // stop here if form is invalid
    if (this.updateForm.invalid) {
      return;
    }
    if (!this.data.body) {
      this.hscbService.insertCanBoBangCap(this.updateForm.value, this.common.HSCB_CanBoBangCap.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      //this.updateForm.value.createDate == null;
      //this.updateForm.value.updateDate == null;
      this.hscbService.editCanBoBangCap({
        data: this.updateForm.value,
        condition: { id: this.data.body.id }
      }, this.common.HSCB_CanBoBangCap.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
