import { CommonComponent } from './common.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'TEST'
    },
    children: [
      {
        path: '',
        redirectTo: ''
      },
      {
        path: 'test',
        component: CommonComponent,
        data: {
          title: 'Test'
        }
      },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommonRoutingModule { }
