import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { GdQpanService } from '../../../services/gd-qpan.service';

@Component({
  selector: 'app-dialog-update-hinhthucdaotao',
  templateUrl: './dialog-update-hinhthucdaotao.component.html',
})
export class DialogUpdateHinhThucDaoTaoComponent implements OnInit {
  hinhThucDaoTaoForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.hinhThucDaoTaoForm.controls; }
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private GdQpanService: GdQpanService,
    public dialogRef: MatDialogRef<DialogUpdateHinhThucDaoTaoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.hinhThucDaoTaoForm = this.fb.group(
      {
        name: [null, [Validators.required]],
        code: [null, [Validators.required]],
        description: [''],
        sortOrder: [0, [Validators.required, Validators.min(0)]],
      });
    if (this.data) {
      this.hinhThucDaoTaoForm.setValue({
        'name': this.data.name,
        'code': this.data.name,
        'description': this.data.description,
        'sortOrder': this.data.sortOrder,
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.hinhThucDaoTaoForm.invalid) {
      return;
    }
    if (!this.data) {
      this.GdQpanService.insertHinhThucDaoTao(this.hinhThucDaoTaoForm.value, this.common.GD_QPAN_HinhThucDaoTao.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.GdQpanService.editHinhThucDaoTao({
        data: this.hinhThucDaoTaoForm.value,
        condition: { id: this.data.id }
      }, this.common.GD_QPAN_HinhThucDaoTao.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
