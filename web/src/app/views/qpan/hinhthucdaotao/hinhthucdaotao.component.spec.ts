import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HinhthucdaotaoComponent } from './hinhthucdaotao.component';

describe('HinhthucdaotaoComponent', () => {
  let component: HinhthucdaotaoComponent;
  let fixture: ComponentFixture<HinhthucdaotaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HinhthucdaotaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HinhthucdaotaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
