import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TruongHocComponent } from './truonghoc/truonghoc.component';
import { PhanLoaiDoiTuongComponent } from './phanloaidoituong/phanloaidoituong.component';
import { PhanLoaiLopHocComponent } from './phanloailophoc/phanloailophoc.component';
import { DoiTuongComponent } from './doituong/doituong.component';
import { HinhThucDaoTaoComponent } from './hinhthucdaotao/hinhthucdaotao.component';
import { LyLuanChinhTriComponent } from './lyluanchinhtri/lyluanchinhtri.component';
import { KetQuaComponent } from './ketqua/ketqua.component';
import { XepLoaiLopHocComponent } from './xeploailophoc/xeploailophoc.component';
import { QuanLyGiaoVienComponent } from './quanlygiaovien/quanlygiaovien.component';
import { DaoTaoTheoLopHocComponent } from './daotaotheolophoc/daotaotheolophoc.component';
import { QuanLyKQDTTheoLopComponent } from './quanlykqdttheolop/quanlykqdttheolop.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: ''
      },
      {
        path: 'truonghoc',
        component: TruongHocComponent,
      },
      {
        path: 'phanloaidoituong',
        component: PhanLoaiDoiTuongComponent,
      },
      {
        path: 'doituong',
        component: DoiTuongComponent,
      },
      {
        path: 'phanloailophoc',
        component: PhanLoaiLopHocComponent,
      },
      {
        path: 'hinhthucdaotao',
        component: HinhThucDaoTaoComponent,
      },
      {
        path: 'lyluanchinhtri',
        component: LyLuanChinhTriComponent,
      },
      {
        path: 'ketqua',
        component: KetQuaComponent,
      },
      {
        path: 'xeploailophoc',
        component: XepLoaiLopHocComponent,
      },
      {
        path: 'quanlygiaovien',
        component: QuanLyGiaoVienComponent,
      },
      {
        path: 'daotaotheolophoc',
        component: DaoTaoTheoLopHocComponent,
      },
      {
        path: 'quanlykqdttheolop',
        component: QuanLyKQDTTheoLopComponent,
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QpanRoutingModule { }
