import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TruonghocComponent } from './truonghoc.component';

describe('TruonghocComponent', () => {
  let component: TruonghocComponent;
  let fixture: ComponentFixture<TruonghocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TruonghocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TruonghocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
