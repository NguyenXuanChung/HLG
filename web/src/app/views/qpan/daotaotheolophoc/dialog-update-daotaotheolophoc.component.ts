import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from '../../../services/common.service';
import { GdQpanService } from '../../../services/gd-qpan.service';
import { DanhMucService } from './../../../services/danhmuc.service';
import { DonViService } from '../../../services/don-vi.service';
import { TqDqtvService } from '../../../services/tq-dqtv.service';
interface select {
  id: number;
  name: string;
}
@Component({
  selector: 'app-dialog-update-daotaotheolophoc',
  templateUrl: './dialog-update-daotaotheolophoc.component.html',
})
export class DialogUpdateDaoTaoTheoLopHocComponent implements OnInit {
  daoTaoTheoLopHocForm: FormGroup;
  submitted = false;
  phanLoaiDoiTuongs: select[] = [];
  phanLoaiLopHocs: any[] = [];
  doiTuongs: any[] = [];
  tinhs: any[];
  huyens: any[];
  xas: any[];
  donVis: any[];
  // convenience getter for easy access to form fields
  get f() { return this.daoTaoTheoLopHocForm.controls; }
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private GdQpanService: GdQpanService,
    public dmService: DanhMucService,
    private TqDqtvService: TqDqtvService,
    public dialogRef: MatDialogRef<DialogUpdateDaoTaoTheoLopHocComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.loadCombo();
    this.createForm();
    this.getPhanLoaiDoiTuong();
  }
  getPhanLoaiDoiTuong(){
    this.GdQpanService.getPhanLoaiDoiTuong({}, this.common.GD_QPAN_PhanLoaiDoiTuong.View)
    .subscribe(res =>{
      if(res.error){
      }else{
        res.data.forEach(element =>{
          this.phanLoaiDoiTuongs.push({id:element.id, name:element.name});
        })
      }
    })
  }
  loadCombo() {
    this.GdQpanService.getDoiTuong({}, this.common.GD_QPAN_DoiTuong.View).subscribe(res => {
      if (!res.error) {
        this.doiTuongs = res.data;
      }
    });
    this.GdQpanService.getPhanLoaiLopHoc({}, this.common.GD_QPAN_PhanLoaiLopHoc.View).subscribe(res => {
      if (!res.error) {
        this.phanLoaiLopHocs = res.data;
      }
    });
  }
  createForm() {
    this.daoTaoTheoLopHocForm = this.fb.group(
      {
        
      });
    if (this.data) {
      this.daoTaoTheoLopHocForm.setValue({
        
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.daoTaoTheoLopHocForm.invalid) {
      return;
    }
    if (!this.data) {
      this.GdQpanService.insertDaoTaoTheoLopHoc(this.daoTaoTheoLopHocForm.value, this.common.GD_QPAN_DaoTaoTheoLopHoc.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.GdQpanService.editDaoTaoTheoLopHoc({
        data: this.daoTaoTheoLopHocForm.value,
        condition: { id: this.data.id }
      }, this.common.GD_QPAN_DaoTaoTheoLopHoc.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
