import { DialogUpdateDaoTaoTheoLopHocComponent } from './dialog-update-daotaotheolophoc.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { CheckableSettings } from '@progress/kendo-angular-treeview';
import { GroupService } from '../../../services/group.service';
import * as moment from 'moment';
// Translate
import { CommonService } from '../../../services/common.service';
import { Observable, of } from 'rxjs';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';
import { Router } from '@angular/router';
import { GdQpanService } from '../../../services/gd-qpan.service';
interface select {
  code: string;
  name: string;
}
@Component({
  selector: 'app-daotaotheolophoc',
  templateUrl: './daotaotheolophoc.component.html',
  styleUrls: ['./daotaotheolophoc.component.css']
})
export class DaoTaoTheoLopHocComponent implements OnInit {
  displayedColumns = ['index', 'name', 'tuNgay','denNgay','id_phanLoaiDoiTuong','noiDungGiangDay','ghiChuKq', 'actions'];
  dataSource: MatTableDataSource<any>;
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  phanLoaiDoiTuongs: select[] = [];
  searchForm: FormGroup;
  permission: any;
  get fs() { return this.searchForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private GdQpanService: GdQpanService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService,
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
  }
  getPhanLoaiDoiTuong(){
    this.GdQpanService.getPhanLoaiDoiTuong({}, this.common.GD_QPAN_PhanLoaiDoiTuong.View)
    .subscribe(res =>{
      if(res.error){
      }else{
        res.data.forEach(element =>{
          this.phanLoaiDoiTuongs.push({code:element.code, name:element.name});
        })
      }
    })
  }
  ngOnInit() {
    this.createSearchForm();
    this.getPhanLoaiDoiTuong();
    if (!this.permission.GD_QPAN_DaoTaoTheoLopHoc._view) {
      this.router.navigate(['/']);
    }
    this.onSearch();
  }
  createSearchForm() {
    this.searchForm = this.fb.group(
      {
        code_phanLoaiDoiTuong: [null],
        tuNgay: [null],
        denNgay: [null]
      });
  }
  onSearch() {
    console.log(this.searchForm.value);
    this.getGroup();
  }
  getGroup() {
    this.GdQpanService.getDaoTaoTheoLopHoc({}, this.common.GD_QPAN_DaoTaoTheoLopHoc.View).subscribe(res => {
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource = new MatTableDataSource();
      } else {
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
  openUpdateDialog(data: any): void {
        const dialogRef = this.dialog.open(DialogUpdateDaoTaoTheoLopHocComponent, {
          width: '700px',
          data: data
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.onSearch();
            this.common.messageRes(result);
          }
        });
  }
  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.GdQpanService.deleteDaoTaoTheoLopHoc({ id: data.id }, this.common.GD_QPAN_DaoTaoTheoLopHoc.Delete).subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.onSearch();
            this.common.messageRes(res.message);
          }
        });
      }
    });
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
