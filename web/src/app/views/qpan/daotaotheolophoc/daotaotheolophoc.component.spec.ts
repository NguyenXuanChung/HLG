import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DaoTaoTheoLopHocComponent } from './daotaotheolophoc.component';

describe('DaoTaoTheoLopHocComponent', () => {
  let component: DaoTaoTheoLopHocComponent;
  let fixture: ComponentFixture<DaoTaoTheoLopHocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaoTaoTheoLopHocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaoTaoTheoLopHocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
