import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhanloailophocComponent } from './phanloailophoc.component';

describe('PhanloailophocComponent', () => {
  let component: PhanloailophocComponent;
  let fixture: ComponentFixture<PhanloailophocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhanloailophocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhanloailophocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
