import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LyluanchinhtriComponent } from './lyluanchinhtri.component';

describe('LyluanchinhtriComponent', () => {
  let component: LyluanchinhtriComponent;
  let fixture: ComponentFixture<LyluanchinhtriComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LyluanchinhtriComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LyluanchinhtriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
