import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { GdQpanService } from '../../../services/gd-qpan.service';

@Component({
  selector: 'app-dialog-update-xeploailophoc',
  templateUrl: './dialog-update-xeploailophoc.component.html',
})
export class DialogUpdateXepLoaiLopHocComponent implements OnInit {
  xepLoaiLopHocForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.xepLoaiLopHocForm.controls; }
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private GdQpanService: GdQpanService,
    public dialogRef: MatDialogRef<DialogUpdateXepLoaiLopHocComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.xepLoaiLopHocForm = this.fb.group(
      {
        name: [null, [Validators.required]],
        code: [null, [Validators.required]],
        description: [''],
        sortOrder: [0, [Validators.required, Validators.min(0)]],
      });
    if (this.data) {
      this.xepLoaiLopHocForm.setValue({
        'name': this.data.name,
        'code': this.data.name,
        'description': this.data.description,
        'sortOrder': this.data.sortOrder,
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.xepLoaiLopHocForm.invalid) {
      return;
    }
    if (!this.data) {
      this.GdQpanService.insertXepLoaiLopHoc(this.xepLoaiLopHocForm.value, this.common.GD_QPAN_XepLoaiLopHoc.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.GdQpanService.editXepLoaiLopHoc({
        data: this.xepLoaiLopHocForm.value,
        condition: { id: this.data.id }
      }, this.common.GD_QPAN_XepLoaiLopHoc.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
