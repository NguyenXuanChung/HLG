import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XeploailophocComponent } from './xeploailophoc.component';

describe('XeploailophocComponent', () => {
  let component: XeploailophocComponent;
  let fixture: ComponentFixture<XeploailophocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XeploailophocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XeploailophocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
