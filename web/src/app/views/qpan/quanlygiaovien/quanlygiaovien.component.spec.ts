import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanlygiaovienComponent } from './quanlygiaovien.component';

describe('QuanlygiaovienComponent', () => {
  let component: QuanlygiaovienComponent;
  let fixture: ComponentFixture<QuanlygiaovienComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuanlygiaovienComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuanlygiaovienComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
