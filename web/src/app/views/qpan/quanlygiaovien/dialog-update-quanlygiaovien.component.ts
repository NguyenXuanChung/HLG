import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { GdQpanService } from '../../../services/gd-qpan.service';

@Component({
  selector: 'app-dialog-update-quanlygiaovien',
  templateUrl: './dialog-update-quanlygiaovien.component.html',
})
export class DialogUpdateQuanLyGiaoVienComponent implements OnInit {
  quanLyGiaoVienForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.quanLyGiaoVienForm.controls; }
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private GdQpanService: GdQpanService,
    public dialogRef: MatDialogRef<DialogUpdateQuanLyGiaoVienComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.quanLyGiaoVienForm = this.fb.group(
      {
        id_truongHoc: [null, [Validators.required]],
        nhuCau: [null, [Validators.required]],
        ctDTCQ: [null, [Validators.required]],
        ctVB2: [null, [Validators.required]],
        ctCCTren6Thang: [null, [Validators.required]],
        bctVB2: [null, [Validators.required]],
        bctCCTren6Thang: [null, [Validators.required]],
        SQBietPhai: [null, [Validators.required]],
        ghiChu: [''],
        sortOrder: [0, [Validators.required, Validators.min(0)]],
      });
    if (this.data) {
      this.quanLyGiaoVienForm.setValue({
        'id_truongHoc': this.data.id_truongHoc,
        'nhuCau': this.data.nhuCau,
        'ctDTCQ': this.data.ctDTCQ,
        'ctVB2': this.data.ctVB2,
        'ctCCTren6Thang': this.data.ctCCTren6Thang,
        'bctVB2': this.data.bctVB2,
        'bctCCTren6Thang': this.data.ctCCTren6Thang,
        'SQBietPhai': this.data.SQBietPhai,
        'ghiChu': this.data.ghiChu,
        'sortOrder': this.data.sortOrder,
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.quanLyGiaoVienForm.invalid) {
      return;
    }
    if (!this.data) {
      this.GdQpanService.insertQuanLyGiaoVien(this.quanLyGiaoVienForm.value, this.common.GD_QPAN_QuanLyGiaoVien.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.GdQpanService.editQuanLyGiaoVien({
        data: this.quanLyGiaoVienForm.value,
        condition: { id: this.data.id }
      }, this.common.GD_QPAN_QuanLyGiaoVien.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
