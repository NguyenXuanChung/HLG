import { DialogUpdateQuanLyGiaoVienComponent } from './dialog-update-quanlygiaovien.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { CheckableSettings } from '@progress/kendo-angular-treeview';

import * as moment from 'moment';
// Translate
import { CommonService } from '../../../services/common.service';
import { Observable, of } from 'rxjs';
import { StorageService } from '../../../services/storage.service';
import { AuthConstants } from '../../../config/auth-constants';
import { Router } from '@angular/router';
import { GdQpanService } from '../../../services/gd-qpan.service';
import { DonViService } from '../../../services/don-vi.service';

@Component({
  selector: 'app-quanlygiaovien',
  templateUrl: './quanlygiaovien.component.html',
  styleUrls: ['./quanlygiaovien.component.css']
})

export class QuanLyGiaoVienComponent implements OnInit {
  displayedColumns = ['index', 'id_truongHoc', 'nhuCau','ctDTCQ','ctVB2','ctCCTren6Thang','bctVB2','bctCCTren6Thang','SQBietPhai','ghiChu', 'actions'];
  dataSource: MatTableDataSource<any>;
  selectedRow: boolean;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  numbers: number[];
  public id_donVis: any[] = [];
  donVis: any[];
  searchForm: FormGroup;
  permission: any;
  get fs() { return this.searchForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private GdQpanService: GdQpanService,
    public dialog: MatDialog,
    private router: Router,
    private storageService: StorageService,
    private donVi: DonViService,
  ) {
    this.storageService.get(AuthConstants.PERMISSION).then(res => {
      this.permission = res;
    });
    const date = new Date();
    this.numbers = Array(30).fill(0).map((x, i) => (i + date.getFullYear() - 25));
  }

  ngOnInit() {
    this.createSearchForm();
    this.onSearch();
    if (!this.permission.GD_QPAN_QuanLyGiaoVien._view) {
      this.router.navigate(['/']);
    }
    this.loadCombo();
  }

  loadCombo() {
    const today = moment().format('YYYY/MM/DD');
    this.donVi.getTreeToValue({ id_trungTam: 1, tuNgay: today, denNgay: today }, this.common.TQ_CongDan.View)
      .subscribe((res: any) => {
        console.log(res);
        if (res.error) {
          this.common.messageErr(res);
        } else {
          this.donVis = res.data;
        }
      });
  }

  createSearchForm() {
    const date = new Date();
    this.searchForm = this.fb.group(
      {
        nam: [date.getFullYear()],
        thang: [null],
        id_donViQuanLys: [''],
      });
  }

  onSearch() {
    this.getGroup();
    this.fs.id_donViQuanLys.setValue(this.id_donVis.toString());
  }
  getGroup() {
    this.GdQpanService.getQuanLyGiaoVien({}, this.common.GD_QPAN_QuanLyGiaoVien.View).subscribe(res => {
      console.log(res);
      if (res.error) {
        this.common.messageErr(res);
        this.dataSource = new MatTableDataSource();
      } else {
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    });
  }
  openUpdateDialog(data: any): void {
    const dialogRef = this.dialog.open(DialogUpdateQuanLyGiaoVienComponent, {
      width: '700px',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onSearch();
        this.common.messageRes(result);
      }
    });
  }
  openDeleteDialog(data: any): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: { delete: 1, name: data.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.GdQpanService.deleteQuanLyGiaoVien({ id: data.id }, this.common.GD_QPAN_QuanLyGiaoVien.Delete).subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.onSearch();
            this.common.messageRes(res.message);
          }
        });
      }
    });
  }
  onSelectedRow(row: any) {
    if (!this.selectedRow) {
      this.selectedRow = row;
    } else {
      this.selectedRow = row;
    }
  }
}
