import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhanloaidoituongComponent } from './phanloaidoituong.component';

describe('PhanloaidoituongComponent', () => {
  let component: PhanloaidoituongComponent;
  let fixture: ComponentFixture<PhanloaidoituongComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhanloaidoituongComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhanloaidoituongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
