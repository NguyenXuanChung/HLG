import { QpanRoutingModule } from './qpan-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { AngularMaterial } from '../../angular-material';
import { MomentModule } from 'ngx-moment';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { EditorModule } from 'primeng/editor';
import { QuillModule } from 'ngx-quill';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { MatTabsModule } from '@angular/material/tabs';
import { TreeModule } from 'primeng/tree';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { TruongHocComponent } from './truonghoc/truonghoc.component';
import { PhanLoaiDoiTuongComponent } from './phanloaidoituong/phanloaidoituong.component'
import { DoiTuongComponent } from './doituong/doituong.component';
import {PhanLoaiLopHocComponent } from './phanloailophoc/phanloailophoc.component';
import { HinhThucDaoTaoComponent } from './hinhthucdaotao/hinhthucdaotao.component';
import { LyLuanChinhTriComponent } from './lyluanchinhtri/lyluanchinhtri.component';
import { KetQuaComponent } from './ketqua/ketqua.component';
import { QuanLyGiaoVienComponent } from './quanlygiaovien/quanlygiaovien.component';
import { XepLoaiLopHocComponent } from './xeploailophoc/xeploailophoc.component';
import { DaoTaoTheoLopHocComponent } from './daotaotheolophoc/daotaotheolophoc.component';

import { DialogUpdatePhanLoaiDoiTuongComponent } from './phanloaidoituong/dialog-update-phanloaidoituong.component';
import { DialogUpdateTruongHocComponent } from './truonghoc/dialog-update-truonghoc.component';
import { DialogUpdateDoiTuongComponent } from './doituong/dialog-update-doituong.component';
import { DialogUpdatePhanLoaiLopHocComponent } from './phanloailophoc/dialog-update-phanloailophoc.component';
import { DialogUpdateHinhThucDaoTaoComponent } from './hinhthucdaotao/dialog-update-hinhthucdaotao.component';
import { DialogUpdateLyLuanChinhTriComponent } from './lyluanchinhtri/dialog-update-lyluanchinhtri.component';
import { DialogUpdateKetQuaComponent } from './ketqua/dialog-update-ketqua.component';
import { DialogUpdateXepLoaiLopHocComponent } from './xeploailophoc/dialog-update-xeploailophoc.component';
import { DialogUpdateQuanLyGiaoVienComponent } from './quanlygiaovien/dialog-update-quanlygiaovien.component';
import { DialogUpdateDaoTaoTheoLopHocComponent } from './daotaotheolophoc/dialog-update-daotaotheolophoc.component';
export function translateHttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
  TruongHocComponent,
  PhanLoaiDoiTuongComponent,
  DoiTuongComponent,
  PhanLoaiLopHocComponent,
  HinhThucDaoTaoComponent,
  LyLuanChinhTriComponent,
  KetQuaComponent,
  XepLoaiLopHocComponent,
  QuanLyGiaoVienComponent,
  DaoTaoTheoLopHocComponent,

  DialogUpdateTruongHocComponent,
  DialogUpdatePhanLoaiDoiTuongComponent,
  DialogUpdateDoiTuongComponent,
  DialogUpdatePhanLoaiLopHocComponent,
  DialogUpdateHinhThucDaoTaoComponent,
  DialogUpdateLyLuanChinhTriComponent,
  DialogUpdateKetQuaComponent,
  DialogUpdateXepLoaiLopHocComponent,
  DialogUpdateQuanLyGiaoVienComponent,
  DialogUpdateDaoTaoTheoLopHocComponent
],
  entryComponents: [

  DialogUpdateTruongHocComponent,
  DialogUpdatePhanLoaiDoiTuongComponent,
  DialogUpdateDoiTuongComponent,
  DialogUpdatePhanLoaiLopHocComponent,
  DialogUpdateHinhThucDaoTaoComponent,
  DialogUpdateLyLuanChinhTriComponent,
  DialogUpdateKetQuaComponent,
  DialogUpdateXepLoaiLopHocComponent,
  DialogUpdateQuanLyGiaoVienComponent,
  DialogUpdateDaoTaoTheoLopHocComponent
  ],
  imports: [
    CommonModule,
    QpanRoutingModule,
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: translateHttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    AngularMaterial,
    MomentModule,
    InputTextareaModule,
    EditorModule,
    QuillModule.forRoot({
      modules: {
        syntax: false,
        toolbar: [
          ['bold', 'italic', 'underline', 'strike'],
          [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
          [{ 'list': 'ordered' }, { 'list': 'bullet' }],
          [{ 'indent': '-1' }, { 'indent': '+1' }],
          ['blockquote', 'code-block']
        ]
      }
    }),
    TreeViewModule,
    BsDropdownModule.forRoot(),
    MatTabsModule,
    TreeModule,
    MatAutocompleteModule
  ]
})
export class QpanModule { }
