import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../services/common.service';
import { ApplicationService } from '../../services/application.service';
import { ConfigurationService } from '../../services/configuration.service';
import { DiaBanService } from '../../services/diaban.service';
import { GroupService } from '../../services/group.service';

@Component({
  selector: 'app-dialog-update-version',
  templateUrl: './dialog-update-version.component.html',
})
export class DialogUpdateVersionComponent implements OnInit {
  versionForm: FormGroup;
  configs: any[];
  diaBans: any[];
  groups: any[];
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.versionForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private appService: ApplicationService,
    private configService: ConfigurationService,
    private groupService: GroupService,
    private diaBanService: DiaBanService,
    public dialogRef: MatDialogRef<DialogUpdateVersionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.versionForm = this.fb.group(
      {
        id_application : ['', [Validators.required]],
        version: ['', [Validators.required]],
        url: ['', [Validators.required]],
        apkHash: ['', [Validators.required]],
      });
    if (this.data.version) {
      this.versionForm.setValue({
        'id_application': this.data.id_application,
        'version': this.data.version,
        'url': this.data.url,
        'apkHash': this.data.apkHash,
      });
    } else {
      this.f.id_application.setValue(this.data.id_application);
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.versionForm.invalid) {
      return;
    }
    if (!this.data.version) {
      this.appService.insertVersion(this.versionForm.value, this.common.Application.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.appService.editVersion({
        data: this.versionForm.value,
        condition: { id: this.data.id }
      }, this.common.Application.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  // loadCombo() {
  //   this.configService.getConfig({}, this.common.Application.Insert).subscribe(res => {
  //     if (res.error) {
  //       this.common.messageErr(res);
  //     } else {
  //       this.configs = res.data;
  //     }
  //   });
  //   this.diaBanService.getDiaBan({}, this.common.Application.Insert).subscribe(res => {
  //     if (res.error) {
  //       this.common.messageErr(res);
  //     } else {
  //       this.diaBans = res.data;
  //     }
  //   });
  //   this.groupService.getGroup({}, this.common.Application.Insert).subscribe(res => {
  //     if (res.error) {
  //       this.common.messageErr(res);
  //     } else {
  //       this.groups = res.data;
  //     }
  //   });
  // }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
