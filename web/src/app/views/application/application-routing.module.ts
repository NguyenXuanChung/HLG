import { AppVersionComponent } from './app-version.component';
import { ApplicationComponent } from './application.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'App'
    },
    children: [
      {
        path: '',
        data: {
          title: 'App'
        },
        component: ApplicationComponent,
      },
      {
        path: 'version/:id_project',
        component: AppVersionComponent,
      },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationRoutingModule { }
