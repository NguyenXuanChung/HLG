import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../services/common.service';
import { GroupService } from '../../services/group.service';

@Component({
  selector: 'app-dialog-update-group',
  templateUrl: './dialog-update-group.component.html',
})
export class DialogUpdateGroupComponent implements OnInit {
  groupForm: FormGroup;
  configs: any[];
  diaBans: any[];
  groups: any[];
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.groupForm.controls; }

  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private groupService: GroupService,
    public dialogRef: MatDialogRef<DialogUpdateGroupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.groupForm = this.fb.group(
      {
        name: ['', [Validators.required]],
        // licenseKey: [null],
        description: [''],
      });
    if (this.data) {
      this.groupForm.setValue({
        'name': this.data.name,
        // 'licenseKey': this.data.licenseKey,
        'description': this.data.description,
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.groupForm.invalid) {
      return;
    }
    if (!this.data) {
      this.groupService.insertGroup(this.groupForm.value, this.common.Group.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.groupService.editGroup({
        data: this.groupForm.value,
        condition: { id: this.data.id }
      }, this.common.Group.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }

  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
