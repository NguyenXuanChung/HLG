import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { AngularMaterial } from '../../angular-material';
import { MomentModule } from 'ngx-moment';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { EditorModule } from 'primeng/editor';
import { QuillModule } from 'ngx-quill';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { MatTabsModule } from '@angular/material/tabs';
import { TreeModule } from 'primeng/tree';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { TqDqtvRoutingModule } from './tq-dqtv-routing.module';

import { DialogUpdateNgheNghiepComponent } from './nghenghiep/dialog-update-nghenghiep.component';
import { DialogUpdateChucDanhComponent } from './chucdanh/dialog-update-chucdanh.component';
import { DialogUpdateDoiTuongComponent } from './doituong/dialog-update-doituong.component';
import { DialogUpdateNangKhieuComponent } from './nangkhieu/dialog-update-nangkhieu.component';
import { DialogUpdateSucKhoeComponent } from './suckhoe/dialog-update-suckhoe.component';
import { DialogUpdateToChucXaHoiComponent } from './tochucxahoi/dialog-update-tochucxahoi.component';
import { DialogUpdateTrinhDoHocVanComponent } from './trinhdohocvan/dialog-update-trinhdohocvan.component';
import { DialogUpdateTamHoanComponent } from './tamhoan/dialog-update-tamhoan.component';
import { DialogUpdateTrinhDoVanHoaComponent } from './trinhdovanhoa/dialog-update-trinhdovanhoa.component';
import { DialogUpdateHocViComponent } from './hocvi/dialog-update-hocvi.component';
import { DialogUpdateQuanHeComponent } from './quanhe/dialog-update-quanhe.component';
import { DialogUpdateNganhNgheCmktComponent } from './nganhnghecmkt/dialog-update-nganhnghecmkt.component';
import { DialogUpdateMienComponent } from './mien/dialog-update-mien.component';
import { DialogUpdateDanTocComponent } from './dantoc/dialog-update-dantoc.component';
import { DialogUpdateTonGiaoComponent } from './tongiao/dialog-update-tongiao.component';


import { ChucDanhComponent } from './chucdanh/chucdanh.component';
import { DoiTuongComponent } from './doituong/doituong.component';
import { NangkhieuComponent } from './nangkhieu/nangkhieu.component';
import { NghenghiepComponent } from './nghenghiep/nghenghiep.component';
import { SucKhoeComponent } from './suckhoe/suckhoe.component';
import { ToChucXaHoiComponent } from './tochucxahoi/tochucxahoi.component';
import { TrinhDoHocVanComponent } from './trinhdohocvan/trinhdohocvan.component';
import { TamHoanComponent } from './tamhoan/tamhoan.component';
import { TrinhDoVanHoaComponent } from './trinhdovanhoa/trinhdovanhoa.component';
import { HocViComponent } from './hocvi/hocvi.component';
import { QuanHeComponent } from './quanhe/quanhe.component';
import { NganhNgheCmktComponent } from './nganhnghecmkt/nganhnghecmkt.component';
import { MienComponent } from './mien/mien.component';
import { DanTocComponent } from './dantoc/dantoc.component';
import { TonGiaoComponent } from './tongiao/tongiao.component';
import { DonViTvComponent } from './donViTv/donViTv.component';
import { DialogUpdateDonViTvComponent } from './donViTv/dialog-update-donVitv.component';

export function translateHttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
    ChucDanhComponent,
    DoiTuongComponent,
    NangkhieuComponent,
    NghenghiepComponent,
    SucKhoeComponent,
    ToChucXaHoiComponent,
    TrinhDoHocVanComponent,
    TamHoanComponent,
    TrinhDoVanHoaComponent,
    HocViComponent,
    QuanHeComponent,
    NganhNgheCmktComponent,
    MienComponent,
    DanTocComponent,
    TonGiaoComponent,
    DonViTvComponent,
    DialogUpdateChucDanhComponent,
    DialogUpdateDoiTuongComponent,
    DialogUpdateNangKhieuComponent,
    DialogUpdateNgheNghiepComponent,
    DialogUpdateSucKhoeComponent,
    DialogUpdateToChucXaHoiComponent,
    DialogUpdateTrinhDoHocVanComponent,
    DialogUpdateTamHoanComponent,
    DialogUpdateTrinhDoVanHoaComponent,
    DialogUpdateHocViComponent,
    DialogUpdateQuanHeComponent,
    DialogUpdateNganhNgheCmktComponent,
    DialogUpdateMienComponent,
    DialogUpdateDanTocComponent,
    DialogUpdateTonGiaoComponent,
    DialogUpdateDonViTvComponent
  ],
  entryComponents: [
    DialogUpdateChucDanhComponent,
    DialogUpdateDoiTuongComponent,
    DialogUpdateNangKhieuComponent,
    DialogUpdateNgheNghiepComponent,
    DialogUpdateSucKhoeComponent,
    DialogUpdateToChucXaHoiComponent,
    DialogUpdateTrinhDoHocVanComponent,
    DialogUpdateTamHoanComponent,
    DialogUpdateTrinhDoVanHoaComponent,
    DialogUpdateHocViComponent,
    DialogUpdateQuanHeComponent,
    DialogUpdateNganhNgheCmktComponent,
    DialogUpdateMienComponent,
    DialogUpdateDanTocComponent,
    DialogUpdateTonGiaoComponent,
    DialogUpdateDonViTvComponent
  ],
  imports: [
    CommonModule,
    TqDqtvRoutingModule,
    HttpClientModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: translateHttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    AngularMaterial,
    MomentModule,
    InputTextareaModule,
    EditorModule,
    QuillModule.forRoot({
      modules: {
        syntax: false,
        toolbar: [
          ['bold', 'italic', 'underline', 'strike'],
          [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
          [{ 'list': 'ordered' }, { 'list': 'bullet' }],
          [{ 'indent': '-1' }, { 'indent': '+1' }],
          ['blockquote', 'code-block']
        ]
      }
    }),
    TreeViewModule,
    BsDropdownModule.forRoot(),
    MatTabsModule,
    TreeModule,
    MatAutocompleteModule
  ]
})
export class TqDqtvModule { }
