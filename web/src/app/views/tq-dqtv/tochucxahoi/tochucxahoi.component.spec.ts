import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TochucxahoiComponent } from './tochucxahoi.component';

describe('TochucxahoiComponent', () => {
  let component: TochucxahoiComponent;
  let fixture: ComponentFixture<TochucxahoiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TochucxahoiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TochucxahoiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
