import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanheComponent } from './quanhe.component';

describe('QuanheComponent', () => {
  let component: QuanheComponent;
  let fixture: ComponentFixture<QuanheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuanheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuanheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
