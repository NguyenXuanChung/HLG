import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { TqDqtvService } from '../../../services/tq-dqtv.service';

@Component({
  selector: 'app-dialog-update-nghenghiep',
  templateUrl: './dialog-update-nghenghiep.component.html',
})
export class DialogUpdateNgheNghiepComponent implements OnInit {
  ngheNghiepForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.ngheNghiepForm.controls; }
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private tqDqtvService: TqDqtvService,
    public dialogRef: MatDialogRef<DialogUpdateNgheNghiepComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.ngheNghiepForm = this.fb.group(
      {
        name: [null, [Validators.required]],
        code: [null, [Validators.required]],
        description: [''],
        sortOrder: [0, [Validators.required, Validators.min(0)]],
      });
    if (this.data) {
      this.ngheNghiepForm.setValue({
        'name': this.data.name,
        'code': this.data.name,
        'description': this.data.description,
        'sortOrder': this.data.sortOrder,
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.ngheNghiepForm.invalid) {
      return;
    }
    if (!this.data) {
      this.tqDqtvService.insertNgheNghiep(this.ngheNghiepForm.value, this.common.TQ_DQTV_NgheNghiep.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.tqDqtvService.editNgheNghiep({
        data: this.ngheNghiepForm.value,
        condition: { id: this.data.id }
      }, this.common.TQ_DQTV_NgheNghiep.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
