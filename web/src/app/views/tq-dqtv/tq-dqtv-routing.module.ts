import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChucDanhComponent } from './chucdanh/chucdanh.component';
import { DoiTuongComponent } from './doituong/doituong.component';
import { NangkhieuComponent } from './nangkhieu/nangkhieu.component';
import { NghenghiepComponent } from './nghenghiep/nghenghiep.component';
import { SucKhoeComponent } from './suckhoe/suckhoe.component';
import { ToChucXaHoiComponent } from './tochucxahoi/tochucxahoi.component';
import { TrinhDoHocVanComponent } from './trinhdohocvan/trinhdohocvan.component';
import { TamHoanComponent } from './tamhoan/tamhoan.component';
import { TrinhDoVanHoaComponent } from './trinhdovanhoa/trinhdovanhoa.component';
import { HocViComponent } from './hocvi/hocvi.component';
import { QuanHeComponent } from './quanhe/quanhe.component';
import { NganhNgheCmktComponent } from './nganhnghecmkt/nganhnghecmkt.component';
import { MienComponent } from './mien/mien.component';
import { DanTocComponent } from './dantoc/dantoc.component';
import { TonGiaoComponent } from './tongiao/tongiao.component';
import { DonViTvComponent } from './donViTv/donViTv.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: ''
      },
      {
        path: 'chucdanh',
        component: ChucDanhComponent,
      },
      {
        path: 'doituong',
        component: DoiTuongComponent,
      },
      {
        path: 'nangkhieu',
        component: NangkhieuComponent,
      },
      {
        path: 'nghenghiep',
        component: NghenghiepComponent,
      },
      {
        path: 'suckhoe',
        component: SucKhoeComponent,
      },
      {
        path: 'tochucxahoi',
        component: ToChucXaHoiComponent,
      },
      {
        path: 'trinhdohocvan',
        component: TrinhDoHocVanComponent,
      },
      {
        path: 'tamhoan',
        component: TamHoanComponent,
      },
      {
        path: 'trinhdovanhoa',
        component: TrinhDoVanHoaComponent,
      },
      {
        path: 'hocvi',
        component: HocViComponent,
      },
      {
        path: 'quanhe',
        component: QuanHeComponent,
      },
      {
        path: 'nganhnghecmkt',
        component: NganhNgheCmktComponent,
      },
      {
        path: 'mien',
        component: MienComponent,
      },
      {
        path: 'dantoc',
        component: DanTocComponent,
      },
      {
        path: 'tongiao',
        component: TonGiaoComponent,
      },
      {
        path: 'donvitv',
        component: DonViTvComponent,
      }

    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TqDqtvRoutingModule { }
