import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DantocComponent } from './dantoc.component';

describe('DantocComponent', () => {
  let component: DantocComponent;
  let fixture: ComponentFixture<DantocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DantocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DantocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
