import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { TqDqtvService } from '../../../services/tq-dqtv.service';

@Component({
  selector: 'app-dialog-update-hocvi',
  templateUrl: './dialog-update-hocvi.component.html',
})
export class DialogUpdateHocViComponent implements OnInit {
  hocViForm: FormGroup;
  submitted = false;
  // convenience getter for easy access to form fields
  get f() { return this.hocViForm.controls; }
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private tqDqtvService: TqDqtvService,
    public dialogRef: MatDialogRef<DialogUpdateHocViComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.hocViForm = this.fb.group(
      {
        name: [null, [Validators.required]],
        code: [null, [Validators.required]],
        description: [''],
        sortOrder: [0, [Validators.required, Validators.min(0)]],
      });
    if (this.data) {
      this.hocViForm.setValue({
        'name': this.data.name,
        'code': this.data.name,
        'description': this.data.description,
        'sortOrder': this.data.sortOrder,
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.hocViForm.invalid) {
      return;
    }
    if (!this.data) {
      this.tqDqtvService.insertHocVi(this.hocViForm.value, this.common.TQ_DQTV_HocVi.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.tqDqtvService.editChucDanh({
        data: this.hocViForm.value,
        condition: { id: this.data.id }
      }, this.common.TQ_DQTV_ChucDanh.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
