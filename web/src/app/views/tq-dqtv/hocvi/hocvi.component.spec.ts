import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HocviComponent } from './hocvi.component';

describe('HocviComponent', () => {
  let component: HocviComponent;
  let fixture: ComponentFixture<HocviComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HocviComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HocviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
