import { DanhMucService } from './../../../services/danhmuc.service';
import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { conditionallyCreateMapObjectLiteral } from '@angular/compiler/src/render3/view/util';
// Translate
import { CommonService } from './../../../services/common.service';
import { TqDqtvService } from '../../../services/tq-dqtv.service';

@Component({
  selector: 'app-dialog-update-donvitv',
  templateUrl: './dialog-update-donViTv.component.html',
})
export class DialogUpdateDonViTvComponent implements OnInit {
  donViTvForm: FormGroup;
  submitted = false;
  quyMoToChucs: any[];
  trucThuocs: any[];
  phanCaps: any[];
  phanLoaiDns: any[];
  loaiDns: any[];
  xas: any[];
  huyens: any[];
  tinhs: any[];
  xaDungChans: any[];
  huyenDungChans: any[];
  tinhDungChans: any[];
  // convenience getter for easy access to form fields
  get f() { return this.donViTvForm.controls; }
  constructor(
    private common: CommonService,
    private fb: FormBuilder,
    private tqDqtvService: TqDqtvService,
    private dmService: DanhMucService,
    public dialogRef: MatDialogRef<DialogUpdateDonViTvComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
  }

  ngOnInit() {
    this.createForm();
    this.loadCombo();
  }
  loadCombo() {
    this.tqDqtvService.getLoaiDn({}, this.common.TQ_DQTV_ChucDanh.Insert).subscribe(res => {
      if (!res.error) {
        this.loaiDns = res.data;
      }
    });
    this.tqDqtvService.getQuyMoToChuc({}, this.common.TQ_DQTV_ChucDanh.Insert).subscribe(res => {
      if (!res.error) {
        this.quyMoToChucs = res.data;
      }
    });
    this.tqDqtvService.getTrucThuoc({}, this.common.TQ_DQTV_ChucDanh.Insert).subscribe(res => {
      if (!res.error) {
        this.trucThuocs = res.data;
      }
    });
    this.tqDqtvService.getPhanCap({}, this.common.TQ_DQTV_ChucDanh.Insert).subscribe(res => {
      if (!res.error) {
        this.phanCaps = res.data;
      }
    });
    this.tqDqtvService.getPhanLoaiDn({}, this.common.TQ_DQTV_ChucDanh.Insert).subscribe(res => {
      if (!res.error) {
        this.phanLoaiDns = res.data;
      }
    });
    this.dmService.getTinh({}, this.common.TQ_DQTV_ChucDanh.Insert).subscribe(res => {
      if (!res.error) {
        this.tinhs = res.data;
        this.tinhDungChans = res.data;
      }
    });
  }
  loadHuyen(e) {
    this.f.id_xa.setValue(null);
    this.getHuyen(e);
  }
  loadXa(e) {
    this.f.id_xa.setValue(null);
    this.getXa(e);
  }
  getHuyen(e) {
    this.dmService.getHuyen({ id_tinh: e }, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (res.error) {
      } else {
        this.huyens = res.data;
      }
    });
  }
  getXa(e) {
    this.dmService.getXa({ id_huyen: e }, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (res.error) {
      } else {
        this.xas = res.data;
      }
    });
  }
  loadHuyenDungChan(e) {
    this.f.id_xa_dungChan.setValue(null);
    this.getHuyenDungChan(e);
  }
  loadXaDungChan(e) {
    this.f.id_xa_dungChan.setValue(null);
    this.getXaDungChan(e);
  }
  getHuyenDungChan(e) {
    this.dmService.getHuyen({ id_tinh: e }, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (res.error) {
      } else {
        this.huyenDungChans = res.data;
      }
    });
  }
  getXaDungChan(e) {
    this.dmService.getXa({ id_huyen: e }, this.common.TQ_CongDan.Insert).subscribe(res => {
      if (res.error) {
      } else {
        this.xaDungChans = res.data;
      }
    });
  }
  createForm() {
    this.donViTvForm = this.fb.group(
      {
        name: [null, [Validators.required]],
        id_xa: [null, [Validators.required]],
        id_xa_dungChan: [null, [Validators.required]],
        id_quyMoToChuc: [null, [Validators.required]],
        id_thucThuoc: [null, [Validators.required]],
        id_phanCap: [null, [Validators.required]],
        id_phanLoaiDn: [null, [Validators.required]],
        id_loaiDn: [null, [Validators.required]],
        soLuongLaoDong: [null, [Validators.required]],
        ngayToChucDang: [null, [Validators.required]],
        ngayThanhLapBchqs: [null, [Validators.required]],
        coBchqs: [null, [Validators.required]],
        description: [''],
        sortOrder: [0, [Validators.required, Validators.min(0)]],
      });
    if (this.data) {
      this.donViTvForm.setValue({
        'id_thucThuoc': this.data.id_thucThuoc,
        'id_xa': this.data.id_xa,
        'id_xa_dungChan': this.data.id_xa_dungChan,
        'id_quyMoToChuc': this.data.id_quyMoToChuc,
        'id_phanCap': this.data.id_phanCap,
        'id_phanLoaiDn': this.data.id_phanLoaiDn,
        'id_loaiDn': this.data.id_loaiDn,
        'soLuongLaoDong': this.data.soLuongLaoDong,
        'ngayThanhLapBchqs': null,
        'coBchqs': this.data.coBchqs,
        'name': this.data.name,
        'ngayToChucDang': this.data.ngayToChucDang,
        'description': this.data.description,
        'sortOrder': this.data.sortOrder,
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.donViTvForm.invalid) {
      return;
    }
    if (!this.data) {
      this.tqDqtvService.insertDonViTv(this.donViTvForm.value, this.common.TQ_DQTV_ChucDanh.Insert)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    } else {
      this.tqDqtvService.editDonViTv({
        data: this.donViTvForm.value,
        condition: { id: this.data.id }
      }, this.common.TQ_DQTV_ChucDanh.Update)
        .subscribe(res => {
          if (res.error) {
            this.common.messageErr(res);
          } else {
            this.dialogRef.close(res.message);
          }
        });
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
