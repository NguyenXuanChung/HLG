import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrinhdohocvanComponent } from './trinhdohocvan.component';

describe('TrinhdohocvanComponent', () => {
  let component: TrinhdohocvanComponent;
  let fixture: ComponentFixture<TrinhdohocvanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrinhdohocvanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrinhdohocvanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
