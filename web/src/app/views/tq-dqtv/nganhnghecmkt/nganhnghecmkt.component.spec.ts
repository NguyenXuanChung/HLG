import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NganhnghecmktComponent } from './nganhnghecmkt.component';

describe('NganhnghecmktComponent', () => {
  let component: NganhnghecmktComponent;
  let fixture: ComponentFixture<NganhnghecmktComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NganhnghecmktComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NganhnghecmktComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
