import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NangkhieuComponent } from './nangkhieu.component';

describe('NangkhieuComponent', () => {
  let component: NangkhieuComponent;
  let fixture: ComponentFixture<NangkhieuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NangkhieuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NangkhieuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
