import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TamhoanComponent } from './tamhoan.component';

describe('TamhoanComponent', () => {
  let component: TamhoanComponent;
  let fixture: ComponentFixture<TamhoanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TamhoanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TamhoanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
