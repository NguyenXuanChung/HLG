import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TongiaoComponent } from './tongiao.component';

describe('TongiaoComponent', () => {
  let component: TongiaoComponent;
  let fixture: ComponentFixture<TongiaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TongiaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TongiaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
