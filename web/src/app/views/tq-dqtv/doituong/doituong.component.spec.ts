import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoituongComponent } from './doituong.component';

describe('DoituongComponent', () => {
  let component: DoituongComponent;
  let fixture: ComponentFixture<DoituongComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoituongComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoituongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
