import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrinhdovanhoaComponent } from './trinhdovanhoa.component';

describe('TrinhdovanhoaComponent', () => {
  let component: TrinhdovanhoaComponent;
  let fixture: ComponentFixture<TrinhdovanhoaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrinhdovanhoaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrinhdovanhoaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
