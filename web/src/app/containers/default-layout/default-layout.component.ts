import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../services/auth.service';
import { StorageService } from './../../services/storage.service';
import { AuthConstants } from '../../config/auth-constants';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LanguageService } from '../../services/language.service';
import { INavData } from '@coreui/angular';
import { CommonService } from './../../services/common.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit {
  public sidebarMinimized = false;
  public minimized = true;
  public navItems: INavData[];
  public avatar: any = '';

  user: {};

  constructor(
    private authService: AuthService,
    private storageService: StorageService,
    private router: Router,
    private translateService: TranslateService,
    private languageService: LanguageService,
    private common: CommonService,
    private titleService: Title
  ) { }

  ngOnInit() {

    this.authService.getUserInfo(this.common.Login.View).subscribe(res => {
      console.log(res);
      if (res.data) {
        this.user = res.data[0];
        this.avatar = res.avatar;
        this.storageService.store(AuthConstants.ID, res.data[0].portal_account_id).then();
        this.storageService.store(AuthConstants.PERMISSION, res.permission).then();
        this.storageService.store(AuthConstants.AUTH, this.user).then();
        if (res.menu[0]) {
          this.navItems = res.menu[0].children as INavData[];
        }
      } else {
        this.router.navigate(['/login']);
      }
    }, err => {
      console.log(err);
      this.router.navigate(['/login']);
    });
    this.selectLanguageVI();
    setTimeout(() => {
      this.titleService.setTitle(this.common.trans('V-Military'));
    }, 800);
  }
  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }
  selectLanguageVI() {
    this.translateService.use('vi');
  }
  selectLanguageEN() {
    this.translateService.use('en');
  }
  logout() {
    this.authService.logout();
  }
}
