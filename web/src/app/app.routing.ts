import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import { ChangePassComponent } from './views/change-pass/change-pass.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'admin',
        loadChildren: () => import('./views/admin/admin.module').then(m => m.AdminModule)
      },
      {
        path: 'common',
        loadChildren: () => import('./views/common/common.module').then(m => m.CommonAppModule)
      },
      {
        path: 'group',
        loadChildren: () => import('./views/group/group.module').then(m => m.GroupModule)
      },
      {
        path: 'diaban',
        loadChildren: () => import('./views/diaban/diaban.module').then(m => m.DiaBanModule)
      },
      // quốc phòng an ninh
      {
        path: 'gd-qpan',
        loadChildren: () => import('./views/qpan/qpan.module').then(m => m.QpanModule)
      },
      {
        path: 'location',
        loadChildren: () => import('./views/location/location.module').then(m => m.LocationModule)
      },
      {
        path: 'configuration',
        loadChildren: () => import('./views/configuration/configuration.module').then(m => m.ConfigurationModule)
      },
      {
        path: 'application',
        loadChildren: () => import('./views/application/application.module').then(m => m.ApplicationModule)
      },
      {
        path: 'dm',
        loadChildren: () => import('./views/danhmuc/danhmuc.module').then(m => m.DanhMucModule)
      },
      {
        path: 'tq',
        loadChildren: () => import('./views/tq/tq.module').then(m => m.TuyenQuanModule)
      },
      {
        path: 'tq-dqtv',
        loadChildren: () => import('./views/tq-dqtv/tq-dqtv.module').then(m => m.TqDqtvModule)
      },
      {
        path: 'dqtv',
        loadChildren: () => import('./views/dqtv/dqtv.module').then(m => m.DqtvModule)
      },
      {
        path: 'qltb',
        loadChildren: () => import('./views/qltb/qltb.module').then(m => m.QltbModule)
      },
      {
        path: 'base',
        loadChildren: () => import('./views/base/base.module').then(m => m.BaseModule)
      },
      {
        path: 'buttons',
        loadChildren: () => import('./views/buttons/buttons.module').then(m => m.ButtonsModule)
      },
      {
        path: 'charts',
        loadChildren: () => import('./views/chartjs/chartjs.module').then(m => m.ChartJSModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'icons',
        loadChildren: () => import('./views/icons/icons.module').then(m => m.IconsModule)
      },
      {
        path: 'notifications',
        loadChildren: () => import('./views/notifications/notifications.module').then(m => m.NotificationsModule)
      },
      {
        path: 'theme',
        loadChildren: () => import('./views/theme/theme.module').then(m => m.ThemeModule)
      },
      {
        path: 'widgets',
        loadChildren: () => import('./views/widgets/widgets.module').then(m => m.WidgetsModule)
      },
      {
        path: 'change-pass',
        loadChildren: () => import('./views/change-pass/change-pass.module').then(m => m.ChangePassModule)
      }
    ]
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
