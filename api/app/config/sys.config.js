module.exports = {
	CONG_TY: 'BITECCO GROUP',
	TRUNG_TAM: 'BITECCO JSC',
	EXCEL_TEMPLATE: 'templates/excel/',
	WORD_TEMPLATE: 'templates/word/',

	// REDIS
	REDIS_INFO: 'userInfo',
	REDIS_MENU: 'menu',
	REDIS_PERMISSION: 'permission',
	REDIS_ANHCANBO: 'anhCanBo',
};
