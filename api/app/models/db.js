const mariaDb = require("mariadb");
const dbConfig = require("../config/db.config");

var connection = mariaDb.createPool({
  host: dbConfig.HOST,
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  database: dbConfig.DATABASE,
  port: dbConfig.PORT,
  connectionLimit: 5,
  timezone: 'UTC'
});

module.exports = connection;