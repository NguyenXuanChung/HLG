module.exports = (app) => {
	const VerifyToken = require('../controllers/VerifyToken');
	const chucDanhs = require('../controllers/tq_dqtv/dm_chucDanh.controller');
	const congCuHoTros = require('../controllers/tq_dqtv/dm_congCuHoTro.controller');
	const vuKhis = require('../controllers/tq_dqtv/dm_vuKhi.controller');
	const giupDans = require('../controllers/tq_dqtv/dm_laoDongGiupDan.controller');
	const doiTuongs = require('../controllers/tq_dqtv/dm_doiTuong.controller');
	const miens = require('../controllers/tq_dqtv/dm_mien.controller');
	const nangKhieus = require('../controllers/tq_dqtv/dm_nangKhieu.controller');
	const tamHoans = require('../controllers/tq_dqtv/dm_tamHoan.controller');
	const ngheNghieps = require('../controllers/tq_dqtv/dm_ngheNghiep.controller');
	const toChucXaHois = require('../controllers/tq_dqtv/dm_toChucXaHoi.controller');
	const trinhDoHocVans = require('../controllers/tq_dqtv/dm_trinhDoHocVan.controller');
	const sucKhoes = require('../controllers/tq_dqtv/dm_sucKhoe.controller');
	const hocVis = require('../controllers/tq_dqtv/dm_hocVi.controller');
	const nganhNgheCMKTs = require('../controllers/tq_dqtv/dm_nganhNgheCmkt.controller');
	const quanHes = require('../controllers/tq_dqtv/dm_quanHe.controller');
	const quanHams = require('../controllers/tq_dqtv/dm_quanHam.controller');
	const donViDbdvs = require('../controllers/tq_dqtv/dm_donViDbdv.controller');
	const donViTvs = require('../controllers/tq_dqtv/dm_donViTv.controller');
	const capBacs = require('../controllers/tq_dqtv/dm_capBac.controller');
	const trinhDoVanHoas = require('../controllers/tq_dqtv/dm_trinhDoVanHoa.controller');
	const capHanhChinhs = require('../controllers/tq_dqtv/dm_capHanhChinh.controller');
	const loaiXas = require('../controllers/tq_dqtv/dm_loaiXa.controller');
	const lucLuongs = require('../controllers/tq_dqtv/dm_lucLuong.controller');
	const phanLoaiDiaLys = require('../controllers/tq_dqtv/dm_phanLoaiDiaLy.controller');
	const phanLoaiTrongDiems = require('../controllers/tq_dqtv/dm_phanLoaiTrongDiem.controller');
	const phanLoaiLucLuongs = require('../controllers/tq_dqtv/dm_phanLoaiLucLuong.controller');
	const quyMoToChucs = require('../controllers/tq_dqtv/dm_quyMoToChuc.controller');

	const loaiDns = require('../controllers/tq_dqtv/dm_loaiDn.controller');
	const trucThuocs = require('../controllers/tq_dqtv/dm_trucThuoc.controller');
	const phanLoaiDns = require('../controllers/tq_dqtv/dm_phanLoaiDn.controller');
	const phanCaps = require('../controllers/tq_dqtv/dm_phanCap.controller');
	const phanLoaiVuKhis = require('../controllers/tq_dqtv/dm_phanLoaiVuKhi.controller.js');
	// Quản lý danh mục tuyển quân và dân quân tự vệ.
	//#region phanloaivukhi
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-phanloaivukhi/get', VerifyToken, phanLoaiVuKhis.get);

	// Create a new item
	app.post('/tq-dqtv/dm-phanloaivukhi/insert', VerifyToken, phanLoaiVuKhis.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-phanloaivukhi/update', VerifyToken, phanLoaiVuKhis.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-phanloaivukhi/delete', VerifyToken, phanLoaiVuKhis.delete);
	//#endregion
		//#region loaidn
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-loaidn/get', VerifyToken, loaiDns.get);

	// Create a new item
	app.post('/tq-dqtv/dm-loaidn/insert', VerifyToken, loaiDns.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-loaidn/update', VerifyToken, loaiDns.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-loaidn/delete', VerifyToken, loaiDns.delete);
	//#endregion
		//#region phancap
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-phancap/get', VerifyToken, phanCaps.get);

	// Create a new item
	app.post('/tq-dqtv/dm-phancap/insert', VerifyToken, phanCaps.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-phancap/update', VerifyToken, phanCaps.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-phancap/delete', VerifyToken, phanCaps.delete);
	//#endregion
		//#region phanloaidn
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-phanloaidn/get', VerifyToken, phanLoaiDns.get);

	// Create a new item
	app.post('/tq-dqtv/dm-phanloaidn/insert', VerifyToken, phanLoaiDns.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-phanloaidn/update', VerifyToken, phanLoaiDns.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-phanloaidn/delete', VerifyToken, phanLoaiDns.delete);
	//#endregion
		//#region tructhuoc
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-tructhuoc/get', VerifyToken, trucThuocs.get);

	// Create a new item
	app.post('/tq-dqtv/dm-tructhuoc/insert', VerifyToken, trucThuocs.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-tructhuoc/update', VerifyToken, trucThuocs.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-tructhuoc/delete', VerifyToken, trucThuocs.delete);
	//#endregion
		//#region caphanhchinh
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-caphanhchinh/get', VerifyToken, capHanhChinhs.get);

	// Create a new item
	app.post('/tq-dqtv/dm-caphanhchinh/insert', VerifyToken, capHanhChinhs.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-caphanhchinh/update', VerifyToken, capHanhChinhs.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-caphanhchinh/delete', VerifyToken, capHanhChinhs.delete);
	//#endregion
	//#region loaixa
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-loaixa/get', VerifyToken, loaiXas.get);

	// Create a new item
	app.post('/tq-dqtv/dm-loaixa/insert', VerifyToken, loaiXas.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-loaixa/update', VerifyToken, loaiXas.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-loaixa/delete', VerifyToken, loaiXas.delete);
	//#endregion
	//#region lucluong
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-lucluong/get', VerifyToken, lucLuongs.get);

	// Create a new item
	app.post('/tq-dqtv/dm-lucluong/insert', VerifyToken, lucLuongs.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-lucluong/update', VerifyToken, lucLuongs.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-lucluong/delete', VerifyToken, lucLuongs.delete);
	//#endregion
	//#region phanloaidialy
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-phanloaidialy/get', VerifyToken, phanLoaiDiaLys.get);

	// Create a new item
	app.post('/tq-dqtv/dm-phanloaidialy/insert', VerifyToken, phanLoaiDiaLys.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-phanloaidialy/update', VerifyToken, phanLoaiDiaLys.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-phanloaidialy/delete', VerifyToken, phanLoaiDiaLys.delete);
	//#endregion
	//#region phanloaitrongdiem
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-phanloaitrongdiem/get', VerifyToken, phanLoaiTrongDiems.get);

	// Create a new item
	app.post('/tq-dqtv/dm-phanloaitrongdiem/insert', VerifyToken, phanLoaiTrongDiems.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-phanloaitrongdiem/update', VerifyToken, phanLoaiTrongDiems.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-phanloaitrongdiem/delete', VerifyToken, phanLoaiTrongDiems.delete);
	//#endregion
	//#region phanloailucluong
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-phanloailucluong/get', VerifyToken, phanLoaiLucLuongs.get);

	// Create a new item
	app.post('/tq-dqtv/dm-phanloailucluong/insert', VerifyToken, phanLoaiLucLuongs.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-phanloailucluong/update', VerifyToken, phanLoaiLucLuongs.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-phanloailucluong/delete', VerifyToken, phanLoaiLucLuongs.delete);
	//#endregion
	//#region quymotochuc
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-quymotochuc/get', VerifyToken, quyMoToChucs.get);

	// Create a new item
	app.post('/tq-dqtv/dm-quymotochuc/insert', VerifyToken, quyMoToChucs.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-quymotochuc/update', VerifyToken, quyMoToChucs.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-quymotochuc/delete', VerifyToken, quyMoToChucs.delete);
	//#endregion
	//#region quanham
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-quanham/get', VerifyToken, quanHams.get);

	// Create a new item
	app.post('/tq-dqtv/dm-quanham/insert', VerifyToken, quanHams.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-quanham/update', VerifyToken, quanHams.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-quanham/delete', VerifyToken, quanHams.delete);
	//#endregion

	//#region donvidbdv
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-donvidbdv/get', VerifyToken, donViDbdvs.get);

	// Create a new item
	app.post('/tq-dqtv/dm-donvidbdv/insert', VerifyToken, donViDbdvs.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-donvidbdv/update', VerifyToken, donViDbdvs.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-donvidbdv/delete', VerifyToken, donViDbdvs.delete);
	//#endregion
		//#region donvitv
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-donvitv/get', VerifyToken, donViTvs.get);

	// Create a new item
	app.post('/tq-dqtv/dm-donvitv/insert', VerifyToken, donViTvs.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-donvitv/update', VerifyToken, donViTvs.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-donvitv/delete', VerifyToken, donViTvs.delete);
	//#endregion
	//#region dm_capbac
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-capbac/get', VerifyToken, capBacs.get);

	// Create a new item
	app.post('/tq-dqtv/dm-capbac/insert', VerifyToken, capBacs.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-capbac/update', VerifyToken, capBacs.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-capbac/delete', VerifyToken, capBacs.delete);
	//#endregion		
	//#region dm_chucdanh
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-chucdanh/get', VerifyToken, chucDanhs.get);

	// Create a new item
	app.post('/tq-dqtv/dm-chucdanh/insert', VerifyToken, chucDanhs.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-chucdanh/update', VerifyToken, chucDanhs.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-chucdanh/delete', VerifyToken, chucDanhs.delete);
	//#endregion
	//#region dm_hocvi
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-hocvi/get', VerifyToken, hocVis.get);

	// Create a new item
	app.post('/tq-dqtv/dm-hocvi/insert', VerifyToken, hocVis.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-hocvi/update', VerifyToken, hocVis.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-hocvi/delete', VerifyToken, hocVis.delete);
	//#endregion
	//#region dm_nganhnghecmkt
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-nganhnghecmkt/get', VerifyToken, nganhNgheCMKTs.get);

	// Create a new item
	app.post('/tq-dqtv/dm-nganhnghecmkt/insert', VerifyToken, nganhNgheCMKTs.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-nganhnghecmkt/update', VerifyToken, nganhNgheCMKTs.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-nganhnghecmkt/delete', VerifyToken, nganhNgheCMKTs.delete);
	//#endregion
	//#region dm_quanhe
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-quanhe/get', VerifyToken, quanHes.get);

	// Create a new item
	app.post('/tq-dqtv/dm-quanhe/insert', VerifyToken, quanHes.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-quanhe/update', VerifyToken, quanHes.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-quanhe/delete', VerifyToken, quanHes.delete);
	//#endregion
	//#region dm_trinhdovanhoa
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-trinhdovanhoa/get', VerifyToken, trinhDoVanHoas.get);

	// Create a new item
	app.post('/tq-dqtv/dm-trinhdovanhoa/insert', VerifyToken, trinhDoVanHoas.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-trinhdovanhoa/update', VerifyToken, trinhDoVanHoas.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-trinhdovanhoa/delete', VerifyToken, trinhDoVanHoas.delete);
	//#endregion
	//#region dm_congcuhotro
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-congcuhotro/get', VerifyToken, congCuHoTros.get);

	// Create a new item
	app.post('/tq-dqtv/dm-congcuhotro/insert', VerifyToken, congCuHoTros.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-congcuhotro/update', VerifyToken, congCuHoTros.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-congcuhotro/delete', VerifyToken, congCuHoTros.delete);
	//#endregion
	//#region dm_vukhi
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-vukhi/get', VerifyToken, vuKhis.get);

	// Create a new item
	app.post('/tq-dqtv/dm-vukhi/insert', VerifyToken, vuKhis.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-vukhi/update', VerifyToken, vuKhis.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-vukhi/delete', VerifyToken, vuKhis.delete);
	//#endregion
	//#region dm_giupdan
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-giupdan/get', VerifyToken, giupDans.get);

	// Create a new item
	app.post('/tq-dqtv/dm-giupdan/insert', VerifyToken, giupDans.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-giupdan/update', VerifyToken, giupDans.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-giupdan/delete', VerifyToken, giupDans.delete);
	//#endregion
	//#region dm_doituong
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-doituong/get', VerifyToken, doiTuongs.get);

	// Create a new item
	app.post('/tq-dqtv/dm-doituong/insert', VerifyToken, doiTuongs.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-doituong/update', VerifyToken, doiTuongs.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-doituong/delete', VerifyToken, doiTuongs.delete);
	//#endregion
	//#region dm_mien
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-mien/get', VerifyToken, miens.get);

	// Create a new item
	app.post('/tq-dqtv/dm-mien/insert', VerifyToken, miens.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-mien/update', VerifyToken, miens.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-mien/delete', VerifyToken, miens.delete);
	//#endregion
	//#region dm_nangkhieu
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-nangkhieu/get', VerifyToken, nangKhieus.get);

	// Create a new item
	app.post('/tq-dqtv/dm-nangkhieu/insert', VerifyToken, nangKhieus.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-nangkhieu/update', VerifyToken, nangKhieus.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-nangkhieu/delete', VerifyToken, nangKhieus.delete);
	//#endregion
	//#region dm_nghenghiep
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-nghenghiep/get', VerifyToken, ngheNghieps.get);

	// Create a new item
	app.post('/tq-dqtv/dm-nghenghiep/insert', VerifyToken, ngheNghieps.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-nghenghiep/update', VerifyToken, ngheNghieps.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-nghenghiep/delete', VerifyToken, ngheNghieps.delete);
	//#endregion
	//#region dm_suckhoe
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-suckhoe/get', VerifyToken, sucKhoes.get);

	// Create a new item
	app.post('/tq-dqtv/dm-suckhoe/insert', VerifyToken, sucKhoes.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-suckhoe/update', VerifyToken, sucKhoes.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-suckhoe/delete', VerifyToken, sucKhoes.delete);
	//#endregion
	//#region dm_tamhoan
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-tamhoan/get', VerifyToken, tamHoans.get);

	// Create a new item
	app.post('/tq-dqtv/dm-tamhoan/insert', VerifyToken, tamHoans.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-tamhoan/update', VerifyToken, tamHoans.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-tamhoan/delete', VerifyToken, tamHoans.delete);
	//#endregion
	//#region dm_tochucxahoi
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-tochucxahoi/get', VerifyToken, toChucXaHois.get);

	// Create a new item
	app.post('/tq-dqtv/dm-tochucxahoi/insert', VerifyToken, toChucXaHois.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-tochucxahoi/update', VerifyToken, toChucXaHois.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-tochucxahoi/delete', VerifyToken, toChucXaHois.delete);
	//#endregion
	//#region dm_trinhdohocvan
	// Retrieve data with condition in body
	app.post('/tq-dqtv/dm-trinhdohocvan/get', VerifyToken, trinhDoHocVans.get);

	// Create a new item
	app.post('/tq-dqtv/dm-trinhdohocvan/insert', VerifyToken, trinhDoHocVans.create);

	// Update a item with id
	app.post('/tq-dqtv/dm-trinhdohocvan/update', VerifyToken, trinhDoHocVans.update);

	// Delete a item with id
	app.post('/tq-dqtv/dm-trinhdohocvan/delete', VerifyToken, trinhDoHocVans.delete);
	//#endregion


};
