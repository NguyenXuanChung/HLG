module.exports = app => {
  const nhomQuyens = require("../controllers/sys/portal_nhomQuyen.controller");
  const VerifyToken = require("../controllers/VerifyToken");

  app.post("/portal-nhom-quyens/get", VerifyToken, nhomQuyens.get);
};