module.exports = (app) => {
	const VerifyToken = require('../controllers/VerifyToken');
	const dqtvs = require('../controllers/dqtv/dqtv.controller');
	const vuKhis = require('../controllers/dqtv/vuKhi.controller');
	const dotHuanLuyens = require('../controllers/dqtv/dotHuanLuyen.controller');

	//#region dqtv
	// Retrieve data with condition in body
	app.post('/dqtv/get', VerifyToken, dqtvs.get);
	app.post('/dqtv/getlist', VerifyToken, dqtvs.getList);
	app.post('/dqtv/getbc', VerifyToken, dqtvs.getBcDqtv);

	// Create a new item
	app.post('/dqtv/insert', VerifyToken, dqtvs.create);

	// Update a item with id
	app.post('/dqtv/update', VerifyToken, dqtvs.update);

	// Delete a item with id
	app.post('/dqtv/delete', VerifyToken, dqtvs.delete);
	//#endregion
	
	//#region dqtv
	// Retrieve data with condition in body
	app.post('/dqtv/dothuanluyen/get', VerifyToken, dotHuanLuyens.get);

	// Create a new item
	app.post('/dqtv/dothuanluyen/insert', VerifyToken, dotHuanLuyens.create);

	// Update a item with id
	app.post('/dqtv/dothuanluyen/update', VerifyToken, dotHuanLuyens.update);

	// Delete a item with id
	app.post('/dqtv/dothuanluyen/delete', VerifyToken, dotHuanLuyens.delete);
	//#endregion
		
	//#region dqtv
	// Retrieve data with condition in body
	app.post('/dqtv/vukhi/get', VerifyToken, vuKhis.get);

	// Create a new item
	app.post('/dqtv/vukhi/insert', VerifyToken, vuKhis.create);

	// Update a item with id
	app.post('/dqtv/vukhi/update', VerifyToken, vuKhis.update);

	// Delete a item with id
	app.post('/dqtv/vukhi/delete', VerifyToken, vuKhis.delete);
	//#endregion
};
