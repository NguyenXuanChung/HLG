module.exports = app => {
  const VerifyToken = require('../controllers/VerifyToken');
  const canBos = require('../controllers/cb/canbo.controller');
  const quaTrinhCongTacs = require('../controllers/cb/canbo_quaTrinhCongTac.controller');
  const hopDongLaoDongs = require('../controllers/cb/canbo_hopDongLaoDong.controller');
  const ngoaiNgus = require('../controllers/cb/canbo_ngoaiNgu.controller');
  const bangCaps = require('../controllers/cb/canbo_bangCap.controller');
  const sucKhoeHangNams = require('../controllers/cb/canbo_sucKhoeHangNam.controller');
  const tienSuBenhAns = require('../controllers/cb/canbo_tienSuBenhAn.controller');
  const thanNhans = require('../controllers/cb/canbo_thanNhan.controller');
  const diaChis = require('../controllers/cb/canbo_diaChi.controller');
  const bienDongLuongCungs = require('../controllers/cb/canbo_bienDongLuongCung.controller');
  const kyLuats = require('../controllers/cb/canbo_kyLuat.controller');
  const khenThuongs = require('../controllers/cb/canbo_khenThuong.controller');
  const quaTrinhKiemNhiems = require('../controllers/cb/canbo_quaTrinhKiemNhiem.controller');


  //#region Cán bộ
  // Retrieve data with condition in body
  app.post('/cb/canbo/get', VerifyToken, canBos.get);

  app.post('/cb/canbo/getby-donvis', VerifyToken, canBos.getByDonVis);

  app.post('/cb/canbo/getby-idcanbo', VerifyToken, canBos.getByIdCanBo);

  // Create a new item
  app.post('/cb/canbo/insert', VerifyToken, canBos.create);

  // Update a item with id
  app.post('/cb/canbo/update', VerifyToken, canBos.update);

  // Delete a item with id
  app.post('/cb/canbo/delete', VerifyToken, canBos.delete);
  //#endregion


   //#region quá trình công tác
  // Retrieve data with condition in body
  app.post('/cb/quatrinhcongtac/get', VerifyToken, quaTrinhCongTacs.get);

  app.post('/cb/quatrinhcongtac/getByCanBo', VerifyToken, quaTrinhCongTacs.getByCanBo);
  
  // Create a new item
  app.post('/cb/quatrinhcongtac/insert', VerifyToken, quaTrinhCongTacs.insert);

  // Update a item with id
  app.post('/cb/quatrinhcongtac/update', VerifyToken, quaTrinhCongTacs.update);

  // Delete a item with id
  app.post('/cb/quatrinhcongtac/delete', VerifyToken, quaTrinhCongTacs.delete);
  //#endregion


   //#region hợp đồng lao động
  // Retrieve data with condition in body
  app.post('/cb/hopdonglaodong/get', VerifyToken, hopDongLaoDongs.get);

  app.post('/cb/hopdonglaodong/getByCanBo', VerifyToken, hopDongLaoDongs.getByCanBo);
  
  // Create a new item
  app.post('/cb/hopdonglaodong/insert', VerifyToken, hopDongLaoDongs.insert);

  // Update a item with id
  app.post('/cb/hopdonglaodong/update', VerifyToken, hopDongLaoDongs.update);

  // Delete a item with id
  app.post('/cb/hopdonglaodong/delete', VerifyToken, hopDongLaoDongs.delete);
  //#endregion


   //#region ngoại ngữ
  // Retrieve data with condition in body
  app.post('/cb/ngoaingu/get', VerifyToken, ngoaiNgus.get);

  app.post('/cb/ngoaingu/getByCanBo', VerifyToken, ngoaiNgus.getByCanBo);
  
  // Create a new item
  app.post('/cb/ngoaingu/insert', VerifyToken, ngoaiNgus.create);

  // Update a item with id
  app.post('/cb/ngoaingu/update', VerifyToken, ngoaiNgus.update);

  // Delete a item with id
  app.post('/cb/ngoaingu/delete', VerifyToken, ngoaiNgus.delete);
  //#endregion


  //#region bằng cấp
  // Retrieve data with condition in body
  app.post('/cb/bangcap/get', VerifyToken, bangCaps.get);

  app.post('/cb/bangcap/getByCanBo', VerifyToken, bangCaps.getByCanBo);
  
  // Create a new item
  app.post('/cb/bangcap/insert', VerifyToken, bangCaps.create);

  // Update a item with id
  app.post('/cb/bangcap/update', VerifyToken, bangCaps.update);

  // Delete a item with id
  app.post('/cb/bangcap/delete', VerifyToken, bangCaps.delete);
  //#endregion


  //#region sức khỏe hằng năm
  // Retrieve data with condition in body
  app.post('/cb/suckhoehangnam/get', VerifyToken, sucKhoeHangNams.get);

  app.post('/cb/suckhoehangnam/getByCanBo', VerifyToken, sucKhoeHangNams.getByCanBo);
  
  // Create a new item
  app.post('/cb/suckhoehangnam/insert', VerifyToken, sucKhoeHangNams.create);

  // Update a item with id
  app.post('/cb/suckhoehangnam/update', VerifyToken, sucKhoeHangNams.update);

  // Delete a item with id
  app.post('/cb/suckhoehangnam/delete', VerifyToken, sucKhoeHangNams.delete);
  //#endregion


   //#region tiền sử bệnh án
  // Retrieve data with condition in body
  app.post('/cb/tiensubenhan/get', VerifyToken, tienSuBenhAns.get);

  app.post('/cb/tiensubenhan/getByCanBo', VerifyToken, tienSuBenhAns.getByCanBo);
  
  // Create a new item
  app.post('/cb/tiensubenhan/insert', VerifyToken, tienSuBenhAns.create);

  // Update a item with id
  app.post('/cb/tiensubenhan/update', VerifyToken, tienSuBenhAns.update);

  // Delete a item with id
  app.post('/cb/tiensubenhan/delete', VerifyToken, tienSuBenhAns.delete);
  //#endregion


   //#region thân nhân
  // Retrieve data with condition in body
  app.post('/cb/thannhan/get', VerifyToken, thanNhans.get);

  app.post('/cb/thannhan/getByCanBo', VerifyToken, thanNhans.getByCanBo);
  
  // Create a new item
  app.post('/cb/thannhan/insert', VerifyToken, thanNhans.create);

  // Update a item with id
  app.post('/cb/thannhan/update', VerifyToken, thanNhans.update);

  // Delete a item with id
  app.post('/cb/thannhan/delete', VerifyToken, thanNhans.delete);
  //#endregion


  //#region địa chỉ
  // Retrieve data with condition in body
  app.post('/cb/diachi/get', VerifyToken, diaChis.get);

  // Update a item with id
  app.post('/cb/diachi/update', VerifyToken, diaChis.update);

  // Delete a item with id
  app.post('/cb/diachi/delete', VerifyToken, diaChis.delete);
  //#endregion

  //#region biến động lương cứng
  // Retrieve data with condition in body
  app.post('/cb/biendongluongcung/get', VerifyToken, bienDongLuongCungs.get);

  app.post('/cb/biendongluongcung/getByCanBo', VerifyToken, bienDongLuongCungs.getByCanBo);

  app.post('/cb/biendongluongcung/updateDenNgay', VerifyToken, bienDongLuongCungs.updateDenNgay);
  
  // Create a new item
  app.post('/cb/biendongluongcung/insert', VerifyToken, bienDongLuongCungs.create);

  // Update a item with id
  app.post('/cb/biendongluongcung/update', VerifyToken, bienDongLuongCungs.update);

  // Delete a item with id
  app.post('/cb/biendongluongcung/delete', VerifyToken, bienDongLuongCungs.delete);
  //#endregion

  //#region kỷ luật
  // Retrieve data with condition in body
  app.post('/cb/kyluat/get', VerifyToken, kyLuats.get);

  app.post('/cb/kyluat/getByCanBo', VerifyToken, kyLuats.getByCanBo);
  
  // Create a new item
  app.post('/cb/kyluat/insert', VerifyToken, kyLuats.create);

  // Update a item with id
  app.post('/cb/kyluat/update', VerifyToken, kyLuats.update);

  // Delete a item with id
  app.post('/cb/kyluat/delete', VerifyToken, kyLuats.delete);
  //#endregion

  //#region khen thưởng
  // Retrieve data with condition in body
  app.post('/cb/khenthuong/get', VerifyToken, khenThuongs.get);

  app.post('/cb/khenthuong/getByCanBo', VerifyToken, khenThuongs.getByCanBo);
  
  // Create a new item
  app.post('/cb/khenthuong/insert', VerifyToken, khenThuongs.create);

  // Update a item with id
  app.post('/cb/khenthuong/update', VerifyToken, khenThuongs.update);

  // Delete a item with id
  app.post('/cb/khenthuong/delete', VerifyToken, khenThuongs.delete);
  //#endregion

  //#region quá trình kiêm nhiệm
  // Retrieve data with condition in body
  app.post('/cb/quatrinhkiemnhiem/get', VerifyToken, quaTrinhKiemNhiems.get);

  app.post('/cb/quatrinhkiemnhiem/getByCanBo', VerifyToken, quaTrinhKiemNhiems.getByCanBo);
  
  // Create a new item
  app.post('/cb/quatrinhkiemnhiem/insert', VerifyToken, quaTrinhKiemNhiems.create);

  // Update a item with id
  app.post('/cb/quatrinhkiemnhiem/update', VerifyToken, quaTrinhKiemNhiems.update);

  // Delete a item with id
  app.post('/cb/quatrinhkiemnhiem/delete', VerifyToken, quaTrinhKiemNhiems.delete);
  //#endregion
}