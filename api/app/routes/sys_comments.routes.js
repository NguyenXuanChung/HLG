module.exports = app => {
  const VerifyToken = require('../controllers/VerifyToken');
  const comments = require('../controllers/sys/comments.controller');
  
  //#region Comment
	// Retrieve data with condition in body
  app.post('/sys/comments/get', VerifyToken, comments.get);

  app.post('/sys/comments/getbyid-data', VerifyToken, comments.getByID_Data);

  // Create a new item
  app.post('/sys/comments/insert', VerifyToken, comments.create);
  
  // Update a item with id
	app.post('/sys/comments/update', VerifyToken, comments.update);

	// Delete a item with id
	app.post('/sys/comments/delete', VerifyToken, comments.delete);
  //#endregion
}