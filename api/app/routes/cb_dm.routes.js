module.exports = app => {
  const VerifyToken = require('../controllers/VerifyToken');
  const tinhs = require('../controllers/cb/dm_tinh.controller');
  const huyens = require('../controllers/cb/dm_huyen.controller');
  const xa = require('../controllers/cb/dm_xa.controller');
  const quocGias = require('../controllers/cb/dm_quocGia.controller');
  const tonGiaos = require('../controllers/cb/dm_tonGiao.controller');
  const danTocs = require('../controllers/cb/dm_danToc.controller');
  const loaiNgachLuongs = require('../controllers/cb/dm_loaiNgachLuong.controller');
  const thangBangLuongs = require('../controllers/cb/dm_thangBangLuong.controller');
  const ngachLuongs = require('../controllers/cb/dm_ngachLuong.controller');
  const bacLuongs = require('../controllers/cb/dm_bacLuong.controller');
  const heSoLuongs = require('../controllers/cb/dm_heSoLuong.controller');
  const nhomViTriViecLams = require('../controllers/cb/dm_nhomViTriViecLam.controller');
  const viTriVieclams = require('../controllers/cb/dm_viTriViecLam.controller');
  const hocHams = require('../controllers/cb/dm_hocHam.controller');
  const hocVis = require('../controllers/cb/dm_hocVi.controller');
  const chucDanhs = require('../controllers/cb/dm_chucDanh.controller');
  const chucVus = require('../controllers/cb/dm_chucVu.controller');
  const capDaoTaos = require('../controllers/cb/dm_capDaoTao.controller');
  const nganhs = require('../controllers/cb/dm_nganh.controller');
  const chuyenNganhs = require('../controllers/cb/dm_chuyennganh.controller');
  const ngoaiNgus = require('../controllers/cb/dm_ngoaiNgu.controller');
  const phanLoaiTrinhDoNgoaiNgus = require('../controllers/cb/dm_phanLoaiTrinhDo_ngoaiNgu.controller');
  const trinhDoNgoaiNgus = require('../controllers/cb/dm_trinhDoNgoaiNgu.controller');
  const xepLoais = require('../controllers/cb/dm_xepLoai.controller');
  const quanHes = require('../controllers/cb/dm_quanHe.controller');
  const tinhTrangHonNhans = require('../controllers/cb/dm_tinhTrangHonNhan.controller');
  const vanBangs = require('../controllers/cb/dm_vanBang.controller');
  const lyLuanChinhTris = require('../controllers/cb/dm_lyLuanChinhTri.controller');
  const trinhDoQuanLys = require('../controllers/cb/dm_trinhDoQuanLy.controller');
  const phanLoaiTrinhDoTinHocs = require('../controllers/cb/dm_phanLoaiTrinhDo_tinHoc.controller');
  const trinhDoTinHocs = require('../controllers/cb/dm_trinhDoTinHoc.controller');
  const quanLyBenhViens = require('../controllers/cb/dm_quanLyBenhVien.controller');
  const xepLoaiSucKhoes = require('../controllers/cb/dm_xepLoaiSucKhoe.controller');
  const phanLoaiBenhs = require('../controllers/cb/dm_phanLoaiBenh.controller');
  const benhs = require('../controllers/cb/dm_benh.controller');
  const fixedDmTrinhDoVanHoas = require('../controllers/cb/fixeddm_trinhDoVanHoa.controller');
  const fixedDmLoaiLyDoNghis = require('../controllers/cb/fixeddm_loaiLyDoNghi.controller');
  const fixedDmLoaiQuaTrinhCongTacs = require('../controllers/cb/fixeddm_loaiQuaTrinhCongTac.controller');
  const fixedDmNhomMaus = require('../controllers/cb/fixeddm_nhomMau.controller');
  const fixedDmPhanLoaiHopDongs = require('../controllers/cb/fixeddm_phanLoaiHopDong.controller');
  const fixedDmLoaiCanBos = require('../controllers/cb/fixeddm_loaiCanBo.controller');
  const nguoiKyBaoCaos = require('../controllers/cb/dm_nguoiKyBaoCao.controller');
  const lyDoNghis = require('../controllers/cb/dm_lyDoNghi.controller');
  const mucLuongCoSos = require('../controllers/cb/dm_mucLuongCoSo.controller');
  const loaiHopDongs = require('../controllers/cb/dm_loaiHopDong.controller');
  const fixedDmCapQuyetDinhs = require('../controllers/cb/fixeddm_capQuyetDinh.controller');
  const fixedDmLoaiHinhThucKyLuats = require('../controllers/cb/fixeddm_loaiHinhThucKyLuat.controller');
  const hinhThucKyLuats = require('../controllers/cb/dm_hinhThucKyLuat.controller');
  const noiBanHanhs = require('../controllers/cb/dm_noiBanHanh.controller');
  const fixedDmLoaiHinhThucKhenThuongs = require('../controllers/cb/fixeddm_loaiHinhThucKhenThuong.controller');
  const hinhThucKhenThuongs = require('../controllers/cb/dm_hinhThucKhenThuong.controller');
  const fixedDmPhanTramHuongs = require('../controllers/cb/fixeddm_phanTramHuong.controller');
  const fixedDmLoaiLenLuongs = require('../controllers/cb/fixeddm_loaiLenLuong.controller');

  //#region dm_tinh
  // Retrieve data with condition in body
  app.post('/cb/dm-tinh/get', VerifyToken, tinhs.get);

  // Create a new item
  app.post('/cb/dm-tinh/insert', VerifyToken, tinhs.create);

  // Update a item with id
  app.post('/cb/dm-tinh/update', VerifyToken, tinhs.update);

  // Delete a item with id
  app.post('/cb/dm-tinh/delete', VerifyToken, tinhs.delete);
  //#endregion


  //#region dm_huyen
  // Retrieve data with condition in body
  app.post('/cb/dm-huyen/get', VerifyToken, huyens.get);

  // Create a new item
  app.post('/cb/dm-huyen/insert', VerifyToken, huyens.create);

  // Update a item with id
  app.post('/cb/dm-huyen/update', VerifyToken, huyens.update);

  // Delete a item with id
  app.post('/cb/dm-huyen/delete', VerifyToken, tinhs.delete);
  //#endregion


  //#region dm_xa
  // Retrieve data with condition in body
  app.post('/cb/dm-xa/get', VerifyToken, xa.get);

  // Create a new item
  app.post('/cb/dm-xa/insert', VerifyToken, xa.create);

  // Update a item with id
  app.post('/cb/dm-xa/update', VerifyToken, xa.update);

  // Delete a item with id
  app.post('/cb/dm-xa/delete', VerifyToken, xa.delete);
  //#endregion


  //#region dm_dantoc
  // Retrieve data with condition in body
  app.post('/cb/dm-dantoc/get', VerifyToken, danTocs.get);

  // Create a new item
  app.post('/cb/dm-dantoc/insert', VerifyToken, danTocs.create);

  // Update a item with id
  app.post('/cb/dm-dantoc/update', VerifyToken, danTocs.update);

  // Delete a item with id
  app.post('/cb/dm-dantoc/delete', VerifyToken, danTocs.delete);
  //#endregion


  //#region dm_tongiao
  // Retrieve data with condition in body
  app.post('/cb/dm-tongiao/get', VerifyToken, tonGiaos.get);

  // Create a new item
  app.post('/cb/dm-tongiao/insert', VerifyToken, tonGiaos.create);

  // Update a item with id
  app.post('/cb/dm-tongiao/update', VerifyToken, tonGiaos.update);

  // Delete a item with id
  app.post('/cb/dm-tongiao/delete', VerifyToken, tonGiaos.delete);
  //#endregion


  //#region dm_quocgia
  // Retrieve data with condition in body
  app.post('/cb/dm-quocgia/get', VerifyToken, quocGias.get);

  // Create a new item
  app.post('/cb/dm-quocgia/insert', VerifyToken, quocGias.create);

  // Update a item with id
  app.post('/cb/dm-quocgia/update', VerifyToken, quocGias.update);

  // Delete a item with id
  app.post('/cb/dm-quocgia/delete', VerifyToken, quocGias.delete);
  //#endregion


  //#region dm_loaingachluong
  // Retrieve data with condition in body
  app.post('/cb/dm-loaingachluong/get', VerifyToken, loaiNgachLuongs.get);

  // Create a new item
  app.post('/cb/dm-loaingachluong/insert', VerifyToken, loaiNgachLuongs.create);

  // Update a item with id
  app.post('/cb/dm-loaingachluong/update', VerifyToken, loaiNgachLuongs.update);

  // Delete a item with id
  app.post('/cb/dm-loaingachluong/delete', VerifyToken, loaiNgachLuongs.delete);
  //#endregion


  //#region dm_thangbangluong
  // Retrieve data with condition in body
  app.post('/cb/dm-thangbangluong/get', VerifyToken, thangBangLuongs.get);

  // Create a new item
  app.post('/cb/dm-thangbangluong/insert', VerifyToken, thangBangLuongs.create);

  // Update a item with id
  app.post('/cb/dm-thangbangluong/update', VerifyToken, thangBangLuongs.update);

  // Delete a item with id
  app.post('/cb/dm-thangbangluong/delete', VerifyToken, thangBangLuongs.delete);
  //#endregion


  //#region dm_ngachluong
  // Retrieve data with condition in body
  app.post('/cb/dm-ngachluong/get', VerifyToken, ngachLuongs.get);

  // Create a new item
  app.post('/cb/dm-ngachluong/insert', VerifyToken, ngachLuongs.create);

  // Update a item with id
  app.post('/cb/dm-ngachluong/update', VerifyToken, ngachLuongs.update);

  // Delete a item with id
  app.post('/cb/dm-ngachluong/delete', VerifyToken, ngachLuongs.delete);
  //#endregion


  //#region dm_bacluong
  // Retrieve data with condition in body
  app.post('/cb/dm-bacluong/get', VerifyToken, bacLuongs.get);

  // Create a new item
  app.post('/cb/dm-bacluong/insert', VerifyToken, bacLuongs.create);

  // Update a item with id
  app.post('/cb/dm-bacluong/update', VerifyToken, bacLuongs.update);

  // Delete a item with id
  app.post('/cb/dm-bacluong/delete', VerifyToken, bacLuongs.delete);
  //#endregion


  //#region dm_hesoluong
  // Retrieve data with condition in body
  app.post('/cb/dm-hesoluong/get', VerifyToken, heSoLuongs.get);

  // Retrieve data with condition in body
  app.post('/cb/dm-hesoluong/get1', VerifyToken, heSoLuongs.get1);

  // Create a new item
  app.post('/cb/dm-hesoluong/insert', VerifyToken, heSoLuongs.create);

  // Update a item with id
  app.post('/cb/dm-hesoluong/update', VerifyToken, heSoLuongs.update);

  // Delete a item with id
  app.post('/cb/dm-hesoluong/delete', VerifyToken, heSoLuongs.delete);
  //#endregion


  //#region dm_nhomvitrivieclam
  // Retrieve data with condition in body
  app.post('/cb/dm-nhomvitrivieclam/get', VerifyToken, nhomViTriViecLams.get);

  // Create a new item
  app.post('/cb/dm-nhomvitrivieclam/insert', VerifyToken, nhomViTriViecLams.create);

  // Update a item with id
  app.post('/cb/dm-nhomvitrivieclam/update', VerifyToken, nhomViTriViecLams.update);

  // Delete a item with id
  app.post('/cb/dm-nhomvitrivieclam/delete', VerifyToken, nhomViTriViecLams.delete);
  //#endregion


  //#region dm_vitrivieclam
  // Retrieve data with condition in body
  app.post('/cb/dm-vitrivieclam/get', VerifyToken, viTriVieclams.get);

  // Create a new item
  app.post('/cb/dm-vitrivieclam/insert', VerifyToken, viTriVieclams.create);

  // Update a item with id
  app.post('/cb/dm-vitrivieclam/update', VerifyToken, viTriVieclams.update);

  // Delete a item with id
  app.post('/cb/dm-vitrivieclam/delete', VerifyToken, viTriVieclams.delete);
  //#endregion


  //#region dm_hocham
  // Retrieve data with condition in body
  app.post('/cb/dm-hocham/get', VerifyToken, hocHams.get);

  // Create a new item
  app.post('/cb/dm-hocham/insert', VerifyToken, hocHams.create);

  // Update a item with id
  app.post('/cb/dm-hocham/update', VerifyToken, hocHams.update);

  // Delete a item with id
  app.post('/cb/dm-hocham/delete', VerifyToken, hocHams.delete);
  //#endregion


  //#region dm_hocvi
  // Retrieve data with condition in body
  app.post('/cb/dm-hocvi/get', VerifyToken, hocVis.get);

  // Create a new item
  app.post('/cb/dm-hocvi/insert', VerifyToken, hocVis.create);

  // Update a item with id
  app.post('/cb/dm-hocvi/update', VerifyToken, hocVis.update);

  // Delete a item with id
  app.post('/cb/dm-hocvi/delete', VerifyToken, hocVis.delete);
  //#endregion


  //#region dm_chucdanh
  // Retrieve data with condition in body
  app.post('/cb/dm-chucdanh/get', VerifyToken, chucDanhs.get);

  // Create a new item
  app.post('/cb/dm-chucdanh/insert', VerifyToken, chucDanhs.create);

  // Update a item with id
  app.post('/cb/dm-chucdanh/update', VerifyToken, chucDanhs.update);

  // Delete a item with id
  app.post('/cb/dm-chucdanh/delete', VerifyToken, chucDanhs.delete);
  //#endregion


  //#region dm_chucvu
  // Retrieve data with condition in body
  app.post('/cb/dm-chucvu/get', VerifyToken, chucVus.get);

  // Create a new item
  app.post('/cb/dm-chucvu/insert', VerifyToken, chucVus.create);

  // Update a item with id
  app.post('/cb/dm-chucvu/update', VerifyToken, chucVus.update);

  // Delete a item with id
  app.post('/cb/dm-chucvu/delete', VerifyToken, chucVus.delete);
  //#endregion


  //#region dm_capdaotao
  // Retrieve data with condition in body
  app.post('/cb/dm-capdaotao/get', VerifyToken, capDaoTaos.get);

  // Create a new item
  app.post('/cb/dm-capdaotao/insert', VerifyToken, capDaoTaos.create);

  // Update a item with id
  app.post('/cb/dm-capdaotao/update', VerifyToken, capDaoTaos.update);

  // Delete a item with id
  app.post('/cb/dm-capdaotao/delete', VerifyToken, capDaoTaos.delete);
  //#endregion


  //#region dm_nganh
  // Retrieve data with condition in body
  app.post('/cb/dm-nganh/get', VerifyToken, nganhs.get);

  // Create a new item
  app.post('/cb/dm-nganh/insert', VerifyToken, nganhs.create);

  // Update a item with id
  app.post('/cb/dm-nganh/update', VerifyToken, nganhs.update);

  // Delete a item with id
  app.post('/cb/dm-nganh/delete', VerifyToken, nganhs.delete);
  //#endregion


  //#region dm_chuyennganh
  // Retrieve data with condition in body
  app.post('/cb/dm-chuyennganh/get', VerifyToken, chuyenNganhs.get);

  // Create a new item
  app.post('/cb/dm-chuyennganh/insert', VerifyToken, chuyenNganhs.create);

  // Update a item with id
  app.post('/cb/dm-chuyennganh/update', VerifyToken, chuyenNganhs.update);

  // Delete a item with id
  app.post('/cb/dm-chuyennganh/delete', VerifyToken, chuyenNganhs.delete);
  //#endregion


  //#region dm_ngoaingu
  // Retrieve data with condition in body
  app.post('/cb/dm-ngoaingu/get', VerifyToken, ngoaiNgus.get);

  // Create a new item
  app.post('/cb/dm-ngoaingu/insert', VerifyToken, ngoaiNgus.create);

  // Update a item with id
  app.post('/cb/dm-ngoaingu/update', VerifyToken, ngoaiNgus.update);

  // Delete a item with id
  app.post('/cb/dm-ngoaingu/delete', VerifyToken, ngoaiNgus.delete);
  //#endregion


  //#region dm_phanloaitrinhdo_ngoaingu
  // Retrieve data with condition in body
  app.post('/cb/dm-phanloaitrinhdo-ngoaingu/get', VerifyToken, phanLoaiTrinhDoNgoaiNgus.get);

  // Create a new item
  app.post('/cb/dm-phanloaitrinhdo-ngoaingu/insert', VerifyToken, phanLoaiTrinhDoNgoaiNgus.create);

  // Update a item with id
  app.post('/cb/dm-phanloaitrinhdo-ngoaingu/update', VerifyToken, phanLoaiTrinhDoNgoaiNgus.update);

  // Delete a item with id
  app.post('/cb/dm-phanloaitrinhdo-ngoaingu/delete', VerifyToken, phanLoaiTrinhDoNgoaiNgus.delete);
  //#endregion


  //#region dm_trinhdongoaingu
  // Retrieve data with condition in body
  app.post('/cb/dm-trinhdongoaingu/get', VerifyToken, trinhDoNgoaiNgus.get);

  // Create a new item
  app.post('/cb/dm-trinhdongoaingu/insert', VerifyToken, trinhDoNgoaiNgus.create);

  // Update a item with id
  app.post('/cb/dm-trinhdongoaingu/update', VerifyToken, trinhDoNgoaiNgus.update);

  // Delete a item with id
  app.post('/cb/dm-trinhdongoaingu/delete', VerifyToken, trinhDoNgoaiNgus.delete);
  //#endregion


  //#region dm_quanhe
  // Retrieve data with condition in body
  app.post('/cb/dm-quanhe/get', VerifyToken, quanHes.get);

  // Create a new item
  app.post('/cb/dm-quanhe/insert', VerifyToken, quanHes.create);

  // Update a item with id
  app.post('/cb/dm-quanhe/update', VerifyToken, quanHes.update);

  // Delete a item with id
  app.post('/cb/dm-quanhe/delete', VerifyToken, quanHes.delete);
  //#endregion


  //#region dm_tinhtranghonnhan
  // Retrieve data with condition in body
  app.post('/cb/dm-tinhtranghonnhan/get', VerifyToken, tinhTrangHonNhans.get);

  // Create a new item
  app.post('/cb/dm-tinhtranghonnhan/insert', VerifyToken, tinhTrangHonNhans.create);

  // Update a item with id
  app.post('/cb/dm-tinhtranghonnhan/update', VerifyToken, tinhTrangHonNhans.update);

  // Delete a item with id
  app.post('/cb/dm-tinhtranghonnhan/delete', VerifyToken, tinhTrangHonNhans.delete);
  //#endregion


  //#region dm_vanbang
  // Retrieve data with condition in body
  app.post('/cb/dm-vanbang/get', VerifyToken, vanBangs.get);

  // Create a new item
  app.post('/cb/dm-vanbang/insert', VerifyToken, vanBangs.create);

  // Update a item with id
  app.post('/cb/dm-vanbang/update', VerifyToken, vanBangs.update);

  // Delete a item with id
  app.post('/cb/dm-vanbang/delete', VerifyToken, vanBangs.delete);
  //#endregion


  //#region dm_lyluanchinhtri
  // Retrieve data with condition in body
  app.post('/cb/dm-lyluanchinhtri/get', VerifyToken, lyLuanChinhTris.get);

  // Create a new item
  app.post('/cb/dm-lyluanchinhtri/insert', VerifyToken, lyLuanChinhTris.create);

  // Update a item with id
  app.post('/cb/dm-lyluanchinhtri/update', VerifyToken, lyLuanChinhTris.update);

  // Delete a item with id
  app.post('/cb/dm-lyluanchinhtri/delete', VerifyToken, lyLuanChinhTris.delete);
  //#endregion


  //#region dm_trinhdoquanly
  // Retrieve data with condition in body
  app.post('/cb/dm-trinhdoquanly/get', VerifyToken, trinhDoQuanLys.get);

  // Create a new item
  app.post('/cb/dm-trinhdoquanly/insert', VerifyToken, trinhDoQuanLys.create);

  // Update a item with id
  app.post('/cb/dm-trinhdoquanly/update', VerifyToken, trinhDoQuanLys.update);

  // Delete a item with id
  app.post('/cb/dm-trinhdoquanly/delete', VerifyToken, trinhDoQuanLys.delete);
  //#endregion


  //#region dm_phanloaitrinhdo_tinhoc
  // Retrieve data with condition in body
  app.post('/cb/dm-phanloaitrinhdo-tinhoc/get', VerifyToken, phanLoaiTrinhDoTinHocs.get);

  // Create a new item
  app.post('/cb/dm-phanloaitrinhdo-tinhoc/insert', VerifyToken, phanLoaiTrinhDoTinHocs.create);

  // Update a item with id
  app.post('/cb/dm-phanloaitrinhdo-tinhoc/update', VerifyToken, phanLoaiTrinhDoTinHocs.update);

  // Delete a item with id
  app.post('/cb/dm-phanloaitrinhdo-tinhoc/delete', VerifyToken, phanLoaiTrinhDoTinHocs.delete);
  //#endregion


  //#region dm_trinhdotinhoc
  // Retrieve data with condition in body
  app.post('/cb/dm-trinhdotinhoc/get', VerifyToken, trinhDoTinHocs.get);

  // Create a new item
  app.post('/cb/dm-trinhdotinhoc/insert', VerifyToken, trinhDoTinHocs.create);

  // Update a item with id
  app.post('/cb/dm-trinhdotinhoc/update', VerifyToken, trinhDoTinHocs.update);

  // Delete a item with id
  app.post('/cb/dm-trinhdotinhoc/delete', VerifyToken, trinhDoTinHocs.delete);
  //#endregion


  //#region dm_quanlybenhvien
  // Retrieve data with condition in body
  app.post('/cb/dm-quanlybenhvien/get', VerifyToken, quanLyBenhViens.get);

  // Create a new item
  app.post('/cb/dm-quanlybenhvien/insert', VerifyToken, quanLyBenhViens.create);

  // Update a item with id
  app.post('/cb/dm-quanlybenhvien/update', VerifyToken, quanLyBenhViens.update);

  // Delete a item with id
  app.post('/cb/dm-quanlybenhvien/delete', VerifyToken, quanLyBenhViens.delete);
  //#endregion


  //#region dm_xepLoai
  // Retrieve data with condition in body
  app.post('/cb/dm-xeploai/get', VerifyToken, xepLoais.get);

  // Create a new item
  app.post('/cb/dm-xeploai/insert', VerifyToken, xepLoais.create);

  // Update a item with id
  app.post('/cb/dm-xeploai/update', VerifyToken, xepLoais.update);

  // Delete a item with id
  app.post('/cb/dm-xeploai/delete', VerifyToken, xepLoais.delete);
  //#endregion


  //#region dm_xepLoaiSucKhoe
  // Retrieve data with condition in body
  app.post('/cb/dm-xeploaisuckhoe/get', VerifyToken, xepLoaiSucKhoes.get);

  // Create a new item
  app.post('/cb/dm-xeploaisuckhoe/insert', VerifyToken, xepLoaiSucKhoes.create);

  // Update a item with id
  app.post('/cb/dm-xeploaisuckhoe/update', VerifyToken, xepLoaiSucKhoes.update);

  // Delete a item with id
  app.post('/cb/dm-xeploaisuckhoe/delete', VerifyToken, xepLoaiSucKhoes.delete);
  //#endregion


  //#region dm_phanloaibenh
  // Retrieve data with condition in body
  app.post('/cb/dm-phanloaibenh/get', VerifyToken, phanLoaiBenhs.get);

  // Create a new item
  app.post('/cb/dm-phanloaibenh/insert', VerifyToken, phanLoaiBenhs.create);

  // Update a item with id
  app.post('/cb/dm-phanloaibenh/update', VerifyToken, phanLoaiBenhs.update);

  // Delete a item with id
  app.post('/cb/dm-phanloaibenh/delete', VerifyToken, phanLoaiBenhs.delete);
  //#endregion


  //#region dm_benh
  // Retrieve data with condition in body
  app.post('/cb/dm-benh/get', VerifyToken, benhs.get);

  // Create a new item
  app.post('/cb/dm-benh/insert', VerifyToken, benhs.create);

  // Update a item with id
  app.post('/cb/dm-benh/update', VerifyToken, benhs.update);

  // Delete a item with id
  app.post('/cb/dm-benh/delete', VerifyToken, benhs.delete);
  //#endregion


  //#region trình độ văn hóa
  // Retrieve data with condition in body
  app.post('/cb/dm-trinhdovanhoa/get', VerifyToken, fixedDmTrinhDoVanHoas.get);
  //#endregion


  //#region Loại lý do nghỉ
  // Retrieve data with condition in body
  app.post('/cb/dm-loailydonghi/get', VerifyToken, fixedDmLoaiLyDoNghis.get);
  //#endregion


  //#region Lý do nghỉ
  // Retrieve data with condition in body
  app.post('/cb/dm-lydonghi/get', VerifyToken, lyDoNghis.get);

  // Create a new item
  app.post('/cb/dm-lydonghi/insert', VerifyToken, lyDoNghis.create);

  // Update a item with id
  app.post('/cb/dm-lydonghi/update', VerifyToken, lyDoNghis.update);

  // Delete a item with id
  app.post('/cb/dm-lydonghi/delete', VerifyToken, lyDoNghis.delete);
  //#endregion

  
  //#region Loại quá trình công tác
  // Retrieve data with condition in body
  app.post('/cb/dm-loaiquatrinhcongtac/get', VerifyToken, fixedDmLoaiQuaTrinhCongTacs.get);
  //#endregion


  //#region nhóm máu
  // Retrieve data with condition in body
  app.post('/cb/dm-nhommau/get', VerifyToken, fixedDmNhomMaus.get);
  //#endregion


  //#region phân loại hợp đồng
  // Retrieve data with condition in body
  app.post('/cb/dm-phanloaihopdong/get', VerifyToken, fixedDmPhanLoaiHopDongs.get);
  //#endregion


  //#region Mức lương cơ sở
  // Retrieve data with condition in body
  app.post('/cb/dm-mucluongcoso/get', VerifyToken, mucLuongCoSos.get);

  app.post('/cb/dm-mucluongcoso/get1', VerifyToken, mucLuongCoSos.get1);

  // Create a new item
  app.post('/cb/dm-mucluongcoso/insert', VerifyToken, mucLuongCoSos.create);

  // Update a item with id
  app.post('/cb/dm-mucluongcoso/update', VerifyToken, mucLuongCoSos.update);

  // Delete a item with id
  app.post('/cb/dm-mucluongcoso/delete', VerifyToken, mucLuongCoSos.delete);
  //#endregion


  //#region Loại hợp đồng
  // Retrieve data with condition in body
  app.post('/cb/dm-loaihopdong/get', VerifyToken, loaiHopDongs.get);

  // Create a new item
  app.post('/cb/dm-loaihopdong/insert', VerifyToken, loaiHopDongs.create);

  // Update a item with id
  app.post('/cb/dm-loaihopdong/update', VerifyToken, loaiHopDongs.update);

  // Delete a item with id
  app.post('/cb/dm-loaihopdong/delete', VerifyToken, loaiHopDongs.delete);
  //#endregion

  
  //#region Loại cán bộ
  // Retrieve data with condition in body
  app.post('/cb/dm-loaicanbo/get', VerifyToken, fixedDmLoaiCanBos.get);
  //#endregion


  //#region Người ký báo cáo
  // Retrieve data with condition in body
  app.post('/cb/dm-nguoikybaocao/get', VerifyToken, nguoiKyBaoCaos.get);

  // Create a new item
  app.post('/cb/dm-nguoikybaocao/insert', VerifyToken, nguoiKyBaoCaos.create);

  // Update a item with id
  app.post('/cb/dm-nguoikybaocao/update', VerifyToken, nguoiKyBaoCaos.update);

  // Delete a item with id
  app.post('/cb/dm-nguoikybaocao/delete', VerifyToken, nguoiKyBaoCaos.delete);
  //#endregion


  //#region Hình thức kỷ luật
  // Retrieve data with condition in body
  app.post('/cb/dm-hinhthuckyluat/get', VerifyToken, hinhThucKyLuats.get);

  // Create a new item
  app.post('/cb/dm-hinhthuckyluat/insert', VerifyToken, hinhThucKyLuats.create);

  // Update a item with id
  app.post('/cb/dm-hinhthuckyluat/update', VerifyToken, hinhThucKyLuats.update);

  // Delete a item with id
  app.post('/cb/dm-hinhthuckyluat/delete', VerifyToken, hinhThucKyLuats.delete);
  //#endregion


  //#region cấp quyết định
  // Retrieve data with condition in body
  app.post('/cb/dm-capquyetdinh/get', VerifyToken, fixedDmCapQuyetDinhs.get);
  //#endregion


  //#region loại hình thức kỷ luật
  // Retrieve data with condition in body
  app.post('/cb/dm-loaihinhthuckyluat/get', VerifyToken, fixedDmLoaiHinhThucKyLuats.get);
  //#endregion


  //#region loại hình thức khen thưởng
  // Retrieve data with condition in body
  app.post('/cb/dm-loaihinhthuckhenthuong/get', VerifyToken, fixedDmLoaiHinhThucKhenThuongs.get);
  //#endregion


  //#region Nơi ban hành
  // Retrieve data with condition in body
  app.post('/cb/dm-noibanhanh/get', VerifyToken, noiBanHanhs.get);

  // Create a new item
  app.post('/cb/dm-noibanhanh/insert', VerifyToken, noiBanHanhs.create);

  // Update a item with id
  app.post('/cb/dm-noibanhanh/update', VerifyToken, noiBanHanhs.update);

  // Delete a item with id
  app.post('/cb/dm-noibanhanh/delete', VerifyToken, noiBanHanhs.delete);
  //#endregion


  //#region Hình thức khen thưởng
  // Retrieve data with condition in body
  app.post('/cb/dm-hinhthuckhenthuong/get', VerifyToken, hinhThucKhenThuongs.get);

  // Create a new item
  app.post('/cb/dm-hinhthuckhenthuong/insert', VerifyToken, hinhThucKhenThuongs.create);

  // Update a item with id
  app.post('/cb/dm-hinhthuckhenthuong/update', VerifyToken, hinhThucKhenThuongs.update);

  // Delete a item with id
  app.post('/cb/dm-hinhthuckhenthuong/delete', VerifyToken, hinhThucKhenThuongs.delete);
  //#endregion

   //#region phần trăm hưởng
  // Retrieve data with condition in body
  app.post('/cb/dm-phantramhuong/get', VerifyToken, fixedDmPhanTramHuongs.get);
  //#endregion

   //#region phần trăm hưởng
  // Retrieve data with condition in body
  app.post('/cb/dm-loailenluong/get', VerifyToken, fixedDmLoaiLenLuongs.get);
  //#endregion
}