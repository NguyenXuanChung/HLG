module.exports = (app) => {
	const VerifyToken = require('../controllers/VerifyToken');
	const tinhs = require('../controllers/dm/dm_tinh.controller');
	const huyens = require('../controllers/dm/dm_huyen.controller');
	const xa = require('../controllers/dm/dm_xa.controller');
	const thon = require('../controllers/dm/dm_thon.controller');
	const tonGiaos = require('../controllers/dm/dm_tonGiao.controller');
	const danTocs = require('../controllers/dm/dm_danToc.controller');

	//#region dm_tinh
	// Retrieve data with condition in body
	app.post('/dm/dm-tinh/get', VerifyToken, tinhs.get);

	// Create a new item
	app.post('/dm/dm-tinh/insert', VerifyToken, tinhs.create);

	// Update a item with id
	app.post('/dm/dm-tinh/update', VerifyToken, tinhs.update);

	// Delete a item with id
	app.post('/dm/dm-tinh/delete', VerifyToken, tinhs.delete);
	//#endregion
	//#region dm_tonGiao
	// Retrieve data with condition in body
	app.post('/dm/dm-tongiao/get', VerifyToken, tonGiaos.get);

	// Create a new item
	app.post('/dm/dm-tongiao/insert', VerifyToken, tonGiaos.create);

	// Update a item with id
	app.post('/dm/dm-tongiao/update', VerifyToken, tonGiaos.update);

	// Delete a item with id
	app.post('/dm/dm-tongiao/delete', VerifyToken, tonGiaos.delete);
	//#endregion
		//#region dm_danToc
	// Retrieve data with condition in body
	app.post('/dm/dm-dantoc/get', VerifyToken, danTocs.get);

	// Create a new item
	app.post('/dm/dm-dantoc/insert', VerifyToken, danTocs.create);

	// Update a item with id
	app.post('/dm/dm-dantoc/update', VerifyToken, danTocs.update);

	// Delete a item with id
	app.post('/dm/dm-dantoc/delete', VerifyToken, danTocs.delete);
	//#endregion
	//#region dm_huyen
	// Retrieve data with condition in body
	app.post('/dm/dm-huyen/get', VerifyToken, huyens.get);

	// Create a new item
	app.post('/dm/dm-huyen/insert', VerifyToken, huyens.create);

	// Update a item with id
	app.post('/dm/dm-huyen/update', VerifyToken, huyens.update);

	// Delete a item with id
	app.post('/dm/dm-huyen/delete', VerifyToken, tinhs.delete);
	//#endregion

	//#region dm_xa
	// Retrieve data with condition in body
	app.post('/dm/dm-xa/get', VerifyToken, xa.get);

	// Create a new item
	app.post('/dm/dm-xa/insert', VerifyToken, xa.create);

	// Update a item with id
	app.post('/dm/dm-xa/update', VerifyToken, xa.update);

	// Delete a item with id
	app.post('/dm/dm-xa/delete', VerifyToken, xa.delete);
	//#endregion

	//#region dm_thon
	// Retrieve data with condition in body
	app.post('/dm/dm-thon/get', VerifyToken, thon.get);

	// Create a new item
	app.post('/dm/dm-thon/insert', VerifyToken, thon.create);

	// Update a item with id
	app.post('/dm/dm-thon/update', VerifyToken, thon.update);

	// Delete a item with id
	app.post('/dm/dm-thon/delete', VerifyToken, thon.delete);
	//#endregion
};
