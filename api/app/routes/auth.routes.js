module.exports = app => {
  const auth = require("../controllers/sys/auth.controller");
  const VerifyToken = require("../controllers/VerifyToken");
  
  // Generate toke key with username and password in request body
  app.post("/auth/token", auth.login);

  // Get user info with token key
  app.get("/auth/getUserInfo", VerifyToken, auth.getUserInfo);
};

