module.exports = app => {
  const VerifyToken = require('../controllers/VerifyToken');
  const workflows = require('../controllers/sys/workflow.controller');
  const states = require('../controllers/sys/workflow_state.controller');
  const actions = require('../controllers/sys/workflow_state_action.controller');
  
  //#region WorkFlow
	// Retrieve data with condition in body
  app.post('/sys/workflow/get', VerifyToken, workflows.get);
  //#endregion

  //#region WorkFlow State
	// Retrieve data with condition in body
  app.post('/sys/workflow-state/get', VerifyToken, states.get);
  //#endregion

  //#region WorkFlow State Action
	// Retrieve data with condition in body
  app.post('/sys/workflow-state-action/get', VerifyToken, actions.get);
  //#endregion
}