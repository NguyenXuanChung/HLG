module.exports = (app) => {
	const VerifyToken = require('../controllers/VerifyToken');
	const doiTuongs = require('../controllers/qpan/dm_doiTuong.controller');
	const hinhThucDaoTaos = require('../controllers/qpan/dm_hinhThucDaoTao.controller');
	const ketQuas = require('../controllers/qpan/dm_ketQua.controller');
	const phanLoaiDoiTuongs = require('../controllers/qpan/dm_phanLoaiDoiTuong.controller');
	const lyLuanChinhTris = require('../controllers/qpan/dm_lyLuanChinhTri.controller');
	const phanLoaiLopHocs = require('../controllers/qpan/dm_phanLoaiLopHoc.controller');
	const truongHocs = require('../controllers/qpan/dm_truongHoc.controller');
	const xepLoaiLopHocs = require('../controllers/qpan/dm_xepLoaiLopHoc.controller');
	const quanLyGiaoViens = require('../controllers/qpan/quanLyGiaoVien.controller');
	const lopHocs = require('../controllers/qpan/lopHoc.controller');
	// Quản lý danh mục tuyển quân và dân quân tự vệ.
	//#region dm_doituong
	// Retrieve data with condition in body
	app.post('/qpan/dm-doituong/get', VerifyToken, doiTuongs.get);

	// Create a new item
	app.post('/qpan/dm-doituong/insert', VerifyToken, doiTuongs.create);

	// Update a item with id
	app.post('/qpan/dm-doituong/update', VerifyToken, doiTuongs.update);

	// Delete a item with id
	app.post('/qpan/dm-doituong/delete', VerifyToken, doiTuongs.delete);
	//#endregion
	//#region dm_lyluanchinhtri
	// Retrieve data with condition in body
	app.post('/qpan/dm-lyluanchinhtri/get', VerifyToken, lyLuanChinhTris.get);

	// Create a new item
	app.post('/qpan/dm-lyluanchinhtri/insert', VerifyToken, lyLuanChinhTris.create);

	// Update a item with id
	app.post('/qpan/dm-lyluanchinhtri/update', VerifyToken, lyLuanChinhTris.update);

	// Delete a item with id
	app.post('/qpan/dm-lyluanchinhtri/delete', VerifyToken, lyLuanChinhTris.delete);
	//#endregion
	//#region dm_hinhthucdaotao
	// Retrieve data with condition in body
	app.post('/qpan/dm-hinhthucdaotao/get', VerifyToken, hinhThucDaoTaos.get);

	// Create a new item
	app.post('/qpan/dm-hinhthucdaotao/insert', VerifyToken, hinhThucDaoTaos.create);

	// Update a item with id
	app.post('/qpan/dm-hinhthucdaotao/update', VerifyToken, hinhThucDaoTaos.update);

	// Delete a item with id
	app.post('/qpan/dm-hinhthucdaotao/delete', VerifyToken, hinhThucDaoTaos.delete);
	//#endregion
	//#region dm_ketQua
	// Retrieve data with condition in body
	app.post('/qpan/dm-ketqua/get', VerifyToken, ketQuas.get);

	// Create a new item
	app.post('/qpan/dm-ketqua/insert', VerifyToken, ketQuas.create);

	// Update a item with id
	app.post('/qpan/dm-ketqua/update', VerifyToken, ketQuas.update);

	// Delete a item with id
	app.post('/qpan/dm-ketqua/delete', VerifyToken, ketQuas.delete);
	//#endregion
	//#region dm_phanloaidoituong
	// Retrieve data with condition in body
	app.post('/qpan/dm-phanloaidoituong/get', VerifyToken, phanLoaiDoiTuongs.get);

	// Create a new item
	app.post('/qpan/dm-phanloaidoituong/insert', VerifyToken, phanLoaiDoiTuongs.create);

	// Update a item with id
	app.post('/qpan/dm-phanloaidoituong/update', VerifyToken, phanLoaiDoiTuongs.update);

	// Delete a item with id
	app.post('/qpan/dm-phanloaidoituong/delete', VerifyToken, phanLoaiDoiTuongs.delete);
	//#endregion
	//#region dm_phanloailophoc
	// Retrieve data with condition in body
	app.post('/qpan/dm-phanloailophoc/get', VerifyToken, phanLoaiLopHocs.get);

	// Create a new item
	app.post('/qpan/dm-phanloailophoc/insert', VerifyToken, phanLoaiLopHocs.create);

	// Update a item with id
	app.post('/qpan/dm-phanloailophoc/update', VerifyToken, phanLoaiLopHocs.update);

	// Delete a item with id
	app.post('/qpan/dm-phanloailophoc/delete', VerifyToken, phanLoaiLopHocs.delete);
	//#endregion
	//#region dm_truonghoc
	// Retrieve data with condition in body
	app.post('/qpan/dm-truonghoc/get', VerifyToken, truongHocs.get);

	// Create a new item
	app.post('/qpan/dm-truonghoc/insert', VerifyToken, truongHocs.create);

	// Update a item with id
	app.post('/qpan/dm-truonghoc/update', VerifyToken, truongHocs.update);

	// Delete a item with id
	app.post('/qpan/dm-truonghoc/delete', VerifyToken, truongHocs.delete);
	//#endregion
	//#region dm_xeploailophoc
	// Retrieve data with condition in body
	app.post('/qpan/dm-xeploailophoc/get', VerifyToken, xepLoaiLopHocs.get);

	// Create a new item
	app.post('/qpan/dm-xeploailophoc/insert', VerifyToken, xepLoaiLopHocs.create);

	// Update a item with id
	app.post('/qpan/dm-xeploailophoc/update', VerifyToken, xepLoaiLopHocs.update);

	// Delete a item with id
	app.post('/qpan/dm-xeploailophoc/delete', VerifyToken, xepLoaiLopHocs.delete);
	//#endregion
	//#region quanlygiaovien
	// Retrieve data with condition in body
	app.post('/qpan/quanlygiaovien/get', VerifyToken, quanLyGiaoViens.get);

	// Create a new item
	app.post('/qpan/quanlygiaovien/insert', VerifyToken, quanLyGiaoViens.create);

	// Update a item with id
	app.post('/qpan/quanlygiaovien/update', VerifyToken, quanLyGiaoViens.update);

	// Delete a item with id
	app.post('/qpan/quanlygiaovien/delete', VerifyToken, quanLyGiaoViens.delete);
	//#endregion

	//#region lophoc
	// Retrieve data with condition in body
	app.post('/qpan/lophoc/get', VerifyToken, lopHocs.get);

	// Create a new item
	app.post('/qpan/lophoc/insert', VerifyToken, lopHocs.create);

	// Update a item with id
	app.post('/qpan/lophoc/update', VerifyToken, lopHocs.update);

	// Delete a item with id
	app.post('/qpan/lophoc/delete', VerifyToken, lopHocs.delete);
	//#endregion
};
