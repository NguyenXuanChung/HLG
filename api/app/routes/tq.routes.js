module.exports = (app) => {
	const VerifyToken = require('../controllers/VerifyToken');
	const congDans = require('../controllers/tq/congDan.controller');
	const congDanDetails = require('../controllers/tq/congDanDetail.controller');
	const congDanNhapNgus = require('../controllers/tq/congDan.controller');
	const dotTuyenQuans = require('../controllers/tq/dotTuyenQuan.controller');
	const quanHes = require('../controllers/tq/quanHe.controller');
	const congDanTypeStates = require('../controllers/tq/congDanTypeState.controller');
	const congDanTypeStateActions = require('../controllers/tq/congDanTypeStateAction.controller');
	const keHoachDbdvs = require('../controllers/tq/keHoachDbdv.controller');
	const theoDoiHuanLuyens = require('../controllers/tq/theoDoiHuanLuyen.controller');
	//#region congdan
	// Retrieve data with condition in body
	app.post('/tq/congdan/get', VerifyToken, congDans.getList);
	app.post('/tq/congdan/getdbdv', VerifyToken, congDans.getDbdv);
	app.post('/tq/congdan/getbycmnd', VerifyToken, congDans.get);
	app.post('/tq/congdan/getlistnu', VerifyToken, congDans.getListNu);
	app.post('/tq/congdan/updatedetail', VerifyToken, congDans.updateDetail);
	app.post('/tq/congdan/updatebienche', VerifyToken, congDans.updateBienChe);
	app.post('/tq/congdan/updatedongvien', VerifyToken, congDans.updateDongVien);
	app.post('/tq/congdan/updategiaingach', VerifyToken, congDans.updateGiaiNgach);

	
	app.post('/tq/congdan/updatequatrinh', VerifyToken, congDans.updateQuaTrinh);
	
	// Get Công dân sãn sàng nhập ngũ
	app.post('/tq/congdan/getssnn', VerifyToken, congDans.getSsnn);

	// Get công dân đủ điều điện dự bị động viên.
	app.post('/tq/congdan/getssdbdv', VerifyToken, congDans.getSsDbdv);

	// Get quá trình của công dân
	app.post('/tq/congdan/getdetail', VerifyToken, congDans.getDetail);
	// Get công dân theo các đợt tuyển quân, xét duyệt xã, huyện
	app.post('/tq/congdan/getxd', VerifyToken, congDans.getXd);

	// Get công dân theo các đợt tuyển quân: nhập ngũ
	app.post('/tq/congdan/getnn', VerifyToken, congDans.getNn);

	// cập nhật thông tin tuyển quân theo các đợtÏ
	app.post('/tq/congdan/updatetq', VerifyToken, congDans.updateTq);

	// cập nhật thông tin tuyển quân theo các đợtÏ
	app.post('/tq/congdan/updatessdbdv', VerifyToken, congDans.updateLatest);
	// cập nhật thông tin xét duyệt
	app.post('/tq/congdan/updatexd', VerifyToken, congDans.updateXd);

	// cập nhật thông tin nhập ngũ
	app.post('/tq/congdan/updatenn', VerifyToken, congDans.updateDetail);
	// Create a new item
	app.post('/tq/congdan/insert', VerifyToken, congDans.create);

	// Update a item with id
	app.post('/tq/congdan/update', VerifyToken, congDans.update);

	// Delete a item with id
	app.post('/tq/congdan/delete', VerifyToken, congDans.delete);
	//#endregion
	//#region congdandetail
	// Retrieve data with condition in body
	app.post('/tq/congdandetail/get', VerifyToken, congDanDetails.get);

	// Create a new item
	app.post('/tq/congdandetail/insert', VerifyToken, congDanDetails.create);

	// Update a item with id
	app.post('/tq/congdandetail/update', VerifyToken, congDanDetails.update);
	app.post('/tq/congdandetail/updatelatest', VerifyToken, congDanDetails.updateLatest);

	// Delete a item with id
	app.post('/tq/congdandetail/delete', VerifyToken, congDanDetails.delete);
	//#endregion
	//#region congdantype
	// Retrieve data with condition in body
	app.post('/tq/congdantypestate/get', VerifyToken, congDanTypeStates.get);

	// Create a new item
	app.post('/tq/congdantypestate/insert', VerifyToken, congDanTypeStates.create);

	// Update a item with id
	app.post('/tq/congdantypestate/update', VerifyToken, congDanTypeStates.update);

	// Delete a item with id
	app.post('/tq/congdantypestate/delete', VerifyToken, congDanTypeStates.delete);
	//#endregion
	//#region congdantype
	// Retrieve data with condition in body
	app.post('/tq/congdantypestateaction/get', VerifyToken, congDanTypeStateActions.get);

	// Create a new item
	app.post('/tq/congdantypestateaction/insert', VerifyToken, congDanTypeStateActions.create);

	// Update a item with id
	app.post('/tq/congdantypestateaction/update', VerifyToken, congDanTypeStateActions.update);

	// Delete a item with id
	app.post('/tq/congdantypestateaction/delete', VerifyToken, congDanTypeStateActions.delete);
	//#endregion
	//#region congdannhapngu
	// Retrieve data with condition in body
	app.post('/tq/congdannhapngu/get', VerifyToken, congDanNhapNgus.get);

	// Create a new item
	app.post('/tq/congdannhapngu/insert', VerifyToken, congDanNhapNgus.create);

	// Update a item with id
	app.post('/tq/congdannhapngu/update', VerifyToken, congDanNhapNgus.update);

	// Delete a item with id
	app.post('/tq/congdannhapngu/delete', VerifyToken, congDanNhapNgus.delete);
	//#endregion
	//#region dottuyenquan
	// Retrieve data with condition in body
	app.post('/tq/dottuyenquan/get', VerifyToken, dotTuyenQuans.get);
	app.post('/tq/dottuyenquan/getbydate', VerifyToken, dotTuyenQuans.getByDate);
	// Create a new item
	app.post('/tq/dottuyenquan/insert', VerifyToken, dotTuyenQuans.create);

	// Update a item with id
	app.post('/tq/dottuyenquan/update', VerifyToken, dotTuyenQuans.update);

	// Delete a item with id
	app.post('/tq/dottuyenquan/delete', VerifyToken, dotTuyenQuans.delete);
	//#endregion
	//#region quanhe
	// Retrieve data with condition in body
	app.post('/tq/quanhe/get', VerifyToken, quanHes.get);
	// Create a new item
	app.post('/tq/quanhe/insert', VerifyToken, quanHes.create);

	// Update a item with id
	app.post('/tq/quanhe/update', VerifyToken, quanHes.update);

	// Delete a item with id
	app.post('/tq/quanhe/delete', VerifyToken, quanHes.delete);
	//#endregion
	//#region kehoachdbdv
	// Retrieve data with condition in body
	app.post('/tq/kehoachdbdv/get', VerifyToken, keHoachDbdvs.get);
	// Create a new item
	app.post('/tq/kehoachdbdv/insert', VerifyToken, keHoachDbdvs.create);

	// Update a item with id
	app.post('/tq/kehoachdbdv/update', VerifyToken, keHoachDbdvs.update);

	// Delete a item with id
	app.post('/tq/kehoachdbdv/delete', VerifyToken, keHoachDbdvs.delete);
	//#endregion
	//#region theodoihuanluyen
	// Retrieve data with condition in body
	app.post('/tq/theodoihuanluyen/get', VerifyToken, theoDoiHuanLuyens.get);
	// Create a new item
	app.post('/tq/theodoihuanluyen/insert', VerifyToken, theoDoiHuanLuyens.create);

	// Update a item with id
	app.post('/tq/theodoihuanluyen/update', VerifyToken, theoDoiHuanLuyens.update);

	// Delete a item with id
	app.post('/tq/theodoihuanluyen/delete', VerifyToken, theoDoiHuanLuyens.delete);
	//#endregion
};
