const Common = require('../../models/common.model');
const bcrypt = require('bcryptjs');
const tableName = 'sys_role_permission';
const Role = function () {};

// Find items with condition
exports.get = (req, res) => {
	var json = {
		table: tableName,
		data: req.body,
	};

	Common.get(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Retrieve data successfully!' });
	});
};

exports.getAllDmByID_Role = (req, res) => {
	var json = {
		select: {
			'A.id': 'permission_id',
			'A.parent_id': '',
			'A.code': 'code',
			'A.name': 'name',
			'B.*': '',
		},
		from: `sys_permission A`,
		left_join: {
			'sys_role_permission B': `B.id_permission = A.id AND B.id_role = ${req.body.id_role} AND B.status = 1`,
		},
		where: {
			'A.status': {
				operator: '=',
				value: 1,
			},
		},
		orderBy: 'A.sortOrder, A.name',
	};

	Common.getAdvance(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: nest(data, 0), message: 'Retrieve data successfully!' });
	});
};

const nest = (items, id = null, link = 'parent_id') =>
	items.filter((item) => item[link] === id).map((item) => ({ ...item, children: nest(items, item.permission_id) }));

exports.getTableByID_Role = (req, res) => {
	var json = {
		select: {
			'A.id': 'permission_id',
			'A.parent_id': '',
			'A.code': 'code',
			'A.name': 'name',
			'B.*': '',
		},
		from: `sys_permission A`,
		left_join: {
			'sys_role_permission B': `B.id_permission = A.id AND B.id_role = ${req.body.id_role} AND B.status = 1`,
		},
		where: {
			'A.status': {
				operator: '=',
				value: 1,
			},
		},
		orderBy: 'A.sortOrder, A.name',
	};

	Common.getAdvance(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: nestTable(data, 0), message: 'Retrieve data successfully!' });
	});
};

const nestTable = (items, id = null, link = 'parent_id') =>
	items
		.filter((item) => item[link] === id)
		.map((item) => ({ data: { ...item }, children: nestTable(items, item.permission_id) }));

// Create and Save a new item
exports.create = (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body, createBy: req.userId, createDate: Common.now() },
	};

	// Save data in the database
	Common.create(json, (err, data) => {
		if (err)
			res.send({
				error: { ...err, message: err.message } || `Some error occurred while creating the ${json.table}.`,
			});
		else res.send({ data: data, message: 'Item was created successfully!' });
	});
};

// Update a item identified by the condition in the request
exports.update = (req, res) => {
	// Validate Request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body.data, updateBy: req.userId, updateDate: Common.now() },
		condition: req.body.condition,
	};

	Common.update(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error updating ${json.table} with ${JSON.stringify(json.condition)}.`,
				});
			}
		} else res.send({ data: data, message: 'Item was updated successfully!' });
	});
};

Role.updateRolePermission = async (req, res) => {
	// Validate Request
	if (!req.body.data && !req.body.data[0]) {
		res.send({ error: { message: 'Content can not be empty!' } });
	}

	var stack = [],
		json = {},
		count = 0;
	stack.push(req.body.data[0]);

	while (stack.length !== 0) {
		count++;
		var node = stack.pop();
		// node.data
		// Thực hiện cập nhật quyền
		json = {
			procedure: 'sys_role_permission_update',
			data: {
				id_role: req.body.id_role,
				id_permission: node.data.permission_id,
				view: Common.bitToValue(node.data._view),
				export: Common.bitToValue(node.data._export),
				insert: Common.bitToValue(node.data._insert),
				update: Common.bitToValue(node.data._update),
				delete: Common.bitToValue(node.data._delete),
				updateBy: req.userId,
			},
		};

		await Common.callProcedure(json, (err, data) => {
			// if (err) {
			// 	if (err.kind === 'not_found') {
			// 		res.send({
			// 			error: { message: 'User info not found.' },
			// 		});
			// 	} else {
			// 		res.send({
			// 			error:
			// 				{ ...err, message: err.message } ||
			// 				`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
			// 		});
			// 	}
			// } else res.send({ data: data, message: `Procedure ${json.procedure} executed successfully!` });
		});

		// Tìm node con
		if (node.children) {
			for (var i = node.children.length - 1; i >= 0; i--) {
				stack.push(node.children[i]);
			}
		}
	}

	res.send({ data: { rows: count }, message: 'Item was updated successfully!' });
};
exports.updateRolePermission = Role.updateRolePermission;

// Delete items with the specified data in body in the request
exports.delete = (req, res) => {
	var json = {
		table: tableName,
		data: req.body,
		userId: req.userId,
	};

	Common.remove(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Could not delete ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ message: `Item was deleted successfully!` });
	});
};
