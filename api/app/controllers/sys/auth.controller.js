const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('../../config/db.config');
const _const = require('../../config/sys.config');
const Common = require('../../models/common.model');
const redis = require('../../models/redis');

const Auth = function () {};

Auth.Login = async (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: 'Content can not be empty!',
		});
	}

	let msg = {};
	try {
		var json = {
			table: 'portal_account',
			data: { username: req.body.username, status: 1 },
		};

		let account = [],
			id_donvi = 0;

		await Common.get(json, (err, data) => {
			if (err) {
				if (err.kind === 'not_found') {
					msg = { error: { message: 'Account not found.' } };
				} else {
					msg = { error: { ...err, message: err.message } };
				}
			} else {
				account = data;

				// Get id đơn vị
				json = {
					selectNguoc: {
						id_donvi: `f_canbo_getIdDonVi(${account[0].id_canbo}, NOW())`,
					},
				};

				Common.getAdvance(json, (err, data) => {
					if (!err) {
						id_donvi = data[0].id_donvi;
					}
				});
			}
		});

		json = {
			table: 'portal_account_password',
			data: { id_portal_account: account[0].portal_account_id, status: 1 },
		};

		let accountPassword = [];

		await Common.get(json, (err, data) => {
			if (err) {
				if (err.kind === 'not_found') {
					msg = { error: { message: 'Password not found.' } };
				} else {
					msg = { error: err.message };
				}
			} else {
				accountPassword = data[0];
			}
		});

		// check if the password is valid
		const comparePass = await bcrypt.compare(req.body.password, accountPassword.password);
		if (comparePass) {
			// Get permission
			let permission = {};

			json = {
				procedure: 'portal_account_getPermission',
				data: { _id: account[0].portal_account_id, _all: 0 },
			};

			await Common.callProcedure(json, (err, data) => {
				if (err) {
					if (err.kind === 'not_found') {
						msg = { error: { message: 'User permission not found.' } };
					} else {
						msg = { error: { ...err, message: err.message } };
					}
				} else {
					permission = convertPermission(data);
				}
			});

			var token = jwt.sign(
				{
					id: account[0].portal_account_id,
					id_canbo: account[0].id_canbo,
					id_donvis: account[0].id_donVis,
					roleLevel: account[0].roleLevel,
					customerId: account[0].id_customer,
					id_donvi: id_donvi,
				},
				config.SECRET,
				{
					expiresIn: '30d', // expires in 30 days
				}
			);

			// REDIS set permission
			await redis.set(
				`${_const.REDIS_PERMISSION}_${account[0].portal_account_id}`,
				JSON.stringify(permission),
				0
			);

			// REDIS remove User info
			await redis.del(`${_const.REDIS_INFO}_${account[0].portal_account_id}`);

			// Update last access date
			json = {
				table: 'portal_account',
				data: { lastAccessDate: Common.now() },
				condition: { portal_account_id: account[0].portal_account_id },
			};
			Common.update(json, (err, data) => {});

			// return the information including token as JSON
			return res.send({ auth: true, token: token, message: 'Login successfully!' });
		} else {
			return res.send({ auth: false, token: null, message: 'Incorrect password!' });
		}
	} catch (error) {
		if (!msg) return res.send(error);
		else return res.send(msg);
	}
};
exports.login = Auth.Login;

Auth.getUserInfo = async (req, res) => {
	let info = JSON.parse(await redis.get(`${_const.REDIS_INFO}_${req.userId}`));
	let permissions = JSON.parse(await redis.get(`${_const.REDIS_PERMISSION}_${req.userId}`));
	var json = {},
		menus = [];

	// Lấy thông tin người dùng
	if (!info) {
		json = {
			procedure: 'portal_account_getUserInfo',
			data: { portal_account_id: req.userId },
		};

		await Common.callProcedure(json, async (err, data) => {
			if (err) {
				if (err.kind === 'not_found') {
					res.send({
						error: { message: 'User info not found.' },
					});
				} else {
					res.send({
						error:
							{ ...err, message: err.message } ||
							`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
					});
				}
			} else {
				info = data;
				// REDIS set userinfo
				await redis.set(`${_const.REDIS_INFO}_${req.userId}`, JSON.stringify(info));
			}
		});
	}

	// Lấy quyền truy cập
	if (!permissions) {
		json = {
			procedure: 'portal_account_getPermission',
			data: { _id: req.userId, _all: 1 },
		};
		await Common.callProcedure(json, async (err, permission) => {
			if (err) {
				if (err.kind === 'not_found') {
					res.send({
						error: { message: 'User permission not found.' },
					});
				} else {
					res.send({
						error:
							{ ...err, message: err.message } ||
							`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
					});
				}
			} else {
				permissions = convertPermission(permission);
				// REDIS set permission
				await redis.set(`${_const.REDIS_PERMISSION}_${req.userId}`, JSON.stringify(permissions), 0);
			}
		});
	}

	// Lấy menu
	json = {
		procedure: 'portal_account_getMenu',
		data: { _id_account: req.userId },
	};
	await Common.callProcedure(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Menu not found.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
				});
			}
		} else {
			menus = Common.trimChildren(nest(data, 0, 0));
			// REDIS set menu
			// redis.set(`${_const.REDIS_MENU}_${req.userId}`, JSON.stringify(menus), 0).then((result) => {});
		}
	});

	let avatar = '';
	if (info[0].anh) {
		avatar = Common.imageToBase64(info[0].anh);
	}
	
	res.send({
		data: info,
		permission: permissions,
		menu: menus,
		avatar: `data:image/jpeg;base64,${avatar}`,
		message: `Get user info successfully!`,
	});
};
exports.getUserInfo = Auth.getUserInfo;

const convertPermission = (data) => {
	var res = {};
	data.forEach(
		(v) =>
			(res[v.code] = {
				_view: v._view,
				_export: v._export,
				_insert: v._insert,
				_update: v._update,
				_delete: v._delete,
			})
	);
	return res;
};

const nest = (items, id = null, level = 0, link = 'parent_id') =>
	items
		.filter((item) => item[link] === id)
		.map((item) => ({
			name: item.name,
			url: item.url,
			icon: item.icon,
			level: level,
			class: `menu-level${level}`,
			// attributes: { collapse: true },
			children: nest(items, item.id, level + 1),
		}));
