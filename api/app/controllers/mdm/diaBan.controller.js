const Common = require('../../models/common.model');
const tableName = 'mdm_diaban';

// Find items with condition
// exports.get = (req, res) => {
// 	var json = {
// 		table: tableName,
// 		data: { id_customer: req.customerId, ...req.body },
// 	};

// 	Common.get(json, (err, data) => {
// 		if (err) {
// 			if (err.kind === 'not_found') {
// 				res.send({
// 					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
// 					error: { message: 'Not found data.' },
// 				});
// 			} else {
// 				res.send({
// 					error:
// 						{ ...err, message: err.message } ||
// 						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
// 				});
// 			}
// 		} else res.send({ data: data, message: 'Retrieve data successfully!' });
// 	});
// };
exports.get = (req, res) => {
		var json = {
			select: {
				'A.*': '',
			},
			from: `${tableName} A`,
			inner_join: {
				'portal_account_diaBan B': 'A.id = B.id_diaBan AND b.status = 1',
			},
			whereGroup: {
				status: [
					{
						'A.status': {
							operator: '=',
							value: 1,
						},
					}
				],
				id_customer: [
					{
						'A.id_customer': {
							operator: '=',
							value: req.customerId,
						},
					},
				],
				id_account: [
					{
						'B.id_account': {
							operator: '=',
							value: req.userId,
						},
					},
				]
			},
			orderBy: 'A.sortOrder, A.name',
		};
	
		Common.getAdvance(json, (err, data) => {
			if (err) {
				if (err.kind === 'not_found') {
					res.send({
						// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
						error: { message: 'Not found data.' },
					});
				} else {
					res.send({
						error:
							{ ...err, message: err.message } ||
							`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
					});
				}
			} else res.send({ data: data, message: 'Retrieve data successfully!' });
		});
};
exports.getList = (req, res) => {
	var json = {
		select: {
			'A.*': '',
		},
		from: `${tableName} A`,
		inner_join: {
			'portal_account_diaBan B': 'A.id = B.id_diaBan AND b.status = 1',
		},
		whereGroup: {
			status: [
				{
					'A.status': {
						operator: '=',
						value: 1,
					},
				}
			],
			id_customer: [
				{
					'A.id_customer': {
						operator: '=',
						value: req.customerId,
					},
				},
			],
			id_account: [
				{
					'B.id_account': {
						operator: '=',
						value: req.userId,
					},
				},
			],
			code: [
				{
					'A.code': {
						operator: 'LIKE',
						value: req.body.code,
					},
				},
			],
			name: [
				{
					'A.name': {
						operator: 'LIKE',
						value: req.body.name,
					},
				},
			],
			description: [
				{
					'A.name': {
						operator: 'LIKE',
						value: req.body.description,
					},
				},
			],
		},
		orderBy: 'A.sortOrder, A.name',
	};

	Common.getAdvance(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Retrieve data successfully!' });
	});
};
exports.getByAccount = (req, res) => {
	var json = {
		select: {
			'A.*': '',
			'C.id_account': 'checked',
		},
		from: `${tableName} A`,
		inner_join: {
			'portal_account_diaBan B': 'A.id = B.id_diaBan AND b.status = 1',
		},
		left_join: {
			'portal_account_diaBan C': 'C.id_diaBan = B.id_diaBan AND C.status = 1 AND C.id_account = ' + req.body.id_account+ '',
		},
		whereGroup: {
			status: [
				{
					'A.status': {
						operator: '=',
						value: 1,
					},
				}
			],
			id_customer: [
				{
					'A.id_customer': {
						operator: '=',
						value: req.customerId,
					},
				},
			],
			id_account: [
				{
					'B.id_account': {
						operator: '=',
						value: req.userId,
					},
				},
			],
		},
		orderBy: 'A.sortOrder, A.name',
	};

	Common.getAdvance(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Retrieve data successfully!' });
	});
};

// Create and Save a new item
exports.create = (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body, id_customer: req.customerId, createBy: req.userId, createDate: Common.now() },
	};

	// Save data in the database
	Common.create(json, (err, data) => {
		if (err)
			res.send({
				error: { ...err, message: err.message } || `Some error occurred while creating the ${json.table}.`,
			});
		else res.send({ data: data, message: 'Item was created successfully!' });
	});
};

// Update a item identified by the condition in the request
exports.update = (req, res) => {
	// Validate Request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body.data, updateBy: req.userId, updateDate: Common.now() },
		condition: req.body.condition,
	};

	Common.update(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error updating ${json.table} with ${JSON.stringify(json.condition)}.`,
				});
			}
		} else res.send({ data: data, message: 'Item was updated successfully!' });
	});
};

// Delete items with the specified data in body in the request
exports.delete = (req, res) => {
	var json = {
		table: tableName,
		data: req.body,
		userId: req.userId,
	};

	Common.remove(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Could not delete ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ message: `Item was deleted successfully!` });
	});
};
