const Common = require('../../models/common.model');
const tableName = 'mdm_thongbao';

// Find items with condition
exports.get = (req, res) => {
	var json = {
		table: tableName,
		data: { id_customer: req.customerId, ...req.body },
	};

	Common.get(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Retrieve data successfully!' });
	});
};

exports.getByDiaBan = (req, res) => {
	var json = {
		select: {
			'A.*': '',
			'C.name': 'tenLoaithongBao',
			'D.name': 'tenLoaiTin',
			'E.name': 'tenCheDoPhat',
			'F.name': 'tenKieuPhat',
			'G.name': 'tenkieuLap',
			'H.name': 'tenTrangThaiDuyet',
			'I.name': 'tenTrangThaiPhat',
		},
		from: `${tableName} A`,
		inner_join: {
			'mdm_dm_loaithongbao C': 'A.id_dm_loaiThongBao = C.id',
			'mdm_dm_loaitin D': 'A.id_dm_loaiTin = D.id',
			'mdm_dm_chedophat E': 'A.id_dm_cheDoPhat = E.id',
			'mdm_dm_kieuphat F': 'A.id_dm_kieuPhat = F.id',
			'mdm_dm_kieulap G': 'A.id_dm_kieuLap = G.id',
		},
		left_join: {
			'mdm_dm_trangthaiduyet H': 'A.id_dm_trangThaiDuyet = H.id',
			'mdm_dm_trangthaiphat I': 'A.id_dm_trangThaiPhat = I.id',
		},
		whereGroup: {
			status: [
				{
					'A.status': {
						operator: '=',
						value: 1,
					},
				},
			],
			id_customer: [
				{
					'A.id_customer': {
						operator: '=',
						value: req.customerId,
					},
				},
			],
			id_diaBan: [
				{
					'A.id_diaBan': {
						operator: '=',
						value: req.body.id_diaBan,
					},
				},
			],
			date: [
				{
					'CAST(A.tuNgay AS DATE)': {
						operator: 'BETWEEN',
						from: req.body.tuNgay,
						to: req.body.denNgay,
					},
				},
				{
					'CAST(A.denNgay AS DATE)': {
						operator: 'BETWEEN',
						from: req.body.tuNgay,
						to: req.body.denNgay,
					},
				},
				{
					[`'${req.body.tuNgay}'`]: {
						operator: 'BETWEEN',
						from: 'CAST(A.tuNgay AS DATE)',
						to: 'CAST(A.denNgay AS DATE)',
					},
				},
			],
		},
		orderBy: 'A.tuNgay, A.gioPhat',
	};

	Common.getAdvance(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: nest(data, 0), message: 'Retrieve data successfully!' });
	});
};

// Create and Save a new item
exports.create = (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body, id_customer: req.customerId, createBy: req.userId, createDate: Common.now() },
	};

	// Save data in the database
	Common.create(json, (err, data) => {
		if (err)
			res.send({
				error: { ...err, message: err.message } || `Some error occurred while creating the ${json.table}.`,
			});
		else res.send({ data: data, message: 'Item was created successfully!' });
	});
};

// Update a item identified by the condition in the request
exports.update = (req, res) => {
	// Validate Request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body.data, updateBy: req.userId, updateDate: Common.now() },
		condition: req.body.condition,
	};

	Common.update(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error updating ${json.table} with ${JSON.stringify(json.condition)}.`,
				});
			}
		} else res.send({ data: data, message: 'Item was updated successfully!' });
	});
};
exports.getDuyet = (req, res) => {
	var json = {
		table: 'mdm_thongbao_duyet',
		data: { ...req.body },
	};

	Common.get(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Retrieve data successfully!' });
	});
};
// Create and Save a new item
exports.createDuyet = (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: 'mdm_thongbao_duyet',
		data: { ...req.body, createBy: req.userId, createDate: Common.now() },
	};

	// Save data in the database
	Common.create(json, (err, data) => {
		if (err)
			res.send({
				error: { ...err, message: err.message } || `Some error occurred while creating the ${json.table}.`,
			});
		else res.send({ data: data, message: 'Item was created successfully!' });
	});
};
// Update a item identified by the condition in the request
exports.updateDuyet = (req, res) => {
	// Validate Request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: 'mdm_thongbao_duyet',
		data: { ...req.body.data, updateBy: req.userId, updateDate: Common.now() },
		condition: req.body.condition,
	};

	Common.update(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error updating ${json.table} with ${JSON.stringify(json.condition)}.`,
				});
			}
		} else res.send({ data: data, message: 'Item was updated successfully!' });
	});
};
// Delete items with the specified data in body in the request
exports.delete = (req, res) => {
	var json = {
		table: tableName,
		data: req.body,
		userId: req.userId,
	};

	Common.remove(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Could not delete ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ message: `Item was deleted successfully!` });
	});
};
