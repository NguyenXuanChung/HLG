const Common = require('../../models/common.model');
const bcrypt = require('bcryptjs');
const tableName = 'canbo_hopdonglaodong';

const Hdld = function () { };
// Find items with condition
exports.get = (req, res) => {
	var json = {
		table: tableName,
		data: req.body,
	};

	Common.get(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Retrieve data successfully!' });
	});
};

// get by cán bộ
exports.getByCanBo = (req, res) => {
	var json = {
		procedure: 'canbo_hopDongLaoDong_getByCanBo',
		data: { id_canbo: req.body.id_canBo },
	};
	Common.callProcedure(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Retrieve data successfully!' });
	});
};

// Create and Save a new item
exports.create = (req, res) => {
	// Validate request
	if (!req.body || !req.body.id_canBo || !req.body.tuNgay) {
		res.send({
			error: { message: 'Body content incorrect!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body, createBy: req.userId, createDate: Common.now() },
	};

	// Save data in the database
	Common.create(json, (err, data) => {
		if (err)
			res.send({
				error: { ...err, message: err.message } || `Some error occurred while creating the ${json.table}.`,
			});
		else res.send({ data: data, message: 'Item was created successfully!' });
	});
};

Hdld.insert = async (req, res) => {
	//validate request
	if (!req.body || !req.body.id_canBo || !req.body.tuNgay) {
		res.send({
			error: { message: 'Body content incorrect!' },
		});
	};

	var json = {
		table: tableName,
		data: { tableName: req.body.tableName, id_canBo: req.body.id_canBo, tuNgay: req.body.tuNgay },
	};

	await Common.get(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				//Nếu chưa tồn tại thì thực hiện insert vào bảng canbo_hopdonglaodong
				var json = {
					table: tableName,
					data: { ...req.body, createBy: req.userId, createDate: Common.now() },
				};
				Common.create(json, (err, data) => {
					if (err)
						res.send({
							error: { ...err, message: err.message } || `Some error occurred while creating the ${json.table}.`,
						});
					else res.send({ data: data, message: 'Item was created successfully!' });
				});
			}
		} else res.send({
			error: { message: 'Record already exists!' }
		});

	});
};
exports.insert = Hdld.insert;

// Update a item identified by the condition in the request
exports.update = (req, res) => {
	// Validate Request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body.data, updateBy: req.userId, updateDate: Common.now() },
		condition: req.body.condition,
	};

	Common.update(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error updating ${json.table} with ${JSON.stringify(json.condition)}.`,
				});
			}
		} else res.send({ data: data, message: 'Item was updated successfully!' });
	});
};

// Delete items with the specified data in body in the request
exports.delete = (req, res) => {
	var json = {
		table: tableName,
		data: req.body,
		userId: req.userId,
	};

	Common.remove(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Could not delete ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ message: `Item was deleted successfully!` });
	});
};