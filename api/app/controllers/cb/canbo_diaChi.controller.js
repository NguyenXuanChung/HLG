const Common = require('../../models/common.model');
const bcrypt = require('bcryptjs');
const tableName = 'canbo_diachi';

// Find items with condition
exports.get = (req, res) => {
    var json = {
        table: tableName,
        data: req.body,
    };

    Common.get(json, (err, data) => {
        if (err) {
            if (err.kind === 'not_found') {
                res.send({
                    // error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
                    error: { message: 'Not found data.' },
                });
            } else {
                res.send({
                    error:
                        { ...err, message: err.message } ||
                        `Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
                });
            }
        } else res.send({ data: data, message: 'Retrieve data successfully!' });
    });
};

// insert and update canbo_diachi
exports.update = (req, res) => {
    var json = {
        procedure: 'canbo_diaChi_update',
        data: {
            id_canBo: req.body.id_canBo,
            id_tinh: req.body.id_tinh,
            id_huyen: req.body.id_huyen,
            id_xa: req.body.id_xa,
            thonXom: req.body.thonXom,
            tableName: req.body.tableName,
            fieldName: req.body.fieldName,
            createBy: req.userId,
        },
    };

    Common.callProcedure(json, (err, data) => {
        if (err) {
            if (err.kind === 'not_found') {
                res.send({
                    error: { message: 'Not found data.' },
                });
            } else {
                res.send({
                    error:
                        { ...err, message: err.message } ||
                        `Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
                });
            }
        } else res.send({ data: data, message: 'Retrieve data successfully!' });
    });
}

// Delete items with the specified data in body in the request
exports.delete = (req, res) => {
    var json = {
        table: tableName,
        data: req.body,
        userId: req.userId,
    };

    Common.remove(json, (err, data) => {
        if (err) {
            if (err.kind === 'not_found') {
                res.send({
                    error: { message: 'Not found data.' },
                });
            } else {
                res.send({
                    error:
                        { ...err, message: err.message } ||
                        `Could not delete ${json.table} with ${JSON.stringify(json.data)}`,
                });
            }
        } else res.send({ message: `Item was deleted successfully!` });
    });
};