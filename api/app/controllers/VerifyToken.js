const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
const config = require('../config/db.config'); // get our config file
const _const = require('../config/sys.config');
const Common = require('../models/common.model');
const redis = require('../models/redis');

function verifyToken(req, res, next) {
	// check header or url parameters or post parameters for token
	var token = req.headers['x-access-token'];
	if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });

	// verifies secret and checks exp
	jwt.verify(token, config.SECRET, function (err, decoded) {
		if (err) return res.status(401).send({ auth: false, message: 'Failed to authenticate token.' });

		// Thực hiện check quyền dựa trên req.headers['x-access-permission']
		if (req.headers['x-access-permission']) {
			let json = JSON.parse(req.headers['x-access-permission']);
			// let permission = decoded.permission[json.code];
			// REDIS get permission
			redis.get(`${_const.REDIS_PERMISSION}_${decoded.id}`).then((result) => {
				if (result) {
					let permission = JSON.parse(result)[json.code];

					if (!permission) {
						return res.send({ error: { message: 'Not have permission.' } });
					} else if (!Common.bitToValue(permission[json.action])) {
						return res.send({ error: { message: 'Not have permission.' } });
					} else {
						// if everything is good, save to request for use in other routes
						req.userId = decoded.id;
						req.canboId = decoded.id_canbo;
						req.id_donvis = decoded.id_donvis;
						req.roleLevel = decoded.roleLevel;
						req.customerId = decoded.customerId;
						req.id_donvi = decoded.id_donvi;
						next();
					}
				} else return res.send({ error: { message: 'Not have permission.' } });
			});
		} else return res.send({ error: { message: 'The permission is not transmitted.' } });
	});
}

module.exports = verifyToken;
