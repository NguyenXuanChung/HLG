const Common = require('../../models/common.model');
const tableName = 'tq_theoDoiHuanLuyen';

// Find items with condition
exports.get = (req, res) => {
	var json = {
		select: {
			'A.hoTen': '',
			'A.ngaySinh': '',
			'A.cmnd': '',
			'G.id': 'id_theoDoiHuanLuyen',
			'B.name': 'tenTrangThai',
			'f_donvi_getName(A.id_donViQuanLy, A.createDate)': 'tenDonViQuanLy',
			'C.*': '',
			'D.name': 'tenQuanHam',
			'E.name': 'tenCapBac',
			'F.name': 'tenDonViDbdv',
		},
		from: `tq_dqtv_congDan A`,
		inner_join: {
			'tq_dqtv_congDan_detail C': 'A.id = C.id_congDan',
			'tq_theoDoiHuanLuyen G': 'G.id_congDan_detail = C.id',
			'tq_dqtv_congdan_type_state B': 'A.id_tq_type_congDan = B.id'
		},
		left_join: {
			'tq_dqtv_dm_quanham D': 'D.id = C.id_quanHam',
			'tq_dqtv_dm_capBac E': 'E.id = C.id_capBac',
			'tq_dqtv_dm_donViDbdv F': 'F.id = C.id_donViDbdv',
		},
		whereGroup: {
			status: [
				{
					'A.status': {
						operator: '=',
						value: 1,
					},
				},
			],
			status1: [
				{
					'C.status': {
						operator: '=',
						value: 1,
					},
				},
			],
			status2: [
				{
					'G.status': {
						operator: '=',
						value: 1,
					},
				},
			],
			nam: [
				{
					'G.nam': {
						operator: '=',
						value: req.body.nam,
					},
				},
			],
		},
		orderBy: 'A.ngaySinh DESC',
	};
	if (req.body.id_donViQuanLys) {
		json.whereGroup[id_donViQuanLy] = [
			{
				'FIND_IN_SET(A.id_donViQuanLy': {
					list: req.body.id_donViQuanLys,
					operator: '>=',
					value: 1,
				},
			},
		]
	}
	Common.getAdvance(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Retrieve data successfully!' });
	});
};
exports.create = (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}
	// Cập nhật công dân
	if (req.body.id_details && req.body.nam && req.body.ngayHuanLuyen && req.body.id_details.length > 0) {
		req.body.id_details.split(',').forEach(async (element) => {
			var json = {
				table: tableName,
				data: {
					id_congDan_detail: element,
					nam: req.body.nam,
					ngayHuanLuyen: req.body.ngayHuanLuyen,
					createBy: req.userId,
					createDate: Common.now()
				},
			};
			// Save data in the database
			await Common.create(json, (err, data) => {
				if (err) res.send({ error: { ...err, message: err.message } });
			});
		});
		res.send({ message: 'Item was created successfully!' });
	} else {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}
};

// Update a item identified by the condition in the request
exports.update = (req, res) => {
	// Validate Request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body.data, updateBy: req.userId, updateDate: Common.now() },
		condition: req.body.condition,
	};

	Common.update(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error updating ${json.table} with ${JSON.stringify(json.condition)}.`,
				});
			}
		} else res.send({ data: data, message: 'Item was updated successfully!' });
	});
};

// Delete items with the specified data in body in the request
exports.delete = (req, res) => {
	var json = {
		table: tableName,
		data: req.body,
		userId: req.userId,
	};

	Common.remove(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Could not delete ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ message: `Item was deleted successfully!` });
	});
};
