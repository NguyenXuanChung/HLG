const Common = require('../../models/common.model');
const tableName = 'tq_dqtv_congDan';

// Find items with condition
exports.get = (req, res) => {
	var json = {
		table: tableName,
		data: req.body,
	};

	Common.get(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Retrieve data successfully!' });
	});
};
exports.getList = (req, res) => {
	var json = {
		select: {
			'A.*': '',
			'C.name': 'tenTrinhDoVanHoa',
			'B.name': 'tenTrangThai',
			'f_donvi_getName(A.id_donViQuanLy, A.createDate)': 'tenDonViQuanLy',
		},
		from: `${tableName} A`,
		inner_join: {
			'tq_dqtv_dm_trinhdovanhoa C': 'A.id_trinhDoVanHoa = C.id',
			'tq_dqtv_congdan_type_state B': 'A.id_tq_type_congDan = B.id'
		},
		where: {
			'FIND_IN_SET(A.id_tq_type_congDan': {
				list: req.body.id_tq_type_congDan,
				operator: '>=',
				value: 1,
			},
			'FIND_IN_SET(A.id_donViQuanLy': {
				list: req.body.id_donViQuanLys,
				operator: '>=',
				value: 1,
			},
			"CONCAT_WS(' ', A.hoTen, A.cmnd)": {
				operator: 'LIKE',
				value: req.body.content,
			},
			'A.status': {
				operator: '=',
				value: 1,
			},
			'A.gioiTinh': {
				operator: '=',
				value: req.body.gioiTinh,
			},
			[`A.ngaySinh < DATE_ADD('${req.body.denNgay}',INTERVAL -${req.body.tuTuoi} YEAR)`]: {
				operator: '',
				value: '',
			},
			[`A.ngaySinh > DATE_ADD('${req.body.tuNgay}',INTERVAL -${req.body.denTuoi} YEAR)`]: {
				operator: '',
				value: '',
			},
		},
		orderBy: 'A.ngaySinh DESC',
	};

	Common.getAdvance(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Retrieve data successfully!' });
	});
};
exports.getBcDqtv = (req, res) => {
	var json = {
		select: {
			'A.hoTen': '',
			'A.ngaySinh': '',
			'A.cmnd': '',
			'B.name': 'tenTrangThai',
			'G.name': 'tenLucLuong',
			'f_donvi_getName(A.id_donViQuanLy, A.createDate)': 'tenDonViQuanLy',
			'C.*': '',
			'D.name': 'tenQuanHam',
			'E.name': 'tenCapBac',
			'F.name': 'tenDonViTv',
		},
		from: `${tableName} A`,
		inner_join: {
			'tq_dqtv_congDan_detail C': 'A.id = C.id_congDan',
      'tq_dqtv_congdan_type_state B': 'A.id_tq_type_congDan = B.id',
      'tq_dqtv_dm_lucLuong G': 'G.id = C.id_lucLuong',
		},
		left_join: {
			'tq_dqtv_dm_quanham D': 'D.id = C.id_quanHam',
			'tq_dqtv_dm_capBac E': 'E.id = C.id_capBac',
			'tq_dqtv_dm_donViTv F': 'F.id = C.id_donViTv',
		},
		whereGroup: {
			status: [
				{
					'A.status': {
						operator: '=',
						value: 1,
					},
				},
			],
			status1: [
				{
					'C.status': {
						operator: '=',
						value: 1,
					},
				},
			],
			id_tq_type_congDan: [
				{
					'FIND_IN_SET(C.id_tq_type_congDan': {
						list: req.body.id_tq_type_congDan,
						operator: '>=',
						value: 1,
					},
				},
			],
			content: [
				{
					"CONCAT_WS(' ', A.hoTen, A.cmnd)": {
						operator: 'LIKE',
						value: req.body.content,
					},
				},
			],
			date: [
				{
					[`'${req.body.ngay}'`]: {
						operator: 'BETWEEN',
						from: 'CAST(C.tuNgay AS DATE)',
						to: 'CAST(C.denNgay AS DATE)',
					},
				},
			],
		},
		orderBy: 'A.ngaySinh DESC',
	};
	if (req.body.id_donViQuanLys) {
		json.whereGroup[id_donViQuanLy] = [
			{
				'FIND_IN_SET(A.id_donViQuanLy': {
					list: req.body.id_donViQuanLys,
					operator: '>=',
					value: 1,
				},
			},
		]
	}
	if (req.body.id_donViTvs) {
		json.whereGroup[id_donViTv] = [
			{
				'FIND_IN_SET(C.id_donViTv': {
					list: req.body.id_donViTv,
					operator: '>=',
					value: 1,
				},
			},
		]
  }
  if (req.body.id_lucLuong) {
		json.whereGroup[id_lucLuong] = [
			{
				'FIND_IN_SET(E.id_lucLuong': {
					list: req.body.id_lucLuong,
					operator: '>=',
					value: 1,
				},
			},
		]
  }
  if (req.body.id_lucLuongs) {
		json.whereGroup[id_lucLuong] = [
			{
				'FIND_IN_SET(G.id': {
					list: req.body.id_lucLuongs,
					operator: '>=',
					value: 1,
				},
			},
		]
	}
	Common.getAdvance(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Retrieve data successfully!' });
	});
};

exports.getDetail = (req, res) => {
	var json = {
		select: {
			'A.*': '',
			'H.name': 'tenDotTuyenQuan',
			'C.name': 'tenQuaTrinh',
			'B.name': '',

		},
		inner_join: {
			'tq_dqtv_congdan_type_state C': 'A.id_type_state = C.id',
		},
		left_join: {
			'tq_dottuyenquan H': 'A.id_dotTuyenQuan = H.id',
			'tq_dqtv_congdan_type_state B': 'A.id_type_beforeState = B.id',

		},
		from: `tq_dqtv_congdan_detail A`,
		whereGroup: {
			id_congDan: [
				{
					'A.id_congDan': {
						operator: '=',
						value: req.body.id_congDan,
					},
				}
			],
			status: [
				{
					'A.status': {
						operator: '=',
						value: 1,
					},
				},
			],
		}, orderBy: 'A.tuNgay DESC, C.sortOrder',
	};
	Common.getAdvance(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Retrieve data successfully!' });
	});
};
exports.updateTq = (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}
	// Cập nhật công dân
	if (req.body.id_congDans && req.body.id_dotTuyenQuan && req.body.id_congDans.length > 0) {
		req.body.id_congDans.split(',').forEach(async (element) => {
			json1 = {
				table: 'tq_dqtv_congDan_detail',
				data: {
					id_congDan: element, id_type: 1, id_type_state: 5,
					id_dotTuyenQuan: req.body.id_dotTuyenQuan,
					id_type_beforeState: 1,
					noiDung: 'Đăng ký',
					tuNgay: req.body.ngay,
					latest: 1,
					createBy: req.userId, createDate: Common.now()
				},
			};
			await Common.create(json1, (err, data) => {
				if (err) res.send({ error: { ...err, message: err.message } });
			});
			json2 = {
				table: tableName,
				data: {
					id_tq_type_congDan_tuNgay: req.body.ngay,
					id_tq_type_congDan_denNgay: req.body.ngay,
					id_dotTuyenQuan: req.body.id_dotTuyenQuan,
					id_tq_type_congDan: 5,
					updateBy: req.userId, updateDate: Common.now()
				},
				condition: { id: element },
			};
			await Common.update(json2, (err, data) => {
				if (err) res.send({ error: { ...err, message: err.message } });
			});
		});
		res.send({ message: 'Item was updated successfully!' });
	}
};
exports.updateXd = async (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}
	// Cập nhật công dân
	if (req.body.id_congDans && req.body.id_tq_type_congDan && req.body.id_congDans.length > 0) {
		await req.body.id_congDans.split(',').forEach(async (element) => {
			var json = {
				procedure: 'tq_dqtv_congDan_detail_add',
				data: {
					id_congDan: element,
					id_type_state: req.body.id_tq_type_congDan,
					id_type_beforeState: req.body.id_tq_type_congDan_before,
					noiDung: req.body.nameSelect,
					id_dotTuyenQuan: req.body.id_dotTuyenQuan,
					id_quanHam: null,
					tuNgay: req.body.ngay,
					denNgay: req.body.ngay,
					updateBy: req.userId,
				},
			};
			await Common.callProcedure(json, (err, data) => {
				if (err) {
					if (err.kind === 'not_found') {
						res.send({
							error: { message: 'Not found data.' },
						});
					} else {
						res.send({
							error:
								{ ...err, message: err.message } ||
								`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
						});
					}
				}
			});
		});
	}
	if (req.body.id_detailUpdates && req.body.id_tq_type_congDan && req.body.id_detailUpdates.length > 0) {
		await req.body.id_detailUpdates.split(',').forEach(async (element) => {
			var json = {
				procedure: 'tq_dqtv_congDan_detail_update',
				data: {
					id_detail: element,
					id_type_state: req.body.id_tq_type_congDan,
					noiDung: req.body.nameSelect,
					tuNgay: req.body.ngay,
					denNgay: req.body.ngay,
					updateBy: req.userId,
				},
			};
			await Common.callProcedure(json, (err, data) => {
				if (err) {
					if (err.kind === 'not_found') {
						res.send({
							error: { message: 'Not found data.' },
						});
					} else {
						res.send({
							error:
								{ ...err, message: err.message } ||
								`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
						});
					}
				}
			});
		});
	}
	await res.send({ message: 'Item was updated successfully!' });

};
exports.updateDetail = (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}
	// Cập nhật công dân
	if (req.body.id_congDans && req.body.id_type_state && req.body.id_type_beforeState && req.body.id_congDans.length > 0) {
		req.body.id_congDans.split(',').forEach(async (element) => {
			var json = {
				procedure: 'tq_dqtv_congDan_detail_add',
				data: {
					id_congDan: element,
					id_type_state: req.body.id_type_state,
					id_type_beforeState: req.body.id_type_beforeState,
					noiDung: req.body.noiDung,
					id_dotTuyenQuan: req.body.id_dotTuyenQuan,
					id_quanHam: req.body.id_quanHam,
					tuNgay: req.body.tuNgay,
					denNgay: req.body.denNgay,
					updateBy: req.userId,
				},
			};
			await Common.callProcedure(json, (err, data) => {
				if (err) {
					if (err.kind === 'not_found') {
						res.send({
							error: { message: 'Not found data.' },
						});
					} else {
						res.send({
							error:
								{ ...err, message: err.message } ||
								`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
						});
					}
				}
			});
		});
		res.send({ message: 'Item was updated successfully!' });
	} else {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}
};
exports.updateQuaTrinh = (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}
	// Cập nhật công dân
	if (req.body.congDans && req.body.congDans.length > 0) {
		req.body.congDans.forEach(async (element) => {
			var json = {
				procedure: 'tq_dqtv_congDan_quaTrinh_add',
				data: {
					id_congDan: element.id_congDan,
					id_type_state: element.id_type_state,
					id_type_beforeState: element.id_type_beforeState,
					noiDung: element.noiDung,
					tuNgay: element.tuNgay,
					denNgayThongBao: element.denNgayThongBao,
					id_dotTuyenQuan: element.id_dotTuyenQuan,
					id_quanHam: element.id_quanHam,
					id_capBac: element.id_capBac,
					dkDongVien: element.dkDongVien,
					id_donViQuanLy: null,
					id_donViDbdv: element.id_donViDbdv,
					latest: element.latest,
					updateBy: req.userId,
				},
			};
			await console.log(json);
			await Common.callProcedure(json, (err, data) => {
				if (err) {
					if (err.kind === 'not_found') {
						res.send({
							error: { message: 'Not found data.' },
						});
					} else {
						res.send({
							error:
								{ ...err, message: err.message } ||
								`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
						});
					}
				}
			});
		});
		res.send({ message: 'Item was updated successfully!' });
	} else {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}
};
exports.updateGiaiNgach = (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}
	// Cập nhật công dân
	if (req.body.id_congDans && req.body.id_type_state && req.body.id_type_beforeState && req.body.id_congDans.length > 0) {
		req.body.id_congDans.split(',').forEach(async (element) => {
			var json = {
				procedure: 'tq_dqtv_congDan_detail_giaingach',
				data: {
					id_congDan: element,
					id_type_state: req.body.id_type_state,
					id_type_beforeState: req.body.id_type_beforeState,
					noiDung: req.body.noiDung,
					tuNgay: req.body.tuNgay,
					denNgay: null,
					updateBy: req.userId,
				},
			};
			await Common.callProcedure(json, (err, data) => {
				if (err) {
					if (err.kind === 'not_found') {
						res.send({
							error: { message: 'Not found data.' },
						});
					} else {
						res.send({
							error:
								{ ...err, message: err.message } ||
								`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
						});
					}
				}
			});
		});
		res.send({ message: 'Item was updated successfully!' });
	} else {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}
};
exports.updateBienChe = (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}
	// Cập nhật công dân
	if (req.body.id_congDans && req.body.id_type_state && req.body.id_type_beforeState && req.body.id_congDans.length > 0) {
		req.body.id_congDans.split(',').forEach(async (element) => {
			var json = {
				procedure: 'tq_dqtv_congDan_detail_addBc',
				data: {
					id_congDan: element,
					id_type_state: req.body.id_type_state,
					id_type_beforeState: req.body.id_type_beforeState,
					noiDung: req.body.noiDung,
					id_dotTuyenQuan: req.body.id_dotTuyenQuan,
					id_capBac: req.body.id_capBac,
					id_donViDbdv: req.body.id_donViDbdv,
					tuNgay: req.body.tuNgay,
					denNgay: req.body.denNgay,
					updateBy: req.userId,
				},
			};
			await Common.callProcedure(json, (err, data) => {
				if (err) {
					if (err.kind === 'not_found') {
						res.send({
							error: { message: 'Not found data.' },
						});
					} else {
						res.send({
							error:
								{ ...err, message: err.message } ||
								`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
						});
					}
				}
			});
		});
		res.send({ message: 'Item was updated successfully!' });
	} else {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}
};
exports.updateDongVien = (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}
	// Cập nhật công dân
	if (req.body.id_details && req.body.dkDongVien && req.body.id_details.length > 0) {
		req.body.id_details.split(',').forEach(async (element) => {
			json2 = {
				table: 'tq_dqtv_congDan_detail',
				data: {
					dkDongVien: req.body.dkDongVien,
					updateBy: req.userId, updateDate: Common.now()
				},
				condition: { id: element },
			};
			await Common.update(json2, (err, data) => {
				if (err) res.send({ error: { ...err, message: err.message } });
			});
		});
		res.send({ message: 'Item was updated successfully!' });
	} else {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}
};
exports.updateLatest = (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}
	if (req.body.id_congDans && req.body.id_type_state && req.body.id_congDans.length > 0) {
		req.body.id_congDans.split(',').forEach(async (element) => {
			var json = {
				procedure: 'tq_dqtv_congDan_detail_add_latest',
				data: {
					id_congDan: element,
					id_type_state: req.body.id_type_state,
					id_type_beforeState: req.body.id_type_beforeState,
					noiDung: req.body.noiDung,
					tuNgay: req.body.tuNgay,
					denNgay: req.body.denNgay,
					updateBy: req.userId,
				},
			};
			await Common.callProcedure(json, (err, data) => {
				if (err) {
					if (err.kind === 'not_found') {
						res.send({
							error: { message: 'Not found data.' },
						});
					} else {
						res.send({
							error:
								{ ...err, message: err.message } ||
								`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
						});
					}
				}
			});
		});
		res.send({ message: 'Item was updated successfully!' });
	} else {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}
};
// Create and Save a new item
exports.create = (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body, id_customer: req.customerId, createBy: req.userId, createDate: Common.now() },
	};

	// Save data in the database
	Common.create(json, (err, data) => {
		if (err)
			res.send({
				error: { ...err, message: err.message } || `Some error occurred while creating the ${json.table}.`,
			});
		else res.send({ data: data, message: 'Item was created successfully!' });
	});
};

// Update a item identified by the condition in the request
exports.update = (req, res) => {
	// Validate Request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body.data, updateBy: req.userId, updateDate: Common.now() },
		condition: req.body.condition,
	};

	Common.update(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error updating ${json.table} with ${JSON.stringify(json.condition)}.`,
				});
			}
		} else res.send({ data: data, message: 'Item was updated successfully!' });
	});
};

// Delete items with the specified data in body in the request
exports.delete = (req, res) => {
	var json = {
		table: tableName,
		data: req.body,
		userId: req.userId,
	};

	Common.remove(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Could not delete ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ message: `Item was deleted successfully!` });
	});
}

