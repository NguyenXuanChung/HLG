const express = require('express');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const fs = require('fs');
const cors = require('cors');

const app = express();

app.use(fileUpload({ createParentPath: true, limits: { fileSize: 50 * 1024 * 1024 }, abortOnLimit: true }));
app.use(cors());

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get('/', (req, res) => {
	res.json({ message: 'Welcome to BiHRP-Admin API application.' });
});

// Quản trị hệ thống
require('./app/routes/portal_account.routes')(app);
require('./app/routes/auth.routes')(app);
require('./app/routes/tq.routes')(app);
require('./app/routes/qpan.routes')(app);
require('./app/routes/dqtv.routes')(app);
require('./app/routes/tq_dqtv.routes')(app);
require('./app/routes/cb_canbo.routes')(app);
require('./app/routes/cb_dm.routes')(app);
require('./app/routes/portal_nhomQuyen.routes')(app);
require('./app/routes/sys_attachFiles.routes')(app);
require('./app/routes/sys_admin.routes')(app);
require('./app/routes/sys_comments.routes')(app);
require('./app/routes/sys_workflow.routes')(app);
// Quản lý cán bộ
require('./app/routes/donVi.routes')(app);
// Danh mục
require('./app/routes/dm.routes')(app);

// //if we are here then the specified request is not found
// app.use((req,res,next)=> {
//   const err = new Error("Not Found");
//   err.status = 404;
//   next(err);
// });

// //all other requests are not implemented.
// app.use((err,req, res, next) => {
//  res.status(err.status || 501);
//  res.json({
//      error: {
//          code: err.status || 501,
//          message: err.message
//      }
//  });
// });

// SSL
var privateKey = fs.readFileSync('sslcert/server.key', 'utf8');
var certificate = fs.readFileSync('sslcert/server.crt', 'utf8');
var credentials = { key: privateKey, cert: certificate };

// var server = require('https').createServer(credentials, app);
var server = require('http').createServer(app);
// set port, listen for requests
const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
	console.log(`Server is running on port ${PORT}.`);
});